import React, {Fragment, useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
// import * as credentials from './constant/framesAPI';

import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import {ScrollContext} from 'react-router-scroll-4';
import * as serviceWorker from './serviceWorker';

// ** Import custom components for redux**
import {Provider} from 'react-redux';
import store from './doctor-side/store/index';
import App from "./doctor-side/components/app";
import AppNew from "./doctor-side/components/app-new";


// Import custom components
import Dashboard from './doctor-side/components/dashboard/dashboard';
import Login from './doctor-side/components/eyeCandi/login';
import Subscription from './doctor-side/components/eyeCandi/payment/subscription';
import subcheck from './doctor-side/components/eyeCandi/payment/CheckoutForm';
import termCondi from './doctor-side/components/eyeCandi/termConditions';
import SignUp from './doctor-side/components/eyeCandi/signup';
import ForgotPwd from "./doctor-side/components/eyeCandi/forgot-pass";
import resetPwd from "./doctor-side/components/eyeCandi/resetPwd";
import SetupWizard from "./doctor-side/components/eyeCandi/Setup-Wizard";
import SetupWizard_ThankYou from "./doctor-side/components/eyeCandi/WizardSteps/thankyou";
import EditSetupWizard from "./doctor-side/components/eyeCandi/edit-Setup-Wizard";
import FormWizard from './doctor-side/components/wizard/form-wizard';
import FramesGallery from './doctor-side/components/eyeCandi/pages/frames';
import framesFeatures from "./doctor-side/components/eyeCandi/pages/framesFeatures";
import PublicGallery from './doctor-side/components/eyeCandi/pages/my_public_gallery';
import ProductDetail from './doctor-side/components/ecommerce-app/product-detail/product-detail';
import Public_ProductDetail from './doctor-side/components/ecommerce-app/product-detail/private-product-detail';
import Setting from './doctor-side/components/eyeCandi/pages/setting';
import Error404 from './doctor-side/components/eyeCandi/pages/error404';
import userCsv from './doctor-side/components/eyeCandi/front-pages/UserCsv';

//Import Super Admin Sections
import AdminDashboard from './doctor-side/components/Admin/dashboard/admindashboard';
import All_Users from './doctor-side/components/Admin/pages/all_users';
import Promo_Code from './doctor-side/components/Admin/pages/promocode/promocode';
import PromoCode_list from './doctor-side/components/Admin/pages/promocode/list';
import Promo_Code_Edit from "./doctor-side/components/Admin/pages/promocode/promocode_edit";


//New Design Components
import Home from './doctor-side/components/eyeCandi/front-pages/home';
import ThankYou from './doctor-side/components/eyeCandi/front-pages/thank-you';
import BookanExamThankYou from './doctor-side/components/eyeCandi/front-pages/bookanExam-thank-you';
import BookExam from './doctor-side/components/eyeCandi/front-pages/bookanExam';
import About from './doctor-side/components/eyeCandi/front-pages/about';
import termConditions from './doctor-side/components/eyeCandi/front-pages/term-conditions';
import privacy from './doctor-side/components/eyeCandi/front-pages/privacy';
import MyTryOnList from './doctor-side/components/eyeCandi/front-pages/my-try-on-list';
import tryOnListForm from './doctor-side/components/eyeCandi/front-pages/try-on-list-form';
import Mens_EyeglassesFrame from './doctor-side/components/eyeCandi/front-pages/eyeGlasses/mens-eyeglassesFrames';
import Womens_Eyeglasses from './doctor-side/components/eyeCandi/front-pages/eyeGlasses/womens-eyeglassesFrames';
import Mens_SunglassesFrame from './doctor-side/components/eyeCandi/front-pages/eyeGlasses/mens-sunglassesFrames';
import Womens_SunglassesFrame from './doctor-side/components/eyeCandi/front-pages/eyeGlasses/womens-sunglassesFrames';
import singleProductPage_user from './doctor-side/components/eyeCandi/front-pages/eyeGlasses/private-SingleProductPage';

// sample page
import Samplepage from './doctor-side/components/sample/samplepage';
import SupportTicket from './doctor-side/components/support-ticket/supportTicket';
import * as credentials from "./doctor-side/constant/framesAPI";

//firebase Auth
function Root() {
    // const [apiToken , setapiToken]  = useState('');
    useEffect(() => {

        // const themeColor = localStorage.getItem('theme-color')
        // const layout = localStorage.getItem('layout_version')
        document.getElementById("color").setAttribute("href", `${process.env.PUBLIC_URL}/assets/css/color_4.css`);
        document.body.classList.add("light");

    }, []);

    return (
        <div className="App">
            <Provider store={store}>
                <BrowserRouter basename={'/'}>
                    <ScrollContext>
                        <Switch>
                            <Route path={`${process.env.PUBLIC_URL}/login`} component={Login}/>
                            <Route path={`${process.env.PUBLIC_URL}/subscription`} component={Subscription}/>
                            <Route path={`${process.env.PUBLIC_URL}/terms-and-conditions`} component={termCondi}/>
                            {/*<Route path={`${process.env.PUBLIC_URL}/subcheck`} component={subcheck}/>*/}
                            <Route path={`${process.env.PUBLIC_URL}/signup/:affiliate?`} component={SignUp}/>
                            <Route path={`${process.env.PUBLIC_URL}/forgotPwd/:token`} component={ForgotPwd}/>
                            <Route path={`${process.env.PUBLIC_URL}/resetPwd`} component={resetPwd}/>
                            <Route path={`${process.env.PUBLIC_URL}/getcsv`} component={userCsv}/>
                            {/*<App_loader>*/}
                            <Route exact path={`${process.env.PUBLIC_URL}/Custom-Setup-Wizard`} component={FormWizard}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/SetupWizard`} component={SetupWizard}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/ThankYou`} component={SetupWizard_ThankYou}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/EditSetupWizard`}
                                   component={EditSetupWizard}/>

                            {/*</App_loader>*/}

                            {/* Super Admin Pages */}
                            <Route exact path={`${process.env.PUBLIC_URL}/admin/`} component={AdminDashboard}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/admin/users`} component={All_Users}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/admin/promocode/create`} component={Promo_Code}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/admin/promocode/list`} component={PromoCode_list}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/admin/promocode/detail/:id`} component={Promo_Code_Edit}/>


                            {/* Public Gallery Page */}
                            {/*<Route exact path={`${process.env.PUBLIC_URL}/:publicUrl`}  component={PublicGallery}   />*/}
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl`} component={Home}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/about`} component={About}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/termConditions`} component={termConditions}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/privacy`} component={privacy}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/book-eye-exam`}
                                   component={BookExam}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/try-On-List-Form`}
                                   component={tryOnListForm}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/my-try-on-list`}
                                   component={MyTryOnList}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/thank-you`} component={ThankYou}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/ThankYouBookanExam/`} component={BookanExamThankYou}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/product-detail/:id`}
                                   component={Public_ProductDetail}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/eyeglasses/mens`}
                                   component={Mens_EyeglassesFrame}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/eyeglasses/womens`}
                                   component={Womens_Eyeglasses}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/sunglasses/mens`}
                                   component={Mens_SunglassesFrame}/>
                            <Route exact path={`${process.env.PUBLIC_URL}/:publicUrl/sunglasses/womens`}
                                   component={Womens_SunglassesFrame}/>
                            <Route exact
                                   path={`${process.env.PUBLIC_URL}/:publicUrl/eyeglasses/product-detail/:brand/:id`}
                                   component={singleProductPage_user}/>

                            <Fragment>
                                <App>
                                    {/* dashboard menu */}
                                    <Route exact path={`${process.env.PUBLIC_URL}/`} component={Dashboard}/>

                                    {/* Frames Gallery Page */}
                                    <Route exact path={`${process.env.PUBLIC_URL}/pages/FramesFeatures`}
                                           component={framesFeatures}/>
                                    <Route exact path={`${process.env.PUBLIC_URL}/pages/FramesGallery`}
                                           component={FramesGallery}/>
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/pages/FramesGallery/product-detail/:brand/:id`}
                                        component={ProductDetail}/>

                                    {/* Setting Page */}
                                    <Route exact path={`${process.env.PUBLIC_URL}/profile/wizard`} component={Setting}/>

                                    <Route path={`${process.env.PUBLIC_URL}/404`} component={Error404}/>

                                </App>
                            </Fragment>


                            {/*<Fragment>*/}
                            {/*    <AppNew>*/}
                            {/*<Route exact path={`${process.env.PUBLIC_URL}/:publicURL/Home`} component={Home} />*/}
                            {/*</AppNew>*/}
                            {/*</Fragment>*/}

                        </Switch>
                    </ScrollContext>
                </BrowserRouter>
            </Provider>
        </div>
    );
}

ReactDOM.render(<Root/>, document.getElementById('root'));

serviceWorker.unregister();
