import {
    Home,
    File,
    Headphones, Star, Command, Calendar, Eye, Sliders, MessageSquare, Settings
} from 'react-feather';
import {getUser} from "../components/eyeCandi/functions";

// const publicURL = localStorage.publicURL;
// console.log(publicURL,"dsdsd");

// var user_id = localStorage.getItem('user_id');
// if (user_id) {
//     var publicURL = "";
//     getUser(user_id).then(res => {
//        localStorage.setItem('publicURL_admin', res.data.public_url);
//     });
// }

export const MENUITEMS = [
    {

        title: 'Dashboard',
        icon: Home,
        type: 'link',
        path: `${process.env.PUBLIC_URL}/`,
        badgeType: 'primary',
        active: true,
        // children: [
        // { path: '/dashboard/ecommerce', title: 'E-Commerce', type: 'link' },
        // { path: '/dashboard/university', title: 'University', type: 'link' },
        // { path: '/dashboard/crypto', title: 'Crypto', type: 'link' },
        // { path: '/dashboard/project', title: 'Project', type: 'link' }
        // ]
    },
    {
        title: 'Frames', icon: Eye, type: 'link', path: `${process.env.PUBLIC_URL}/pages/FramesFeatures`, active: false
    },
    {
        title: 'Settings', icon: Settings, type: 'link', path: `${process.env.PUBLIC_URL}/profile/wizard`, active: false
    },
    // {
    //     title: 'Public Gallery',
    //     icon: Command,
    //     type: 'link',
    //     path: `${process.env.PUBLIC_URL}/${localStorage.getItem('publicURL_admin')}`,
    //     active: false,
    //     target: '_blank'
    // },
    // {
    //     title: 'Ratings', icon: Star, type: 'link', path: '/pages/ratings', active: false
    // },
]
