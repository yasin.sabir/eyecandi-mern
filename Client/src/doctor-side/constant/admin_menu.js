import {
    Home,
    File,
    Headphones, Star, Command, Calendar , Eye ,Sliders, MessageSquare , Settings ,Layers , Bookmark , Users
} from 'react-feather';
// title: 'Dashboard', icon: Home, type: 'sub', badgeType: 'primary', active: false,
export const MENUITEMS = [
    {
        title: 'Users', icon: Users, type: 'link', path: `${process.env.PUBLIC_URL}/admin/users`, active: false
    },
    {
        title: 'Promo Code', icon: Bookmark, type: 'sub', active: false ,
        children: [
            { path: `${process.env.PUBLIC_URL}/admin/promocode/create`, title: 'Create', type: 'link' },
            { path: `${process.env.PUBLIC_URL}/admin/promocode/list`, title: 'View All', type: 'link' },
            // { path: '/dashboard/university', title: 'University', type: 'link' },
            // { path: '/dashboard/crypto', title: 'Crypto', type: 'link' },
            // { path: '/dashboard/server', title: 'Server', type: 'link' },
            // { path: '/dashboard/project', title: 'Project', type: 'link' }
        ]

    },
]
