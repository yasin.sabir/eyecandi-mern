import {
    Eye,
    Calendar,
    Layers,
    Heart,
} from 'react-feather';

export const MENUITEMS = [
    {
        title: 'Eyeglasses', icon: Eye, type: 'sub',badgeType: 'primary', active: false , children: [
            { path: '/#', title: 'Mens', type: 'link' },
            { path: '/#', title: 'Womens', type: 'link' },
        ]
    },
    {
        title: 'Sunglasses', icon: Eye, type: 'sub', badgeType: 'primary', active: false , children: [
            { path: '/#', title: 'Mens', type: 'link' },
            { path: '/#', title: 'Womens', type: 'link' },
        ]
    },
    {
        title: 'About', icon: Layers, type: 'link', path: '/#', badgeType: 'primary', active: false
    },
    {
        title: 'My Try-On List', icon: Heart, type: 'link', path: '/#', badgeType: 'primary', active: false
    },
    {
        title: 'Book an Exam', icon: Calendar, type: 'link', path: '/#', badgeType: 'primary', active: false
    },
]
