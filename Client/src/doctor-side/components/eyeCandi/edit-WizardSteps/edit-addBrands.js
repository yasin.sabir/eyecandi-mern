import React, {Component, Fragment} from 'react';
import TypeaheadOne from "../../base/typeaheadComponent/typeahead-one";
import 'react-bootstrap-typeahead/css/Typeahead.css';
import {Typeahead} from "react-bootstrap-typeahead";
import options from "../../base/typeaheadComponent/typeheadData";
import SimpleReactValidator from 'simple-react-validator';
import * as credentials from '../../../constant/framesAPI';

class Edit_Add_brands extends Component {


    constructor(props) {
        super(props);

        this.state = {
            allBrands: [],
            Brands:[],
            loading: true,
            values: [],
        };

        this.validator = new SimpleReactValidator({
            messages: {
                // all_brands: 'Brand are require!'
                default: 'Brand are require!'
            },
        });

    }


    componentDidMount() {
        this.setState({
            values: this.props
        })
        const {values} = this.props;
        console.log(values.api_token + " --");

        fetch('https://api.framesdata.com/api/brands?auth=' + values.api_token + '&mkt=USA&status=A')
            .then(response => response.json())
            .then((data) => {
                    this.setState({
                        allBrands: data.Brands.map((value, id) => {
                            return {id: id, name: value.BrandName , BrandFrameMasterID:value.BrandFramesMasterID};
                        }),
                        loading: false
                    });
                }
            );
    }

    componentWillMount() {
    }


    handleInputChange = (input, e) =>{
        console.log("value", input);
    }

    handleTypeaheadChangeIndustry = selected => {
        const industry = selected.map(option => option.value);
        this.setState({
            Brands : industry
        })
    };

    saveAndContinue = (e) => {
        e.preventDefault();

        // console.log(this.props);

        if (this.validator.allValid() && this.props.values.all_brands.length !== 0) {
            this.props.nextStep()
        }else{
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();

    }

    render() {

        const {values} = this.props;

        // values.allBrands = this.props.userData.all_brands.all_brands;

            console.log(values.all_brands);


        return (
            <div className="container">
                <div className="row mt-5 pt-3">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="u-pearl done col-4">
                                <div className="u-pearl-icon">1</div>
                                <span className="u-pearl-title">
                                Build Profile
                            </span>
                            </div>
                            <div className="u-pearl current col-4">
                                <div className="u-pearl-icon">2</div>
                                <span className="u-pearl-title">
                                Add Brands
                            </span>
                            </div>
                            <div className="u-pearl col-4">
                                <div className="u-pearl-icon">3</div>
                                <span className="u-pearl-title">
                                Personalize
                            </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <p className="h4 txt-info mt-5 mb-5">
                            Easily search for the brands you carry below. Simply start typing the name of the brand and choose it from the list to add. Add as many brands as you wish.
                        </p>
                        <form className="needs-validation">
                            <div className="form-row">
                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom04">Brands</label>
                                    <Typeahead
                                        style={{'background-color':'white !important'}}
                                        className="shadow-0"
                                        id="public-typeahead"
                                        defaultSelected={values.all_brands}
                                        name="all_brands"
                                        labelKey="name"
                                        multiple
                                        required
                                        onInputChange={this.handleInputChange}
                                        onChange={this.props.handleTypeaheadChangeIndustry}
                                        options={this.state.allBrands} //Names fetch from Api
                                        placeholder={this.state.loading ? 'Please wait' : 'Start typing a brand name...'}
                                        ref={(ref) => this._typeahead = ref}
                                    />
                                    {/*{this.validator.message('all_brands', this.state.Brands, 'required', )}*/}

                                </div>
                            </div>
                            <button className="btn btn-secondary pull-left" type="button" onClick={this.back}>Back
                            </button>
                            <button className="btn btn-secondary pull-right" type="button"
                                    onClick={this.saveAndContinue}>Next
                            </button>
                        </form>
                    </div>
                    <div className="col-md-3"></div>

                </div>

            </div>
        );
    }


}

export default Edit_Add_brands;
