import React, {Component} from 'react';
import TypeaheadOne from "../../base/typeaheadComponent/typeahead-one";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import {Redirect} from "react-router";
import SimpleReactValidator from "simple-react-validator";
import {getUser, update, productupdate, update_UserData, getURL} from '../functions';
import * as credentials from "../../../constant/framesAPI";
import {useHistory} from "react-router-dom";
import {SwatchesPicker} from "react-color";


class Edit_Public_gallery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            public_url_error: false,
            public_url_error_msg: "Public Url Already Exist",
            profile_logo_error: false,
            profile_logo_error_msg: "File size should not be exceeds 3MB",
            ToFrameGallery: false,
            croppedImage: null,
            src: '',
            crop: {
                unit: "%",
                width: 50,
                height: 50,
                //aspect: 16 / 9
                aspect: 4 / 4
            },
            disableButton: false,
            color: '',
            openColor: false,
            url_valid: false,
            url_value: ''

        }
        this.validator = new SimpleReactValidator();
    }

    componentDidMount() {
        console.log(this.props);
        this.setState({
            src: this.props.userData.profile_pic,
            color: this.props.values.brand_color
        })
    }

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {

            const FSize = e.target.files[0].size;
            const ImageSize = Math.round((FSize / 1024));

            if (ImageSize > 3072) {

                this.setState({
                    profile_logo_error: true
                })

            } else {

                this.setState({
                    profile_logo_error: false
                })
                const reader = new FileReader();
                reader.addEventListener("load", () =>
                    this.setState({
                        src: reader.result,
                    })
                );
                reader.readAsDataURL(e.target.files[0]);

            }


        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onVideoLoaded = video => {
        this.videoRef = video;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        this.setState({crop});
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                "newFile.jpeg"
            );
            this.setState({croppedImageUrl});
        } else if (this.videoRef && crop.width && crop.height) {
            const croppedVideoUrl = await this.getCroppedVid(
                this.videoRef,
                crop,
                "mp4"
            );
            this.setState({croppedVideoUrl});
        }
    }

    getCroppedVid(video, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = video.naturalWidth / video.width;
        const scaleY = video.naturalHeight / video.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            video,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toDataURL(blob => {
                if (!blob) {
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, "video/mp4");
        });
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            var image = canvas.toDataURL('image/jpeg', 0.5);
            console.log(image);

            this.setState({
                croppedImage: image
            })

            // canvas.toBlob(blob => {
            //     if (!blob) {
            //         //reject(new Error('Canvas is empty'));
            //         return;
            //     }
            //     blob.name = fileName;
            //     window.URL.revokeObjectURL(this.fileUrl);
            //     this.fileUrl = window.URL.createObjectURL(blob);
            //     resolve(this.fileUrl);
            // }, "image/jpeg");

        });
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    };

    handleColorChange = (color, event) => {
        this.setState({color: color.hex, openColor: false})

    };

    handleColorFocus = () => {
        this.setState({
            openColor: true
        })
    }

    checkValidation = () => {
        getURL(this.props.values.public_gallery_url).then((res) => {
            if (res.data.error) {
                console.log('true');
                this.setState({
                    url_valid: true,
                    url_value:'is-valid'
                })
            }else{
                this.setState({
                    url_valid: false,
                    url_value:'is-invalid'
                })
            }
        })
    }

    OnSubmit = (e) => {
        e.preventDefault();
        this.setState({
            disableButton: true
        })
        const {values: {brief_bio, cover_pic, all_brands, profile_pic, public_gallery_url,brand_color}} = this.props;
        // console.log(values);
        e.preventDefault();

        if (this.validator.allValid()) {
            const data = {
                'brief_bio': brief_bio,
                'cover_pic': cover_pic,
                'all_brands': {all_brands},
                'public_gallery_url': public_gallery_url,
                'brand_color': this.state.color,
                'profile_pic': this.state.croppedImage, // discuss
                'completed': true
            };
            console.log(this.state.src, this.state.crop, this.state.croppedImageUrl)

            update_UserData(data).then((res) => {
                if (!res.error) {
                    console.log(res);
                    this.setState({
                        disableButton: false
                    })
                    this.props.toggle();
                    window.location.reload(false);
                } else {

                }
            })

        } else {
            this.validator.showMessages();
            this.setState({
                disableButton: false
            })
            this.forceUpdate();
        }

    };


    render() {

        if (this.state.ToFrameGallery === true) {
            return <Redirect to="/FramesGallery"/>
        }
        const {crop, croppedImageUrl, croppedVideoUrl, src} = this.state;
        const {values} = this.props;

        return (

            <div className="container">
                <div className="row mt-5 pt-3">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="u-pearl done col-4">
                                <div className="u-pearl-icon">1</div>
                                <span className="u-pearl-title">
                                Build Profile
                            </span>
                            </div>
                            <div className="u-pearl done col-4">
                                <div className="u-pearl-icon">2</div>
                                <span className="u-pearl-title">
                                Add Brands
                            </span>
                            </div>
                            <div className="u-pearl current col-4">
                                <div className="u-pearl-icon">3</div>
                                <span className="u-pearl-title">
                                Personalize
                            </span>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6 mb-5">
                        <p className="h4 txt-info mt-5 mb-5">
                            This is your chance to create a custom URL for your
                            public frames gallery. You can't change this later so
                            make it easy to remember and recall.
                        </p>
                        <form className="needs-validation">

                            <div className="form-row">
                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom01">Public Gallery URL</label>
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">https://www.eyecandi.com/</span>
                                        </div>
                                        <input
                                            name="brief_bio"
                                            className={'form-control '+this.state.url_value}
                                            type="text"
                                            defaultValue={values.public_gallery_url}
                                            onChange={this.props.handleChange('public_gallery_url')}
                                            onBlur={this.checkValidation}
                                        />
                                        <div className="valid-feedback feedback-icon"
                                             style={{
                                                 position: 'absolute',
                                                 width: 'auto',
                                                 bottom: 0,
                                                 right: '10px',
                                                 marginTop: 0,
                                                 fontSize: '19px'
                                             }}>
                                            <i className="fa fa-check-circle"></i>
                                        </div>
                                        <div className="invalid-feedback feedback-icon"
                                             style={{
                                                 position: 'absolute',
                                                 width: 'auto',
                                                 bottom: 0,
                                                 right: '10px',
                                                 marginTop: 0,
                                                 fontSize: '19px'
                                             }}>
                                            <i className="fa fa-times"></i>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom01">Brand Color</label>
                                    <div>
                                        {!this.state.openColor &&
                                        <input
                                            name="brief_bio"
                                            type="text"
                                            className={'form-control'}
                                            defaultValue={this.state.color}
                                            onFocus={this.handleColorFocus}
                                            placeholder={'Choose the best color that matches your brand or type your own hex code.'}
                                        />
                                        }
                                        {this.state.openColor &&
                                        <SwatchesPicker onChange={this.handleColorChange}/>
                                        }
                                    </div>
                                </div>
                                <div className="col-md-12 mb-3">
                                    <label>Profile Logo</label>
                                    <div className="input-cropper" style={{padding: '5px', width: '100%'}}>
                                        <input type="file" name="profile_pic"
                                               defaultValue={this.state.src}
                                               onChange={this.onSelectFile}
                                               className="custom-file-width"
                                        />
                                        {this.state.profile_logo_error ? <span
                                            className="mt-1 text-left txt-danger">{this.state.profile_logo_error_msg}</span> : ""}
                                    </div>

                                    {src && (
                                        <ReactCrop
                                            src={src}
                                            crop={crop}
                                            onImageLoaded={this.onImageLoaded}
                                            onComplete={this.onCropComplete}
                                            onChange={this.onCropChange}
                                        />
                                    )}
                                    {croppedImageUrl && (
                                        <img alt="Crop" style={{width: "100px !important"}} src={croppedImageUrl}
                                             className="crop-portion"/>
                                    )}


                                    {croppedVideoUrl && (
                                        <video controls width="250">
                                            <source src={croppedVideoUrl} type="video/mp4"/>
                                        </video>
                                    )}

                                </div>
                                {/*onClick={submitFun}*/}
                            </div>
                            <button className="btn btn-secondary pull-left" type="button" onClick={this.back}>Back
                            </button>
                            <button className="btn btn-secondary pull-right" type="button"
                                    disabled={this.state.disableButton}
                                    onClick={this.OnSubmit}>{this.state.disableButton ? 'Please Wait ...' : 'Finish Setup'}</button>
                        </form>
                    </div>
                    <div className="col-md-3"></div>

                </div>


            </div>

        );

    }


}

export default Edit_Public_gallery;
