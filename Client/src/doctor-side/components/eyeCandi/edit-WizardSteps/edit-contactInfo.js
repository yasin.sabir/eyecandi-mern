import React, {Component} from 'react';
import SimpleReactValidator from 'simple-react-validator';
import ReactCrop from "react-image-crop";


class Edit_Contact_info extends Component {


    constructor(props){
        super(props);

        this.state = {
            profile_cover_error:false,
            profile_cover_error_msg:"File size should not be exceeds 3MB",
            ToFrameGallery: false,
            disableButton: false,
            cover_pic:null,
            src: null,
            crop: {
                unit: "%",
                width: 1024,
                height: 800,
                aspect: 16 / 9
                // aspect: 4 / 4
            }
        };

        this.validator = new SimpleReactValidator();
    }
    componentDidMount() {
        //console.log('cover',this.props.cover_pic);
        this.setState({
            src: this.props.cover_pic
        })
    }
    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {

            const FSize = e.target.files[0].size;
            const ImageSize = Math.round((FSize / 1024));

            if(ImageSize > 3072 ){

                this.setState({
                    profile_cover_error:true
                })

            }else{

                this.setState({
                    profile_cover_error:false
                })
                const reader = new FileReader();
                reader.addEventListener("load", () =>
                    this.setState({src: reader.result,
                    })
                );
                reader.readAsDataURL(e.target.files[0]);

            }


        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onVideoLoaded = video => {
        this.videoRef = video;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        this.setState({crop});
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                "newFile.jpeg"
            );
            //console.log(croppedImageUrl);
            this.setState({croppedImageUrl});
        } else if (this.videoRef && crop.width && crop.height) {
            const croppedVideoUrl = await this.getCroppedVid(
                this.videoRef,
                crop,
                "mp4"
            );
            this.setState({croppedVideoUrl});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            var image = canvas.toDataURL('image/jpeg',0.5);
            // console.log(image);

            this.setState({
                cover_pic : image
            })
            this.props.handleCoverChange(image);

            // canvas.toBlob(blob => {
            //     if (!blob) {
            //         //reject(new Error('Canvas is empty'));
            //         return;
            //     }
            //     blob.name = fileName;
            //     window.URL.revokeObjectURL(this.fileUrl);
            //     this.fileUrl = window.URL.createObjectURL(blob);
            //     resolve(this.fileUrl);
            // }, "image/jpeg");

        });
    }
    saveAndContinue = (e) => {
        e.preventDefault();
        if (this.validator.allValid()) {
            this.props.nextStep()
        }else{
            this.validator.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }


    };

    render() {
        const {values} = this.props;
        const {crop, croppedImageUrl, croppedVideoUrl, src} = this.state;
        //console.log(values.cover_pic);

        return (
            <div className="container">
                <div className="row mt-5 pt-3">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="u-pearl current col-4">
                                <div className="u-pearl-icon">1</div>
                                <span className="u-pearl-title">
                                Build Profile
                            </span>
                            </div>
                            <div className="u-pearl col-4">
                                <div className="u-pearl-icon">2</div>
                                <span className="u-pearl-title">
                                Add Brands
                            </span>
                            </div>
                            <div className="u-pearl col-4">
                                <div className="u-pearl-icon">3</div>
                                <span className="u-pearl-title">
                                Personalize
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-6">
                    <p className="h4 txt-info mt-5 mb-5" style={{
                        color:'#147BFC',
                        fontSize: '20px'
                    }}>
                        The following information will be used in your frames gallery to let your patients and potential patients know who you are and what services you provide.
                    </p>
                    <form className="needs-validation">
                            <div className="form-row">
                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom01">Brief Bio</label>
                                    <div>
                                        <div className="input-group">
                                            <textarea
                                                rows="8" cols="90"
                                                name="brief_bio"
                                                type="textarea"
                                                defaultValue={values.brief_bio}
                                                onChange={this.props.handleChange('brief_bio')}
                                            />
                                        </div>
                                        {this.validator.message('brief_bio', values.brief_bio, 'required', )}
                                    </div>
                                </div>
                                <div className="col-md-12 mb-3">
                                    <label>Profile Cover Photo</label>
                                    <div className="input-cropper" style={{padding: '5px', width: '100%'}}>
                                        <input type="file" name="cover_pic"
                                               defaultValue={values.cover_pic}
                                               onChange={this.onSelectFile}
                                               className="custom-file-width"
                                        />
                                        {this.validator.message('cover_pic', this.state.cover_pic, 'required', )}
                                        { this.state.profile_cover_error ? <span className="mt-1 text-left txt-danger">{this.state.profile_cover_error_msg}</span> :"" }
                                    </div>

                                    {values.cover_pic && (
                                        <ReactCrop
                                            src={values.cover_pic}
                                            crop={crop}
                                            onImageLoaded={this.onImageLoaded}
                                            onComplete={this.onCropComplete}
                                            onChange={this.onCropChange}
                                        />
                                    )}
                                    {croppedImageUrl && (
                                        <img alt="Crop" style={{width: "100px !important"}} src={croppedImageUrl}
                                             className="crop-portion"/>
                                    )}

                                    {croppedVideoUrl && (
                                        <video controls width="250">
                                            <source src={croppedVideoUrl} type="video/mp4"/>
                                        </video>
                                    )}

                                </div>
                            </div>
                            <button className="btn btn-secondary pull-right" type="button" onClick={this.saveAndContinue} >Next</button>

                        </form>
                    </div>
                    <div className="col-md-3"></div>
                </div>

            </div>
        );
    }
}

export default Edit_Contact_info
