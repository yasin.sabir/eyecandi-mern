import axios from 'axios';
import jwt_decode from "jwt-decode";

const url = "";

export const createCustomer = newCustomer => {
    return axios
        .post(url + '/users/create-customer', {
            payment_method: newCustomer.payment_method,
            email: newCustomer.email,
            card: newCustomer.tokenID,
        })
        .then(res => {
            return res.data;
        })
        .catch(err => {
            console.log(err)
        })
}

export const createCustomerSubscription = newSubscription => {
    return axios
        .post(url + '/users/create-subscription', {
            payment_method: newSubscription.payment_method,
            email: newSubscription.email,
            card: newSubscription.tokenID,
        })
        .then(res => {
            return res.data;
        })
        .catch(err => {
            console.log(err)
        })
}

export const createCustomerSubscriptionOneDollar = newSubscription => {
    return axios
        .post(url + '/users/create-subscription-one-dollar', {
            payment_method: newSubscription.payment_method,
            email: newSubscription.email,
            card: newSubscription.tokenID,
        })
        .then(res => {
            return res.data;
        })
        .catch(err => {
            console.log(err)
        })
}

// export const createCustomerSubscription = newSubscription => {
//     return axios
//         .post(url + '/users/create-subscription', {
//             customerID: newSubscription.customerID,
//         })
//         .then(res => {
//             return res.data;
//         })
//         .catch(err => {
//             console.log(err)
//         })
// }


export const register = newUser => {
    return axios
        .post(url + '/users/register', {

            email: newUser.email,
            password: newUser.password,
            confirm_password: newUser.confirm_password,
            promo_code: newUser.promo_code,
            affiliateLink: newUser.affiliateLink
            //first_name: newUser.first_name,
            //last_name: newUser.last_name,
            //practice_name: newUser.practice_name,
            //address:newUser.address,
            //oa_member: newUser.oa_member,
            //oa_member_code: newUser.oa_member_code,
        })
        .then(res => {
            localStorage.setItem('usertoken', res.data.token)
            localStorage.setItem('user_id', res.data.id)
            return res.data;
        })
        .catch(err => {
            console.log(err)
        })
};

export const login = user => {

    return axios
        .post(url + '/users/login', {
            email: user.email,
            password: user.password
        })
        .then(res => {

            localStorage.setItem('usertoken', res.data.token);
            localStorage.setItem('user_id', res.data.id);

            const decoded = jwt_decode(res.data.token);
            getUser(decoded._id).then(res => {
                localStorage.setItem( 'publicURL' , res.data.public_url );
                localStorage.setItem( 'stripeCustomerID' , res.data.stripeCustomerID );
                localStorage.setItem( 'stripeCustomerSubscriptionID' , res.data.stripeCustomerSubscriptionID );
            });

            return res.data;

        })
        .catch(err => {
            console.log(err,"err");
        });

};
export const change = () => {
    // const usertoken = localStorage.getItem('usertoken');
    return axios
        .get(url + '/users/change/')
        .then(res => {
            return res;
        }).catch(err => {
            console.log(err)
        })
};
export const verify = () => {
    const usertoken = localStorage.getItem('usertoken');
    return axios
        .get(url + '/users/verify/' + usertoken)
        .then(res => {
            return res;
        }).catch(err => {
            console.log(err)
        })
};

export const forgetPwd = (email) => {
    return axios
        .post(url + '/users/auth/recover', {
            email: email,
        })
        .then(res => {
            return res;
        }).catch(err => {
            console.log(err)
        })
}

export const changePwd = data => {
    return axios
        .post(url + '/users/auth/reset', data)
        .then(res => {
            return res;
        }).catch(err => {
            console.log(err)
        })
}

export const update = user => {
    const id = localStorage.getItem('user_id');
    return axios
        .put(url + '/users/' + id, {
            data: user
        })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
};

export const updateStripeCredentials = user => {
    const id = localStorage.getItem('user_id');
    return axios
        .put(url + '/users/stripeCredentials/' + id, {
            stripeCustomerID : user.stripeCustomerID,
            stripeCustomerSubscriptionID : user.stripeCustomerSubscriptionID
        })
        .then(res => {
            return res
        })
        .catch(err => {
            console.log(err)
        })
};



export const delete_userBy_id = (id) => {
    return axios
        .delete(  url + '/users/delete/' + id)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
}

export const update_UserData = user => {
    //console.log(user);

    const id = localStorage.getItem('user_id');
    return axios
        .put(url + '/users/update/' + id, {
            data: user
        })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })

};

export const productupdate = user => {
    const id = localStorage.getItem('user_id');
    return axios
        .put(url + '/users/product/' + id, {
            data: user
        })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })

};
export const showProduct = (id, sku) => {
    return axios
        .put(url + '/users/product/show/' + id + '/' + sku,)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
};

export const viewProduct = (id, sku) => {
    return axios
        .put(url + '/users/product/view/' + id + '/' + sku,)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
};

export const viewBrand = (id, sku) => {
    return axios
        .put(url + '/users/brand/view/' + id + '/' + sku,)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
};

export const hideProduct = (id, sku) => {
    return axios
        .put(url + '/users/product/hide/' + id + '/' + sku,)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
};
export const favoriteProduct = (id, sku) => {
    return axios
        .put(url + '/users/product/favorite/' + id + '/' + sku,)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
};

export const RemoveFavoriteProduct = (id, sku) => {
    return axios
        .put(url + '/users/product/remove_favorite/' + id + '/' + sku,)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
};

export const getUser = (id) => {
    // const id = localStorage.getItem('user_id');
    return axios
        .get(url + '/users/' + id,)
        .then(res => {
            return res;
        })
        .catch(err => {
            console.log(err)
        })

};

export const getUserEmail = (id) => {
    return axios
        .get(url + '/users/email/' + id,)
        .then(res => {
            return res;
        })
        .catch(err => {
            console.log(err)
        })

};

export const getURL = (user_url) => {
    // const id = localStorage.getItem('user_id');
    // console.log(user_url);
    return axios
        .get(url + '/users/public/' + user_url,)
        .then(res => {
            return res;
        })
        .catch(err => {
            console.log(err)
        })
};

export const sendEmail_tryOnRequest = (userEmail) => {
    return axios
        .post(url + '/users/emailtryOnRequest', {
            email_to: userEmail.email_to,
            email_subject: userEmail.email_subject,
            email_body: userEmail.email_body,
            email_body_frame_data: userEmail.email_body_frame_data,
            adminEmail: userEmail.adminEmail,
            userPassword: userEmail.userPassword,
        })
        .then(res => {
            return res;
        })
        .catch(err => {
            console.log("Email - Err :" + err);
        });

};

export const sendEmail_bookExam = (userEmail) => {
    return axios
        .post(url + '/users/bookanexam', {
            email_to: userEmail.email_to,
            email_subject: userEmail.email_subject,
            email_body: userEmail.email_body,
            email_body_frame_data: userEmail.email_body_frame_data,
        })
        .then(res => {
            return res;
        })
        .catch(err => {
            console.log("Email - Err :" + err);
        });

};
export const getCsv = () => {

    return axios
        .get(url + '/users/getcsv').then(res =>{
            return res;
        })
        .catch(err => {
            console.log("Email - Err :" + err);
        });
};



