import React, {Component} from 'react';
import SimpleReactValidator from 'simple-react-validator';
import ReactCrop from "react-image-crop";
import {getURL} from "../functions";


class Contact_info extends Component {

    constructor(props){
        super(props);


        this.state = {
            // profile_cover_error:false,
            // profile_cover_error_msg:"File size should not be exceeds 3MB",
            ToFrameGallery: false,
            disableButton: false,
            // cover_pic:null,
            // src: null,
            // crop: {
            //     unit: "%",
            //     width: 1110,
            //     height: 400,
            //     // aspect: 16 / 9
            //     aspect: 4 / 4
            // },
            // url_valid: false,
            // url_value: ''
        };

        this.validator = new SimpleReactValidator({
            validators: {
                phone: {  // name the rule
                    message: 'Enter valid phone no!',
                    rule: (val, params, validator) => {
                        return validator.helpers.testRegex(val,/^\d{3}-*\d{3}-*\d{4}$/i) && params.indexOf(val) === -1
                    },
                }
            }
        });
    }

    checkValidation = () => {
        getURL(this.props.values.public_gallery_url).then((res) => {
            if (res.data.error) {
                console.log('true');
                this.setState({
                    url_valid: true,
                    url_value:'is-valid'
                })
            }else{
                this.setState({
                    url_valid: false,
                    url_value:'is-invalid'
                })
            }
        })
    }

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {

            const FSize = e.target.files[0].size;
            const ImageSize = Math.round((FSize / 1024));

            if(ImageSize > 3072 ){

                this.setState({
                    profile_cover_error:true
                })

            }else{

                this.setState({
                    profile_cover_error:false
                })
                const reader = new FileReader();
                reader.addEventListener("load", () =>
                    this.setState({src: reader.result,
                    })
                );
                reader.readAsDataURL(e.target.files[0]);

            }


        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onVideoLoaded = video => {
        this.videoRef = video;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        this.setState({crop});
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                "newFile.jpeg"
            );
            console.log(croppedImageUrl);
            this.setState({croppedImageUrl});
        } else if (this.videoRef && crop.width && crop.height) {
            const croppedVideoUrl = await this.getCroppedVid(
                this.videoRef,
                crop,
                "mp4"
            );
            this.setState({croppedVideoUrl});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            var image = canvas.toDataURL('image/jpeg');
            // console.log(image);

            this.setState({
                cover_pic : image
            })
            this.props.handleCoverChange(image);

            // canvas.toBlob(blob => {
            //     if (!blob) {
            //         //reject(new Error('Canvas is empty'));
            //         return;
            //     }
            //     blob.name = fileName;
            //     window.URL.revokeObjectURL(this.fileUrl);
            //     this.fileUrl = window.URL.createObjectURL(blob);
            //     resolve(this.fileUrl);
            // }, "image/jpeg");

        });
    }

    saveAndContinue = (e) => {
        e.preventDefault();
        if (this.validator.allValid()) {
            this.props.nextStep()
        }else{
            this.validator.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }


    };

    render() {
        const {values} = this.props;
        const {crop, croppedImageUrl, croppedVideoUrl, src} = this.state;
        return (
            <div className="container">
                <div className="row mt-5 pt-3">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="u-pearl current col-4">
                                <div className="u-pearl-icon">1</div>
                                <span className="u-pearl-title">
                                Build Profile
                            </span>
                            </div>
                            <div className="u-pearl col-4">
                                <div className="u-pearl-icon section-2">2</div>
                                <span className="u-pearl-title">
                                Add Brands
                            </span>
                            </div>
                            <div className="u-pearl col-4">
                                <div className="u-pearl-icon section-3">3</div>
                                <span className="u-pearl-title">
                                Personalize
                            </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6 p-4 p-md-4">
                        <p className="h4 txt-info mt-5 mb-5" style={{
                            color:'#147BFC',
                            fontSize: '20px'
                        }}>
                            The following information will be used in your frames gallery to let your patients and potential patients know who you are and what services you provide.
                        </p>
                        <form className="needs-validation">
                            <div className="form-row">


                                {/*<div className="col-md-12 mb-3">*/}
                                {/*    <label htmlFor="validationCustom01">Public Gallery URL</label>*/}
                                {/*    <div className="input-group">*/}
                                {/*        <div className="input-group-prepend">*/}
                                {/*            <span className="input-group-text" id="public_url_placeholder">https://www.eyecandi.com/</span>*/}
                                {/*        </div>*/}
                                {/*        <input*/}
                                {/*            name="public_url_field"*/}
                                {/*            id="public_url_field"*/}
                                {/*            className={'form-control '+this.state.url_value}*/}
                                {/*            type="text"*/}
                                {/*            defaultValue={values.public_gallery_url}*/}
                                {/*            onChange={this.props.handleChange('public_gallery_url')}*/}
                                {/*            onBlur={this.checkValidation}*/}
                                {/*        />*/}
                                {/*        <div className="valid-feedback feedback-icon"*/}
                                {/*             style={{*/}
                                {/*                 position: 'absolute',*/}
                                {/*                 width: 'auto',*/}
                                {/*                 bottom: 0,*/}
                                {/*                 top: '6px',*/}
                                {/*                 right: '10px',*/}
                                {/*                 marginTop: 0,*/}
                                {/*                 fontSize: '19px'*/}
                                {/*             }}>*/}
                                {/*            <i className="far fa-check-circle"></i>*/}
                                {/*        </div>*/}
                                {/*        <div className="invalid-feedback feedback-icon"*/}
                                {/*             style={{*/}
                                {/*                 position: 'absolute',*/}
                                {/*                 width: 'auto',*/}
                                {/*                 bottom: 0,*/}
                                {/*                 top:'6px',*/}
                                {/*                 right: '10px',*/}
                                {/*                 marginTop: 0,*/}
                                {/*                 fontSize: '19px'*/}
                                {/*             }}>*/}
                                {/*            <i className="far fa-times-circle"></i>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*    {this.validator.message('public_url_field', values.public_gallery_url, 'required', )}*/}

                                {/*</div>*/}

                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom01">Practice Main Phone Number</label>
                                    <div>
                                        <div className="input-group">
                                            <input className="form-control input-field-style" type="text"
                                                   name="practice_phone_number"
                                                   defaultValue={values.practice_phone_number}
                                                   onChange={this.props.handleChange('practice_phone_number')}
                                            />
                                        </div>
                                        {this.validator.message('practice_phone_number', values.practice_phone_number, 'required|phone:111-111-1111', )}
                                    </div>
                                </div>

                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom01">Practice Address 1</label>
                                    <div>
                                        <div className="input-group">
                                            <input className="form-control input-field-style" type="text"
                                                   name="practice_address"
                                                   defaultValue={values.practice_address}
                                                   onChange={this.props.handleChange('practice_address')}
                                            />
                                        </div>
                                        {this.validator.message('practice_address', values.practice_address, 'required', )}
                                    </div>
                                </div>

                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom01">Practice Address 2 (Optional)</label>
                                    <div>
                                        <div className="input-group">
                                            <input className="form-control input-field-style" type="text"
                                                   name="practice_address2"
                                                   defaultValue={values.practice_address2}
                                                   onChange={this.props.handleChange('practice_address2')}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom01">Practice Website Url</label>
                                    <div>
                                        <div className="input-group">
                                            <input className="form-control input-field-style" type="text"
                                                   name="practice_website_url"
                                                   defaultValue={values.practice_website_url}
                                                   onChange={this.props.handleChange('practice_website_url')}
                                            />
                                        </div>
                                        {this.validator.message('practice_website_url', values.practice_website_url, 'required', )}
                                    </div>
                                </div>

                                <div className="col-md-12 mb-3">

                                    <div className="row">
                                        <div className="col-md-4">
                                            <label htmlFor="validationCustom01">City</label>
                                            <div>
                                                <div className="input-group">
                                                    <input className="form-control input-field-style" type="text"
                                                           name="city"
                                                           defaultValue={values.city}
                                                           onChange={this.props.handleChange('city')}
                                                    />
                                                </div>
                                                {this.validator.message('city', values.city, 'required', )}
                                            </div>
                                        </div>

                                        <div className="col-md-4 mt-3 mt-md-0">
                                            <label htmlFor="validationCustom01">State</label>
                                            <div>
                                                <div className="input-group">
                                                    <select className="form-control digits input-field-style" name="state" id="select_state" defaultValue=""  onChange={this.props.handleChange('state')}>
                                                        <option value="" selected="selected">Select a State</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                </div>
                                                {this.validator.message('state', values.state, 'required', )}
                                            </div>
                                        </div>


                                        <div className="col-md-4 mt-3 mt-md-0">
                                            <label htmlFor="validationCustom01">Zip</label>
                                            <div>
                                                <div className="input-group">
                                                    <input className="form-control input-field-style"
                                                           type="text"
                                                           name="zip"
                                                           defaultValue={values.zip}
                                                           onChange={this.props.handleChange('zip')}
                                                    />
                                                </div>
                                                {this.validator.message('zip', values.zip, 'required', )}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <button className="btn btn-secondary pull-right" type="button" onClick={this.saveAndContinue} >Next</button>

                        </form>
                    </div>
                    <div className="col-md-3"></div>

                </div>

            </div>
        );
    }
}

export default Contact_info
