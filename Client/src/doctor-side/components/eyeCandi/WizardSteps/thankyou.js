import React , {Component} from 'react';
import TypeaheadOne from "../../base/typeaheadComponent/typeahead-one";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import {Redirect} from "react-router";
import SimpleReactValidator from "simple-react-validator";
import {getUser, update,productupdate} from '../functions';
import * as credentials from "../../../constant/framesAPI";
import jwt_decode from "jwt-decode";


class SetupWizard_ThankYou extends Component{

    constructor(props){
        super(props);
        this.state = {
            public_gallery_url: ''
        }
    }

    componentDidMount() {

        const token = localStorage.usertoken;
        //console.log("TOKEN--"+token);

        if(token){
            const decoded = jwt_decode(token);

            getUser(decoded._id).then(res => {
                this.setState({
                    public_gallery_url:res.data.public_url,
                })
                //console.log(res.data.data[0])
            });


        }else{
            this.props.history.push('/login');
        }




    }

    GotoDashboard = () => {
        // this.props.history.push('/pages/FramesGallery');
        this.props.history.push('/');
    }


    GotoPublicView = () => {
        this.props.history.push(this.state.public_gallery_url);
    }
    render(){

        const {values} = this.props;
console.log(values)
        return(

            <div className="container">

                <div className="row">
                    <div className="col-lg-3"></div>
                    <div className="col-lg-6 mb-5">

                        <p className="h4 txt-info mt-5 mb-5">
                            That’s It, you are done setting up your custom frames gallery
                            and a tool for you to manage it. What do you want to do next?
                        </p>

                        <div className="row">
                            <div className="col-sm-6 col-md-6">
                                <div className="card b-r-0">
                                    <div className="card-body text-center">

                                        <i className="far fa-tools"></i>

                                        <p className="font-cu-secondary f-w-700 mt-3">Admin Dashboard</p>

                                        <div className="card-detail">
                                            <p className="mb-0">
                                                This is a tool that only have access to.
                                                It is where you can manage your public gallery
                                                and get special insights.
                                            </p>
                                        </div>

                                        <a className="btn btn-secondary" style={{color:'#fff'}}  onClick={this.GotoDashboard}>Visit Admin Tool</a>

                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-md-6">
                                <div className="card b-r-0">
                                    <div className="card-body text-center">

                                        <i className="far fa-glasses"></i>

                                        <p className="font-cu-secondary f-w-700 mt-3" >Public Gallery</p>

                                        <div className="card-detail">
                                            <p className="mb-0">
                                                This is the place you will want to send your existing
                                                patients and prospect patients to. Unlike the admin tool, it is public.
                                            </p>
                                        </div>

                                        <a className="btn btn-secondary" style={{color:'#fff'}} onClick={this.GotoPublicView}>Visit Public Gallery</a>
                                        {/*<a className="btn btn-secondary"  href={this.props.history.push(this.state.public_gallery_url)} target="_blank" style={{color:'#fff'}} >Visit Public Gallery</a>*/}

                                    </div>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="card b-r-0">
                                    <div className="card-body">

                                        <div className="row">
                                            <div className="col-md-2 m-auto text-center">
                                                <i className="far fa-life-ring"></i>
                                            </div>
                                            <div className="col-md-10">

                                                <p className="font-cu-secondary f-w-700 mt-1 mb-1">
                                                    Remember, you are not alone in this…
                                                </p>
                                                <p>
                                                    Our team is available if you need help with the admin
                                                    or your public gallery, simply email us at <a href="mailto:support@eyecandi.app">support@eyecandi.app</a>.
                                                </p>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="col-lg-3"></div>
                </div>

            </div>

        );

    }


}

export default SetupWizard_ThankYou;
