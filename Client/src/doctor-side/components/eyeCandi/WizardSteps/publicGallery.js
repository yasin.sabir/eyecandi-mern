import React, {Component} from 'react';
import TypeaheadOne from "../../base/typeaheadComponent/typeahead-one";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import {Redirect} from "react-router";
import SimpleReactValidator from "simple-react-validator";
import {getUser, update, productupdate, getURL} from '../functions';
import {SwatchesPicker} from 'react-color';
// import './my-color-chooser-style-override.css';
import * as credentials from "../../../constant/framesAPI";
import jwt_decode from "jwt-decode";


class Public_gallery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pictures: [],
            profile_logo_format_error: false,
            profile_logo_format_error_msg : "Only PNG, JPEG, JPG formats are allowed!",
            profile_logo_error: false,
            profile_logo_error_msg: "File size should not be exceeds 3MB",
            profile_cover_error:false,
            profile_cover_error_msg:"File size should not be exceeds 3MB",
            profile_cover_format_error: false,
            profile_cover_format_error_msg : "Only PNG, JPEG, JPG formats are allowed!",
            ToFrameGallery: false,
            disableButton: false,

            croppedImage: null,
            src: null,
            crop: {
                unit: "%",
                width: 50,
                height: 50,
                //aspect: 16 / 9
                aspect: 4 / 4
            },

            cover_pic: null,
            src2: null,
            crop2: {
                unit: "%",
                width: 100,
                height: 100,
                aspect: 21 / 9
            },

            color: '',
            openColor: false,
            url_valid: false,
            url_value: '',

        }
        this.validator = new SimpleReactValidator();
    }

    componentDidMount() {

        const token = localStorage.usertoken;

        if(token) {
            const decoded = jwt_decode(token);

            getUser(decoded._id).then(res => {
                this.setState({
                    public_gallery_url: res.data.public_url,
                })
            });
        }


    }


    //======== Below Code For Select Logo Icon Start

    onSelectFile = e => {

        var fileName = document.querySelector('#logo-file-input').value;
        var fileExtension = fileName.split('.').pop();
        console.log(fileExtension);

        if(fileExtension === "jpg" || fileExtension === "jpeg" || fileExtension === "png"){

            this.setState({profile_logo_format_error : false});

            if (e.target.files && e.target.files.length > 0) {

                const FSize = e.target.files[0].size;
                const ImageSize = Math.round((FSize / 1024));

                if (ImageSize > 3072) {

                    this.setState({
                        profile_logo_error: true
                    })

                } else {

                    this.setState({
                        profile_logo_error: false
                    })
                    const reader = new FileReader();
                    reader.addEventListener("load", () =>
                        this.setState({
                            src: reader.result,
                        })
                    );
                    reader.readAsDataURL(e.target.files[0]);

                }


            }

        }else{
            this.setState({profile_logo_format_error : true});
        }

    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onVideoLoaded = video => {
        this.videoRef = video;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        this.setState({crop});
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                "newFile.jpeg"
            );
            this.setState({croppedImageUrl});
        } else if (this.videoRef && crop.width && crop.height) {
            const croppedVideoUrl = await this.getCroppedVid(
                this.videoRef,
                crop,
                "mp4"
            );
            this.setState({croppedVideoUrl});
        }
    }

    getCroppedVid(video, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = video.naturalWidth / video.width;
        const scaleY = video.naturalHeight / video.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            video,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toDataURL(blob => {
                if (!blob) {
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, "video/mp4");
        });
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            var image = canvas.toDataURL('image/jpeg', 0.5);
            console.log(image);

            this.setState({
                croppedImage: image
            })

            // canvas.toBlob(blob => {
            //     if (!blob) {
            //         //reject(new Error('Canvas is empty'));
            //         return;
            //     }
            //     blob.name = fileName;
            //     window.URL.revokeObjectURL(this.fileUrl);
            //     this.fileUrl = window.URL.createObjectURL(blob);
            //     resolve(this.fileUrl);
            // }, "image/jpeg");

        });
    }

    //======== Below Code For Select Logo Icon End


    //======== Below Code For Select Profile Cover Start

    onSelectFile2 = e => {
        var fileName = document.querySelector('#cover-file-input').value;
        var fileExtension = fileName.split('.').pop();
        console.log(fileExtension);

        if(fileExtension === "jpg" || fileExtension === "jpeg" || fileExtension === "png"){
            this.setState({profile_cover_format_error : false});
            if (e.target.files && e.target.files.length > 0) {

                const FSize2 = e.target.files[0].size;
                const ImageSize2 = Math.round((FSize2 / 1024));

                if(ImageSize2 > 3072 ){

                    this.setState({
                        profile_cover_error:true
                    })

                }else{

                    this.setState({
                        profile_cover_error:false
                    })
                    const reader2 = new FileReader();
                    reader2.addEventListener("load", () =>
                        this.setState({src2: reader2.result,
                        })
                    );
                    reader2.readAsDataURL(e.target.files[0]);

                }


            }
        }else{
            this.setState({profile_cover_format_error : true});
        }

    };

    // If you setState the crop in here you should return false.
    onImageLoaded2 = image2 => {
        this.imageRef2 = image2;
    };

    onVideoLoaded2 = video2 => {
        this.videoRef2 = video2;
    }

    onCropComplete2 = crop2 => {
        this.makeClientCrop2(crop2);
    };

    onCropChange2 = (crop2, percentCrop2) => {
        // You could also use percentCrop:
        this.setState({crop2});
    };

    async makeClientCrop2(crop2) {
        if (this.imageRef2 && crop2.width && crop2.height) {
            const croppedImageUrl2 = await this.getCroppedImg2(
                this.imageRef2,
                crop2,
                "newFile.jpeg"
            );
            console.log(croppedImageUrl2);
            this.setState({croppedImageUrl2});
        } else if (this.videoRef2 && crop2.width && crop2.height) {
            const croppedVideoUrl2 = await this.getCroppedVid2(
                this.videoRef2,
                crop2,
                "mp4"
            );
            this.setState({croppedVideoUrl2});
        }
    }

    getCroppedImg2(image2, crop2, fileName2) {
        const canvas2 = document.createElement("canvas");
        const scaleX2 = image2.naturalWidth / image2.width;
        const scaleY2 = image2.naturalHeight / image2.height;
        canvas2.width = crop2.width;
        canvas2.height = crop2.height;
        const ctx2 = canvas2.getContext("2d");

        ctx2.drawImage(
            image2,
            crop2.x * scaleX2,
            crop2.y * scaleY2,
            crop2.width * scaleX2,
            crop2.height * scaleY2,
            0,
            0,
            crop2.width,
            crop2.height
        );

        return new Promise((resolve, reject) => {
            var image2 = canvas2.toDataURL('image/jpeg');
             console.log("Cover -- "+image2);

            this.setState({
                cover_pic : image2
            })
            //this.props.handleCoverChange(image2);

            // canvas.toBlob(blob => {
            //     if (!blob) {
            //         //reject(new Error('Canvas is empty'));
            //         return;
            //     }
            //     blob.name = fileName;
            //     window.URL.revokeObjectURL(this.fileUrl);
            //     this.fileUrl = window.URL.createObjectURL(blob);
            //     resolve(this.fileUrl);
            // }, "image/jpeg");

        });
    }

    //======== Below Code For Select Profile Cover End

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    };

    handleColorChange = (color, event) => {
        this.setState({color: color.hex, openColor: false})

    };

    handleColorFocus = () => {
        this.setState({
            openColor: true
        })
    }

    checkValidation = () => {
        getURL(this.props.values.public_gallery_url).then((res) => {
            if (res.data.error) {
                console.log('true');
                this.setState({
                    url_valid: true,
                    url_value:'is-valid'
                })
            }else{
                this.setState({
                    url_valid: false,
                    url_value:'is-invalid'
                })
            }
        })
    }

    OnSubmit = (e) => {
        e.preventDefault();
        this.setState({
            disableButton: true
        })
        const {values: {
            brief_bio ,
            // cover_pic ,
            all_brands ,
            // profile_pic ,
            practice_phone_number,
            practice_address,
            practice_address2,
            practice_website_url,
            city,
            state,
            zip,
            tryOnRequest_Email,
            public_gallery_url ,
            brand_color
        }} = this.props;
        e.preventDefault();

        if (this.validator.allValid()) {
            const data = {
                'practice_phone_number' : practice_phone_number,
                'practice_address' : practice_address,
                'practice_address2' : practice_address2,
                'practice_website_url' : practice_website_url,
                'city' : city,
                'state' : state,
                'zip' : zip ,
                'tryOnRequest_Email' : tryOnRequest_Email,
                'brief_bio': brief_bio,
                // 'cover_pic': this.state.cover_pic,
                'all_brands': {all_brands},
                // 'public_gallery_url': public_gallery_url,
                'brand_color': this.state.color,
                'banner_top_heading' : 'null',
                'banner_middle_heading' : 'null',
                'banner_bottom_heading' : 'null',
                // 'profile_pic': this.state.croppedImage, // discuss
                // 'profile_pic'        : this.state.croppedImageUrl, // discuss
                // 'completed'          : true
            };

            console.log(data);

            update(data).then((res) => {
                if (!res.error) {
                    console.log(res);
                    //this.props.history.push('ThankYou');
                    this.props.history.push(this.state.public_gallery_url);
                    //this.props.history.push('/');
                } else {

                    this.setState({
                        disableButton: false
                    });
                }
            })

        } else {
            this.validator.showMessages();
            this.setState({
                disableButton: false
            });
            this.forceUpdate();

        }

    };


    render() {

        if (this.state.ToFrameGallery === true) {
            return <Redirect to="/pages/FramesGallery"/>
        }
        const {
            crop,
            croppedImageUrl,
            croppedVideoUrl,
            src ,
            crop2,
            croppedImageUrl2 ,
            croppedVideoUrl2 ,
            src2
        } = this.state;
        const {values} = this.props;

        return (

            <div className="container">
                <div className="row mt-5 pt-3">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="u-pearl done col-4">
                                <div className="u-pearl-icon">1</div>
                                <span className="u-pearl-title">
                                Build Profile
                            </span>
                            </div>
                            <div className="u-pearl done col-4">
                                <div className="u-pearl-icon">2</div>
                                <span className="u-pearl-title">
                                Add Brands
                            </span>

                            </div>
                            <div className="u-pearl current col-4">
                                <div className="u-pearl-icon">3</div>
                                <span className="u-pearl-title">
                                Personalize
                            </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6 p-4 p-md-4">
                        <p className="h4 txt-info mt-5 mb-5">
                            Finally, find a color that matches your brand and add a short bio that sets your practice apart.
                        </p>
                        <form className="needs-validation">

                            <div className="col-md-12 mb-3 pl-0 pr-0">
                                <label htmlFor="validationCustom01">Choose Your Brand Color</label>
                                <div>
                                    {!this.state.openColor &&
                                    <input
                                        name="brief_bio"
                                        id="brand_pallete_color"
                                        type="text"
                                        className={'form-control'}
                                        defaultValue={this.state.color}
                                        onFocus={this.handleColorFocus}
                                        placeholder={'Choose the best color that matches your brand or type your own hex code.'}
                                    />
                                    }
                                    {this.state.openColor &&
                                    <SwatchesPicker onChange={this.handleColorChange}/>
                                    }
                                </div>
                            </div>

                            {/*<div className="col-md-12 mb-3 pl-0 pr-0">*/}
                            {/*    <label>Logo Icon</label>*/}
                            {/*    <div className="input-cropper" style={{padding: '5px', width: '100%'}}>*/}
                            {/*        <label htmlFor="logo-file-input" className="custom-input-file-design">Choose*/}
                            {/*            a square .jpg or .png</label>*/}
                            {/*        <input type="file" name="profile_pic"*/}
                            {/*               accept="image/x-png,image/gif,image/jpeg,image/jpg"*/}
                            {/*               id="logo-file-input"*/}
                            {/*               defaultValue={this.state.src}*/}
                            {/*               onChange={this.onSelectFile}*/}
                            {/*               className="custom-file-width"*/}
                            {/*               buttonText={'test'}*/}
                            {/*        />*/}
                            {/*        {this.validator.message('profile_pic', this.state.src, 'required',)}*/}
                            {/*        {this.state.profile_logo_error ? <span*/}
                            {/*            className="mt-1 text-left txt-danger d-block">{this.state.profile_logo_error_msg}</span> : ""}*/}
                            {/*        {this.state.profile_logo_format_error ? <span*/}
                            {/*            className="mt-1 text-left txt-danger d-block">{this.state.profile_logo_format_error_msg}</span> : ""}*/}
                            {/*    </div>*/}
                            {/*    <span className="ml-1" style={{color:'#8d939a'}}><em>Only PNG , JPEG , JPG formats are allowed!</em></span>*/}

                            {/*    {src && (*/}
                            {/*        <ReactCrop*/}
                            {/*            src={src}*/}
                            {/*            crop={crop}*/}
                            {/*            onImageLoaded={this.onImageLoaded}*/}
                            {/*            onComplete={this.onCropComplete}*/}
                            {/*            onChange={this.onCropChange}*/}
                            {/*        />*/}
                            {/*    )}*/}

                            {/*    {croppedImageUrl && (*/}
                            {/*        <img alt="Crop" style={{width: "100px !important"}} src={croppedImageUrl}*/}
                            {/*             className="crop-portion"/>*/}
                            {/*    )}*/}

                            {/*    {croppedVideoUrl && (*/}
                            {/*        <video controls width="250">*/}
                            {/*            <source src={croppedVideoUrl} type="video/mp4"/>*/}
                            {/*        </video>*/}
                            {/*    )}*/}

                            {/*</div>*/}

                            <div className="col-md-12 mb-3 pl-0 pr-0">
                                <label htmlFor="validationCustom01">Profile Bio</label>
                                <div>
                                    <div className="input-group">
                                        {/*<div className="input-group-prepend">*/}
                                        {/*    <span className="input-group-text">https://www.</span>*/}
                                        {/*</div>*/}
                                        <textarea
                                            rows="8" cols="90"
                                            id="profile_brief_bio"
                                            name="brief_bio"
                                            type="textarea"
                                            defaultValue={values.brief_bio}
                                            onChange={this.props.handleChange('brief_bio')}
                                        />
                                    </div>
                                    {this.validator.message('brief_bio', values.brief_bio, 'required', )}
                                </div>
                            </div>

                            {/*<div className="col-md-12 mb-3 pl-0 pr-0">*/}
                            {/*    <label>Profile Cover Photo</label>*/}
                            {/*    <div className="input-cropper" style={{padding: '5px', width: '100%'}}>*/}
                            {/*        <label for="cover-file-input" className="custom-input-file-design">Choose File</label>*/}
                            {/*        <input type="file" name="cover_pic"*/}
                            {/*               id="cover-file-input"*/}
                            {/*               defaultValue={this.state.src2 }*/}
                            {/*               onChange={this.onSelectFile2}*/}
                            {/*               className="custom-file-width"*/}
                            {/*        />*/}
                            {/*        {this.validator.message('cover_pic', this.state.src2, 'required', )}*/}
                            {/*        { this.state.profile_cover_error ? <span className="mt-1 text-left txt-danger d-block">{this.state.profile_cover_error_msg}</span> :"" }*/}
                            {/*        {this.state.profile_cover_format_error ? <span*/}
                            {/*            className="mt-1 text-left txt-danger d-block">{this.state.profile_cover_format_error_msg}</span> : ""}*/}
                            {/*    </div>*/}
                            {/*    <span className="ml-1" style={{color:'#8d939a'}}><em>Only PNG , JPEG , JPG formats are allowed!</em></span>*/}

                            {/*    {src2 && (*/}
                            {/*        <ReactCrop*/}
                            {/*            src={src2}*/}
                            {/*            crop={crop2}*/}
                            {/*            onImageLoaded={this.onImageLoaded2}*/}
                            {/*            onComplete={this.onCropComplete2}*/}
                            {/*            onChange={this.onCropChange2}*/}
                            {/*        />*/}
                            {/*    )}*/}

                            {/*    {croppedImageUrl2 && (*/}
                            {/*        <img alt="Crop" style={{width: "100px !important"}} src={croppedImageUrl2}*/}
                            {/*             className="crop-portion"/>*/}
                            {/*    )}*/}

                            {/*    {croppedVideoUrl2 && (*/}
                            {/*        <video controls width="250">*/}
                            {/*            <source src={croppedVideoUrl2} type="video/mp4"/>*/}
                            {/*        </video>*/}
                            {/*    )}*/}

                            {/*</div>*/}

                            <button className="btn btn-secondary pull-left custom-sw-back-btn" type="button" onClick={this.back}>Back
                            </button>
                            <button className="btn btn-secondary pull-right" type="button"
                                    disabled={this.state.disableButton}
                                    onClick={this.OnSubmit}>{this.state.disableButton ? 'Please Wait...' : 'Finish Setup'}</button>
                        </form>
                    </div>
                </div>

            </div>
        );
    }
}

export default Public_gallery;
