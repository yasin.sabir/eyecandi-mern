import React, {Component, Fragment} from 'react';
import logo from '../../assets/images/eyeCandi/logo-ss.png'
import SimpleReactValidator from 'simple-react-validator';
import {changePwd} from "./functions";


export default class ForgotPwd extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Pwd:'',
            token: '',
            resMsg: '',
            resMsg_error: false

        }

        this.validator = new SimpleReactValidator({
            messages: {
                in: 'Passwords need to match!'
            }
        });

    }

    componentWillMount() {
        var token =  this.props.match.params.token;
        this.setState({
            token
        });
    }

    handleOnChangePwd = (e) =>{
        this.setState({
            Pwd: e.target.value
        })
    }


    OnSubmit = (e) => {
        e.preventDefault();

        if(this.validator.allValid()){
            var data = {
                token : this.state.token,
                password: this.state.Pwd
            }
            changePwd(data).then(res => {
                if(res.data.success){
                    setTimeout(()=>{
                        this.props.history.push('/login')
                    },1000);
                }
                this.setState({
                    resMsg: res.data.message,
                    resMsg_error : true
                })
            }).catch(error => {
                console.log(error);
            })
        }else{
            this.validator.showMessages();
            this.forceUpdate();
        }

    }


    render() {
        return (
            <Fragment>
                <div className="page-wrapper">
                    <div className="container-fluid">
                        {/* <!-- Reset Password page start--> */}
                        <div className="authentication-main">
                            <div className="row">
                                <div className="col-md-12 p-0">
                                    <div className="auth-innerright">
                                        <div className="authentication-box">
                                            <div className="text-center"><img src={logo} alt="" /></div>
                                            <div className="card mt-4 p-4">
                                                <form className="theme-form">
                                                    <div className="text-center">
                                                        <h5 className="text-capitalize txt-danger f-w-700">CREATE YOUR NEW PASSWORD</h5>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="col-form-label">New Password</label>
                                                        <input className="form-control"
                                                               type="password"
                                                               name="Pwd"
                                                               value={this.state.pwd}
                                                               onChange={this.handleOnChangePwd}
                                                               placeholder="*****" />
                                                        {this.validator.message('Pwd', this.state.Pwd, `required`)}
                                                        { this.state.resMsg_error ? <span className="txt-success">{this.state.resMsg}</span> : null }
                                                    </div>
                                                    <div className="form-group form-row mb-0 text-center">
                                                        <div className="col-md-12">
                                                            <button className="btn btn-primary" type="submit" onClick={this.OnSubmit}>Done</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- Reset Password page end--> */}
                    </div>
                </div>
            </Fragment>
        );
    }

};
