import React, {Component, Fragment} from 'react';
import Header from './common/header-component/header';
import Footer from "./common/footer";
import Sidebar from "./common/sidebar-component/sidebar";
import RightSidebar from "./common/right-sidebar";
import LimitedOffer from "./elements/limitedOffer";
import SimpleReactValidator from 'simple-react-validator';
import logo from "../../../assets/images/eyeCandi/logo/logo-circle.svg";
import {getURL, update, sendEmail_tryOnRequest} from "../functions";
import * as credentials from "../../../constant/framesAPI";
import {Email, Item, renderEmail, Span,Image} from "react-html-email";


class tryOnListForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tester_name: "",
            tester_email: "",
            tester_phone: "",
            tester_ques1: "",
            tester_ques2: "",
            tester_ques3: "",
            tester_ques_1_flag :false,
            tester_ques_1_error : "Option is required!",
            banner_top_heading: '',
            banner_middle_heading : '',
            banner_bottom_heading : '',
            public_url: this.props.match.params.publicUrl,
            color: '#00000065',
            footer_font_heading_color: '#0C4C60',
            sidebar_color: '#0C4C60',
            logo: logo,
            data: '',
            email: '',
            userID : '',
            password : '',
            wishlist: [],
            token: '',
            name: '',
            mail_subject: "Eyeglasses Try-on Request",
            mail_body: "Hello. \n I would like to try on request!",
            mail_ques1: "Question : Have you had an eye exam in the last 12 months?",
            mail_ans1: "Answer : ",
            mail_ques2: "Question : When is the best time to contact you?",
            mail_ans2: "Answer : ",
            mail_frame_data: [],
            adminEmail : "",
            tryonList_thankyouNote : 'We are getting your frames ready. Once we have your selection of eyewear ready for you, come on by and try them on. If there is an issue with any of your frame picks, we will contact you. Have a question that needs an answer lickety-split? Email or call us, we\'re here to help.'
        };

        this.validator = new SimpleReactValidator({
            validators: {
                phone: {  // name the rule
                    message: 'Enter valid phone no!',
                    rule: (val, params, validator) => {
                        return validator.helpers.testRegex(val,/^\d{3}-*\d{3}-*\d{4}$/i) && params.indexOf(val) === -1
                    },
                }
            }
        });
    }

    handleChange = input => (e) => {
        this.setState({[input]: e.target.value})
    };

    handle_tester_ques1 = (e) => {
        this.setState({tester_ques1: e.target.value} ,
        this.check_Tester_val(e.target.value)
        );
    }

    check_Tester_val (param) {
        if(param === 'select'){
            this.setState({
                tester_tester_ques_1_flag : true
            })
        }else{
            this.setState({
                tester_tester_ques_1_flag : false
            })
        }

    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.validator.allValid() && this.state.tester_ques1 !== "select") {

            const emailHTML = renderEmail(
                <Email title={this.state.mail_body} align="left">
                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginBottom:'5px'}}>
                            Hello,
                        </Span>
                        <Span fontSize={16}>
                            {this.state.tester_name} wants to try-on the following frames. Please contact them when you have the frames ready for them to come in and try them on. If there are any issues with this request please contact.
                        </Span>
                    </Item>
                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'45px',marginBottom:'5px'}}>
                            Patient Name:
                        </Span>
                        <Span fontSize={16}>
                            {this.state.tester_name}
                        </Span>
                    </Item>
                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                            Patient Email:
                        </Span>
                        <Span fontSize={16}>
                            {this.state.tester_email}
                        </Span>
                    </Item>
                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                            Patient Phone:
                        </Span>
                        <Span fontSize={16}>
                            {this.state.tester_phone}
                        </Span>
                    </Item>

                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                            When is the best time to contact you?
                        </Span>
                        <Span fontSize={16}>
                            {this.state.tester_ques1}
                        </Span>
                    </Item>
                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                            When is the best time to contact you?
                        </Span>
                        <Span fontSize={16}>
                            {this.state.tester_ques2}
                        </Span>
                    </Item>
                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                           What is your preferred method of communication
                        </Span>
                        <Span fontSize={16}>
                            {this.state.tester_ques3}
                        </Span>
                    </Item>


                    {this.state.wishlist.map((item,index) => {
                        return (<Item align="left">
                            {/*<Span fontSize={16}>*/}
                            {/*    {index + 1}*/}
                            {/*</Span>*/}
                            <Image width={250} height={150} alt={item.name} src={'https://api.framesdata.com/api/images?auth=' + this.state.token + item.variation.images}/>
                            <Span fontSize={16}>
                               <strong> {item.sku} / {item.name} / {item.variation.color}  </strong>
                            </Span>
                            {/*<Span fontSize={16}>*/}
                            {/*   <strong>   <br/> </strong>*/}
                            {/*</Span>*/}
                        </Item>);
                    })}
                </Email>
            );
            //console.log(emailHTML);


            const mail_data = {
                email_to: this.state.email,
                email_subject: this.state.mail_subject,
                email_body : this.state.mail_body,
                email_body_frame_data : emailHTML,
                adminEmail : this.state.email,
                userPassword : this.state.password,
            };

            sendEmail_tryOnRequest(mail_data).then((res) => {
                // console.log(res + " TEST ");
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/${this.state.public_url}/thank-you`,
                    state : { tryonList_thankyouNote : this.state.tryonList_thankyouNote }
                });
            })

            //alert("dd");
            //this.props.history.push(`${process.env.PUBLIC_URL}/${this.state.public_url}/thank-you`);
        } else {
            this.validator.showMessages();
            this.setState({ tester_ques_1_flag : true });

            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }
    };

    componentDidMount() {
        window.scrollTo(0, 0);
        getURL(this.state.public_url).then((res) => {
            if (res.data.error) {
                this.props.history.push('/404');
            } else {
                // this.setState({
                //     color: res.data.data[0].brand_color,
                //     logo:res.data.data[0].profile_pic,
                //     data:res.data.data[0],
                //     email:res.data.email
                // })

                if (localStorage.getItem("favorite") !== null) {
                    var whislist = localStorage.getItem("favorite");
                    var token = '';
                    whislist = JSON.parse(whislist);
                    fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                        .then(response => response.json())
                        .then((data) => {
                            token = data.Auth.AuthorizationTicket;
                            console.log( res.data.password);
                            this.setState({
                                color: res.data.data[0].brand_color,
                                logo: res.data.data[0].profile_pic,
                                data: res.data.data[0],
                                email: res.data.email,
                                userID : res.data._id,
                                banner_top_heading: res.data.data[0].banner_top_heading,
                                banner_middle_heading : res.data.data[0].banner_middle_heading,
                                banner_bottom_heading : res.data.data[0].banner_bottom_heading,
                                password : res.data.password,
                                name: res.data.practice_name,
                                wishlist: whislist,
                                token: token
                            })
                        })

                }
            }
        })
    };


    // pl-0 pr-0 m-t-50 p-b-10

    render() {

        const mail_body_frame_data = [];


        // console.log(mail_body_frame_data);


        const frame_body = "Hello.%0D%0AI would like to try on request!%0D%0A";

        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header logo={this.state.logo} color={this.state.color} url={this.state.public_url}
                                name={this.state.name} data={this.state.data} />
                        <Sidebar sidebar_color={this.state.sidebar_color} url={this.state.public_url}/>
                        <RightSidebar/>

                        <div className="page-body custom-public-body-color m-t-50 p-t-50">

                            <div className="container mt-4 m-b-50">

                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <div className="tryOnList-note">
                                            <h4 className="fv-h4 f-w-900">Last step, we need your info.</h4>
                                            <p className="sub-para-heading">
                                                How else are we supposed to contact you when your frames are ready?
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="row m-t-30">
                                    <div className="col-md-3 col-lg-4"></div>
                                    <div className="col-md-6 col-lg-4">
                                        <form action="">

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">Your
                                                    Name</label>
                                                <input className="form-control"
                                                       name="Name"
                                                       id="tester_name"
                                                       type="text"
                                                       onChange={this.handleChange('tester_name')}/>
                                                {this.validator.message('Name', this.state.tester_name, 'required')}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">Your
                                                    Email</label>
                                                <input className="form-control"
                                                       name="Email"
                                                       id="tester_email"
                                                       type="text"
                                                       onChange={this.handleChange('tester_email')}
                                                />
                                                {this.validator.message('Email', this.state.tester_email, 'required|email',)}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">Your
                                                    Phone</label>
                                                <input className="form-control"
                                                       name="Phone"
                                                       id="tester_phone"
                                                       type="text"
                                                       onChange={this.handleChange('tester_phone')}
                                                />
                                                {this.validator.message('Phone', this.state.tester_phone, 'required|phone:111-111-1111',)}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">Have you had an eye exam in the last 12 months?</label>
                                                <select name="Exam"
                                                        id="tester_ques1"
                                                        className="form-control"
                                                        onChange={this.handle_tester_ques1}
                                                >
                                                    <option value="select">Select Option</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="No">No</option>
                                                </select>
                                                { this.state.tester_tester_ques_1_flag ? <span className="text-danger">{this.state.tester_ques_1_error}</span> : "" }

                                                {/*<input className="form-control"*/}
                                                {/*       name="Exam"*/}
                                                {/*       id="tester_ques1"*/}
                                                {/*       type="text"*/}
                                                {/*       onChange={this.handleChange('tester_ques1')}*/}
                                                {/*/>*/}
                                                {/*{this.validator.message('tester_ques1', this.state.tester_ques1, 'required',)}*/}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">When is the best time to contact you?" to a dropdown with these options</label>
                                                <select name="Time"
                                                        id="tester_ques2"
                                                        className="form-control"
                                                        onChange={this.handleChange('tester_ques2')}
                                                >
                                                    <option value="select">Select Option</option>
                                                    <option value="morning">Morning</option>
                                                    <option value="afternoon">Afternoon</option>
                                                </select>

                                                {/*<input className="form-control"*/}
                                                {/*       name="Time"*/}
                                                {/*       id="tester_ques2"*/}
                                                {/*       type="text"*/}
                                                {/*       onChange={this.handleChange('tester_ques2')}*/}
                                                {/*/>*/}
                                                {/*{this.validator.message('tester_ques2', this.state.tester_ques2, 'required',)}*/}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">What is your preferred method of communication</label>
                                                <select name="method_comm"
                                                        id="tester_ques3"
                                                        className="form-control"
                                                        onChange={this.handleChange('tester_ques3')}
                                                >
                                                    <option value="Phone Call">Phone Call</option>
                                                    <option value="SMS">SMS</option>
                                                    <option value="Email">Email</option>
                                                </select>
                                            </div>

                                            <div className="text-center mt-4">

                                                {/*<a href={"mailto:"+this.state.main_contact_email+"?subject="+this.state.mail_subject+"&body="+frame_name} className="btn btn-secondary m-r-10">Request a Try-on</a>*/}
                                                {/*<input type="submit" onClick={this.onSubmit} className="cus-btn cus-btn-primary text-white" value="Submit Your Information"/>*/}

                                                {/*href={"mailto:" + this.state.tester_email + "?subject=" + this.state.mail_subject + "&body=" + frame_body + mail_body_frame_data}*/}


                                                <a onClick={this.onSubmit}
                                                   style={{backgroundColor:this.state.color}}
                                                   className="cus-btn cus-btn-primary text-white tryOnSubmitBtn">
                                                    Submit Your
                                                    Information</a>

                                                <p className="mt-3">Your privacy is <a href="#"
                                                                                       className="custom-a-color">our
                                                    policy</a></p>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-md-3 col-lg-4"></div>
                                </div>
                            </div>

                            <LimitedOffer color={this.state.color} top_heading={this.state.banner_top_heading} middle_heading={this.state.banner_middle_heading} bottom_heading={this.state.banner_bottom_heading}/>

                            <Footer footer_font_heading_color={this.state.footer_font_heading_color}
                                    url={this.state.public_url} data={this.state.data} email={this.state.email}/>
                        </div>

                    </div>
                </div>


            </Fragment>
        )
        }


        }

        export default tryOnListForm
