import React, {useEffect, useState} from "react";
import jwt_decode from "jwt-decode";
import {getUser} from "../../functions";
// import empty from "firebase/empty-import";
import Geocode from "react-geocode";

const Footer = (props) => {

    const [givenURL , setgivenURL] = useState();
    const [checkToken , setcheckToken] = useState();
    const [addressMap , setaddressMap] = useState();
    const [address2Map , setaddress2Map] = useState();
    const [citystateMap , setcitystateMap] = useState();
    const [fullAddress , setfullAddress] = useState();
    const [MapURL , setMapURL] = useState();
    const [PhoneNo , setPhoneNo] = useState();
    //const [lat , setlat] = useState();
    //const [long , setlong] = useState();

    useEffect(() => {

        Geocode.setApiKey("AIzaSyC5axnAzEvPyJ8dLCSMMveliaEeXwGVvmQ");
        Geocode.setLanguage("en");
        Geocode.setRegion("us");

        // banner_bottom_heading: "null"
        // banner_middle_heading: "null"
        // banner_top_heading: "null"
        // brand_color: "#303f9f"
        // brief_bio: "iupiogufguspfioguidfgupfioguipodfgudpfiogupioudfiogusdpfiogusdpfioguiodf"
        // city: "Modi magna ex amet"
        // practice_address: "Velit consequatur al"
        // practice_address2: "Placeat velit recus"
        // practice_phone_number: "1112223333"
        // practice_website_url: "https://www.hatozywe.biz"
        // state: "UT"
        // tryOnRequest_Email: "yasinmaknojia@gmail.com"


        var given_url = props.data.practice_website_url;
        var regex = /^(ftp|http|https):\/\/[^ "]+$/;
        if(regex.test(given_url)){
            setgivenURL(given_url);
        }else{
            var new_url = "https://"+given_url;
            setgivenURL(new_url);
        }

        if(props.data.practice_phone_number != undefined){
            var phone_regex = /^\d{3}-\d{3}-\d{4}$/;
            if(phone_regex.test(props.data.practice_phone_number)){
                setPhoneNo(props.data.practice_phone_number);
            }else{

                 var formatted = props.data.practice_phone_number.replace(/(\d{1,2})(\d{1})?(\d{1,3})?(\d{1,4})?/, function(_, p1, p2, p3, p4){
                     let output = "";
                     if (p1) output = `${p1}`;
                     if (p2) output += `${p2}`;
                     if (p3) output += `-${p3}`;
                     if (p4) output += `-${p4}`;
                     return output;
                 });

                setPhoneNo(formatted);
            }
        }





        // console.log(givenURL);
        setaddressMap( props.data.practice_address );
        setaddress2Map( props.data.practice_address2 );
        setcitystateMap( props.data.city + "," + props.data.state);
        setfullAddress ( addressMap , address2Map , citystateMap );

        Geocode.fromAddress(fullAddress).then(
            response => {
                const { lat, lng } = response.results[0].geometry.location;

                Geocode.fromLatLng(lat, lng).then(
                    response => { const address = response.results[0].formatted_address;
                        setMapURL(address);
                    },
                    error => {
                        console.error(error);
                    }
                );

            },
            error => {
                console.error(error);
            }
        );


        // var token = localStorage.getItem('usertoken');
        // setcheckToken(token);
        // const decoded = jwt_decode(token);
        // getUser(decoded._id).then(res => {
        //     setaddressMap( res.data.data[0].practice_address );
        //     setaddress2Map( res.data.data[0].practice_address2 );
        //     setcitystateMap( res.data.data[0].city + "," + res.data.data[0].state);
        //     setfullAddress ( addressMap , address2Map , citystateMap );
        // });


    })

    // const numberFormatted = (val) =>{
    //
    //     return formatted;
    // }

    return (
        <div className="container px-0">
            <footer className="footer ml-0 px-0 border-0">
                <div className="container footer-top-container">

                    <div className="row">

                        <div className="col-md-4 col-lg-3">
                            <p className="footer-heading" style={{color:props.footer_font_heading_color}}>Our Eyewear</p>

                            <ul className="custom-list">
                                <li className="custom-list-items"><a href={`${process.env.PUBLIC_URL}/`+props.url+`/eyeglasses/mens`}>Men's Eyeglasses</a></li>
                                <li className="custom-list-items"><a href={`${process.env.PUBLIC_URL}/`+props.url+`/eyeglasses/womens`}>Women's Eyeglasses</a></li>
                                <li className="custom-list-items"><a href={`${process.env.PUBLIC_URL}/`+props.url+`/sunglasses/mens`}>Men's Sunglasses</a></li>
                                <li className="custom-list-items"><a href={`${process.env.PUBLIC_URL}/`+props.url+`/sunglasses/womens`}>Women's Sunglasses</a></li>
                            </ul>

                        </div>

                        <div className="col-md-4 col-lg-3">
                            <p className="footer-heading"  style={{color:props.footer_font_heading_color}}>Contact us</p>

                            <div className="footer-col-details mb-4">
                                <p className="m-0 f-w-700">Email:</p>
                                <p className="m-0">
                                    <a className="mailto-footer" href={`mailto:${props.email}`}>{props.email}</a>
                                </p>
                            </div>

                            <div className="footer-col-details">
                                <p className="m-0 f-w-700">Phone:</p>
                                <p className="m-0"><a  style={{color:'#606060'}} href={'tel:' + PhoneNo}>{PhoneNo}</a></p>
                            </div>

                        </div>
                        <div className="col-md-4 col-lg-3">
                            <p className="footer-heading"  style={{color:props.footer_font_heading_color}}>Visit Us</p>

                            <div className="footer-col-details">
                                <p className="mb-4">

                                    <div>
                                        <span className="d-block">{ addressMap ? addressMap  : ""  }</span>
                                        <span className="d-block">{ address2Map ? address2Map : "" }</span>
                                        <span className="d-block">{ citystateMap ? citystateMap : ""}</span>
                                    </div>

                                    {/*{ checkToken != null ?*/}
                                    {/*    ( <div>*/}
                                    {/*        <span className="d-block">{ addressMap ? addressMap  : ""  }</span>*/}
                                    {/*        <span className="d-block">{ address2Map ? address2Map : "" }</span>*/}
                                    {/*        <span className="d-block">{ citystateMap ? citystateMap : ""}</span>*/}
                                    {/*    </div> )*/}
                                    {/*: ""}*/}
                                    {/*10835 700 E <br/> Sandy, UT 84070 <br/>*/}
                                    {/*<a href={`${process.env.PUBLIC_URL}/`+props.url+`/about`} className="map-link-footer">Map Us</a>*/}
                                    {/*<a href="https://www.google.com/maps/place/10835+700+E,+Sandy,+UT+84070,+USA/@40.5543291,-111.8740919,17z/data=!3m1!4b1!4m5!3m4!1s0x8752879087c1c62b:0xb4a61c3c168888a4!8m2!3d40.554325!4d-111.8719032" target="_blank" className="map-link-footer">Map Us</a>*/}
                                    <a href={`https://www.google.com/maps/place/`+MapURL} target="_blank" className="map-link-footer">Map Us</a>
                                </p>
                                <a href={givenURL} target="_blank" className={'mailto-footer'}>
                                   {props.data.practice_website_url}
                                </a>
                            </div>

                        </div>
                        <div className="col-md-4 col-lg-3">
                            <p className="footer-heading"  style={{color:props.footer_font_heading_color}}>About Us</p>
                            {/*display: -webkit-box;*/}
                            {/*-webkit-line-clamp: 3;*/}
                            {/*-webkit-box-orient: vertical;*/}
                            {/*overflow: hidden;*/}

                            <div className="footer-col-details">
                                <p style={{display:"-webkit-box",WebkitLineClamp:'5',WebkitBoxOrient:'vertical',overflow:'hidden'}}>{props.data.brief_bio}</p>
                            </div>

                        </div>

                    </div>
                </div>
                <div className="container border-top-1 footer-sub-container">
                    <div className="row">
                        <div className="col-md-6 footer-copyright">
                            {/*<p className="mb-0">Copyright 2020 © Invision Frames Gallery.</p>*/}
                            <ul className="list-inline">
                                <li className="list-inline-item">
                                    <a className="subFooter-text-color" href={`${process.env.PUBLIC_URL}/`+props.url+"/termConditions"}>Terms & Conditions</a>
                                </li>
                                <li className="list-inline-item">
                                    <span className="vertical-line"></span>
                                </li>
                                <li className="list-inline-item">
                                    <a className="subFooter-text-color"  href={`${process.env.PUBLIC_URL}/`+props.url+"/privacy"}>
                                        Privacy Policy
                                    </a>
                                </li>
                                <li className="list-inline-item">
                                    <span className="vertical-line"></span>
                                </li>

                                { checkToken != null ?
                                    <li className="list-inline-item">
                                        <a className="subFooter-text-color" href={`${process.env.PUBLIC_URL}/`}>
                                            Admin Dashboard
                                        </a>
                                    </li>
                                : ""}

                            </ul>

                        </div>
                        <div className="col-md-6">
                            <p className="pull-right mb-0 subFooter-text-color">
                                Made with
                                <i className="fa fa-heart m-r-5"></i>
                                <a className="subFooter-text-color" href="http://www.eyecandi.io" target="_blank"> EyeCandi Team </a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
};

export default Footer;
