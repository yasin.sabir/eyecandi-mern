import React, {useState, Fragment, useEffect} from 'react';
import Language from './language';
import UserMenu from './userMenu';
import Notification from './notification';
import SearchHeader from './searchHeader';
import {Link} from 'react-router-dom';
import {AlignLeft, Maximize, Bell, MessageCircle, MoreHorizontal} from 'react-feather';
import logo from '../../../../../assets/images/logo/Logo-c@2x.png';

const Header = (props) => {
    const [sidebar, setSidebar] = useState(false);
    const [rightSidebar, setRightSidebar] = useState(true);
    const [headerbar, setHeaderbar] = useState(true);
    const [count, setCount] = useState(0);
    const [givenURL , setgivenURL] = useState();
    const [checkToken , setcheckToken] = useState();
    const [Profile_logo , setProfile_logo] = useState();

    useEffect(() => {
        if (localStorage.getItem("favorite") !== null) {
            var whislist = localStorage.getItem("favorite");
            whislist = JSON.parse(whislist);
            setCount(whislist.length);
        }


        if(props.data.profile_pic){
            setProfile_logo(props.data.profile_pic);
        }else {
            setProfile_logo(logo);
        }


        var token = localStorage.getItem('usertoken');
        setcheckToken(token);

        var given_url = props.data.practice_website_url;
        var regex = /^(ftp|http|https):\/\/[^ "]+$/;
        if(regex.test(given_url)){
            setgivenURL(given_url);
        }else{
            var new_url = "https://"+given_url;
            setgivenURL(new_url);
        }


    });



    const openCloseSidebar = () => {

        if (sidebar) {
            setSidebar(!sidebar);
            document.querySelector(".page-main-header").classList.add('open');
            document.querySelector(".page-sidebar").classList.add('open');
        } else {
            setSidebar(!sidebar);
            document.querySelector(".page-main-header").classList.remove('open');
            document.querySelector(".page-sidebar").classList.remove('open');
        }

        // if (sidebar) {
        //     setSidebar(!sidebar);
        //     document.querySelector(".page-main-header").classList.remove('open');
        //     document.querySelector(".page-sidebar").classList.remove('open');
        // } else {
        //     setSidebar(!sidebar);
        //     document.querySelector(".page-main-header").classList.add('open');
        //     document.querySelector(".page-sidebar").classList.add('open');
        // }
    }

    function showRightSidebar() {
        if (rightSidebar) {
            setRightSidebar(!rightSidebar)
            document.querySelector(".right-sidebar").classList.add('show');
        } else {
            setRightSidebar(!rightSidebar)
            document.querySelector(".right-sidebar").classList.remove('show');
        }
    }

    //full screen function
    function goFull() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }
    const color = props.color;
    // console.log("Header Name :"+props.name);

    const logo_style = {
        width: '200px'
    }

    const profile_logo = {
        width : '65px',
        height : '65px',
        borderRadius : '100%'
    }

    /*
    *  mr-0 custom-logo-setting from img-fluid logo
    *
    */


    return (
        <Fragment>
            <div className="page-main-header open">
                <div className="main-header-right row pl-3 pr-3">
                    <div className="main-header-left d-lg-none">
                        <div className="logo-wrapper pr-4">
                            <Link to={`${process.env.PUBLIC_URL}/`+props.url}>
                                {/*<img className="img-fluid" src={logo} alt="" style={logo_style} />*/}
                                <img className="img-fluid" src={Profile_logo} alt="" style={ props.data.profile_pic ? profile_logo : logo_style  } />
                            </Link>
                            <div className="site-title-section">
                                <h4 className="site-title" style={{color:props.color,textTransform: 'capitalize'}}>
                                    {props.name}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div className="mobile-sidebar d-block">
                        <div className="media-body text-right switch-sm">
                            <label className="switch">
                                <a onClick={() => openCloseSidebar()} >
                                    <AlignLeft/>
                                </a>
                            </label>
                        </div>
                    </div>

                    <div className="nav-left col p-0 d-none d-md-none d-lg-block">
                        <ul className={`custom-h-menu-list nav-menus ${headerbar ? '' : 'open'}`}>
                            <li className="pl-4 pr-3 pt-2 pb-2 onhover-dropdown">
                                <a href="#javascript" className="custom-h-menu-list-item">Eyeglasses</a>
                                <ul className="onhover-show-div">
                                    <li>
                                        <a href={`${process.env.PUBLIC_URL}/`+props.url+`/eyeglasses/mens`}>Men's Collection</a>
                                    </li>
                                    <li>
                                        <a href={`${process.env.PUBLIC_URL}/`+props.url+`/eyeglasses/womens`}>Women's Collection</a>
                                    </li>
                                </ul>
                            </li>
                            <li className="pl-3 pr-3 pt-2 pb-2 onhover-dropdown">
                                <a href="#javascript" className="custom-h-menu-list-item">Sunglasses</a>
                                <ul className="onhover-show-div">
                                    <li className="">
                                        <a href={`${process.env.PUBLIC_URL}/`+props.url+`/sunglasses/mens`}>Men's Collection</a>
                                    </li>
                                    <li className="">
                                        <a href={`${process.env.PUBLIC_URL}/`+props.url+`/sunglasses/womens`}>Women's Collection</a>
                                    </li>
                                </ul>
                            </li>
                            <li className="pl-3 pr-3 pt-2 pb-2">
                                <a href={`${process.env.PUBLIC_URL}/`+props.url+`/about`} className="custom-h-menu-list-item">About</a>
                            </li>
                            <li className="pl-3 pr-3 pt-2 pb-2">
                                {/*{props.data.practice_website_url}*/}
                                {

                                }
                                <a href={givenURL} target="_blank" className="custom-h-menu-list-item">Main Website</a>
                            </li>

                            { checkToken != null ?
                                <li className="pl-3 pr-3 pt-2 pb-2">
                                    <a href={`${process.env.PUBLIC_URL}/`} className="custom-h-menu-list-item">Admin</a>
                                </li>
                            : '' }


                            <li className="onhover-dropdown">
                                {/*<a className="txt-dark" href="#javascript">*/}
                                {/*  <h6>EN</h6></a>*/}
                                <Language />
                            </li>
                            <li className="onhover-dropdown">
                                {/*<Notification />*/}
                                {/*<Bell />*/}
                                {/*<span className="dot"></span>*/}
                                {/*<Notification />*/}
                            </li>
                            <li>
                                {/*<a onClick={showRightSidebar}>*/}
                                {/*  <MessageCircle />*/}
                                {/*  <span className="dot"></span>*/}
                                {/*</a>*/}
                            </li>
                            {/*<UserMenu />*/}
                        </ul>
                        <div className="d-lg-none mobile-toggle pull-right" onClick={() => setHeaderbar(!headerbar)}>
                            <MoreHorizontal/></div>
                    </div>
                    <div className="nav-right col p-0 d-sm-none d-lg-block">
                        <ul className={`custom-h-menu-list nav-menus ${headerbar ? '' : 'open'}`}>
                            <li className="pl-1 pr-1 pt-2 pb-2 mr-3">
                                <a href={`${process.env.PUBLIC_URL}/`+props.url+`/my-try-on-list`} className="custom-h-menu-list-item">

                                    { count > 0 ?
                                        <i className="fa fa-heart mr-2" style={{color: '#EF0000'}}></i>
                                        :
                                        <i className="fa fa-heart-o heart-outline-red mr-2"></i>
                                    }
                                    {/*<i className="fa fa-heart-o heart-outline-red mr-2"></i>*/}
                                    My Try-On List
                                    <span className="ml-1 small-try-out">({count} of 5)</span>


                                </a>
                            </li>
                            <li className="pl-1 pr-1 pt-2 pb-2">
                                <a href={`${process.env.PUBLIC_URL}/`+props.url+`/book-eye-exam`} style={{backgroundColor:color}} className="cus-btn cus-btn-primary text-white">Book an Exam</a>
                                {/*<a href="#" className="cus-btn btn-primary text-white">Book an Exam</a>*/}
                            </li>
                        </ul>
                        {/*<div className="d-lg-none mobile-toggle pull-right" onClick={() => setHeaderbar(!headerbar)}>*/}
                        {/*    <MoreHorizontal/></div>*/}
                    </div>

                    <script id="result-template" type="text/x-handlebars-template">
                        <div className="ProfileCard u-cf">
                            <div className="ProfileCard-avatar">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                                     strokeLinejoin="round" className="feather feather-airplay m-0">
                                    <path
                                        d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1">
                                    </path>
                                    <polygon points="12 15 17 21 7 21 12 15"></polygon>
                                </svg>
                            </div>
                            <div className="ProfileCard-details">
                                <div className="ProfileCard-realName"></div>
                            </div>
                        </div>
                    </script>
                    <script id="empty-template" type="text/x-handlebars-template">
                        <div className="EmptyMessage">Your search turned up 0 results. This most likely means the
                            backend is down, yikes!
                        </div>
                    </script>
                </div>
            </div>
        </Fragment>
    )
};


export default Header;
