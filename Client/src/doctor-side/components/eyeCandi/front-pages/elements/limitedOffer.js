import React, {Component, Fragment} from 'react';

class LimitedOffer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            color : '#000'
        }
    }

    render() {
        return (
            <Fragment>
                <div className="container">
                    <div className="jumbotron-section text-center m-t-30 m-b-40">
                        <div className="color-overlay" style={{
                            background: this.props.color
                        }}></div>
                        <div className="row" style={{paddingTop:'0px'}}>
                            <div className="col-lg-12">
                                <p className="jumbotron-para f-w-600">
                                    { this.props.top_heading != 'null' ? this.props.top_heading : 'Life is too short to wear ugly glasses!'}
                                </p>
                                <h3 className="fv-h3 text-white mt-3">
                                    { this.props.middle_heading != 'null' ? this.props.middle_heading : 'Create your favorites list and request a personalized fitting or eye exam at our office.'}
                                </h3>
                                {/*<p className="jumbotron-para mt-3">*/}
                                {/*    {this.props.bottom_heading}*/}
                                {/*</p>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }


}

export default LimitedOffer
