import React, {Component, Fragment} from 'react';
import img from "../../../../assets/images/New-Design/mens/iStock-1178540754.png";
import mimg from "../../../../assets/images/New-Design/mens-eyeglasses.jpg";
import mimg_sunGlasses from "../../../../assets/images/New-Design/mens-sunglasses.jpg";
import wimg from "../../../../assets/images/New-Design/womens/iStock-1136855170.png";
import wimg_sunGlasses from "../../../../assets/images/New-Design/womens/womens-sunglasses.jpg";

class Hero extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        // background-repeat: no-repeat;
        // background-position: 87% 28%;
        // background-size: 100% 185%;
        // -515px -983px
        var style = {
            hero:{
                background: 'linear-gradient(to left, transparent, ' + this.props.color + ' ),' +
                    'url(' + mimg + ')',
                height: '386px'
            },
            heroM_sunGlasses:{
                background: 'linear-gradient(to left, transparent, ' + this.props.color + ' ),' +
                    'url(' + mimg_sunGlasses + ') ',
                height: '386px'
            },
            heroW:{
                background: 'linear-gradient(to left, transparent, ' + this.props.color + ' ),' +
                    'url(' + wimg + ')',
                height: '386px'
            },
            heroW_sunGlasses:{
                background: 'linear-gradient(to left, transparent, ' + this.props.color + ' ),' +
                    'url(' + wimg_sunGlasses + ')',
                height: '386px',
            },
            container:{
                //paddingTop:'110px'
            }
        }
        return (
            <Fragment>
                {/*// style={ this.props.hero_class == 'hero-section-w-sunglasses' ? style.heroW_sunGlasses: this.props.hero_class == 'hero-section-w' ? style.heroW: style.hero }>*/}
                <div className="container-fluid pl-0 pr-0">
                    <div className={`${this.props.hero_class} hero-p`}
                         style={ this.props.hero_class == 'hero-section-w-sunglasses' ? style.heroW_sunGlasses: this.props.hero_class == 'hero-section-w' ? style.heroW : this.props.hero_class == 'hero-section-m-sunglasses' ? style.heroM_sunGlasses: style.hero  } >
                         {/*style={ this.props.hero_class == 'hero-section-m-sunglasses' ? style.heroM_sunGlasses: style.hero } >*/}
                         <div className="container" style={style.container}>
                            <div className="row">
                                <div className="col-lg-9 text-left">
                                    <h1 className="fv-h1 text-white hero-title">{this.props.title}</h1>
                                    <h1 className="fv-h1 text-white">{this.props.subtitle}</h1>
                                    <div className="hero-description mt-3">
                                        <p>
                                            <a href={`${process.env.PUBLIC_URL}/`+this.props.url+`/book-eye-exam`} className="cus-btn cus-btn-white text-black f-w-700 mt-2">Need an Exam?</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }


}

export default Hero
