import React, {Component, Fragment} from 'react';
import img from '../../../../assets/images/New-Design/iStock-1039303884.png';
import img2 from '../../../../assets/images/New-Design/home-billboard.jpg';

class Hero_Home extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Fragment>
                <div className="container-fluid pl-0 pr-0">
                    <div className="hero-section hero-p" style={{
                        background: 'linear-gradient(to left, transparent, ' + this.props.color + ' ),' +
                            'url(' + img2 + ')',
                        height: '600px',
                    }}>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-9 text-left">
                                    <h2 className="fv-h2 text-white">
                                        {/*Find your favorite frames and request to try them on.*/}
                                        Real frames for real <br/> people in one place.
                                    </h2>
                                    <div className="hero-description mt-3">
                                        <p className="text-white">
                                            Browse our collection of eyeglasses and sunglasses in one place and
                                            choose
                                            up to five of your favorites to request an in person try-on in our
                                            store.
                                        </p>
                                        <p>
                                            <a href={this.props.data.practice_website_url} className="cus-btn cus-btn-white text-black f-w-700 mt-2">Return
                                                To Main Website</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )

    }


}

export default Hero_Home
