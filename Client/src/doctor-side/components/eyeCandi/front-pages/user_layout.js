import React, {Component, Fragment} from 'react';
import Header from '../components/common/header-component/header';
import Footer from "../components/common/footer";
import Sidebar from "../components/common/sidebar-component/sidebar";
import RightSidebar from "../components/common/right-sidebar";
import {ToastContainer} from "react-toastify";

function User_layout (props) {
        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header/>
                        <Sidebar/>
                        <RightSidebar/>
                            <div className="page-body custom-public-body-color pl-0 pr-0 m-t-50 p-b-10">
                                {props.layout}
                            </div>
                        <Footer/>
                    </div>
                </div>
                <ToastContainer/>
            </Fragment>
        );

}

export default User_layout
