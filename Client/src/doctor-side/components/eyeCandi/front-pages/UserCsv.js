import React, {Component, Fragment} from 'react';
import {getCsv} from "../functions";
import CsvDownload from 'react-json-to-csv';
import logo from "../../../assets/images/eyeCandi/logo-ss.png";
import Modal from 'react-bootstrap/Modal';

class userCsv extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: '',
            show: true,
            password: '',
            authentication_error: false,
            authentication_error_msg: 'Wrong Password'
        }
    }

    componentDidMount() {
        getCsv().then(res => {
            this.setState({
                data: res.data,

            });
        })
    }


    handleOnChangePassword = (e) => {
        this.setState({
            password: e.target.value,
        });
    };

    onSubmit = () => {
        if(this.state.password === 'joshchild123!'){
            this.setState({
                show : false
            })
        }else{
            this.setState({
                password: '',
                authentication_error: true,
            })
        }
    }

    onExit = () => {
        if(this.state.password !== 'joshchild123!'){
            this.setState({
                show : true
            })
        }
    }

    render() {
        return (

            <div className="container">
                <Modal
                    show={this.state.show}
                    onHide={() => this.setState({
                        show: false
                    })}
                    onExit={this.onExit}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                    style={{top:'20%'}}
                >
                    <Modal.Header>
                        <p className="h4 txt-info text-center col-md-12">
                            Enter Password For Super Admin
                        </p>
                    </Modal.Header>
                    <Modal.Body>
                        <div className={'container'}>
                            <div className={'row'}>
                                <label className="col-form-label pt-0 col-md-6 offset-3 text-center">Password</label>
                                <input className="col-form-label pt-0 col-md-6 offset-3 text-center" placeholder={'Enter Password'} type="password" name="password" required=""
                                       value={this.state.password} onChange={this.handleOnChangePassword}/>
                                {
                                    this.state.authentication_error ?
                                        <span className="txt-danger col-md-6 offset-3 text-center">{this.state.authentication_error_msg}</span> : ""
                                }
                                <div className="form-group mt-3 mb-0 col-md-6 offset-md-3">
                                    <button className="btn btn-primary btn-block custom-btn-color text-center" type="submit"
                                            onClick={this.onSubmit}>Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
                <div className="row">
                    <div className="col-lg-3"></div>
                    <div className="col-lg-6 mb-5">
                        <div className="text-center"><img src={logo} alt=""/></div>
                        <p className="h4 txt-info mt-5 text-center">
                            Here is the data for all user registered in our system
                        </p>
                        <p className="h4 txt-info mb-4 text-center">
                            (Click on Button to Download the csv file)
                        </p>

                        <div className="row">
                            <div className="col-sm-6 col-md-6 offset-md-3 text-center">
                                <CsvDownload data={this.state.data} filename={'Users.csv'}
                                             className={'btn btn-secondary text-center'}/>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default userCsv;
