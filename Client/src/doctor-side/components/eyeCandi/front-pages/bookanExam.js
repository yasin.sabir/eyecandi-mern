import React, {Component, Fragment} from 'react';
import Header from './common/header-component/header';
import Footer from "./common/footer";
import Sidebar from "./common/sidebar-component/sidebar";
import RightSidebar from "./common/right-sidebar";
import LimitedOffer from "./elements/limitedOffer";
import SimpleReactValidator from 'simple-react-validator';
import logo from "../../../assets/images/eyeCandi/logo/logo-circle.svg";
import {getURL, sendEmail_bookExam, sendEmail_tryOnRequest} from "../functions";
import { Email, Item, Span, A, renderEmail } from 'react-html-email';




class BookExam extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tester_name:"",
            tester_email:"",
            tester_phone:"",
            tester_ques1:"",
            tester_ques2:"",
            banner_top_heading: '',
            banner_middle_heading : '',
            banner_bottom_heading : '',
            mail_subject:"Book An Exam",
            mail_body: 'Book an Exam for following Patient',
            public_url : this.props.match.params.publicUrl,
            color: '#00000065',
            footer_font_heading_color: '#0C4C60',
            sidebar_color:'#0C4C60',
            logo: logo,
            data: '',
            email: '',
            name: '',
            bookanExam_thankyouNote: ' We are getting your frames ready. We will gather your frames and contact you once we have ' +
                '                                            them ready to come in and try them on. If there is an issue with any of your frame picks,\n' +
                '                                            we will contact you. Feel free to contact us or to setup an eye exam while you wait.\n' +
                '                                            Needs to say something like: Thank you for submitting your request.\n' +
                '                                            A team member will be contacting you shortly.'
        };

        this.validator = new SimpleReactValidator({
            validators: {
                phone: {  // name the rule
                    message: 'Enter valid phone no!',
                    rule: (val, params, validator) => {
                        return validator.helpers.testRegex(val,/^\d{3}-*\d{3}-*\d{4}$/i) && params.indexOf(val) === -1
                    },
                }
            }
        });
    }

    handleChange = input => (e) => {
        this.setState({[input]: e.target.value})
    };

    onSubmit = (e) => {
        e.preventDefault();
        if (this.validator.allValid()) {

            const emailHTML = renderEmail(
                <Email title={this.state.mail_body}>
                    <Item align="left">
                        <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginBottom:'5px'}}>
                            Patient Name:
                        </Span>
                        <Span fontSize={16} >
                            {this.state.tester_name}
                        </Span>
                    </Item>
                    <Item align="left">
                    <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                            Patient Email:
                        </Span>
                        <Span fontSize={16} >
                            {this.state.tester_email}
                        </Span>
                    </Item>
                    <Item align="left">
                    <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                            Patient Phone:
                        </Span>
                        <Span fontSize={16} >
                            {this.state.tester_phone}
                        </Span>
                    </Item>
                    <Item align="left">
                    <Span fontSize={16} fontWeight={'bold'} style={{display:'block',marginTop:'25px',marginBottom:'5px'}}>
                            Main Concern:
                        </Span>
                        <Span fontSize={16} >
                            {this.state.tester_ques1}
                        </Span>
                    </Item>
                </Email>
            );
            const mail_data = {
                email_to : this.state.data.tryOnRequest_Email,
                email_subject : this.state.mail_subject,
                email_body : this.state.mail_body,
                email_body_frame_data : emailHTML
            };
            // console.log(emailHTML);
            sendEmail_bookExam( mail_data ).then((res) =>{
                // console.log(res,'sf');
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/${this.state.public_url}/ThankYouBookanExam`,
                    state : {BookanExam_Note : this.state.bookanExam_thankyouNote}
                });
            });
        }else{
            this.validator.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }
    };

    componentDidMount() {
        window.scrollTo(0, 0);
        getURL(this.state.public_url).then((res) => {
            if (res.data.error) {
                this.props.history.push('/404');
            } else {
                this.setState({
                    color: res.data.data[0].brand_color,
                    logo:res.data.data[0].profile_pic,
                    data:res.data.data[0],
                    email:res.data.email,
                    name: res.data.practice_name,
                    banner_top_heading: res.data.data[0].banner_top_heading,
                    banner_middle_heading : res.data.data[0].banner_middle_heading,
                    banner_bottom_heading : res.data.data[0].banner_bottom_heading,
                })
            }
        })
    };

    // pl-0 pr-0 m-t-50 p-b-10

    render() {
        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header logo={this.state.logo} color={this.state.color} url={this.state.public_url} name={this.state.name} data={this.state.data}/>
                        <Sidebar sidebar_color={this.state.sidebar_color} url={this.state.public_url} />
                        <RightSidebar/>

                        <div className="page-body custom-public-body-color m-t-50 p-t-50">

                            <div className="container mt-4 m-b-50">

                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <div className="bookanExam-note">
                                            <h4 className="fv-h4 f-w-900">Book an Eye Exam</h4>
                                            <p className="sub-para-heading">
                                                Has it been a while since you've had a comprehensive eye exam? <br/>
                                                No worries, we can help you.
                                                {/*Book an Eye Exam Complete the form below to request an eye exam.*/}
                                                {/*<br/>*/}
                                                {/*An eye care expert will contact you soon.*/}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="row m-t-30">
                                    <div className="col-md-3 col-lg-4"></div>
                                    <div className="col-md-6 col-lg-4">
                                        <form action="">

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">Your Name</label>
                                                <input className="form-control"
                                                       name="tester_name"
                                                       id="tester_name"
                                                       type="text"
                                                       onChange={this.handleChange('tester_name')}
                                                />
                                                {this.validator.message('tester_name', this.state.tester_name, 'required', )}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">Your Email</label>
                                                <input className="form-control"
                                                       name="tester_email"
                                                       id="tester_email"
                                                       type="text"
                                                       onChange={this.handleChange('tester_email')}
                                                />
                                                {this.validator.message('tester_email', this.state.tester_email, 'required|email', )}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">Your Phone</label>
                                                <input className="form-control"
                                                       name="tester_phone"
                                                       id="tester_phone"
                                                       type="text"
                                                       onChange={this.handleChange('tester_phone')}
                                                />
                                                {this.validator.message('tester_phone', this.state.tester_phone, 'required|phone:111-111-1111', )}

                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlInput1" className="f-w-700">What are your main concerns?</label>
                                                <textarea className="form-control"
                                                       name="tester_ques1"
                                                          rows="5"

                                                       id="tester_ques1"
                                                       onChange={this.handleChange('tester_ques1')}
                                                />
                                                {this.validator.message('tester_ques1', this.state.tester_ques1, 'required', )}

                                            </div>

                                            <div className="text-center mt-4">
                                                <input type="submit" onClick={this.onSubmit} style={{backgroundColor:this.state.color}} className="cus-btn cus-btn-primary text-white" value="Submit Your Information"/>
                                                <p className="mt-3">Your privacy is <a href="#" className="custom-a-color">our policy</a></p>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-md-3 col-lg-4"></div>
                                </div>
                            </div>

                            <LimitedOffer color={this.state.color}  top_heading={this.state.banner_top_heading} middle_heading={this.state.banner_middle_heading} bottom_heading={this.state.banner_bottom_heading}/>

                            <Footer footer_font_heading_color={this.state.footer_font_heading_color} url={this.state.public_url}
                                    data={this.state.data} email={this.state.email}
                            />
                        </div>

                    </div>
                </div>


            </Fragment>
    )
    }


    }

    export default BookExam
