import React, {Component, Fragment} from 'react';
import Header from './common/header-component/header';
import Footer from "./common/footer";
import Sidebar from "./common/sidebar-component/sidebar";
import RightSidebar from "./common/right-sidebar";
import LimitedOffer from "./elements/limitedOffer";
import Hero_Home from "./elements/hero-home";
import _mens_bg from '../../../assets/images/New-Design/iStock-1010658416.png';
import _womens_bg from '../../../assets/images/New-Design/iStock-1174988209.png';
import img from "../../../assets/images/New-Design/iStock-1039303884.png";
import img2 from "../../../assets/images/New-Design/home-billboard.jpg";
import logo from '../../../assets/images/eyeCandi/logo/logo-circle.svg';
import {getURL} from '../functions';
import jwt_decode from "jwt-decode";


class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            public_url: this.props.match.params.publicUrl,
            color: '#00000065',
            footer_font_heading_color: '#0C4C60',
            brands: 0,
            femaleCount: 0,
            maleCount: 0,
            sidebar_color:'#0C4C60',
            logo: logo,
            data: '',
            email: '',
            banner_top_heading: '',
            banner_middle_heading: '',
            banner_bottom_heading: ''

        }

    }

    componentDidMount() {
        window.scrollTo(0, 0);
        getURL(this.state.public_url).then((res) => {
            if (res.data.error) {
                this.props.history.push('/404');
            } else {
                console.log(res);
                this.setState({
                    name: res.data.practice_name,
                    color: res.data.data[0].brand_color,
                    logo: res.data.data[0].profile_pic,
                    brands: res.data.productdata[0].filterbrands.length,
                    femaleCount: res.data.productdata[0].FemaleCount,
                    maleCount: res.data.productdata[0].MaleCount,
                    data:res.data.data[0],
                    email:res.data.email,
                    banner_top_heading: res.data.data[0].banner_top_heading,
                    banner_middle_heading: res.data.data[0].banner_middle_heading,
                    banner_bottom_heading: res.data.data[0].banner_bottom_heading
                })
            }
        })
    }

    render() {

        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header logo={this.state.logo} color={this.state.color} url={this.state.public_url} name={this.state.name} data={this.state.data} />
                        <Sidebar sidebar_color={this.state.sidebar_color} url={this.state.public_url} />
                        <RightSidebar/>
                        {/*style={{paddingTop: '30px'}}*/}
                        <div className="page-body custom-public-body-color pl-0 pr-0 m-t-50 p-b-10" >
                            <Hero_Home color={this.state.color} data={this.state.data}/>

                            <div className="container">
                                <div className="row m-t-30">
                                    <div className="col-lg-6">
                                        <div className="image-box image-box mb-4 mb-sm-4 mb-md-4 mb-lg-0" style={{
                                            background: 'linear-gradient(to left, transparent, ' + this.state.color + ' ),' +
                                                'url(' + _womens_bg + ')',
                                            padding:'40px'

                                        }}>

                                            <h2 className="text-white">Women's <br/> Collection</h2>
                                            <p className="image-box-sub-detail mb-2" style={{color: '#fff'}}>
                                                {this.state.brands} Brands & {this.state.femaleCount} Frames
                                            </p>


                                            <div className="image-box-btns">
                                                <a href={`${this.state.public_url}/eyeglasses/womens`} className="cus-btn cus-btn-white text-black f-w-700">Browse Eyeglasses</a>
                                                <br/>
                                                <a href={`${this.state.public_url}/sunglasses/womens`}
                                                   className="cus-btn btn-outline-light-2x text-black f-w-700 mt-3">Browse
                                                    Sunglasses</a>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="image-box" style={{
                                            background: 'linear-gradient(to left, transparent, ' + this.state.color + ' ),' +
                                                'url(' + _mens_bg + ')',
                                            padding:'40px'
                                        }}>

                                            <h2 className="text-white">Men's <br/> Collection</h2>
                                            <p className="image-box-sub-detail mb-2" style={{color: '#fff'}}>
                                                {this.state.brands} Brands & {this.state.maleCount} Frames
                                            </p>

                                            <div className="image-box-btns">
                                                <a href={`${this.state.public_url}/eyeglasses/mens`} className="cus-btn cus-btn-white text-black f-w-700">Browse Eyeglasses</a>
                                                <br/>
                                                <a href={`${this.state.public_url}/sunglasses/mens`}
                                                   className="cus-btn btn-outline-light-2x text-black f-w-700 mt-3">Browse
                                                    Sunglasses</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <LimitedOffer color={this.state.color} top_heading={this.state.banner_top_heading} middle_heading={this.state.banner_middle_heading} bottom_heading={this.state.banner_bottom_heading}/>

                            <Footer footer_font_heading_color={this.state.footer_font_heading_color} url={this.state.public_url} data={this.state.data} email={this.state.email} phone={this.state.data} />

                        </div>

                    </div>
                </div>
            </Fragment>
        )
    }


}

export default Home
