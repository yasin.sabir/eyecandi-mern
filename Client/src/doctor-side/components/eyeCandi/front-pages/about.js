import React, {Component, Fragment} from 'react';
import Header from './common/header-component/header';
import Footer from "./common/footer";
import Sidebar from "./common/sidebar-component/sidebar";
import RightSidebar from "./common/right-sidebar";
import LimitedOffer from "./elements/limitedOffer";
import SimpleReactValidator from 'simple-react-validator';
import {getURL, getUser} from "../functions";
import logo from "../../../assets/images/eyeCandi/logo/logo-circle.svg";
import cover_image from "../../../assets/images/eyeCandi/iStock-banner.jpg";
import cover_image_ from "../../../assets/images/eyeCandi/about-hero.jpg";

import Geocode from "react-geocode";
import jwt_decode from "jwt-decode";

// import Map from './common/map';

class About extends Component {

    constructor(props) {
        super(props);
        this.state = {
            public_url: this.props.match.params.publicUrl,
            color: '#00000065',
            footer_font_heading_color: '#0C4C60',
            sidebar_color: '#0C4C60',
            logo: logo,
            name: '',
            email: '',
            data: '',
            banner_top_heading: '',
            banner_middle_heading: '',
            banner_bottom_heading: '',
            map_address: '',
            practice_address: '',
            practice_address2: '',
            city: '',
            state: '',
            full_address: '',
        }

    }

    componentDidMount() {
        window.scrollTo(0, 0);
        Geocode.setApiKey("AIzaSyC5axnAzEvPyJ8dLCSMMveliaEeXwGVvmQ");
        Geocode.setLanguage("en");
        Geocode.setRegion("us");

        var fullAddress = "";

        var token = localStorage.getItem('usertoken');
        const decoded = jwt_decode(token);
        getUser(decoded._id).then(res => {
            //console.log(res.data.data[0].practice_address + "," + res.data.data[0].city + "," + res.data.data[0].state);
            this.setState({
                practice_address: res.data.data[0].practice_address,
                practice_address2: res.data.data[0].practice_address2,
                city: res.data.data[0].city,
                state: res.data.data[0].state,
                fullAddress: res.data.data[0].practice_address + "," + res.data.data[0].practice_address2 + "," + res.data.data[0].city + "," + res.data.data[0].state
            });
            this.getAddress(res.data.data[0].practice_address + " " + res.data.data[0].city + " " + res.data.data[0].state);
        });


        // if(res){
        //     const stripeCustomerID = localStorage.getItem('stripeCustomerID');
        //     const stripeCustomerSubscriptionID = localStorage.getItem('stripeCustomerSubscriptionID');
        //     if(stripeCustomerID != 'undefined' && stripeCustomerSubscriptionID != 'undefined' && ){
        //         this.props.history.push('/');
        //     }else{
        //         this.props.history.push('/Subscription');
        //     }
        // }else{
        //     this.setState({
        //         authentication_error: true
        //     })
        // }

        // if(res.error){
        //     const stripeCustomerID = localStorage.getItem('stripeCustomerID');
        //     const stripeCustomerSubscriptionID = localStorage.getItem('stripeCustomerSubscriptionID');
        //     if(stripeCustomerID != 'undefined' && stripeCustomerSubscriptionID != 'undefined'){
        //         this.props.history.push('/');
        //     }else{
        //         this.props.history.push('/Subscription');
        //     }
        //
        // }else{
        //     this.setState({
        //         authentication_error: true
        //     })
        // }

    }

    getAddress = (address) => {

        Geocode.fromAddress( address ).then(
            response => {
                const { lat, lng } = response.results[0].geometry.location;
                console.log(lat +","+lng);

                Geocode.fromLatLng(lat, lng).then(
                    response => { const address = response.results[0].formatted_address;
                        this.setState({
                            map_address : address
                        });
                    },
                    error => {
                        console.error(error);
                    }
                );

            },
            error => {
                console.error(error);
            }
        );

    }

    componentWillMount() {
        getURL(this.state.public_url).then((res) => {
            if (res.data.error) {
                this.props.history.push('/404');
            } else {
                // console.log(res);

                this.setState({
                    color: res.data.data[0].brand_color,
                    logo: res.data.data[0].profile_pic,
                    banner_top_heading: res.data.data[0].banner_top_heading,
                    banner_middle_heading: res.data.data[0].banner_middle_heading,
                    banner_bottom_heading: res.data.data[0].banner_bottom_heading,
                    data: res.data.data[0],
                    email: res.data.email,
                    name: res.data.practice_name,
                    practice_address: res.data.data[0].practice_address,
                    practice_address2: res.data.data[0].practice_address2,
                    city: res.data.data[0].city,
                    state: res.data.data[0].state
                })

            }
        });


    }

    // pl-0 pr-0 m-t-50 p-b-10

    handleMapURL = (e) => {
        //console.log(this.state.map_address);
        window.open( "https://www.google.com/maps/place/"+this.state.map_address, '_blank');
        // window.location.href = "https://www.google.com/maps/place/"+;
    };


    render() {

        if (this.state.lat != '') {
            const latVal = parseFloat(this.state.lat);
            const lngVal = parseFloat(this.state.lng);
            var cord = {
                lat: latVal,
                lng: lngVal
            };
        }
        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header logo={this.state.logo} color={this.state.color} url={this.state.public_url}
                                name={this.state.name} data={this.state.data}/>
                        <Sidebar sidebar_color={this.state.sidebar_color} url={this.state.public_url}/>
                        <RightSidebar/>

                        <div className="page-body custom-public-body-color m-t-50 p-t-50">

                            <div className="container">
                                <div className="about-jumbotron-section text-center m-t-30 m-b-40" style={{
                                    backgroundImage: `url(${cover_image_})`
                                }}
                                >
                                    {/*<img className="img-fluid mr-0 custom-logo-setting" style={{borderRadius: '10px',width:'1110px',height:'400px'}} src={this.state.data.cover_pic} alt=""/>*/}
                                </div>
                            </div>

                            <div className="container m-b-50">
                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <div className="thank-you-note pt-0">
                                            <h4 className="fv-h4 f-w-900">About {this.state.name}</h4>
                                            <p className="sub-para-heading">
                                                {this.state.data.brief_bio}
                                            </p>

                                            <ul className="list-inline details-buttons m-t-35">
                                                <li className="list-inline-item">
                                                    <a href={'mailto:' + this.state.email}
                                                       style={{cursor : 'pointer'}}
                                                       className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0">
                                                        {/*<i className="icofont icofont-envelope-open custom-icon-color mr-2"></i>*/}
                                                        <i className="far fa-envelope-open-text custom-icon-color mr-2"></i>
                                                        Email Us
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href={'tel:' + this.state.data.practice_phone_number}
                                                       style={{cursor : 'pointer'}}
                                                       className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0">
                                                        <i className="far fa-mobile custom-icon-color mr-2"></i>
                                                        Call Us {this.state.data.practice_phone_number}
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    {/*<a href="https://www.google.com/maps/place/10835+700+E,+Sandy,+UT+84070,+USA/@40.5543291,-111.8740919,17z/data=!3m1!4b1!4m5!3m4!1s0x8752879087c1c62b:0xb4a61c3c168888a4!8m2!3d40.554325!4d-111.8719032"*/}
                                                    {/*map_address*/}
                                                    <a onClick={this.handleMapURL}
                                                       style={{cursor : 'pointer'}}
                                                       className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0">
                                                        <i className="far fa-map-signs custom-icon-color mr-2"></i>
                                                        Map Us
                                                    </a>
                                                </li>
                                            </ul>

                                            {/*<div className="map-section mt-5">*/}
                                            {/*    {this.state.lat != '' &&*/}
                                            {/*    <Map*/}
                                            {/*        google={this.props.google}*/}
                                            {/*        center={cord}*/}
                                            {/*        height='250px'*/}
                                            {/*        zoom={20}*/}

                                            {/*    />*/}
                                            {/*    }*/}
                                            {/*</div>*/}

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <LimitedOffer color={this.state.color} top_heading={this.state.banner_top_heading}
                                          middle_heading={this.state.banner_middle_heading}
                                          bottom_heading={this.state.banner_bottom_heading}/>

                            <Footer footer_font_heading_color={this.state.footer_font_heading_color}
                                    url={this.state.public_url} data={this.state.data} email={this.state.email}/>
                        </div>

                    </div>
                </div>


            </Fragment>
        )
    }


}

export default About
