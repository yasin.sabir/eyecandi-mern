import React, {Component, Fragment} from 'react';
import Header from './common/header-component/header';
import Footer from "./common/footer";
import Sidebar from "./common/sidebar-component/sidebar";
import RightSidebar from "./common/right-sidebar";
import LimitedOffer from "./elements/limitedOffer";
import SimpleReactValidator from 'simple-react-validator';
import img_frame from "../../../assets/images/New-Design/frames/frame-1.png";
import logo from "../../../assets/images/eyeCandi/logo/logo-circle.svg";
import {getURL} from "../functions";
import * as credentials from "../../../constant/framesAPI";


class ThankYou extends Component {

    constructor(props) {
        super(props);
        this.state = {
            public_url : this.props.match.params.publicUrl,
            color: '#00000065',
            footer_font_heading_color: '#0C4C60',
            sidebar_color:'#0C4C60',
            banner_top_heading: '',
            banner_middle_heading : '',
            banner_bottom_heading : '',
            logo: logo,
            data: '',
            email: '',
            wishlist: [],
            token: '',
            name: ''
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        getURL(this.state.public_url).then((res) => {

            if (res.data.error) {
                this.props.history.push('/404');
            } else {
                if (localStorage.getItem("favorite") !== null) {
                    var whislist = localStorage.getItem("favorite");
                    var token = '';
                    whislist = JSON.parse(whislist);
                    fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                        .then(response => response.json())
                        .then((data) => {
                            token = data.Auth.AuthorizationTicket;
                            this.setState({
                                // color: res.data.data[0].brand_color,
                                // logo: res.data.data[0].profile_pic,
                                // data: res.data.data[0],
                                // email: res.data.email,
                                // name: res.data.practice_name,
                                wishlist: whislist,
                                token:token
                            })
                        });
                }

                this.setState({
                    color: res.data.data[0].brand_color,
                    logo: res.data.data[0].profile_pic,
                    data: res.data.data[0],
                    email: res.data.email,
                    name: res.data.practice_name,
                    banner_top_heading: res.data.data[0].banner_top_heading,
                    banner_middle_heading : res.data.data[0].banner_middle_heading,
                    banner_bottom_heading : res.data.data[0].banner_bottom_heading,
                });

            }
        })
    }

    OnSubmit = (e) => {
        e.preventDefault();
        this.props.history.push(`${process.env.PUBLIC_URL}/${this.state.public_url}/try-On-List-Form`);
    };

    OnDelete = (sku) => {
        if (localStorage.getItem("favorite") !== null) {
            var whislist = localStorage.getItem("favorite");
            whislist = JSON.parse(whislist);
            for( var i = 0; i < whislist.length; i++){ if ( whislist[i].sku === sku) { whislist.splice(i, 1); }}
            this.setState({
                wishlist: whislist
            })
            whislist = JSON.stringify(whislist);
            localStorage.setItem("favorite",whislist);
        }
    };

    // pl-0 pr-0 m-t-50 p-b-10

    render() {
        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header data={this.state.data} logo={this.state.logo} color={this.state.color} url={this.state.public_url} name={this.state.name}/>
                        <Sidebar sidebar_color={this.state.sidebar_color} url={this.state.public_url}/>
                        <RightSidebar/>

                        <div className="page-body custom-public-body-color m-t-50 p-t-50">

                            <div className="container mt-4 m-b-50">

                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <div className="tryOnList-note">
                                            <h4 className="fv-h4 f-w-900">My Try-On List</h4>
                                            <p className="sub-para-heading">
                                                Simply add up-to five frames, request a try-on,
                                                <br/>
                                                And we'll contact you when they're ready for you to come in and try them on.
                                                <br/>
                                                What could be easier?
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="row m-t-30">
                                    <div className="col-md-1 col-lg-2 d-lg-block"></div>
                                    <div className="col-md-10 col-lg-8">

                                        {/*List Data*/}

                                        { this.state.wishlist && this.state.wishlist.length > 0 ?

                                            <div className="row tryOnlist">
                                                <div className="col-md-12">
                                                    {this.state.wishlist.map((item,index) => {
                                                        return (
                                                            <div className="row tryOnlist-row">
                                                                <div className="col-sm-12">
                                                                    <ul className="list-inline">
                                                                        <li className="list-inline-item">
                                                                          <span className="number-circle">
                                                                              {index + 1}
                                                                          </span>
                                                                    </li>
                                                                        <li className="list-inline-item">
                                                                            <img width="150" src={'https://api.framesdata.com/api/images?auth=' + this.state.token + item.variation.images}/>
                                                                        </li>
                                                                        <li className="list-inline-item">
                                                                            <div className="tryOnlist-text">
                                                                                <h6 className="fv-h6 tryOnlist-heading" style={{fontsize:'17px'}}>{item.sku}</h6>
                                                                                <p className="tryOnlist-sub-heading">{item.name} <br/> {item.variation.color}
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                        <li className="list-inline-item">
                                                                            <a href="" onClick={(e) => {e.preventDefault();this.OnDelete(item.sku)}} className="tryOnlist-delete">
                                                                                <i className="far fa-trash-alt"></i></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        )
                                                    })}

                                                    {/*<div className="row tryOnlist-row">*/}
                                                    {/*    <div className="col-sm-12">*/}
                                                    {/*        <ul className="list-inline">*/}
                                                    {/*            <li className="list-inline-item">*/}
                                                    {/*          <span className="number-circle">*/}
                                                    {/*            1*/}
                                                    {/*          </span>*/}
                                                    {/*            </li>*/}
                                                    {/*            <li className="list-inline-item">*/}
                                                    {/*                <img width="150" src={img_frame}/>*/}
                                                    {/*            </li>*/}
                                                    {/*            <li className="list-inline-item">*/}
                                                    {/*                <div className="tryOnlist-text">*/}
                                                    {/*                    <h6 className="fv-h6 tryOnlist-heading">RX7029</h6>*/}
                                                    {/*                    <p className="tryOnlist-sub-heading">Ray-Ban <br/> Black*/}
                                                    {/*                    </p>*/}
                                                    {/*                </div>*/}
                                                    {/*            </li>*/}
                                                    {/*            <li className="list-inline-item">*/}
                                                    {/*                <a href="" className="tryOnlist-delete">*/}
                                                    {/*                    <i className="fa fa-trash-o"></i></a>*/}
                                                    {/*            </li>*/}
                                                    {/*        </ul>*/}
                                                    {/*    </div>*/}
                                                    {/*</div>*/}

                                                </div>
                                            </div>

                                            :null }


                                        {/*Request Button*/}

                                        { this.state.wishlist && this.state.wishlist.length > 0 ?

                                            <div className="row m-t-40">
                                                <div className="col-md-12 text-center">
                                                    <form action="">
                                                        <input type="submit" onClick={this.OnSubmit}  style={{backgroundColor:this.state.color}}  className="cus-btn cus-btn-primary text-white" value="Request to Try-On Now"/>
                                                    </form>
                                                </div>
                                            </div>
                                        :null}

                                    </div>
                                    <div className="col-md-1 col-lg-3 d-lg-block"></div>
                                </div>

                            </div>

                            <LimitedOffer color={this.state.color} top_heading={this.state.banner_top_heading} middle_heading={this.state.banner_middle_heading} bottom_heading={this.state.banner_bottom_heading}/>

                            <Footer footer_font_heading_color={this.state.footer_font_heading_color} url={this.state.public_url} data={this.state.data} email={this.state.email}  />

                        </div>

                    </div>
                </div>


            </Fragment>
        )
    }


}

export default ThankYou
