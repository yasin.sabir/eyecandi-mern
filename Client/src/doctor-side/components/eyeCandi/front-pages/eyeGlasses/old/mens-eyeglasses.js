import React, {Component, Fragment, useState} from 'react';
import Header from '../../common/header-component/header';
import Footer from "../../common/footer";
import Sidebar from "../../common/sidebar-component/sidebar";
import RightSidebar from "../../common/right-sidebar";
import LimitedOffer from "../../elements/limitedOffer";
import Hero_Home from "../../elements/hero-home";
import Hero from "../../elements/hero";
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import Slider from "react-slick";
import $ from 'jquery';
import img from "../../../../../assets/images/New-Design/frames/frame-1.png";
import {getUser} from "../../../functions";
import * as credentials from "../../../../../constant/framesAPI";
import {getVisibleproducts} from "../../../../../services";
import errorImg from "../../../../../assets/images/search-not-found.png";
import SingleProductComponent from "../../../pages/singleProductComponent";
import {Pagination, PaginationItem, PaginationLink} from "reactstrap";

class Mens_Eyeglasses extends Component {

    constructor(props) {
        super(props);


        this.state = {
            loader: true,
            token: '',
            product: [],
            pagesSize : 22,
            CurrentPage : 0,
            pagesCount : 0,

            public_url: this.props.match.params.publicUrl,
            hero_title: "Men's",
            nav1 : null,
            nav2 : null,
            slideIndex: 0,
            updateCount: 0,
            Brands_selectedOption: null,
            Brands_options: [
                {value: 'bebe', label: 'Bebe'},
                {value: 'Calvin Klein', label: 'Calvin Klein'},
                {value: 'Cole Haan', label: 'Cole Haan'},
                {value: 'Dragon', label: 'Dragon'},
            ],
            Colors_selectedOption: null,
            Colors_options: [
                {value: 'Red', label: 'Red'},
                {value: 'Black', label: 'Black'},
                {value: 'Green', label: 'Green'},
                {value: 'Silver', label: 'Silver'},
            ],
            Fits_selectedOption: null,
            Fits_options: [
                {value: 'Black', label: 'Black'},
                {value: 'Narrow', label: 'Narrow'},
                {value: 'Medium', label: 'Medium'},
                {value: 'Wide', label: 'Wide'},
            ],
            Shapes_selectedOption: null,
            Shapes_options: [
                {value: 'Square', label: 'Square'},
                {value: 'Rectangle', label: 'Rectangle'},
                {value: 'Round', label: 'Round'},
            ],
            Materials_selectedOption: null,
            Materials_options: [
                {value: 'Acetate', label: 'Acetate'},
                {value: 'Metal', label: 'Metal'},
                {value: 'Mixed', label: 'Mixed'},
            ],
            Ages_selectedOption: null,
            Ages_options: [
                {value: 'Adult', label: 'Adult'},
                {value: 'Youth', label: 'Youth'},
            ]


        }

    }


    OnhandleChange_brands = (Brands_selectedOption) => {
        this.setState({Brands_selectedOption},
            () => console.log(`Brands Option selected:`, this.state.Brands_selectedOption));
    }

    OnhandleChange_colors = (Colors_selectedOption) => {
        this.setState({Colors_selectedOption},
            () => console.log(`Colors Option selected:`, this.state.Colors_selectedOption));
    }

    OnhandleChange_shapes = (Shapes_selectedOption) => {
        this.setState({Shapes_selectedOption},
            () => console.log(`Shapes Option selected:`, this.state.Shapes_selectedOption));
    }

    OnhandleChange_fits = (Fit_selectedOption) => {
        this.setState({Fit_selectedOption},
            () => console.log(`Fits Option selected:`, this.state.Fit_selectedOption));
    }

    OnhandleChange_materials = (Materials_selectedOption) => {
        this.setState({Materials_selectedOption},
            () => console.log(`Materials Option selected:`, this.state.Materials_selectedOption));
    }

    OnhandleChange_ages = (Ages_selectedOption) => {
        this.setState({Ages_selectedOption},
            () => console.log(`Ages Option selected:`, this.state.Ages_selectedOption));
    }


    render() {
        const {
            Brands_selectedOption,
            Colors_selectedOption,
            Fits_selectedOption,
            Shapes_selectedOption,
            Materials_selectedOption,
            Ages_selectedOption
        } = this.state;

        console.log(this.props.token+" ##");

        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header url={this.state.public_url}/>
                        <Sidebar/>
                        <RightSidebar/>
                        <div className="page-body pl-0 pr-0 m-t-50 p-b-10">
                            <Hero title={this.state.hero_title}/>

                            {/*Filter section*/}
                            <div className="container-fluid filters">
                                <div className="row px-5 py-4">
                                    <div className="col-md-12 text-center">

                                        <ul className="list-inline">
                                            <li className="list-inline-item">

                                                <ReactMultiSelectCheckboxes
                                                    placeholderButtonLabel="Brands"
                                                    value={Brands_selectedOption}
                                                    onChange={this.OnhandleChange_brands}
                                                    options={this.state.Brands_options}/>

                                            </li>
                                            <li className="list-inline-item">

                                                <ReactMultiSelectCheckboxes
                                                    placeholderButtonLabel="Colors"
                                                    value={Colors_selectedOption}
                                                    onChange={this.OnhandleChange_colors}
                                                    options={this.state.Colors_options}/>

                                            </li>
                                            <li className="list-inline-item">

                                                <ReactMultiSelectCheckboxes
                                                    placeholderButtonLabel="Fit"
                                                    value={Fits_selectedOption}
                                                    onChange={this.OnhandleChange_fits}
                                                    options={this.state.fits_options}/>

                                            </li>
                                            <li className="list-inline-item">

                                                <ReactMultiSelectCheckboxes
                                                    placeholderButtonLabel="Shape"
                                                    value={Shapes_selectedOption}
                                                    onChange={this.OnhandleChange_shapes}
                                                    options={this.state.Shapes_options}/>

                                            </li>
                                            <li className="list-inline-item">

                                                <ReactMultiSelectCheckboxes
                                                    placeholderButtonLabel="Material"
                                                    value={Materials_selectedOption}
                                                    onChange={this.OnhandleChange_materials}
                                                    options={this.state.Materials_options}/>


                                            </li>
                                            <li className="list-inline-item">

                                                <ReactMultiSelectCheckboxes
                                                    placeholderButtonLabel="Age"
                                                    value={Ages_selectedOption}
                                                    onChange={this.OnhandleChange_ages}
                                                    options={this.state.Ages_options}/>


                                            </li>
                                            <li className="list-inline-item">

                                            </li>
                                            <li className="list-inline-item">

                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>

                            {/*Frames Section*/}
                            <div className="container frames-gallery-section">

                                <div className="row">
                                    <div className="col-md-4 col-lg-4 p-4">
                                        <div className="frame-sec">

                                            <div className="frame-top-detail-sec">
                                                <span>Ray-ban</span>
                                                <h5>RX3716VM</h5>
                                            </div>

                                            <div className="frame-image-sec mt-4">
                                                <img src={img} alt=""/>
                                            </div>

                                            <div className="frame-price-highlight-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol"></i>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-color-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item color-active">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #000000 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #74340B 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #2E1B1C 0%, #8D9DAD 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #3C3125 0%, #594E43 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-wishlist-sec mt-2">
                                                <a href="#" className="wishlist-btn">
                                                    <i className="fa fa-heart-o"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="col-md-4 col-lg-4 p-4">
                                        <div className="frame-sec">

                                            <div className="frame-top-detail-sec">
                                                <span>Ray-ban</span>
                                                <h5>RX3716VM</h5>
                                            </div>

                                            <div className="frame-image-sec mt-4">
                                                <img src={img} alt=""/>
                                            </div>

                                            <div className="frame-price-highlight-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol"></i>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-color-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item color-active">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #000000 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #74340B 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #2E1B1C 0%, #8D9DAD 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #3C3125 0%, #594E43 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-wishlist-sec mt-2">
                                                <a href="#" className="wishlist-btn">
                                                    <i className="fa fa-heart-o filled"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="col-md-4 col-lg-4 p-4">
                                        <div className="frame-sec">

                                            <div className="frame-top-detail-sec">
                                                <span>Ray-ban</span>
                                                <h5>RX3716VM</h5>
                                            </div>

                                            <div className="frame-image-sec mt-4">
                                                <img src={img} alt=""/>
                                            </div>

                                            <div className="frame-price-highlight-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol"></i>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-color-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item color-active">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #000000 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #74340B 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #2E1B1C 0%, #8D9DAD 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #3C3125 0%, #594E43 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-wishlist-sec mt-2">
                                                <a href="#" className="wishlist-btn">
                                                    <i className="fa fa-heart-o"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4 col-lg-4 p-4">
                                        <div className="frame-sec">

                                            <div className="frame-top-detail-sec">
                                                <span>Ray-ban</span>
                                                <h5>RX3716VM</h5>
                                            </div>

                                            <div className="frame-image-sec mt-4">
                                                <img src={img} alt=""/>
                                            </div>

                                            <div className="frame-price-highlight-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol"></i>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-color-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item color-active">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #000000 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #74340B 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #2E1B1C 0%, #8D9DAD 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #3C3125 0%, #594E43 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-wishlist-sec mt-2">
                                                <a href="#" className="wishlist-btn">
                                                    <i className="fa fa-heart-o"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="col-md-4 col-lg-4 p-4">
                                        <div className="frame-sec">

                                            <div className="frame-top-detail-sec">
                                                <span>Ray-ban</span>
                                                <h5>RX3716VM</h5>
                                            </div>

                                            <div className="frame-image-sec mt-4">
                                                <img src={img} alt=""/>
                                            </div>

                                            <div className="frame-price-highlight-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol"></i>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-color-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item color-active">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #000000 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #74340B 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #2E1B1C 0%, #8D9DAD 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #3C3125 0%, #594E43 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-wishlist-sec mt-2">
                                                <a href="#" className="wishlist-btn">
                                                    <i className="fa fa-heart-o"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="col-md-4 col-lg-4 p-4">
                                        <div className="frame-sec">

                                            <div className="frame-top-detail-sec">
                                                <span>Ray-ban</span>
                                                <h5>RX3716VM</h5>
                                            </div>

                                            <div className="frame-image-sec mt-4">
                                                <img src={img} alt=""/>
                                            </div>

                                            <div className="frame-price-highlight-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol-colored"></i>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <i className="fa fa-dollar custom-dollar-symbol"></i>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-color-sec mt-3">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item color-active">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                            <span className="frame-color-circle"
                                                                  style={{background: "transparent linear-gradient(180deg, #902C34 0%, #CA6B81 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #000000 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #74340B 0%, #211C21 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #2E1B1C 0%, #8D9DAD 100%) "}}></span>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="#">
                                                        <span className="frame-color-circle"
                                                              style={{background: "transparent linear-gradient(180deg, #3C3125 0%, #594E43 100%)"}}></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-wishlist-sec mt-2">
                                                <a href="#" className="wishlist-btn">
                                                    <i className="fa fa-heart-o"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>



                        </div>
                        <Footer/>
                    </div>
                </div>
            </Fragment>
        )
    }


}

export default Mens_Eyeglasses
