import React, {Component, Fragment} from 'react';
import Header from '../../common/header-component/header';
import Footer from "../../common/footer";
import Sidebar from "../../common/sidebar-component/sidebar";
import RightSidebar from "../../common/right-sidebar";
import Hero from "../../elements/hero";


class Womens_Eyeglasses extends Component {

    constructor(props) {
        super(props);

        this.state = {
            public_url : this.props.match.params.publicUrl
        }

    }

    render() {

        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header url={this.state.public_url}/>
                        <Sidebar/>
                        <RightSidebar/>
                        <div className="page-body pl-0 pr-0 m-t-50 p-b-10">
                            <Hero/>

                        </div>
                        <Footer/>
                    </div>
                </div>
            </Fragment>
        )
    }


}

export default Womens_Eyeglasses
