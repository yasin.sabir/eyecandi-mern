import React, {Fragment, useState, useEffect, Component} from 'react';
import {useSelector, useDispatch, connect} from 'react-redux';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import Slider from "react-slick";
import img from "../../../../assets/images/New-Design/frames/frame-1.png";
import SweetAlert from 'react-bootstrap-sweetalert';


class UserSingleProductComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wishlist_count : '',
            alert:null,
            show: false,
            basicTitle:'',
            basicType:"default",
            try_req_err_msg: 'Only 5 frames are allowed!',
            try_req_err: false,
            nav1: null,
            nav2: null,
            slideIndex: 0,
            updateCount: 0,
            productLayout_attributes: [
                this.props.url,
                this.props.logo,
                this.props.color,
                this.props.footer_font_heading_color,
                this.props.sidebar_color
            ],
            check_wishlist_product: []
        }
    }


    componentDidMount() {
        window.scrollTo(0, 0);
        this.setState({
            nav1: this.slider,
        });
        window.scrollTo(0, 0);
        // if (localStorage.getItem("favorite") !== null) {
        //
        // }

    }

    closeAlert = () => {
        this.setState({
            show: false
        });
    }

    buttonExample = (type) => {
        switch (type) {
            case 'custom':
                this.setState({
                    alert: (
                        <SweetAlert
                            custom
                            showCancel
                            confirmBtnText="Yes"
                            cancelBtnText="No"
                            confirmBtnBsStyle="primary"
                            cancelBtnBsStyle="default"
                            customIcon="thumbs-up.jpg"
                            title="Do you like thumbs?"
                            onConfirm={this.hideAlert}
                            onCancel={this.hideAlert}
                        >
                            You will find they are up!
                        </SweetAlert>
                    )
                });
                break;
            case 'input':
                this.setState({
                    alert: (
                        <SweetAlert
                            input
                            showCancel
                            cancelBtnBsStyle="default"
                            title="An input!"
                            placeHolder="Write something"
                            onConfirm={this.onRecieveInput}
                            onCancel={this.hideAlert}
                        >
                            Write something interesting:
                        </SweetAlert>
                    )
                });
                break;
            case 'password':
                this.setState({
                    alert: (
                        <SweetAlert
                            input
                            required
                            inputType="password"
                            title="Enter Password"
                            validationMsg="You must enter your password!"
                            onConfirm={this.hideAlert}
                        >
                            Write something interesting:
                        </SweetAlert>
                    )
                });
                break;
            default:
                this.setState({
                    alert: (
                        <SweetAlert
                            showCancel
                            confirmBtnText="Continue"
                            confirmBtnBsStyle={type}
                            type={type}
                            title="Are you sure?"
                            onCancel={this.hideAlert}
                            onConfirm={this.hideAlert}
                        >
                            You will not be able to recover this imaginary file!
                        </SweetAlert>
                    )
                });
                break;
        }
    }

    hideAlert = () => {
        this.setState({
            alert: null
        });
    }


    addToFavorite = () => {
        var fav = {
            name: this.props.item.name,
            sku: this.props.item.sku,
            variation: this.props.item.variants[this.state.slideIndex]
        }
        if (localStorage.getItem("favorite") === null) {
            var whislist = [];
            whislist.push(fav);
            whislist = JSON.stringify(whislist);
            localStorage.setItem("favorite", whislist);
        } else {
            var whislist = localStorage.getItem("favorite");
            whislist = JSON.parse(whislist);
            // if (whislist.find(task => (task.sku === this.props.item.sku)) == undefined && whislist.length < 5) {
            if ( whislist.length < 5) {
                console.log(whislist , whislist.length);

                var fav = {
                    name: this.props.item.name,
                    sku: this.props.item.sku,
                    variation: this.props.item.variants[this.state.slideIndex]
                }
                whislist.push(fav);
                whislist = JSON.stringify(whislist);
                localStorage.setItem("favorite", whislist);
                //this.setState({try_req_err: false});
            } else {
                this.setState({ show: true , basicType:'danger' });
                //this.setState({try_req_err: true});
                // alert("Only 5 frames are allowed!")
            }
        }
    }

    checkFavourite = (sku) => {
        if (localStorage.getItem("favorite") != null) {
            var whislist = localStorage.getItem("favorite");
            whislist = JSON.parse(whislist);
            if (whislist.find(task => (task.sku === sku) == true)) {
                return true;
            } else {
                return false;
            }
        }
    }

    render() {
        const settings = {
            dots: false,
            infinite: false,
            draggable: false,
            speed: 500,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: () =>
                this.setState(state => ({updateCount: state.updateCount + 1})),
            beforeChange: (current, next) => this.setState({slideIndex: next})
        };


        return (
            <Fragment>
                <div className="frame-sec">
                    <SweetAlert
                        show={this.state.show}
                        type={this.state.basicType}
                        title={this.state.basicTitle}
                        onConfirm={this.closeAlert}
                    >
                        Only 5 frames are allowed!
                    </SweetAlert>
                    {/*<button type="button" className="btn btn-danger sweet-5"*/}
                    {/*        onClick={() => this.setState({ show: true, basicType:'danger', basicTitle:'Danger Alert' })}>Danger*/}
                    {/*    alert*/}
                    {/*</button>*/}

                    <div className="frame-top-detail-sec">
                        <span>
                            {/*onClick={() => this.props.onClickDetailPage(this.props.item)}*/}
                            <a href="#" onClick={() => this.props.onClickDetailPage(this.props.item)}>
                                {this.props.item.name}
                            </a>
                        </span>
                        <h5 className="fv-h5">{this.props.item.sku}</h5>
                    </div>

                    <div className="frame-image-sec mt-4">
                        <div className="product-img text-center">

                            <Slider {...settings} asNavFor={this.state.nav2} ref={slider => (this.slider = slider)}
                                    className="product-slider">
                                {this.props.item.variants ? this.props.item.variants.map((item, i) => {
                                        return (
                                            <div className="item" key={i}>
                                                {this.props.token !== undefined && this.props.token !== '' ?
                                                    // onClick={() => this.props.onClickDetailPage(this.props.item)}
                                                    <img onClick={() => this.props.onClickDetailPage(this.props.item)}
                                                         src={'https://api.framesdata.com/api/images?auth=' + this.props.token + item.images}
                                                         alt="" className="img-fluid frame-img-sec"/> : ''}
                                            </div>
                                        )
                                    }) :
                                    ''}
                            </Slider>

                        </div>
                    </div>

                    <div className="frame-price-highlight-sec mt-1">
                        <ul className="list-inline">
                            <li className="list-inline-item">
                                {this.props.item.price <= 50 &&
                                <div>
                                    <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                </div>
                                }
                                {(this.props.item.price <= 100 && this.props.item.price > 50) &&
                                <div>
                                    <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                    <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                </div>
                                }
                                {this.props.item.price > 100 &&
                                <div>
                                    <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                    <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                    <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                </div>
                                }
                            </li>
                        </ul>
                    </div>

                    <div className="frame-color-sec">
                        <div className="product-filter slider-product border-0 py-2">
                            <div className="color-selector">
                                <ul>
                                    {this.props.item.colors.map((item, i) => {
                                        if (i < 3) {
                                            return (
                                                <li onClick={e => this.slider.slickGoTo(e.target.value)} value={i}
                                                    className={item} key={i}
                                                    title={item}></li>
                                            )
                                        }
                                    })}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="frame-wishlist-sec" onClick={this.addToFavorite}>
                        <a href="#" className="wishlist-btn">
                            {/*<i className="fa fa-heart-o"></i>*/}
                            {this.checkFavourite(this.props.item.sku) ?
                                <i className="fa fa-heart" style={{color: '#EF0000'}}></i> :
                                <i className="fa fa-heart-o"></i>
                            }
                        </a>
                    </div>

                    {/*<div className="product-box">*/}
                    {/*    <div className="product-details p-sm-0 pl-0">*/}
                    {/*        /!* color *!/*/}
                    {/*        /!*<div className="product-filter slider-product border-0">*!/*/}
                    {/*        /!*    <div className="color-selector">*!/*/}
                    {/*        /!*        <ul>*!/*/}
                    {/*        /!*            {this.props.item.colors.map((item, i) => {*!/*/}
                    {/*        /!*                if (i < 3) {*!/*/}
                    {/*        /!*                    return (*!/*/}
                    {/*        /!*                        <li onClick={e => this.slider.slickGoTo(e.target.value)} value={i} className={item} key={i}*!/*/}
                    {/*        /!*                            title={item}></li>*!/*/}
                    {/*        /!*                    )*!/*/}
                    {/*        /!*                }*!/*/}
                    {/*        /!*            })}*!/*/}
                    {/*        /!*        </ul>*!/*/}
                    {/*        /!*    </div>*!/*/}
                    {/*        /!*</div>*!/*/}


                    {/*        /!*<div className="product-price">*!/*/}
                    {/*        /!*    <del>*!/*/}
                    {/*        /!*        {symbol} {item.discountPrice}*!/*/}
                    {/*        /!*    </del>*!/*/}
                    {/*        /!*        {symbol} {item.price}*!/*/}
                    {/*        /!*</div>*!/*/}

                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
            </Fragment>
        )
    }
}


export default UserSingleProductComponent;
