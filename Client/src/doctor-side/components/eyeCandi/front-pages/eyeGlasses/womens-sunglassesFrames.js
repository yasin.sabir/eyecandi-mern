import React, {Fragment, useState, useEffect, Component} from 'react';
import {useSelector, useDispatch, connect} from 'react-redux';
import Breadcrumb from '../../../common/breadcrumb';
import {Grid, List, ChevronDown} from 'react-feather';
import banner from '../../../../assets/images/ecommerce/banner.jpg';
import errorImg from '../../../../assets/images/search-not-found.png';
import Modal from 'react-responsive-modal';
import {
    getAgeFront,
    getBrandsFront,
    getColors,
    getColorsFront,
    getMaterialFront,
    getShapesFront,
    getVisibleproducts
} from '../../../../services';
import Carousal from '../../../ecommerce-app/filters/carousal';
import LazyLoad from 'react-lazy-load';
import AllFilters from '../../../ecommerce-app/filters/allfilters';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import SingleProductComponent from "../../pages/singleProductComponent";
import {
    filterBrand,
    filterColor,
    filterPrice,
    getList,
    filterRim,
    filterShape,
    filterMaterial,
    filterGender,
    filterAge,
    filterGroup
} from '../../../../actions/ecommerce.actions';
import {TabContent, TabPane, Nav, NavItem, NavLink} from 'reactstrap';
import logo from '../../../../assets/images/eyeCandi/logo-ss.png';
import one from "../../../../assets/images/image-placeholder/1.jpg";
import * as credentials from "../../../../constant/framesAPI";
import jwt_decode from 'jwt-decode'
import {getURL, getUser} from "../../functions";
import Loader from "../../../common/loader";
import {Pagination, PaginationItem, PaginationLink} from "reactstrap";
import {useHistory} from "react-router-dom";
import Header from "../common/header-component/header";
import Sidebar from "../common/sidebar-component/sidebar";
import RightSidebar from "../common/right-sidebar";
import Hero from "../elements/hero";
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import Footer from "../common/footer";
import UserSingleProductComponent from "./userSingleProductComponent";
import hero_section_w from '../../../../assets/images/New-Design/iStock-1032483062.png';
import X from "../../../../assets/images/New-Design/filtersSVG/x.svg";
import LimitedOffer from "../elements/limitedOffer";
import wimg_sunGlasses from "../../../../assets/images/New-Design/womens/womens-sunglasses.jpg";

const WomensEyeglassesFrames = (props) => {

    const data = useSelector(content => content.data.productItems);
    // console.log('data',data);
    const brands = getBrandsFront(data);
    const colors = getColorsFront(data);
    const materials = getMaterialFront(data);
    const shapes = getShapesFront(data);
    const ages = getAgeFront(data);
    const filters = useSelector(content => content.filters);
    // console.log(filters);
    const products = getVisibleproducts(data, filters);
    // console.log('data',products)
    const symbol = useSelector(content => content.data.symbol);
    const searchProducts = useSelector(content => content.data.products);
    const dispatch = useDispatch();

    const [singleProduct, setSingleProduct] = useState([]);
    const [searchKeyword, setSearchKeyword] = useState('');
    const [layoutColumns, setLayoutColumns] = useState(3);
    const [sidebaron, setSidebaron] = useState(true);
    const [stock, setStock] = useState('');
    const [quantity, setQuantity] = useState(1);
    const [open, setOpen] = useState(false);
    const [filterSidebar, setFilterSidebar] = useState(true);
    const [apiToken, setapiToken] = useState('');
    const [publicUrl, setpublicUrl] = useState();

    const [productData, setproductData] = useState([]);
    const [pagesSize, setpagesSize] = useState(24);
    const [CurrentPage, setCurrentPage] = useState(0);
    const [pagesCount, setpagesCount] = useState(0);
    const [hero_title, sethero_title] = useState("Women's");
    const [hero_subtitle, sethero_subtitle] = useState("Sunglasses");
    const [search, setSearch] = useState(false)


    const onCloseModal = () => {
        setOpen(false)
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        setpublicUrl(props.public_url);
        setpagesCount(Math.ceil(products.length / pagesSize));
        // setCurrentPage(0);


    });

    const handleClick = (index) => {
        props.showLoader();
        setCurrentPage(index);
        window.scrollTo(0, 130)
        setTimeout(() => {
            props.hideLoader();
        }, 1500)

    };

    const filterSortFunc = (event) => {
        dispatch({type: 'SORT_BY', sort_by: event})
    }
    const gridLayout = () => {
        document.querySelector(".product-wrapper-grid").classList.remove("list-view");
        var elems = document.querySelector(".gridRow").childNodes;
        [].forEach.call(elems, function (el) {
            el.className = '';
            el.classList.add('col-xl-3');
            el.classList.add('col-sm-6');
            el.classList.add('xl-4')
        });
    }
    //Grid Layout View
    const listLayout = () => {
        document.querySelector(".product-wrapper-grid").classList.add("list-view");
        var elems = document.querySelector(".gridRow").childNodes;
        [].forEach.call(elems, function (el) {
            el.className = '';
            el.classList.add('col-xl-12');
        });
    }

    // Layout Column View
    const LayoutView = (layoutColumns) => {
        if (!document.querySelector(".product-wrapper-grid").classList.contains("list-view")) {
            var elems = document.querySelector(".gridRow").childNodes;
            [].forEach.call(elems, function (el) {
                el.className = '';
                el.classList.add('col-xl-' + layoutColumns);
            });
        }
    }

    let history = useHistory();
    const onClickDetailPage = (product) => {
        const id = product.sku;
        const brand = product.name;
        //history.push(`${process.env.PUBLIC_URL}/pages/FramesGallery/product-detail/${brand}/${id}`)
        history.push(`${process.env.PUBLIC_URL}/${publicUrl}/eyeglasses/product-detail/${brand}/${id}`);
    }

    const onClickFilter = () => {
        if (sidebaron) {
            setSidebaron(false)
            document.querySelector(".product-wrapper").classList.add('sidebaron');
        } else {
            setSidebaron(true)
            document.querySelector(".product-wrapper").classList.remove('sidebaron');
        }
    }

    const minusQty = () => {
        if (quantity > 1) {
            setStock('InStock')
            setQuantity(quantity - 1)
        }
    }

    const onOpenModal = (productId) => {
        setOpen(true);
        products.map((product, i) => {
            if (product.id === productId) {
                setSingleProduct(product)
            }
        })
    };

    const plusQty = () => {
        if (quantity >= 1) {
            setQuantity(quantity + 1)
        } else {
            setStock('Out of Stock !')
        }
    }

    const changeQty = (e) => {
        setQuantity(parseInt(e.target.value))
    }

    const addcart = (product, qty) => {
        dispatch({type: 'ADD_TO_CART', payload: {product, qty}})
        //props.history.push(`${process.env.PUBLIC_URL}/ecommerce/cart/${product.id}`);
    }

    const addWishList = (product) => {
        dispatch({type: 'ADD_TO_WISHLIST', payload: product});
        //props.history.push(`${process.env.PUBLIC_URL}/ecommerce/wishlist/${product.id}`);
    }


    const colorHandle = (event, color) => {
        var elems = document.querySelectorAll(".color-selector ul li");
        [].forEach.call(elems, function (el) {
            el.classList.remove("active");
        });
        event.target.classList.add('active');
        filterColor(color);
    }

    const [activeTab, setActiveTab] = useState('1');
    const [color, setcolor] = useState([]);

    const [Brands_selectedOption, setBrands_selectedOption] = useState(null);


    const [Brand, setBrand] = useState(null);

    const [Colors_selectedOption, setColors_selectedOption] = useState(null);

    const [Shapes_selectedOption, setShapes_selectedOption] = useState(null);

    const [Materials_selectedOption, setMaterials_selectedOption] = useState(null);

    const [Ages_selectedOption, setAges_selectedOption] = useState(null);

    const [staffPick_selectedOption, setstaffPick_selectedOption] = useState(null);
    const [staffPick_options, setstaffPick_options] = useState([
        {value: '', label: 'Sort By'},
        {value: 'Staff Picks', label: 'Staff Picks'},
        {value: 'Newest', label: 'New Arrivals'},
        {value: 'Price (Low - Hi)', label: 'Price (Low - Hi)'},
        {value: 'Price (Hi - Low)', label: 'Price (Hi - Low)'},
    ]);

    const OnhandleChange_brands = (e) => {

        var brandOption = [];
        var brand = [];
        e.map(e => {
            brandOption.push(e)
            brand.push(e.value);
        })
        setBrands_selectedOption(brandOption);
        console.log(props)
        if (brandOption.length > 0) {
            props.fun.filterBrand(brand);
        } else {
            props.fun.filterBrand(props.product.filterbrands);
        }


    };

    const OnhandleChange_colors = (e) => {
        var brandOption = [];
        var brand = [];
        e.map(e => {
            brandOption.push(e)
            brand.push(e.value);
        })
        setColors_selectedOption(brandOption);
        if (brandOption.length > 0) {
            props.fun.filterColor(brand);
        } else {
            colors.map((e) => brand.push(e.value));
            props.fun.filterColor(brand);
        }
    };

    const OnhandleChange_shapes = (e) => {
        var brandOption = []
        var brand = [];
        e.map(e => {
            brandOption.push(e)
            brand.push(e.value);
        })
        setShapes_selectedOption(brandOption);
        if (brandOption.length > 0) {
            props.fun.filterShape(brand);
        } else {
            props.fun.filterShape(props.product.filterShape);
        }
    };

    const OnhandleChange_materials = (e) => {
        var brandOption = []
        var brand = [];
        e.map(e => {
            brandOption.push(e)
            brand.push(e.value);
        })
        setMaterials_selectedOption(brandOption);
        if (brandOption.length > 0) {
            props.fun.filterMaterial(brand);
        } else {
            props.fun.filterMaterial(props.product.filterMaterial);
        }
    };

    const OnhandleChange_ages = (e) => {
        var brandOption = []
        var brand = [];
        e.map(e => {
            brandOption.push(e)
            brand.push(e.value);
        })
        setAges_selectedOption(brandOption);
        if (brandOption.length > 0) {
            props.fun.filterAge(brand);
        } else {
            props.fun.filterAge(props.product.filterAge);
        }
    }

    const OnhandleChange_staffPick = (e) => {
        console.log(e)
        dispatch({type: 'SORT_BY', sort_by: e.value})
        // setstaffPick_selectedOption(staffPick_selectedOption);
    }
    const styles = {
        search: {
            width: '80%',
            border: 'none',
            fontSize: '18px',
            fontWeight: '700',
            color: '#757575'
        },
        cancel:{
            position: 'absolute',
            top:'7px',
            right:'0',
            cursor: 'pointer'
        }
    }
    const handleSearchKeyword = (keyword) => {
        setSearchKeyword(keyword)
        dispatch({type: 'SEARCH_BY', search: keyword})
    }

    const ClearFilter = () => {
        setBrands_selectedOption([]);
        setAges_selectedOption([]);
        setMaterials_selectedOption([]);
        setShapes_selectedOption([]);
        console.log(props);
        props.fun.filterBrand(props.product.filterbrands);
        props.fun.filterAge(props.product.filterAge);
        props.fun.filterShape(props.product.filterShape);
        props.fun.filterMaterial(props.product.filterMaterial);
    };


    return (
        <Fragment>
            {/*<Breadcrumb publicURL={publicUrl} title="Frames" parent="Frames"/>*/}

            <div className="page-wrapper" id="page-wrapper">
                <div className="page-body-wrapper" id="page-body-wrapper">
                    <Header logo={props.logo} color={props.color} url={props.public_url} name={props.name} data={props.data} />
                    <Sidebar sidebar_color={props.footer_font_heading_color} url={props.public_url}/>
                    <RightSidebar/>
                    <div className="page-body custom-public-body-color pl-0 pr-0 m-t-50 p-b-10">

                        <Hero color={props.color} url={props.public_url} title={hero_title} subtitle={hero_subtitle} hero_class="hero-section-w-sunglasses"/>

                        {/*Filter section*/}
                        <div className="container-fluid filters">
                            <div className="container">
                                {search == false ?
                                    <div className="row px-4 py-4">
                                        <div className="col-md-12 col-lg-10">

                                            <ul className="list-inline">
                                                <li className="list-inline-item">

                                                    <ReactMultiSelectCheckboxes
                                                        placeholderButtonLabel="Brands"
                                                        value={Brands_selectedOption}
                                                        onChange={(e) => OnhandleChange_brands(e)}
                                                        options={brands}/>

                                                </li>
                                                <li className="list-inline-item">

                                                    <ReactMultiSelectCheckboxes
                                                        placeholderButtonLabel="Colors"
                                                        value={Colors_selectedOption}
                                                        onChange={(e) => OnhandleChange_colors(e)}
                                                        options={colors}/>

                                                </li>
                                                <li className="list-inline-item">

                                                    <ReactMultiSelectCheckboxes
                                                        placeholderButtonLabel="Shape"
                                                        value={Shapes_selectedOption}
                                                        onChange={(e) => OnhandleChange_shapes(e)}
                                                        options={shapes}/>

                                                </li>
                                                <li className="list-inline-item">

                                                    <ReactMultiSelectCheckboxes
                                                        placeholderButtonLabel="Material"
                                                        value={Materials_selectedOption}
                                                        onChange={(e) => OnhandleChange_materials(e)}
                                                        options={materials}/>


                                                </li>
                                                {/*mt-sm-0 mt-md-0 mt-lg-3 mt-xl-0*/}
                                                <li className="list-inline-item ">
                                                    <ReactMultiSelectCheckboxes
                                                        placeholderButtonLabel="Age"
                                                        value={Ages_selectedOption}
                                                        onChange={(e) => OnhandleChange_ages(e)}
                                                        options={ages}/>
                                                </li>
                                                <li className="list-inline-item">
                                                    {/*<button className="cus-btn cus-btn-transparent px-0"><img*/}
                                                    {/*    className="custom-cross-icon" src={X} alt="" srcSet=""/>Reset*/}
                                                    {/*    Filters*/}
                                                    {/*</button>*/}

                                                    <a href="#_" className="custom-resetBtn cus-btn-transparent px-0"
                                                       onClick={(e)=>{
                                                           e.preventDefault();
                                                           ClearFilter();
                                                       }}
                                                    >
                                                        <i className="far fa-times custom-cross-icon mr-2"></i>
                                                        Reset Filters
                                                    </a>


                                                </li>
                                                <li className="list-inline-item">

                                                </li>
                                            </ul>

                                        </div>
                                        <div className="col-md-12 col-lg-2">
                                            <div className="d-flex">

                                                <a href="#_" className="cus-btn cus-btn-transparent px-0 pr-3"
                                                   onClick={e => {
                                                       e.preventDefault();
                                                       setSearch(true);
                                                   }} >
                                                    <i className="far fa-search"></i>
                                                </a>

                                                {/*<button className="cus-btn cus-btn-transparent px-0 pr-2" onClick={e => {*/}
                                                {/*    e.preventDefault();*/}
                                                {/*    setSearch(true);*/}
                                                {/*}}><i className="far fa-search"></i>*/}
                                                {/*</button>*/}

                                                <ReactMultiSelectCheckboxes
                                                    placeholderButtonLabel="Sort By"
                                                    value={staffPick_selectedOption}
                                                    onChange={(e) => OnhandleChange_staffPick(e)}
                                                    isMulti={false}
                                                    className="custom_select_staffPick"
                                                    options={staffPick_options}/>
                                            </div>
                                        </div>
                                    </div>
                                    :
                                    <div className="row px-4 py-4">

                                        <div className="col-md-12">
                                            <input style={styles.search} placeholder={'Start typing...'}
                                                   defaultValue={searchKeyword}
                                                   onChange={(e) => handleSearchKeyword(e.target.value)}/>
                                            <button className="cus-btn cus-btn-transparent px-0"><img
                                                className="custom-cross-icon" style={styles.cancel} src={X} alt="" srcSet="" onClick={e => {
                                                e.preventDefault();
                                                setSearch(false);
                                            }}/>
                                            </button>
                                        </div>


                                    </div>}
                            </div>

                        </div>

                        {/*Frames Section*/}
                        <div className="container frames-gallery-section">

                            {products.length <= 0 ?
                                <div className="search-not-found text-center">
                                    <div>
                                        <img className="img-fluid second-search" src={errorImg}/>
                                        <p>Sorry, We didn't find any results matching this search</p>
                                    </div>
                                </div>
                                :
                                <div className="row">
                                    {products ? products.slice(CurrentPage * pagesSize, (CurrentPage + 1) * pagesSize).map((item, i) => {
                                            return <div
                                                className="col-md-4 col-lg-4 py-4 px-0 custom-line" key={i}>
                                                <UserSingleProductComponent item={item}
                                                                            logo={props.logo}
                                                                            color={props.color}
                                                                            sidebar_color={props.sidebar_color}
                                                                            url={publicUrl}
                                                                            footer_font_heading_color={props.footer_font_heading_color}
                                                                            onClickDetailPage={onClickDetailPage}
                                                                            token={props.token}/>
                                            </div>
                                        }
                                    ) : ''
                                    }
                                </div>
                            }

                            <div className="row">
                                <div className="col-md-12 mt-5 mb-5 text-center">
                                    <Pagination aria-label="Page navigation"
                                                className="pagination  justify-content-center pagination-custom-color">

                                        <PaginationItem disabled={CurrentPage <= 0}>
                                            <PaginationLink onClick={(e) => {
                                                e.preventDefault();
                                                handleClick(CurrentPage - 1)
                                            }} href="#">
                                                <i className="fa fa-angle-left mr-2"></i>
                                                Prev Page
                                            </PaginationLink>
                                        </PaginationItem>

                                        {[...Array(pagesCount)].map((page, i) => {
                                                if (CurrentPage + 3 >= i && CurrentPage - 3 <= i) {
                                                    return <PaginationItem active={i === CurrentPage} key={i}>
                                                        <PaginationLink onClick={e => {
                                                            e.preventDefault();
                                                            handleClick(i)
                                                        }} href="#">
                                                            {i + 1}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                }
                                            }
                                        )}

                                        <PaginationItem disabled={CurrentPage >= pagesCount - 1}>
                                            <PaginationLink last onClick={(e) => {
                                                e.preventDefault();
                                                handleClick(CurrentPage + 1)
                                            }} href="#">
                                                Next Page
                                                <i className="fa fa-angle-right ml-2"></i>
                                            </PaginationLink>
                                        </PaginationItem>

                                    </Pagination>

                                </div>
                            </div>
                        </div>

                        <LimitedOffer color={props.color} top_heading={props.banner_top_heading} middle_heading={props.banner_middle_heading} bottom_heading={props.banner_bottom_heading}/>

                        <Footer footer_font_heading_color={props.footer_font_heading_color} url={props.public_url}
                                data={props.data} email={props.email}/>

                    </div>
                </div>
            </div>

        </Fragment>
    );
};

// export default FramesGallery;
class WomensEyeglassesFramesComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: true,
            token: '',
            product: [],
            name: 'Brand',
            public_url: '',
            footer_font_heading_color: '#0C4C60',
            sidebar_color: '#0C4C60',
            data: '',
            email: '',
            banner_bottom_heading:'',
            banner_top_heading:'',
            banner_middle_heading:'',
        }
    }

    componentWillMount() {
        getURL(this.props.match.params.publicUrl).then((res) => {
            if (res.data.error) {
                this.props.history.push('/404');
            } else {
                var productData = res.data.productdata;
                // console.log(productData[0].products);
                this.props.getList(productData[0].products)
                this.props.filterBrand(productData[0].filterbrands);
                this.props.filterGender(['Male', 'Unisex']);
                this.props.filterColor(productData[0].filterColor);
                this.props.filterAge(productData[0].filterAge);
                this.props.filterMaterial(productData[0].filterMaterial);
                this.props.filterShape(productData[0].filterShape);
                this.props.filterRim(productData[0].filterRim);
                this.props.filterGroup(['Sunglasses']);
                // this.props.filterGroup(productData[0].filterGroup)
                setTimeout(() => {
                    this.setState({
                        product: productData[0],
                        name: res.data.practice_name,
                        logo: res.data.data[0].profile_pic,
                        color: res.data.data[0].brand_color,
                        data: res.data.data[0],
                        email: res.data.email,
                        public_url:res.data.public_url,
                        banner_top_heading : res.data.data[0].banner_top_heading,
                        banner_middle_heading : res.data.data[0].banner_middle_heading,
                        banner_bottom_heading : res.data.data[0].banner_bottom_heading,
                    })
                }, 2000)
                fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                    .then(response => response.json())
                    .then((data) => {
                        let token = data.Auth.AuthorizationTicket;
                        this.setState({
                            token: token,
                            loader: false,
                        })
                    })
            }
        })
    }

    showLoader = () => {
        this.setState({
            loader: false
        })
    }
    hideLoader = () => {
        this.setState({
            loader: false
        })
    }

    render() {
        // console.log('loader',this.state.loader)
        return (
            <div>
                <div loader="thisONe"
                     className={`custom-loader-css loader-wrapper ${this.state.loader ? '' : 'loderhide'}`}>
                    <div className="loader bg-white">
                        <div className="whirly-loader"></div>
                    </div>
                </div>
                <WomensEyeglassesFrames showLoader={this.showLoader} hideLoader={this.hideLoader} name={this.state.name}
                                        product={this.state.product}
                                        token={this.state.token}
                                        public_url={this.state.public_url}
                                        footer_font_heading_color={this.state.footer_font_heading_color}
                                        sidebar_color={this.state.sidebar_color}
                                        color={this.state.color}
                                        logo={this.state.logo}
                                        data={this.state.data}
                                        email={this.state.email}
                                        banner_top_heading = {this.state.banner_top_heading}
                                        banner_middle_heading = {this.state.banner_middle_heading}
                                        banner_bottom_heading = {this.state.banner_bottom_heading}

                />
            </div>

        );
    }
}


const mapStateToProps = (state) => ({
    products: state.data.productItems,

})

export default connect(
    mapStateToProps,
    {getList, filterColor, filterBrand, filterGender, filterMaterial, filterAge, filterShape, filterRim, filterGroup}
)(WomensEyeglassesFramesComponent)
