import React, {Component, Fragment, useState} from 'react';
import {Link} from 'react-router-dom';
import Breadcrumb from '../common/breadcrumb';
import {connect} from 'react-redux';
// import Tablet from './tabset';
import {getSingleItem, addToCart, getList} from '../../../../actions/ecommerce.actions';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {getURL, getUser, viewProduct, viewBrand} from "../../../eyeCandi/functions";
import * as credentials from "../../../../constant/framesAPI";
import avatar from '../../../../assets/images/image-placeholder/1.jpg';
import Header from "../common/header-component/header";
import Sidebar from "../common/sidebar-component/sidebar";
import RightSidebar from "../common/right-sidebar";
import Hero from "../elements/hero";
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import X from "../../../../assets/images/New-Design/filtersSVG/x.svg";
import errorImg from "../../../../assets/images/search-not-found.png";
import UserSingleProductComponent from "./userSingleProductComponent";
import LimitedOffer from "../elements/limitedOffer";
import Footer from "../common/footer";
import img from "../../../../assets/images/New-Design/frames/frame-1.png";
import img2 from "../../../../assets/images/New-Design/frames/9c6880391e81f83cf9b597a8bf704185.png";
import logo from "../../../../assets/images/eyeCandi/logo-ss.png";

class PrivateSingleProductPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: 1,
            nav1: null,
            nav2: null,
            profilePic: avatar,
            name: '',
            colorCount: 0,
            public_gallery_url: '',
            ActiveTab: 1,
            loader: true,
            mail_subject: "Eyeglasses%20Try-on%20Request",
            token: '',
            slideIndex: 0,
            updateCount: 0,
            color: '#000',
            public_url: this.props.match.params.publicUrl,
            footer_font_heading_color: '#0C4C60',
            sidebar_color: '#0C4C60',
            logo: logo,
            data: '',
            email: '',
            related_product: [],
            img: '',
            watchList_items: [],
            watchlist_condition: false,
        }
    }

    componentWillMount() {
        window.scrollTo(0, 0);
        var user_id = "";
        localStorage.removeItem('filter');
        //this.props.getSingleItem(this.props.match.params.id);
        // console.log(this.props.match.params.publicUrl);


        getURL(this.props.match.params.publicUrl).then(res => {
            // console.log(res)
            if (res.data.error) {
                this.props.history.push('/404')
            } else {
                fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                    .then(response => response.json())
                    .then((data) => {
                        let token = data.Auth.AuthorizationTicket;
                        this.setState({
                            token: token,
                        })
                    })
                user_id = res.data.id;
                var productData = res.data.productdata[0];
                // console.log(productData);
                this.props.getList(productData.products)
                this.props.getSingleItem(this.props.match.params.id);
                var related_product = [];
                var x = 0;
                while (productData.products.length >= x) {
                    if (typeof productData.products[x] !== 'undefined' && related_product.length < 7) {
                        if (productData.products[x].sku != this.props.match.params.id) {
                            related_product.push(productData.products[x]);
                        }
                    }
                    x++;
                }
                // console.log('related_product', related_product);
                // console.log("!!!" + related_product);
                setTimeout(() => {
                    this.setState({
                        practiceName: res.data.practice_name,
                        profilePic: res.data.data[0].profile_pic,
                        public_gallery_url: res.data.public_url,
                        main_contact_email: res.data.data[0].main_contact_email,
                        loader: false,
                        public_url: res.data.public_url,
                        logo: res.data.data[0].profile_pic,
                        color: res.data.data[0].brand_color,
                        data: res.data.data[0],
                        email: res.data.email,
                        name: res.data.practice_name,
                        related_product: related_product,
                        img: this.props.singleItem.variants[0].images

                    })
                    viewProduct(user_id, this.props.match.params.id).then(res => {
                        viewBrand(user_id, this.props.match.params.brand).then(res => {
                            console.log(res);
                        });
                    });

                }, 1500)


            }

        });

    }

    componentDidMount() {

        this.setState({
            nav1: this.slider1,
            nav2: this.slider2,
        });

        if (localStorage.getItem("favorite") !== null) {
            var whislist = localStorage.getItem("favorite");
            whislist = JSON.parse(whislist);

            this.setState({
                watchList_items: whislist
            })

        }

    }

    handleActiveTab = val => () => {
        if (val === 1) {
            localStorage.removeItem('filter');
            this.props.history.push('/' + this.state.public_gallery_url);
            this.ActiveTab(1)
        } else if (val == 2) {
            localStorage.setItem('filter', 'men');
            this.props.history.push('/' + this.state.public_gallery_url);
            this.ActiveTab(2)
        } else if (val === 3) {
            localStorage.setItem('filter', 'women');
            this.props.history.push('/' + this.state.public_gallery_url);
            this.ActiveTab(3)
        } else if (val === 4) {
            localStorage.setItem('filter', 'youth');
            this.props.history.push('/' + this.state.public_gallery_url);
            this.ActiveTab(4)
        } else {
            this.ActiveTab(1)
        }
    };

    ActiveTab(val) {
        this.setState({
            ActiveTab: val
        })
    }

    onClickDetailPage = (product, props) => {
        const id = product.sku;
        const brand = product.name;
        //console.log(`${process.env.PUBLIC_URL}/${this.props.match.params.publicUrl}/eyeglasses/product-detail/${brand}/${id}`);
        //this.props.history.push(`${process.env.PUBLIC_URL}/${this.props.match.params.publicUrl}/eyeglasses/product-detail/${brand}/${id}`);
        window.location.href = `${process.env.PUBLIC_URL}/${this.props.match.params.publicUrl}/eyeglasses/product-detail/${brand}/${id}`;
        //window.location.reload();
    }

    onclickColor = (img, i) => {
        // console.log('img', img);
        this.setState({
            img: img,
            colorCount: i
        })
    }

    addToFavorite = () => {
        // console.log(this.props);
        var fav = {
            name: this.props.singleItem.name,
            sku: this.props.singleItem.sku,
            variation: this.props.singleItem.variants[this.state.colorCount]
        }
        if (localStorage.getItem("favorite") === null) {
            var whislist = [];
            whislist.push(fav);
            var count = whislist.length;
            whislist = JSON.stringify(whislist);
            localStorage.setItem("favorite", whislist);
            this.setState({
                listcount: count
            })
        } else {
            var whislist = localStorage.getItem("favorite");
            whislist = JSON.parse(whislist);
            if (whislist.find(task => (task.sku === this.props.singleItem.sku)) == undefined && whislist.length < 6) {
                var fav = {
                    name: this.props.singleItem.name,
                    sku: this.props.singleItem.sku,
                    variation: this.props.singleItem.variants[this.state.colorCount]
                }
                whislist.push(fav);
                var count = whislist.length;
                whislist = JSON.stringify(whislist);
                localStorage.setItem("favorite", whislist);
                this.setState({
                    listcount: count
                })
            }
        }
        window.location.reload();
    }

    render() {
        const {quantity} = this.state
        const {products, singleItem, symbol} = this.props;
        var settings = {
            dots: true,
            infinite: true,
            draggable: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: () =>
                this.setState(state => ({updateCount: state.updateCount + 1})),
            beforeChange: (current, next) => this.setState({slideIndex: next})
        };
        var basic_settings = {
            dots: true,
            infinite: true,
            speed: 100,
            arrows: true,
            slidesToShow: 3,
            slidesToScroll: 5,
            adaptiveHeight: false,
            centerPadding: '10px',
            responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 0,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };

        const frame_name = "Hello.%0D%0AI would like to try on the " + singleItem.name + " " + singleItem.sku + ".";
        //console.log(this.props.logo+"-|-"+this.props.color+"-|-"+this.props.footer_font_heading_color+"-|-"+this.props.sidebar_color+"-|-"+this.props.publicUrl );
        // console.log("SideBar" + this.props.sidebar_color + this.state.color);
        const text1 = "Request a Try-on";
        const text2 = "Added to Try-On List";
        var is_exist = false;
        this.state.watchList_items.map((item, i) => {
            if (item.sku === singleItem.sku) {
                is_exist = true;
            }
        })
        let button;
            if (is_exist == true) {
                button =
                    <a className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0">
                        <i className="fa fa-heart mr-2"
                           style={{color: '#EF0000'}}></i>
                        {text2}
                    </a>;
            } else {
                button = <a className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0" onClick={this.addToFavorite}>
                    <i className="fa fa-heart-o heart-outline-red mr-2"></i>
                    {text1}
                </a>
            }
console.log('public_url',this.props.match.params.publicUrl);

        return (
            <Fragment>
                {/*<Breadcrumb title="Product Detail" parent="Ecommerce" />*/}
                <div className={`public-loader-page loader-wrapper ${this.state.loader ? '' : 'loderhide'}`}>
                    <div className="loader bg-white">
                        <div className="whirly-loader"></div>
                    </div>
                </div>


                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header logo={this.state.logo} color={this.state.color} url={this.state.public_gallery_url}
                                name={this.state.name} data={this.state.data}/>
                        <Sidebar sidebar_color={this.state.color} url={this.state.public_gallery_url}/>
                        <RightSidebar/>
                        <div className="page-body custom-public-body-color pl-0 pr-0 m-t-50 p-b-10">

                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12 text-center">
                                        <div className="Singleframe-top-detail-sec">
                                            <h6 className="fv-h6">
                                                <a href="" className="frame-brand-name"
                                                   onClick={() => this.props.onClickDetailPage(singleItem.item)}>
                                                    {singleItem.name}
                                                </a>
                                            </h6>
                                            <h4 className="fv-h4"
                                                style={{color: this.state.color}}>{singleItem.sku}</h4>

                                            <div className="frame-price-highlight-sec mt-1">
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">

                                                        {singleItem.price <= 50 &&
                                                        <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                                        }
                                                        {( singleItem.price <= 100 && singleItem.price > 50 ) &&
                                                        <div>
                                                            <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                                            <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                                        </div>
                                                        }
                                                        {singleItem.price > 100 &&
                                                        <div>
                                                            <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                                            <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                                            <i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>
                                                        </div>
                                                        }

                                                        {/*<i className="far fa-dollar-sign custom-dollar-symbol-colored"></i>*/}
                                                        {/*{singleItem.price}*/}
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="frame-color-sec">
                                                <div className="product-filter slider-product border-0 py-2">
                                                    <div className="color-selector">
                                                        <ul>
                                                            {products.map((item, i) => {
                                                                {
                                                                    if (item.id == singleItem.id) {
                                                                        const color = Array.from(new Set(item.colors))
                                                                        return (
                                                                            <ul>
                                                                                {item.colors.map((color, i) => {
                                                                                    return (
                                                                                        <li onClick={e => this.onclickColor(item.variants[i].images, i)}
                                                                                            value={i} className={color}
                                                                                            key={i}
                                                                                            title={color}></li>
                                                                                    )
                                                                                })}
                                                                            </ul>
                                                                        )
                                                                    } else {
                                                                        // console.log("id not match");
                                                                    }
                                                                }
                                                            })}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div className="row product-page-main m-b-30">
                                    <div className="col-md-12 text-center">

                                        <div className="Slider-Img">
                                            <Slider {...settings} asNavFor={this.state.nav2}
                                                    ref={slider => (this.slider = slider)}
                                                    className="product-slider">
                                                <div className="item" key={0}>
                                                    <img
                                                        src={'https://api.framesdata.com/api/images?auth=' + this.state.token + this.state.img}
                                                        alt="" className="img-fluid"/>
                                                </div>
                                                <div className="item" key={1}>
                                                    <img
                                                        src={'https://api.framesdata.com/api/images?auth=' + this.state.token + this.state.img + 'vtc=F'}
                                                        alt="" className="img-fluid"/>
                                                </div>
                                            </Slider>
                                        </div>

                                        <div className="m-t-50">
                                            {/*<a className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0">*/}
                                            {/*    <i className="fa fa-heart-o heart-outline-red mr-2"></i>*/}
                                            {/*    {text1}*/}
                                            {/*</a>*/}

                                            {/*task => (task.sku === this.props.singleItem.sku)*/}

                                            {button}

                                        </div>

                                    </div>
                                </div>

                                <hr/>

                                <div className="row">
                                    <div className="col-md-12">

                                        <div className="other-frames-section text-center m-t-40 m-b-50">
                                            <h5 className="fv-h5">
                                                Others You Might Like
                                            </h5>
                                        </div>


                                        <Slider {...basic_settings} className="mt-3 mb-3 other-products-slider">
                                            {this.state.related_product ? this.state.related_product.map((item, i) => {
                                                return (
                                                    <UserSingleProductComponent item={item}
                                                                                logo={this.state.logo}
                                                                                color={this.state.color}
                                                                                sidebar_color={this.state.sidebar_color}
                                                                                url={this.props.match.params.publicUrl}
                                                                                footer_font_heading_color={this.state.footer_font_heading_color}
                                                                                onClickDetailPage={this.onClickDetailPage}
                                                                                token={this.state.token}/>
                                                )
                                            }) : ''}
                                            {/*<img className="p-2" src={require('../../../../assets/images/slider/3.jpg')} alt=""/>*/}
                                            {/*<img className="p-2" src={require('../../../../assets/images/slider/4.jpg')} alt=""/>*/}
                                        </Slider>


                                    </div>
                                </div>

                            </div>

                            <LimitedOffer color={this.state.color}/>

                            <Footer footer_font_heading_color={this.state.color}
                                    data={this.state.data} email={this.state.email}/>

                        </div>

                    </div>
                </div>

            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    products: state.data.productItems,
    singleItem: state.data.singleItem,
    symbol: state.data.symbol,
    practiceName: '',
    profilePic: '',
    main_contact_email: ''
})

export default connect(
    mapStateToProps,
    {getSingleItem, addToCart, getList}
)(PrivateSingleProductPage)
