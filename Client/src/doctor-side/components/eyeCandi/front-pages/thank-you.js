import React, {Component, Fragment} from 'react';
import Header from './common/header-component/header';
import Footer from "./common/footer";
import Sidebar from "./common/sidebar-component/sidebar";
import RightSidebar from "./common/right-sidebar";
import LimitedOffer from "./elements/limitedOffer";
import SimpleReactValidator from 'simple-react-validator';
import {getURL, getUser} from "../functions";
import logo from "../../../assets/images/eyeCandi/logo/logo-circle.svg";
import X from "../../../assets/images/New-Design/filtersSVG/x.svg";
import Geocode from "react-geocode";
import jwt_decode from "jwt-decode";

class ThankYou extends Component {

    constructor(props) {
        super(props);
        this.state = {
            public_url : this.props.match.params.publicUrl,
            color: '#00000065',
            footer_font_heading_color: '#0C4C60',
            sidebar_color:'#0C4C60',
            logo: logo,
            data: '',
            email: '',
            name: '',
            banner_top_heading: '',
            banner_middle_heading : '',
            banner_bottom_heading : '',
        }

    }


    componentDidMount() {
        window.scrollTo(0, 0);
        //console.log(this.props);
        Geocode.setApiKey("AIzaSyC5axnAzEvPyJ8dLCSMMveliaEeXwGVvmQ");
        Geocode.setLanguage("en");
        Geocode.setRegion("us");


        var fullAddress = "";

        var token = localStorage.getItem('usertoken');
        const decoded = jwt_decode(token);
        getUser(decoded._id).then(res => {
            //console.log(res.data.data[0].practice_address + "," + res.data.data[0].city + "," + res.data.data[0].state);
            this.setState({
                practice_address: res.data.data[0].practice_address,
                practice_address2: res.data.data[0].practice_address2,
                banner_top_heading: res.data.data[0].banner_top_heading,
                banner_middle_heading : res.data.data[0].banner_middle_heading,
                banner_bottom_heading : res.data.data[0].banner_bottom_heading,
                city: res.data.data[0].city,
                state: res.data.data[0].state,
                fullAddress: res.data.data[0].practice_address + "," + res.data.data[0].practice_address2 + "," + res.data.data[0].city + "," + res.data.data[0].state
            });
            this.getAddress(res.data.data[0].practice_address + " " + res.data.data[0].city + " " + res.data.data[0].state);
        });




        getURL(this.state.public_url).then((res) => {
            if (res.data.error) {
                this.props.history.push('/404');
            } else {
                this.setState({
                    color: res.data.data[0].brand_color,
                    logo:res.data.data[0].profile_pic,
                    data:res.data.data[0],
                    email:res.data.email,
                    name: res.data.practice_name,
                })
            }
        })

        localStorage.removeItem("favorite");
    }

    getAddress = (address) => {

        Geocode.fromAddress( address ).then(
            response => {
                const { lat, lng } = response.results[0].geometry.location;
                console.log(lat +","+lng);

                Geocode.fromLatLng(lat, lng).then(
                    response => { const address = response.results[0].formatted_address;
                        this.setState({
                            map_address : address
                        });
                    },
                    error => {
                        console.error(error);
                    }
                );

            },
            error => {
                console.error(error);
            }
        );

    }

    handleMapURL = (e) => {
        window.open( "https://www.google.com/maps/place/"+this.state.map_address, '_blank');
        // window.location.href = "https://www.google.com/maps/place/"+;
    };

    // pl-0 pr-0 m-t-50 p-b-10

    render() {

        return (
            <Fragment>
                <div className="page-wrapper" id="page-wrapper">
                    <div className="page-body-wrapper" id="page-body-wrapper">
                        <Header data={this.state.data} logo={this.state.logo} color={this.state.color} url={this.state.public_url} name={this.state.name}/>
                        <Sidebar sidebar_color={this.state.sidebar_color} url={this.state.public_url}/>
                        <RightSidebar/>

                        <div className="page-body custom-public-body-color m-t-50 p-t-50">

                            <div className="container mt-4 m-b-50">

                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <div className="thank-you-note">
                                            <h4 className="fv-h4 f-w-900">We are getting your frames ready.</h4>
                                            <p className="sub-para-heading">
                                                You're one step closer to looking ever more fabulous! Once your frames are ready,
                                                we'll contact you to come in and try them on. If there is an issue with any of your frame picks,
                                                we'll let you know. If you need an eye exam, you can book one here, too!

                                                {/*{ thi s.props.history.location.state != undefined ? this.props.history.location.state.BookanExam_Note :*/}
                                                {/*   ''*/}
                                                {/*}*/}
                                                {/*{this.props.history.location.state != undefined ? this.props.history.location.state.tryonList_thankyouNote :*/}
                                                {/*       '' }*/}
                                            </p>
                                            <p className="sub-para-heading">
                                                Thank you for submitting your request. A team member will be contacting you shortly.
                                            </p>

                                            <ul className="list-inline details-buttons m-t-35">
                                                <li className="list-inline-item">
                                                    <a href={'mailto:' + this.state.email} className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0" style={{cursor : 'pointer'}}>
                                                        <i className="far fa-envelope-open-text custom-icon-color mr-2"></i>Email Us
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href={'tel:' + this.state.data.practice_phone_number} style={{cursor : 'pointer'}} className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0">
                                                        <i className="far fa-mobile custom-icon-color mr-2"></i>Call Us  {this.state.data.practice_phone_number}
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a onClick={this.handleMapURL} style={{cursor : 'pointer'}}
                                                       className="cus-btn cus-btn-white-outline fv-fonts-Nunito px-0">
                                                        <i className="far fa-map-signs custom-icon-color mr-2"></i>Map Us
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <LimitedOffer color={this.state.color} top_heading={this.state.banner_top_heading} middle_heading={this.state.banner_middle_heading} bottom_heading={this.state.banner_bottom_heading} />

                            <Footer footer_font_heading_color={this.state.footer_font_heading_color}
                                    url={this.state.public_url}
                                    data={this.state.data} email={this.state.email}
                            />
                        </div>

                    </div>
                </div>


            </Fragment>
        )
    }


}

export default ThankYou
