import React, {Fragment, Component} from 'react';
import axios from 'axios';
import useForm from 'react-hook-form'
import logo from '../../assets/images/eyeCandi/logo-ss.png';
import SimpleReactValidator from 'simple-react-validator';
import Map from './../common/map';
import {register, getCsv} from "./functions";
import Loader from '../common/loader';


export default class Signup extends Component {

    constructor(props) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            practice_name: '',
            email: '',
            password: '',
            confirm_password: '',
            state: '',
            area: '',
            address: '',
            city: '',
            checkbox:'',
            isChecked: true,
            isChecked_err: false,
            oa_member: '',
            oa_member_code: '',
            oa_member_state: false,
            promo_code: '',
            promo_code_error: false,
            promo_code_error_msg: 'Promo-code not exist!',
            error: false,
            register: false,
            user_exists_error: false,
            user_exists_error_msg: "User has already been registered!",
            affiliate_link: ''
        };

        //this.validator = new SimpleReactValidator();
        this.validator = new SimpleReactValidator({

            messages: {
                in: 'Passwords need to match!'
            },
            validators:{
                password: {  // name the rule
                    message: 'Passwords need to match at least 1 number and 1 character with 8 characters long!',
                    rule: (val, params, validator) => {
                        return validator.helpers.testRegex(val,/^(?=.*\d)(?=.*[a-zA-Z]).{8,}$/i) && params.indexOf(val) === -1
                    },
                },
                term:{
                    message: 'Term & Condition is check is required!',
                }
            }

        });

    }

    componentWillMount() {
        localStorage.clear();
        if (this.props.match.params.affiliate) {
            this.setState({
                affiliate_link: this.props.match.params.affiliate
            })
        }
    }

    toggleChange = () => {
        this.setState({
            isChecked: !this.state.isChecked,
            isChecked_err : !this.state.isChecked
        });
    }

    handleOnChangeFirstName = (e) => {

        this.setState({
            first_name: e.target.value,
        });
    };

    handleOnChangeLastName = (e) => {
        this.setState({
            last_name: e.target.value,
        });
    };

    handleOnChangeEmail = (e) => {
        this.setState({
            email: e.target.value,
            user_exists_error: false
        });
    };

    handleOnChangePassword = (e) => {
        this.setState({
            password: e.target.value,
        });
    };

    handleOnChangeConfirmPassword = (e) => {
        this.setState({
            confirm_password: e.target.value,
        });
    };

    handleOnChangePracticeName = (e) => {
        this.setState({
            practice_name: e.target.value,
        });
    };

    handleOnChangeState = (e) => {
        this.setState({
            state: e.target.value,
        });
    };

    handleOnChangeOAmember = (e) => {
        this.setState({oa_member: e.target.value},
            () => console.log("Value Change:", this.state.oa_member),
            this.handleOAmember_selectCondition(e.target.value)
        );
    };

    handleOAmember_selectCondition(param) {
        if (param === 'yes') {
            console.log("--YES");
            this.operation(true)
        } else if (param === 'no') {
            console.log("--NO");
            this.operation(false)
        } else if (param === 'select') {
            console.log("--Select");
            this.operation(false)
        } else {
            this.operation(false)
        }
    }

    operation(param) {
        this.setState({
            oa_member_state: param
        });
    }

    handleOnChangeOAmember_Code = (e) => {
        this.setState({
            oa_member_code: e.target.value,
        });
    };

    handleOnChangePromoCode = (e) => {
        this.setState({
            promo_code: e.target.value,
            promo_code_error: false
        });
    };

    handleOnChangeAddress = (e) => {
        this.setState({
            address: e.target.value,
        });
    };

    onSubmit = e => {
        e.preventDefault();

        if (this.validator.allValid() && this.state.isChecked !== true) {

            if (this.state.promo_code !== '' && this.state.promo_code !== 'UOA100' && this.state.promo_code !== 'DEMO100' && this.state.promo_code !== 'THEMATRIX') {
                this.setState({
                    promo_code_error: true
                })
                return false;
            }


            const data = {
                email: this.state.email,
                password: this.state.password,
                confirm_password: this.state.password,
                promo_code: this.state.promo_code,
                affiliateLink: this.state.affiliate_link
            };
            register(data).then(res => {
                // console.log(res);
                if (!res.error) {

                    localStorage.setItem('reg_user_email', this.state.email);

                    if(this.state.promo_code == "UOA100" || this.state.promo_code == "DEMO100"){
                        this.props.history.push('/SetupWizard');
                    }else{
                        this.props.history.push('/Subscription');
                    }

                } else {
                    this.setState({
                        user_exists_error: true
                    })
                }
            });
        } else {

            this.setState({
                isChecked_err : true
            });

            this.validator.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }

    }

    onPlaceChange = (data) => {
        console.log(data);
        this.setState({
            //  lat:position.lat,
            state: data.state,
            area: data.area,
            address: data.address,
            city: data.city,
            lat: data.latValue,
            lng: data.lngValue
        })
    }


    render() {
        const latVal = parseFloat(this.state.lat);
        const lngVal = parseFloat(this.state.lng);
        var cord = {
            lat: latVal,
            lng: lngVal
        };
        // console.log(cord);
        return (
            <Fragment>
                <Loader/>
                <div className="page-wrapper">
                    <div className="container-fluid">
                        {/* <!-- sign up page start--> */}
                        <div className="authentication-main">
                            <div className="row">
                                <div className="col-sm-12 p-0">
                                    <div className="auth-innerright">
                                        <div className="authentication-box">
                                            <div className="text-center"><img src={logo} alt=""/></div>
                                            <div className="card mt-4 p-4">
                                                <h4 className="text-center text-capitalize txt-danger f-w-700">Register</h4>
                                                <form className="theme-form needs-validation" noValidate="">

                                                    <div className="form-row">

                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label className="col-form-label">Email</label>
                                                                <input className="form-control" type="text"
                                                                       name="email"
                                                                       value={this.state.email}
                                                                       onChange={this.handleOnChangeEmail}
                                                                />
                                                                {this.validator.message('email', this.state.email, 'required|email',)}
                                                            </div>
                                                        </div>


                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label className="col-form-label">Password</label>
                                                                <input className="form-control" type="password"
                                                                       name="password"
                                                                       value={this.state.password}
                                                                       onChange={this.handleOnChangePassword}
                                                                />
                                                                {this.validator.message('password', this.state.password, 'required|password:testing123',)}
                                                            </div>
                                                        </div>

                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label className="col-form-label">Confirm
                                                                    Password</label>
                                                                <input className="form-control" type="password"
                                                                       name="confirm_password"
                                                                       value={this.state.confirm_password}
                                                                       onChange={this.handleOnChangeConfirmPassword}
                                                                />
                                                                {this.validator.message('confirm_password', this.state.confirm_password, `required|in:${this.state.password}`, {messages: {in: 'Passwords need to match!'}})}
                                                                {/*{this.validator.message('confirm_password', this.state.confirm_password, `required|message:${this.state.password}`, {password: {message: 'Passwords need to match!'}})}*/}
                                                            </div>
                                                        </div>

                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label className="col-form-label">Promo Code</label>
                                                                <input className="form-control" type="text"
                                                                       name="promo_code"
                                                                       value={this.state.promo_code}
                                                                       onChange={this.handleOnChangePromoCode}
                                                                />
                                                            </div>
                                                        </div>


                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <div className="checkbox p-0 mb-4">
                                                                    <input id="checkbox1" name="term" type="checkbox" onChange={this.toggleChange}
                                                                    />
                                                                    <label htmlFor="checkbox1"> I agree with the
                                                                        <a href="/terms-and-conditions"> terms and conditions</a> </label></div>
                                                                {
                                                                    this.state.isChecked_err ?
                                                                        <div className="srv-validation-message">The terms and conditions check is required!
                                                                        </div>
                                                                        : ''
                                                                }
                                                            </div>
                                                        </div>


                                                        <input type="hidden"
                                                               value={this.state.affiliate_link}
                                                        />
                                                    </div>



                                                    <div className="form-row">
                                                        <div className="col-md-12">
                                                            {/*<button className="btn btn-primary btn-block" type="submit">Sign Up*/}
                                                            {/*</button>*/}
                                                            <button
                                                                className="btn btn-primary btn-block custom-btn-color"
                                                                type="submit" onClick={this.onSubmit}>Get Started
                                                            </button>
                                                            {this.state.user_exists_error ? <span
                                                                className="mt-1 text-center txt-danger d-block">{this.state.user_exists_error_msg}</span> : ""}
                                                            {this.state.promo_code_error ? <span
                                                                className="mt-1 text-center txt-danger d-block">{this.state.promo_code_error_msg}</span> : ""}

                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className="text-center mt-4 m-l-20 ">Are have an
                                                                account?&nbsp;
                                                                <a className="btn-link text-capitalize custom-anchor-no-color"
                                                                   href="/login">Login</a></div>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- sign up page ends--> */}
                    </div>
                </div>
            </Fragment>
        );
    }

}


