import React, {Component, Fragment} from 'react';
import Contact_info from './WizardSteps/contactInfo';
import Add_brands from './WizardSteps/addBrands';
import Public_gallery from './WizardSteps/publicGallery';
import TypeaheadOne from "../base/typeaheadComponent/typeahead-one";
import * as credentials from '../../constant/framesAPI';
import jwt_decode from 'jwt-decode';
import Loader from "../common/loader";
import {verify} from "./functions";


export default class SetupWizard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            step: 1,
            brief_bio: '',
            practice_phone_number: '',
            practice_address: '',
            practice_address2: '',
            practice_website_url: '',
            city: '',
            state:'',
            zip:'',
            tryOnRequest_Email:'',
            all_brands: [],
            brand_color: '',
            // public_gallery_url: '',
            // profile_pic: '',
            api_token : '',
            // cover_pic: '',
        }
    }


    componentDidMount() {

        const token = localStorage.usertoken;
        console.log(token);

        verify().then(res => {
            if (res.data.name == 'TokenExpiredError') {
                this.props.history.push('/login');
            }else{
                const decoded = jwt_decode(token);
                this.setState({
                    first_name: decoded.first_name,
                    last_name: decoded.last_name,
                    email: decoded.email,
                });

                fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                    .then(response => response.json())
                    .then((data) => {
                            this.setState({api_token: data.Auth.AuthorizationTicket})
                            //console.log(data.Auth.AuthorizationTicket);
                        }
                    );

            }
        })

    }

    nextStep = () => {
        const {step} = this.state
        this.setState({
            step: step + 1
        })
    };

    prevStep = () => {
        const {step} = this.state
        this.setState({
            step: step - 1
        })
    };

    handleChange = input => (e) => {
        this.setState({[input]: e.target.value})
    };

    handleStateChange = (e) => {
        this.setState({
            state : e.target.value
        });
    }

    handleCoverChange = (url) => {
        // console.log("url")
        this.setState({cover_pic: url})
    }

    handleTypeaheadChangeIndustry = selected => {
        const industry = selected.map(option => {
            option.views = 0;
            return option;
        });
        this.setState({
            all_brands : industry
        })
    };
    render() {
        const {step} = this.state;
        const {
            brief_bio ,
            all_brands ,
            // cover_pic ,
            public_gallery_url ,
            // profile_pic ,
            api_token ,
            practice_phone_number ,
            practice_website_url,
            practice_address ,
            practice_address2 ,
            city ,
            state ,
            zip ,
            tryOnRequest_Email ,
            brand_color
        } = this.state;

        const values = {
            practice_phone_number,
            practice_website_url,
            practice_address,
            practice_address2,
            city,
            state,
            zip,
            /*contact info*/

            /* brands Form */
            all_brands,
            tryOnRequest_Email,

            /* Public Gallery Form */
            // public_gallery_url,
            // profile_pic,
            // cover_pic,
            brief_bio,
            api_token
        };


        switch (step) {
            case 1:
                return (<Contact_info history={this.props.history} nextStep={this.nextStep} handleChange={this.handleChange} handleCoverChange={this.handleCoverChange} values={values} />);
            case 2:
                return ( <Add_brands  history={this.props.history} nextStep={this.nextStep} prevStep={this.prevStep} handleTypeaheadChangeIndustry={this.handleTypeaheadChangeIndustry} handleChange={this.handleChange} values={values} />
                    );
            case 3:
                return (<Public_gallery  history={this.props.history} nextStep={this.nextStep} prevStep={this.prevStep} handleChange={this.handleChange} values={values} />);
        }

        return(

            <div className="container">
                <Loader/>
                <div className="row mt-5">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="u-pearl done col-4">
                                <div className="u-pearl-icon">1</div>
                                <span className="u-pearl-title">
                                build Profile
                            </span>
                            </div>
                            <div className="u-pearl current col-4">
                                <div className="u-pearl-icon">2</div>
                                <span className="u-pearl-title">
                                Add Brands
                            </span>
                            </div>
                            <div className="u-pearl col-4">
                                <div className="u-pearl-icon">3</div>
                                <span className="u-pearl-title">
                                Personalize
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


