import axios from 'axios';

const url = "";

export const store = newPromo => {
    console.log(newPromo);
    return axios
        .post(url + '/promocodes/store', {
            user_id: newPromo.user_id,
            promo_code: newPromo.promo_code,
            discount_frequency: newPromo.discount_frequency,
        })
        .then(res => {
            return res.data;
        })
        .catch(err => {
            console.log(err)
        })
};

export const get_list_all = () => {

    return axios
        .get(url + '/promocodes/list').then(res => {
            return res.data;
        }).catch(err => {
            console.log("Error - " + err);
        })
};

export const get_promoBy_id = (id) => {
    return axios
        .get( url + '/promocodes/get/' + id)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log("Error -" + err);
        })
};

export const update_promoBy_id = ( id , data ) => {
    var promoID = localStorage.getItem('promoID');
    return axios
        .put( url + '/promocodes/update/' + id , { user_id : data.user_id , promo_code:data.promo_code , discount_frequency: data.discount_frequency})
        .then(res => {
            return res.data;
        }).catch(err => {
           console.log(err);
        });
}

export const delete_promoBy_id = (id) => {
    return axios
        .delete(  url + '/promocodes/delete/' + id)
        .then(res => {
            return res.data;
        }).catch(err => {
            console.log(err);
        })
}
