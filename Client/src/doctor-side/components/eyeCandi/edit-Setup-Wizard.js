import React, {Component, Fragment} from 'react';
import Edit_Contact_info from './edit-WizardSteps/edit-contactInfo';
import Edit_Add_brands from './edit-WizardSteps/edit-addBrands';
import Edit_Public_gallery from './edit-WizardSteps/edit-publicGallery';
import TypeaheadOne from "../base/typeaheadComponent/typeahead-one";
import * as credentials from '../../constant/framesAPI';
import jwt_decode from 'jwt-decode';
import Loader from "../common/loader";
import {getUser} from "./functions";


export default class EditSetupWizard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            step: 1,
            main_url: '',
            main_contact_phone: '',
            main_contact_email: '',
            all_brands: [],
            brand_color: '',
            public_gallery_url: '',
            profile_pic: '',
            api_token : '',
            userData:[]
        }
    }


    componentDidMount() {

        const token = localStorage.usertoken;
        console.log("TOKEN--"+token);

        if(token){
            const decoded = jwt_decode(token);
            this.setState({
                first_name : decoded.first_name,
                last_name : decoded.last_name,
                email : decoded.email,
            });
            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                .then(response => response.json())
                .then((data) => {
                        this.setState({api_token: data.Auth.AuthorizationTicket})
                        //console.log(data.Auth.AuthorizationTicket);
                    }
                );

            getUser(decoded._id).then(res => {

                this.setState({
                    userData: res.data.data[0],
                    brief_bio: res.data.data[0].brief_bio,
                    cover_pic:res.data.data[0].cover_pic,
                    public_gallery_url:res.data.data[0].public_gallery_url,
                    profile_pic:res.data.data[0].profile_pic,
                    brand_color:res.data.data[0].brand_color,
                    all_brands: res.data.data[0].all_brands.all_brands
                })
                //console.log(res.data.data[0].all_brands.all_brands);
            });


        }else{
            this.props.history.push('/login');
        }










    }

    nextStep = () => {
        const {step} = this.state
        this.setState({
            step: step + 1
        })
    };

    prevStep = () => {
        const {step} = this.state
        this.setState({
            step: step - 1
        })
    };

    handleCoverChange = (url) => {
        // console.log("url")
        this.setState({cover_pic: url})
    }


    handleChange = input => (e) => {
        this.setState({[input]: e.target.value})
    };
    handleTypeaheadChangeIndustry = selected => {
        // console.log(selected)
        const industry = selected.map(option => option);
        this.setState({
            all_brands : industry
        })
    };
    render() {

        const {step} = this.state;
        const {brief_bio,all_brands,cover_pic, public_gallery_url, profile_pic , api_token,brand_color } = this.state;
        const values = {
            /*contact info*/
            brief_bio,
            /*contact info*/

            /* brands Form */
            all_brands,
            brand_color,
            /* Public Gallery Form */
            public_gallery_url,
            profile_pic,
            cover_pic,
            api_token
        };


        switch (step) {
            case 1:
                return (<Edit_Contact_info history={this.props.history} nextStep={this.nextStep} handleChange={this.handleChange} handleCoverChange={this.handleCoverChange} values={values} userData={this.state.userData} />);
            case 2:
                return ( <Edit_Add_brands  history={this.props.history} nextStep={this.nextStep} prevStep={this.prevStep} handleTypeaheadChangeIndustry={this.handleTypeaheadChangeIndustry} handleChange={this.handleChange} values={values} userData={this.state.userData} />
                );
            case 3:
                return (<Edit_Public_gallery  history={this.props.history} nextStep={this.nextStep} prevStep={this.prevStep} handleChange={this.handleChange} values={values} userData={this.state.userData}  toggle={this.props.toggle}/>);
        }

        return(

            <div className="container">
                <Loader/>
                <div className="row mt-5">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="u-pearl done col-4">
                                <div className="u-pearl-icon">1</div>
                                <span className="u-pearl-title">
                                Contact Info
                            </span>
                            </div>
                            <div className="u-pearl current col-4">
                                <div className="u-pearl-icon">2</div>
                                <span className="u-pearl-title">
                                Add Brands
                            </span>
                            </div>
                            <div className="u-pearl col-4">
                                <div className="u-pearl-icon">3</div>
                                <span className="u-pearl-title">
                                Public Gallery
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


