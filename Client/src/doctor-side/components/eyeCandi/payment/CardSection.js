import React, {Component, useEffect, useState} from "react";
import {CardElement, CardNumberElement, CardExpiryElement, CardCvcElement} from "@stripe/react-stripe-js";
import {getList} from "../../../actions/ecommerce.actions";


const CARD_ELEMENT_OPTIONS = {
    style: {
        base: {
            color: "#303238",
            fontSize: "16px",
            fontFamily: "sans-serif",
            fontSmoothing: "antialiased",
            "::placeholder": {
                color: "#CFD7DF"
            }
        },
        invalid: {
            color: "#e5424d",
            ":focus": {
                color: "#303238"
            }
        }
    }
};


const CardSection = (props) => {

    const [cardValidationError, setcardValidationError] = useState(false);
    const [cardValidationMsg , setcardValidationMsg] = useState('');

    useEffect(()=>{
        setcardValidationError(props.data.cardValidationError);
        setcardValidationMsg(props.data.cardValidationMsg);
    })

    return (
        <label style={{display: 'block', textAlign: 'center'}}>
            <CardElement options={CARD_ELEMENT_OPTIONS}/>
            {   cardValidationError ?
                <div className="srv-validation-message text-left">{cardValidationMsg}</div> : ''
            }
        </label>
    );
}
// export default CardSection;

class CardSectionComponent extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div>
                <CardSection data={this.props} />
            </div>
        )
    }

}export default CardSectionComponent
