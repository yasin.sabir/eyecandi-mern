import React from "react";
import {ElementsConsumer, CardElement} from "@stripe/react-stripe-js";
import CardSection from "./CardSection";
import SimpleReactValidator from "simple-react-validator";
import {createCustomer, createCustomerSubscription, getUser, update, updateStripeCredentials , createCustomerSubscriptionOneDollar } from "../functions";
import Loader from "../../common/loader";
import jwt_decode from "jwt-decode";

class CheckoutForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            name: '',
            phone: '',
            tokenID: '',
            userID: '',
            error: false,
            loginSuccess: false,
            customerCreationMsg: 'Something went wrong in customer creation in stripe!',
            customerCreationError: false,
            createCustomerSubscriptionCreationMsg: 'Something went wrong in customer subscription creation in stripe!',
            createCustomerSubscriptionCreationError: false,
            cardValidationMsg: '',
            cardValidationError: false,
            loader: false,
            userStripeCustomerIDError: false,
            userStripeCustomerID: '',
            userStripeCustomerSubscriptionID: '',
            promoCode:'',
        };

        this.validator = new SimpleReactValidator({
            validators: {
                phone: {  // name the rule
                    message: 'Enter valid phone no!',
                    rule: (val, params, validator) => {
                        return validator.helpers.testRegex(val, /^\d{3}-*\d{3}-*\d{4}$/i) && params.indexOf(val) === -1
                    },
                }
            }
        });
    }

    componentDidMount() {

        const userID_ = localStorage.getItem('user_id');
        const token_ = localStorage.getItem('usertoken');
        const decoded = jwt_decode(token_);

        getUser(decoded._id).then(res => {
            this.setState({
                email: res.data.email,
                promoCode : res.data.promo_code,
                userStripeCustomerID: !res.data.stripeCustomerID ? 'undefined'  : res.data.stripeCustomerID ,
                userStripeCustomerSubscriptionID: !res.data.stripeCustomerSubscriptionID ? 'undefined' : res.data.stripeCustomerSubscriptionID,
            });
        });

        this.setState({
            userID: userID_
        })
    }

    showLoader = () => {
        this.setState({
            loader: true
        })
    }
    hideLoader = () => {
        this.setState({
            loader: false
        })
    }

    handleChange = input => (e) => {
        this.setState({[input]: e.target.value})
    };

    handleSubmit = async event => {
        event.preventDefault();

        if (this.validator.allValid()) {

            if (this.state.userStripeCustomerID != 'undefined') {
                //console.log('ID exists');
                this.setState({
                    userStripeCustomerIDError: true
                })
            } else {
                //console.log('ID not exists');
                // handle payment request
                const {stripe, elements, myprops} = this.props;

                if (!stripe || !elements) {
                    return;
                }

                const card = elements.getElement(CardElement);
                const {error, result} = await stripe.createToken(card);

                if (error) {
                    this.setState({
                        cardValidationMsg: error.message,
                        cardValidationError: true
                    });
                    console.log('[error]', error.message);

                } else {

                    this.showLoader();

                    const payment_ = await stripe.createPaymentMethod({
                        type: 'card',
                        card: elements.getElement(CardElement),
                        billing_details: {
                            email: this.state.email
                        }
                    });

                    const data = {
                        payment_method: payment_.paymentMethod.id,
                        email: this.state.email,
                        name: this.state.name,
                        tokenID: this.state.tokenID,
                    };

                    if(this.state.promoCode == 'THEMATRIX'){
                        createCustomerSubscriptionOneDollar(data).then(res => {
                            console.log(res.error);
                            if (!res.error) {
                                //console.log(res);

                                this.setState({
                                    name: '',
                                    email: '',
                                    phone: '',
                                });

                                const stripeCredentials = {
                                    'stripeCustomerID': res.customerID,
                                    'stripeCustomerSubscriptionID': res.subscriptionId
                                };

                                updateStripeCredentials(stripeCredentials).then((res) => {
                                    if (!res.error) {
                                        //console.log(res);
                                        this.hideLoader();
                                        this.props.data.myprops.history.push('/SetupWizard');
                                    } else {
                                        console.log('[update user error]', res.error);
                                    }
                                });

                            } else {
                                this.setState({
                                    createCustomerSubscriptionError: true
                                })
                            }
                        });
                    }else{
                        createCustomerSubscription(data).then(res => {
                            if (!res.error) {
                                //console.log(res);
                                // client_secret: "pi_1IsQwSIu6dlBZMnLLNWla6Rk_secret_yyMkZPIH6KA6cS1FKMsLATFp6"
                                // customerID: "cus_JVRq1KKIff7a12"
                                // status: "succeeded"
                                // subscriptionId: "sub_JVRqeJKaJLOthk"
                                this.setState({
                                    name: '',
                                    email: '',
                                    phone: '',
                                });

                                const stripeCredentials = {
                                    'stripeCustomerID': res.customerID,
                                    'stripeCustomerSubscriptionID': res.subscriptionId
                                };

                                updateStripeCredentials(stripeCredentials).then((res) => {
                                    if (!res.error) {
                                        //console.log(res);
                                        this.hideLoader();
                                        this.props.data.myprops.history.push('/SetupWizard');
                                    } else {
                                        console.log('[update user error]', res.error);
                                    }
                                });

                            } else {
                                this.setState({
                                    createCustomerSubscriptionError: true
                                })
                            }
                        });
                    }


                }

            }

        } else {
            this.validator.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }

    };

    render() {
        // console.log(this.state.userStripeCustomerID);
        return (
            <div>
                {
                    this.state.loader ?
                        <div loader="thisONe"
                             className="custom-loader-css loader-wrapper" style={{top: 0, opacity: .7}}>
                            <div className="loader bg-white">
                                <div className="whirly-loader"></div>
                            </div>
                        </div>
                        : ''
                }
                <form onSubmit={this.handleSubmit}>

                    {/*<button className="btn btn-primary btn-block custom-btn-color" type="submit"*/}
                    {/*        onClick={this.handleSubmit}>dd*/}
                    {/*</button>*/}

                    {
                        this.state.userStripeCustomerIDError ?
                            <div className="srv-validation-message text-center">This User Subscription has already
                                existed!</div>
                            : ''
                    }

                    <div className="form-group" style={{display: 'block'}}>
                        <input className="shadow-sm StripeElement placeholder-css"
                               type="text"
                               name="name"
                               placeholder="Full Name on Card"
                               value={this.state.name}
                               onChange={this.handleChange('name')}
                        />
                        {this.validator.message('name', this.state.name, 'required',)}

                        <input type="hidden"
                               name="email"
                               placeholder="Email Address"
                               value={this.state.email}
                               onChange={this.handleChange('email')}
                               className="shadow-sm StripeElement placeholder-css"/>
                        {/*{this.validator.message('email', this.state.email, 'required|email',)}*/}

                        {/*<input type="text"*/}
                        {/*       name="phone"*/}
                        {/*       placeholder="Phone:111-111-1111 , 1112223333"*/}
                        {/*       value={this.state.phone}*/}
                        {/*       onChange={this.handleChange('phone')}*/}
                        {/*       className="shadow-sm StripeElement placeholder-css"/>*/}
                        {/*{this.validator.message('phone', this.state.phone, 'required|phone:111-111-1111',)}*/}
                    </div>
                    <CardSection cardValidationError={this.state.cardValidationError}
                                 cardValidationMsg={this.state.cardValidationMsg}/>
                    {
                        this.state.promoCode == 'THEMATRIX' ?
                            <button disabled={!this.props.stripe} className="mt-4 btn btn-primary btn-block custom-btn-color">
                                Pay $1/mo Now
                            </button>
                            :
                            <button disabled={!this.props.stripe} className="mt-4 btn btn-primary btn-block custom-btn-color">
                                Pay $99/mo Now
                            </button>
                    }

                </form>
            </div>
        );
    }
}

export default function ICheckoutForm(props) {
    return (
        <ElementsConsumer ff="ss">
            {({stripe, elements}) => (
                <CheckoutForm stripe={stripe} elements={elements} data={props}/>
            )}
        </ElementsConsumer>
    );
}
