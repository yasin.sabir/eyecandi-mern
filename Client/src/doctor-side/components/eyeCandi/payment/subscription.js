import React, {Component} from 'react';
import {createCustomerSubscription, login, verify} from './../functions';
import logo from '../../../assets/images/eyeCandi/logo-ss.png'
import SimpleReactValidator from 'simple-react-validator';
import Loader from '../../common/loader';
import {CardElement, Elements} from "@stripe/react-stripe-js";
import {loadStripe} from "@stripe/stripe-js";
import CheckoutForm from "./CheckoutForm";
import CardSection from "./CardSection";
import jwt_decode from "jwt-decode";
import * as credentials from "../../../constant/framesAPI";

const stripePromise = loadStripe('pk_live_51I6NL0Iu6dlBZMnLNTkYzjxIWMZYAWiw0zX3mcbjf98ok6R83I8MUDDsEbwExTYgcGwqe4oZjXoynTyeEXXUD9y400LwBGqjzy');
// const stripePromise = loadStripe('pk_test_51I6NL0Iu6dlBZMnLw9Q6bPWQYoxdoJYvys0Le2uSYTE3IwUiTuHZ8DNtTLBxwcjFKN7Gs2irveAlyhxHn4KecNJL00F8fGmOce');

export default class Subscription extends Component {

    constructor(props) {
        super(props);
        // console.log(props);

        this.state = {
            email: '',
            tokenID: '',
            error: false,
            loginSuccess: false,
            ff: 'sds',
            customerCreationMsg: 'Something went wrong in customer creation in stripe!',
            customerCreationError: false,
            createCustomerSubscriptionCreationMsg: 'Something went wrong in customer subscription creation in stripe!',
            createCustomerSubscriptionCreationError: false,
            SubscriptionCreationError: false,
        };

        this.validator = new SimpleReactValidator();
    }

    componentWillMount() {

    }

    componentDidMount() {
        verify().then(res => {
                if (res.data.name == 'TokenExpiredError') {
                    this.props.history.push('/login');
                }
            }
        );
    }

    render() {
        return (
            <div>
                <Loader/>
                <div className="page-wrapper">

                    {this.state.createCustomerSubscriptionCreationError ?
                        <div className="custom-loader-for-subscription">
                            <div className="custom-loader-text">
                                <h3>Please Wait</h3>
                            </div>
                        </div>
                        : ''}

                    <div className="container-fluid p-0">
                        {/* <!-- login page start--> */}
                        <div className="authentication-main pb-0">
                            <div className="row">

                                <div className="col-md-12">
                                    <div className="auth-innerright">
                                        <div className="authentication-box">
                                            <div className="text-center"><img src={logo} alt=""/></div>
                                            <div className="card mt-4">
                                                <div className="card-body">

                                                    <div className="text-center">

                                                        {this.state.SubscriptionCreationError ?
                                                            <span className="alert custom-alert-success d-block">
                                                                 Subscription Succeeded!
                                                            </span>
                                                            : ''}

                                                        <h5 className="text-capitalize txt-danger f-w-700">Enter Payment Information</h5>
                                                    </div>
                                                    <Elements stripe={stripePromise}>
                                                        <CheckoutForm myprops={this.props}/>
                                                    </Elements>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- login page end--> */}
                    </div>
                </div>
            </div>
        );
    }

}


