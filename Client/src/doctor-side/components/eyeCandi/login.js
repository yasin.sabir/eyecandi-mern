import React , {Component} from 'react';
import {getUser, login} from './functions';
import logo from '../../assets/images/eyeCandi/logo-ss.png'
import SimpleReactValidator from 'simple-react-validator';
import Loader from '../common/loader';
import jwt_decode from "jwt-decode";


export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            authentication_error : false,
            authentication_error_msg :"These credentials doesn't match our records",
            promoCode:'',
            error: false,
            loginSuccess: false,
            loader: false,
        };

        this.validator = new SimpleReactValidator();
    }
    componentWillMount() {
       // localStorage.clear();
    }

    componentDidMount() {

    }

    handleOnChangeEmail = (e) => {
        this.setState({
            email: e.target.value,
            authentication_error : false
        });
    };

    handleOnChangePassword = (e) => {
        this.setState({
            password: e.target.value,
            authentication_error : false
        });
    };

    onSubmit = (e) => {
        e.preventDefault();

        if (this.validator.allValid()) {

            const data = {
                email : this.state.email,
                password: this.state.password,
            };

            this.setState({
                loader:true
            });

            login(data)
                .then(res => {

                    if(res){

                        const decoded = jwt_decode(res.token);
                        getUser(decoded._id).then(res => {

                            const stripeCustomerID = localStorage.getItem('stripeCustomerID');
                            const stripeCustomerSubscriptionID = localStorage.getItem('stripeCustomerSubscriptionID');
                            // stripeCustomerID == 'undefined' && stripeCustomerSubscriptionID == 'undefined' &&

                            if( res.data.promo_code == '' && stripeCustomerSubscriptionID == '' ){
                                this.props.history.push('/Subscription');
                            }

                            if( res.data.promo_code == 'UOA100' ||  res.data.promo_code == 'DEMO100'  || stripeCustomerSubscriptionID.length > 0 ){
                                this.props.history.push('/');
                            }else{
                                this.props.history.push('/Subscription');
                            }

                        });

                    }else{
                        this.setState({
                            authentication_error: true,
                            loader:false
                        });

                    }
            })

        } else {
            this.validator.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }

    }


    render() {
        return (
            <div>
                {
                    this.state.loader ?
                        <div loader="thisONe"
                             className="custom-loader-css loader-wrapper" style={{top: 0, opacity: .7}}>
                            <div className="loader bg-white">
                                <div className="whirly-loader"></div>
                            </div>
                        </div>
                        : ''
                }
                <div className="page-wrapper">
                    <div className="container-fluid p-0">
                        {/* <!-- login page start--> */}
                        <div className="authentication-main">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="auth-innerright">
                                        <div className="authentication-box">
                                            <div className="text-center"><img src={logo} alt="" /></div>
                                            <div className="card mt-4">
                                                <div className="card-body">
                                                    <div className="text-center">
                                                        <h4 className="text-capitalize txt-danger f-w-700">LOGIN</h4>
                                                    </div>
                                                    <form className="theme-form">
                                                        <div className="form-group">
                                                            <label className="col-form-label pt-0">Email</label>
                                                            <input className="form-control" type="text" name="email" required="" value={this.state.email} onChange={this.handleOnChangeEmail} />
                                                            {this.validator.message('email', this.state.email, 'required|email',  )}

                                                            {
                                                                this.state.authentication_error ? <span className="txt-danger">{this.state.authentication_error_msg}</span> :""
                                                            }


                                                        </div>

                                                        <div className="form-group">
                                                            <label className="col-form-label">Password</label>
                                                            <input className="form-control" type="password" name="password" required=""  value={this.state.password} onChange={this.handleOnChangePassword } />
                                                            {this.validator.message('password', this.state.password, 'required')}
                                                        </div>
                                                        <div className="checkbox p-0">
                                                            <input id="checkbox1" type="checkbox" />
                                                            <label htmlFor="checkbox1">Remember me</label>
                                                        </div>

                                                        <div className="form-group form-row mt-3 mb-0">
                                                            {/*<a href="/signup" className="custom-anchor-no-color">Signup</a>*/}
                                                            <button className="btn btn-primary btn-block custom-btn-color" type="submit" onClick={this.onSubmit}>Login</button>
                                                        </div>

                                                        <div className="form-group mt-3 mb-0">

                                                            {/*<div className="text-center">*/}
                                                            {/*    Need an account? <a href="/signup" className="txt-dark">Signup</a>*/}
                                                            {/*</div>*/}

                                                            <div className="row">
                                                                <div className="col-md-6 text-lg-center">
                                                                    <div className="float-left">
                                                                        Forgot <a href="/resetPwd" className="txt-dark">Password</a>
                                                                    </div>
                                                                </div>
                                                                <div className="col-md-6 text-lg-center">
                                                                    <div>
                                                                        Need an account? <a href="/signup" className="txt-dark">Signup</a>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- login page end--> */}
                    </div>
                </div>
            </div>
        );
    }

}
