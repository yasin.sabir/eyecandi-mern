import React, {Fragment, useState, useEffect, Component} from 'react';
import {useSelector, useDispatch, connect} from 'react-redux';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import Slider from "react-slick";

class SingleProductComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nav1 : null,
            nav2 : null,
            slideIndex: 0,
            updateCount: 0
        }
    }

    componentDidMount() {
        this.setState({
            nav1: this.slider,
        });
    }

    render()
    {
        const settings = {
            dots: false,
            infinite: false,
            draggable: false,
            speed: 500,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: () =>
                this.setState(state => ({updateCount: state.updateCount + 1})),
            beforeChange: (current, next) => this.setState({slideIndex: next})
        };
        return (
            <Fragment>
                <div className="card">
                    <div className="product-box">
                        <div className="product-img text-center pt-4 pb-4">
                            {/*{(item.status === 'sale') ?*/}
                            {/*    <span className="ribbon ribbon-danger">*/}
                            {/*      {item.status}*/}
                            {/*    </span> : ''}*/}
                            {/*    */}


                            <Slider {...settings} asNavFor={this.state.nav2} ref={slider => (this.slider = slider)}
                                    className="product-slider">
                                {this.props.item.variants ? this.props.item.variants.map((item, i) => {
                                        return (
                                            <div className="item" key={i}>
                                                {this.props.token !== undefined && this.props.token !== '' ?
                                                <img onClick={() => this.props.onClickDetailPage(this.props.item)}
                                                    src={'https://api.framesdata.com/api/images?auth=' + this.props.token + item.images}
                                                    alt="" className="img-fluid"/> :'' }
                                            </div>
                                        )
                                    }) :
                                    '' }
                            </Slider>

                            {/*<div className="product-hover">*/}
                            {/*    <ul>*/}
                            {/*        /!*<li>*!/*/}
                            {/*        /!*    <button className="btn" type="button"*!/*/}
                            {/*        /!*            onClick={() => addcart(item, quantity)}>*!/*/}
                            {/*        /!*        <i className="icon-shopping-cart"></i>*!/*/}
                            {/*        /!*    </button>*!/*/}
                            {/*        /!*</li>*!/*/}
                            {/*        <li>*/}
                            {/*            <button className="btn" type="button"*/}
                            {/*                    data-toggle="modal"*/}
                            {/*                    onClick={() => onOpenModal(item.id)}*/}
                            {/*                    data-target="#exampleModalCenter">*/}
                            {/*                <i className="icon-eye"></i>*/}
                            {/*            </button>*/}
                            {/*        </li>*/}
                            {/*        <li>*/}
                            {/*            <button className="btn" type="button"*/}
                            {/*                    onClick={() => addWishList(item)}>*/}
                            {/*                <i className="icon-heart"></i>*/}
                            {/*            </button>*/}
                            {/*        </li>*/}
                            {/*    </ul>*/}
                            {/*</div>*/}
                        </div>
                        <div className="product-details p-sm-0 pl-0">
                            <h6>
                                <a onClick={() => this.props.onClickDetailPage(this.props.item)}
                                   className="font-secondary f-w-700 custom-pointer">
                                    {this.props.item.name}
                                </a></h6>
                            <p>{this.props.item.sku}</p>
                            {/*<p>{item.modified}</p>*/}
                            {/* color */}
                            <div className="product-filter slider-product border-0">
                                <div className="color-selector">
                                    <ul>
                                        {this.props.item.colors.map((item, i) => {
                                            if (i < 3) {
                                                return (
                                                    <li onClick={e => this.slider.slickGoTo(e.target.value)} value={i} className={item} key={i}
                                                        title={item}></li>
                                                )
                                            }
                                        })}
                                    </ul>
                                </div>
                            </div>


                            {/*<div className="product-price">*/}
                            {/*    <del>*/}
                            {/*        {symbol} {item.discountPrice}*/}
                            {/*    </del>*/}
                            {/*        {symbol} {item.price}*/}
                            {/*</div>*/}

                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
    }


    export default SingleProductComponent;
