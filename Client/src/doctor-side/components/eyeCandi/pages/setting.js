import React, {Component, Fragment} from 'react';
import Breadcrumb from '../../common/breadcrumb';
import seven from '../../../assets/images/user/7.jpg';
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import {Redirect} from "react-router";
import SimpleReactValidator from "simple-react-validator";
import {getURL, getUser, update, update_UserData} from "../functions";
import jwt_decode from "jwt-decode";
import * as credentials from "../../../constant/framesAPI";
import {Typeahead} from "react-bootstrap-typeahead";
import {SwatchesPicker} from "react-color";


class Setting extends Component {

    constructor(props) {
        super(props);

        this.state = {
            all_brands: [],
            allBrands: [],
            Brands: [],
            brand_color: '',
            public_gallery_url: '',
            profile_pic: '',
            api_token: '',
            userData: [],
            profile_cover_error: false,
            profile_cover_error_msg: "File size should not be exceeds 3MB",
            ToFrameGallery: false,
            disableButton: false,
            cover_pic: null,
            src: null,
            crop: {
                unit: "%",
                width: 1110,
                height: 400,
                aspect: 4 / 4
            },
            public_url_error: false,
            public_url_error_msg: "Public Url Already Exist",
            profile_logo_error: false,
            profile_logo_error_msg: "File size should not be exceeds 3MB",
            croppedImage: null,
            croppedImage2: null,
            src2: null,
            crop2: {
                unit: "%",
                width: 100,
                height: 100,
                // aspect: 16 / 9
                aspect: 4 / 4,
            },
            color: '',
            openColor: false,
            url_valid: false,
            url_value: '',
            loading: true,
            practice_phone_number: '',
            practice_address: '',
            practice_address2: '',
            practice_website_url: '',
            city: '',
            state: '',
            zip: '',
            tryOnRequest_Email: '',
            banner_bottom_heading: '',
            banner_top_heading: '',
            banner_middle_heading: '',

        }

        this.validator = new SimpleReactValidator({
            validators: {
                phone: {  // name the rule
                    message: 'Enter valid phone no!',
                    rule: (val, params, validator) => {
                        return validator.helpers.testRegex(val, /^\d{3}-*\d{3}-*\d{4}$/i) && params.indexOf(val) === -1
                    },
                }
            }
        });

    }

    componentWillMount() {

        const token = localStorage.usertoken;
        // console.log("TOKEN--"+token);

        if (token) {
            const decoded = jwt_decode(token);
            this.setState({
                first_name: decoded.first_name,
                last_name: decoded.last_name,
                email: decoded.email,
            });

            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                .then(response => response.json())
                .then((data) => {
                        this.setState({api_token: data.Auth.AuthorizationTicket});

                        fetch('https://api.framesdata.com/api/brands?auth=' + data.Auth.AuthorizationTicket + '&mkt=USA&status=A')
                            .then(response => response.json())
                            .then((data) => {
                                    this.setState({
                                        allBrands: data.Brands.map((value, id) => {
                                            return {
                                                id: id,
                                                name: value.BrandName,
                                                BrandFrameMasterID: value.BrandFramesMasterID
                                            };
                                        }),
                                        loading: false
                                    });
                                }
                            );

                    }
                );


        } else {
            this.props.history.push('/login');
        }

    }

    componentDidMount() {
        const token = localStorage.usertoken;
        // console.log("TOKEN--"+token);
        console.log(this.props.match.params.publicUrl);
        const decoded = jwt_decode(token);
        getUser(decoded._id).then(res => {
            this.setState({
                userData: res.data.data[0],
                brief_bio: res.data.data[0].brief_bio,
                cover_pic: res.data.data[0].cover_pic,
                practice_phone_number: res.data.data[0].practice_phone_number,
                practice_address: res.data.data[0].practice_address,
                practice_address2: res.data.data[0].practice_address2,
                practice_website_url: res.data.data[0].practice_website_url,
                city: res.data.data[0].city,
                state: res.data.data[0].state,
                zip: res.data.data[0].zip,
                tryOnRequest_Email: res.data.data[0].tryOnRequest_Email,
                public_gallery_url: res.data.public_url,
                profile_pic: res.data.data[0].profile_pic,
                src2: res.data.data[0].profile_pic,
                brand_color: res.data.data[0].brand_color,
                color: res.data.data[0].brand_color,
                all_brands: res.data.data[0].all_brands.all_brands,
                banner_top_heading: res.data.data[0].banner_top_heading,
                banner_middle_heading: res.data.data[0].banner_middle_heading,
                // banner_bottom_heading:res.data.data[0].banner_bottom_heading,
            });
        })

    }

    handleCoverChange = (url) => {
        // console.log("url")
        this.setState({cover_pic: url})
    };

    handleChange = input => (e) => {
        this.setState({[input]: e.target.value})
    };

    handleInputChange = (input, e) => {
        // console.log("value", input);
    }

    handleTypeaheadChangeIndustry = selected => {
        const industry = selected.map(option => {
            option.views = 0;
            return option;
        });
        //console.log(industry);
        this.setState({
            all_brands: industry
        });
    };


    //cover_photo Functions start =========

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {

            const FSize = e.target.files[0].size;
            const ImageSize = Math.round((FSize / 1024));

            if (ImageSize > 3072) {

                this.setState({
                    profile_cover_error: true
                })

            } else {

                this.setState({
                    profile_cover_error: false
                })
                const reader = new FileReader();
                reader.addEventListener("load", () =>
                    this.setState({
                        cover_pic: reader.result,
                    })
                );

                reader.readAsDataURL(e.target.files[0]);

            }


        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onVideoLoaded = video => {
        this.videoRef = video;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        this.setState({crop});
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                "newFile.jpeg"
            );
            // console.log(croppedImageUrl);
            this.setState({croppedImageUrl});
        } else if (this.videoRef && crop.width && crop.height) {
            const croppedVideoUrl = await this.getCroppedVid(
                this.videoRef,
                crop,
                "mp4"
            );
            this.setState({croppedVideoUrl});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            var image = canvas.toDataURL('image/jpeg');
            // console.log(image);

            this.setState({
                croppedImage: image
            })
            // this.handleCoverChange(image);

            // canvas.toBlob(blob => {
            //     if (!blob) {
            //         //reject(new Error('Canvas is empty'));
            //         return;
            //     }
            //     blob.name = fileName;
            //     window.URL.revokeObjectURL(this.fileUrl);
            //     this.fileUrl = window.URL.createObjectURL(blob);
            //     resolve(this.fileUrl);
            // }, "image/jpeg");

        });
    }

    //cover_photo Functions ends =========

    //Profile_logo Functions start =========

    handleProfilePicChange = (url) => {
        // console.log("url")
        this.setState({profile_pic: url})
    };

    onSelectFile_profile = e => {
        if (e.target.files && e.target.files.length > 0) {

            const FSize = e.target.files[0].size;
            const ImageSize = Math.round((FSize / 1024));

            if (ImageSize > 3072) {

                this.setState({
                    profile_logo_error: true
                })

            } else {

                this.setState({
                    profile_logo_error: false
                })
                const reader = new FileReader();
                reader.addEventListener("load", () =>
                    this.setState({
                        profile_pic: reader.result,
                    })
                );
                reader.readAsDataURL(e.target.files[0]);

            }


        }
    };

    onImageLoaded_profile = image2 => {
        this.imageRef2 = image2;
    };

    onVideoLoaded_profile = video2 => {
        this.videoRef2 = video2;
    }

    onCropComplete_profile = crop2 => {
        // console.log(crop2);
        this.makeClientCrop_profile(crop2);
    };

    onCropChange_profile = (crop2, percentCrop) => {
        // You could also use percentCrop:
        //console.log(crop2);
        this.setState({crop2});
    };

    async makeClientCrop_profile(crop2) {
        if (this.imageRef2 && crop2.width && crop2.height) {
            const croppedImageUrl2 = await this.getCroppedImg_profile(
                this.imageRef2,
                crop2,
                "newFile.jpeg"
            );
            this.setState({croppedImageUrl2});
        } else if (this.videoRef2 && crop2.width && crop2.height) {
            const croppedVideoUrl = await this.getCroppedVid_profile(
                this.videoRef2,
                crop2,
                "mp4"
            );
            this.setState({croppedVideoUrl});
        }
    }

    getCroppedImg_profile(image2, crop2, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = image2.naturalWidth / image2.width;
        const scaleY = image2.naturalHeight / image2.height;
        canvas.width = crop2.width;
        canvas.height = crop2.height;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
            image2,
            crop2.x * scaleX,
            crop2.y * scaleY,
            crop2.width * scaleX,
            crop2.height * scaleY,
            0,
            0,
            crop2.width,
            crop2.height
        );

        return new Promise((resolve, reject) => {
            var image = canvas.toDataURL('image/jpeg', 0.5);
            //console.log(image);

            this.setState({
                croppedImage2: image
            })
            //this.handleProfilePicChange(image);

            // canvas.toBlob(blob => {
            //     if (!blob) {
            //         //reject(new Error('Canvas is empty'));
            //         return;
            //     }
            //     blob.name = fileName;
            //     window.URL.revokeObjectURL(this.fileUrl);
            //     this.fileUrl = window.URL.createObjectURL(blob);
            //     resolve(this.fileUrl);
            // }, "image/jpeg");

        });
    }


    //Profile_logo Functions ends =========

    handleColorChange = (color, event) => {
        this.setState({color: color.hex, openColor: false})
    };

    handleColorFocus = () => {
        this.setState({
            openColor: true
        })
    };

    checkValidation = () => {
        getURL(this.state.public_gallery_url).then((res) => {
            if (res.data.error) {
                // console.log('true');
                this.setState({
                    url_valid: true,
                    url_value: 'is-valid'
                })
            } else {
                this.setState({
                    url_valid: false,
                    url_value: 'is-invalid'
                })
            }
        })
    }

    OnSubmit = (e) => {
        e.preventDefault();
        this.setState({
            disableButton: true
        })


        if (this.validator.allValid()) {
            var all_brands = this.state.all_brands;
            const data = {
                'practice_phone_number': this.state.practice_phone_number,
                'practice_address': this.state.practice_address,
                'practice_address2': this.state.practice_address2,
                'practice_website_url': this.state.practice_website_url,
                'city': this.state.city,
                'state': this.state.state,
                'zip': this.state.zip,
                'tryOnRequest_Email': this.state.tryOnRequest_Email,
                'brief_bio': this.state.brief_bio,
                // 'cover_pic': this.state.cover_pic,
                'all_brands': {all_brands},
                'public_gallery_url': this.state.public_gallery_url,
                'brand_color': this.state.color,
                'profile_pic': this.state.croppedImage2, // discuss
                'banner_top_heading': this.state.banner_top_heading ? this.state.banner_top_heading : 'null',
                'banner_middle_heading': this.state.banner_middle_heading ? this.state.banner_middle_heading : 'null',
                // 'banner_bottom_heading' : this.state.banner_bottom_heading,
                'completed': true
            };

            // alert(data.brief_bio + " == " + data.all_brands + " == " + data.public_gallery_url + " == " + data.brand_color);
            // console.log(data.banner_top_heading);


            update_UserData(data).then((res) => {
                if (!res.error) {
                    console.log(res);
                    this.setState({
                        disableButton: false
                    })
                    //window.location.reload(true);
                    // window.location.reload(true);
                } else {

                }
            });


        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }
    ;


    render() {

        if (this.state.ToFrameGallery === true) {
            return <Redirect to="/FramesGallery"/>
        }
        const {crop, crop2, croppedImageUrl, croppedImageUrl2, src, src2} = this.state;

        return (
            <Fragment>
                <Breadcrumb title="Dashboard" parent="Dashboard" sub_title="Setting"
                            publicURL={this.state.public_gallery_url}/>
                <div className="container-fluid">
                    <div className="edit-profile">
                        <div className="row">

                            <div className="col-lg-12">
                                <form className="card">
                                    <div className="card-header">
                                        <h4 className="card-title mb-0">Edit Details</h4>
                                        <div className="card-options"><a className="card-options-collapse"
                                                                         href="#javascript"
                                                                         data-toggle="card-collapse"><i
                                            className="fe fe-chevron-up"></i></a><a className="card-options-remove"
                                                                                    href="#javascript"
                                                                                    data-toggle="card-remove"><i
                                            className="fe fe-x"></i></a></div>
                                    </div>
                                    <div className="card-body">

                                        <div className="row">
                                            <div className="col-lg-3"></div>
                                            <div className="col-lg-6">

                                                <div className="form-group">
                                                    <div>
                                                        <label className="form-label d-block">Practice Main Phone
                                                            Number</label>
                                                        <input className="form-control input-field-style" type="text"
                                                               name="practice_phone_number"
                                                               defaultValue={this.state.practice_phone_number}
                                                               onChange={this.handleChange('practice_phone_number')}
                                                        />
                                                        {this.validator.message('practice_phone_number', this.state.practice_phone_number, 'required|phone:111-111-1111',)}
                                                    </div>
                                                </div>

                                                <div className="form-group">
                                                    <div>
                                                        <label className="form-label d-block">Email for Try-On
                                                            Requests</label>
                                                        <input className="form-control input-field-style" type="email"
                                                               name="tryOnRequest_Email"
                                                               defaultValue={this.state.tryOnRequest_Email}
                                                               onChange={this.handleChange('tryOnRequest_Email')}
                                                        />
                                                        {this.validator.message('tryOnRequest_Email', this.state.tryOnRequest_Email, 'required',)}
                                                    </div>
                                                </div>


                                                <hr/>
                                                <div className="form-group">
                                                    <div>
                                                        <label className="form-label d-block">Practice Address 1</label>
                                                        <input className="form-control input-field-style" type="text"
                                                               name="practice_address"
                                                               defaultValue={this.state.practice_address}
                                                               onChange={this.handleChange('practice_address')}
                                                        />
                                                        {this.validator.message('practice_address', this.state.practice_address, 'required',)}
                                                    </div>
                                                </div>
                                                <hr/>

                                                <div className="form-group">
                                                    <div>
                                                        <label className="form-label d-block">Practice Address 2
                                                            (Optional)</label>
                                                        <input className="form-control input-field-style" type="text"
                                                               name="practice_address2"
                                                               defaultValue={this.state.practice_address2}
                                                               onChange={this.handleChange('practice_address2')}
                                                        />
                                                    </div>
                                                </div>
                                                <hr/>

                                                <div className="form-group">
                                                    <div>
                                                        <label className="form-label d-block">Practice Website
                                                            Url</label>
                                                        <input className="form-control input-field-style" type="text"
                                                               name="Practice_website_url"
                                                               defaultValue={this.state.practice_website_url}
                                                               onChange={this.handleChange('practice_website_url')}
                                                        />
                                                    </div>
                                                </div>
                                                <hr/>

                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label htmlFor="validationCustom01">City</label>
                                                        <div>
                                                            <div className="input-group">
                                                                <input className="form-control input-field-style"
                                                                       type="text"
                                                                       name="city"
                                                                       defaultValue={this.state.city}
                                                                       onChange={this.handleChange('city')}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <label htmlFor="validationCustom01">State</label>
                                                        <div>
                                                            <div className="input-group">
                                                                <select
                                                                    className="form-control digits input-field-style"
                                                                    name="state" id="select_state" defaultValue=""
                                                                    onChange={this.handleChange('state')}>
                                                                    <option value=""
                                                                            selected={(this.state.state == "") ? "selected" : ""}>Select
                                                                        a State
                                                                    </option>
                                                                    <option value="AL"
                                                                            selected={(this.state.state == "AL") ? "selected" : ""}>Alabama
                                                                    </option>
                                                                    <option value="AK"
                                                                            selected={(this.state.state == "AK") ? "selected" : ""}>Alaska
                                                                    </option>
                                                                    <option value="AZ"
                                                                            selected={(this.state.state == "AZ") ? "selected" : ""}>Arizona
                                                                    </option>
                                                                    <option value="AR"
                                                                            selected={(this.state.state == "AR") ? "selected" : ""}>Arkansas
                                                                    </option>
                                                                    <option value="CA"
                                                                            selected={(this.state.state == "CA") ? "selected" : ""}>California
                                                                    </option>
                                                                    <option value="CO"
                                                                            selected={(this.state.state == "CO") ? "selected" : ""}>Colorado
                                                                    </option>
                                                                    <option value="CT"
                                                                            selected={(this.state.state == "CT") ? "selected" : ""}>Connecticut
                                                                    </option>
                                                                    <option value="DE"
                                                                            selected={(this.state.state == "DE") ? "selected" : ""}>Delaware
                                                                    </option>
                                                                    <option value="DC"
                                                                            selected={(this.state.state == "DC") ? "selected" : ""}>District
                                                                        Of Columbia
                                                                    </option>
                                                                    <option value="FL"
                                                                            selected={(this.state.state == "FL") ? "selected" : ""}>Florida
                                                                    </option>
                                                                    <option value="GA"
                                                                            selected={(this.state.state == "GA") ? "selected" : ""}>Georgia
                                                                    </option>
                                                                    <option value="HI"
                                                                            selected={(this.state.state == "HI") ? "selected" : ""}>Hawaii
                                                                    </option>
                                                                    <option value="ID"
                                                                            selected={(this.state.state == "ID") ? "selected" : ""}>Idaho
                                                                    </option>
                                                                    <option value="IL"
                                                                            selected={(this.state.state == "IL") ? "selected" : ""}>Illinois
                                                                    </option>
                                                                    <option value="IN"
                                                                            selected={(this.state.state == "IN") ? "selected" : ""}>Indiana
                                                                    </option>
                                                                    <option value="IA"
                                                                            selected={(this.state.state == "IA") ? "selected" : ""}>Iowa
                                                                    </option>
                                                                    <option value="KS"
                                                                            selected={(this.state.state == "KS") ? "selected" : ""}>Kansas
                                                                    </option>
                                                                    <option value="KY"
                                                                            selected={(this.state.state == "KY") ? "selected" : ""}>Kentucky
                                                                    </option>
                                                                    <option value="LA"
                                                                            selected={(this.state.state == "LA") ? "selected" : ""}>Louisiana
                                                                    </option>
                                                                    <option value="ME"
                                                                            selected={(this.state.state == "ME") ? "selected" : ""}>Maine
                                                                    </option>
                                                                    <option value="MD"
                                                                            selected={(this.state.state == "MD") ? "selected" : ""}>Maryland
                                                                    </option>
                                                                    <option value="MA"
                                                                            selected={(this.state.state == "MA") ? "selected" : ""}>Massachusetts
                                                                    </option>
                                                                    <option value="MI"
                                                                            selected={(this.state.state == "MI") ? "selected" : ""}>Michigan
                                                                    </option>
                                                                    <option value="MN"
                                                                            selected={(this.state.state == "MN") ? "selected" : ""}>Minnesota
                                                                    </option>
                                                                    <option value="MS"
                                                                            selected={(this.state.state == "MS") ? "selected" : ""}>Mississippi
                                                                    </option>
                                                                    <option value="MO"
                                                                            selected={(this.state.state == "MO") ? "selected" : ""}>Missouri
                                                                    </option>
                                                                    <option value="MT"
                                                                            selected={(this.state.state == "MT") ? "selected" : ""}>Montana
                                                                    </option>
                                                                    <option value="NE"
                                                                            selected={(this.state.state == "NE") ? "selected" : ""}>Nebraska
                                                                    </option>
                                                                    <option value="NV"
                                                                            selected={(this.state.state == "NV") ? "selected" : ""}>Nevada
                                                                    </option>
                                                                    <option value="NH"
                                                                            selected={(this.state.state == "NH") ? "selected" : ""}>New
                                                                        Hampshire
                                                                    </option>
                                                                    <option value="NJ"
                                                                            selected={(this.state.state == "NJ") ? "selected" : ""}>New
                                                                        Jersey
                                                                    </option>
                                                                    <option value="NM"
                                                                            selected={(this.state.state == "NM") ? "selected" : ""}>New
                                                                        Mexico
                                                                    </option>
                                                                    <option value="NY"
                                                                            selected={(this.state.state == "NY") ? "selected" : ""}>New
                                                                        York
                                                                    </option>
                                                                    <option value="NC"
                                                                            selected={(this.state.state == "NC") ? "selected" : ""}>North
                                                                        Carolina
                                                                    </option>
                                                                    <option value="ND"
                                                                            selected={(this.state.state == "ND") ? "selected" : ""}>North
                                                                        Dakota
                                                                    </option>
                                                                    <option value="OH"
                                                                            selected={(this.state.state == "OH") ? "selected" : ""}>Ohio
                                                                    </option>
                                                                    <option value="OK"
                                                                            selected={(this.state.state == "OK") ? "selected" : ""}>Oklahoma
                                                                    </option>
                                                                    <option value="OR"
                                                                            selected={(this.state.state == "OR") ? "selected" : ""}>Oregon
                                                                    </option>
                                                                    <option value="PA"
                                                                            selected={(this.state.state == "PA") ? "selected" : ""}>Pennsylvania
                                                                    </option>
                                                                    <option value="RI"
                                                                            selected={(this.state.state == "RI") ? "selected" : ""}>Rhode
                                                                        Island
                                                                    </option>
                                                                    <option value="SC"
                                                                            selected={(this.state.state == "SC") ? "selected" : ""}>South
                                                                        Carolina
                                                                    </option>
                                                                    <option value="SD"
                                                                            selected={(this.state.state == "SD") ? "selected" : ""}>South
                                                                        Dakota
                                                                    </option>
                                                                    <option value="TN"
                                                                            selected={(this.state.state == "TN") ? "selected" : ""}>Tennessee
                                                                    </option>
                                                                    <option value="TX"
                                                                            selected={(this.state.state == "TX") ? "selected" : ""}>Texas
                                                                    </option>
                                                                    <option value="UT"
                                                                            selected={(this.state.state == "UT") ? "selected" : ""}>Utah
                                                                    </option>
                                                                    <option value="VT"
                                                                            selected={(this.state.state == "VT") ? "selected" : ""}>Vermont
                                                                    </option>
                                                                    <option value="VA"
                                                                            selected={(this.state.state == "VA") ? "selected" : ""}>Virginia
                                                                    </option>
                                                                    <option value="WA"
                                                                            selected={(this.state.state == "WA") ? "selected" : ""}>Washington
                                                                    </option>
                                                                    <option value="WV"
                                                                            selected={(this.state.state == "WV") ? "selected" : ""}>West
                                                                        Virginia
                                                                    </option>
                                                                    <option value="WI"
                                                                            selected={(this.state.state == "WI") ? "selected" : ""}>Wisconsin
                                                                    </option>
                                                                    <option value="WY"
                                                                            selected={(this.state.state == "WY") ? "selected" : ""}>Wyoming
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <label htmlFor="validationCustom01">Zip</label>
                                                        <div>
                                                            <div className="input-group">
                                                                <input className="form-control input-field-style"
                                                                       type="text"
                                                                       name="city"
                                                                       defaultValue={this.state.zip}
                                                                       onChange={this.handleChange('zip')}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr/>

                                                <div className="form-group">
                                                    <div>
                                                        <label className="form-label d-block">Profile Bio:</label>
                                                        <textarea
                                                            rows="8" cols="80"
                                                            id="profile_brief_bio"
                                                            name="brief_bio"
                                                            type="textarea"
                                                            defaultValue={this.state.brief_bio}
                                                            onChange={this.handleChange('brief_bio')}
                                                        />
                                                    </div>
                                                </div>
                                                <hr/>

                                                {/*<div className="form-group">*/}
                                                {/*    <label className="form-label">Profile Cover Photo</label>*/}
                                                {/*    <div className="input-cropper"*/}
                                                {/*         style={{padding: '5px', width: '100%'}}>*/}
                                                {/*        /!*<label For="cover-file-input" className="custom-input-file-design">Choose File</label>*!/*/}
                                                {/*        <input type="file" name="cover_pic"*/}
                                                {/*               defaultValue={this.state.src}*/}
                                                {/*               onChange={this.onSelectFile}*/}
                                                {/*               className="custom-file-width"*/}
                                                {/*        />*/}

                                                {/*        {this.validator.message('cover_pic', this.state.cover_pic, 'required',)}*/}
                                                {/*        {this.state.profile_cover_error ? <span*/}
                                                {/*            className="mt-1 text-left txt-danger">{this.state.profile_cover_error_msg}</span> : ""}*/}
                                                {/*    </div>*/}

                                                {/*    {this.state.cover_pic && (*/}
                                                {/*        <ReactCrop*/}
                                                {/*            className="cover_pic_viewer"*/}
                                                {/*            src={this.state.cover_pic}*/}
                                                {/*            crop={crop}*/}
                                                {/*            onImageLoaded={this.onImageLoaded}*/}
                                                {/*            onComplete={this.onCropComplete}*/}
                                                {/*            onChange={this.onCropChange}*/}
                                                {/*        />*/}
                                                {/*    )}*/}

                                                {/*    {this.state.croppedImageUrl && (*/}
                                                {/*        <img alt="Crop" style={{width: "100px !important"}}*/}
                                                {/*             src={this.state.croppedImageUrl}*/}
                                                {/*             className="crop-portion"/>*/}
                                                {/*    )}*/}

                                                {/*</div>*/}
                                                <hr/>

                                                <div className="form-group">
                                                    <label className="form-label">Brands:</label>
                                                    <Typeahead
                                                        style={{'background-color': 'white !important'}}
                                                        className="shadow-0 public-typeahead"
                                                        id="public-typeahead"
                                                        selected={this.state.all_brands}
                                                        name="all_brands"
                                                        labelKey="name"
                                                        multiple
                                                        required
                                                        //onInputChange={this.handleInputChange}
                                                        onChange={this.handleTypeaheadChangeIndustry}
                                                        options={this.state.allBrands} //Names fetch from Api
                                                        placeholder={this.state.loading ? 'Please wait' : 'Start typing a brand name...'}
                                                        ref={(ref) => this._typeahead = ref}
                                                    />

                                                </div>
                                                <hr/>


                                                {/*<div className="form-group">*/}
                                                {/*    <label htmlFor="validationCustom01">Public Gallery URL</label>*/}
                                                {/*    <div className="input-group">*/}

                                                {/*        <div className="input-group-prepend">*/}
                                                {/*            <span*/}
                                                {/*                className="input-group-text">https://www.eyecandi.com/</span>*/}
                                                {/*        </div>*/}

                                                {/*        <input*/}
                                                {/*            name="brief_bio"*/}
                                                {/*            id="public_url_field"*/}
                                                {/*            className={'form-control ' + this.state.url_value}*/}
                                                {/*            type="text"*/}
                                                {/*            defaultValue={this.state.public_gallery_url}*/}
                                                {/*            onChange={this.handleChange('public_gallery_url')}*/}
                                                {/*            onBlur={this.checkValidation}*/}
                                                {/*        />*/}
                                                {/*        <div className="valid-feedback feedback-icon"*/}
                                                {/*             style={{*/}
                                                {/*                 position: 'absolute',*/}
                                                {/*                 width: 'auto',*/}
                                                {/*                 top: '6px',*/}
                                                {/*                 bottom: 0,*/}
                                                {/*                 right: '10px',*/}
                                                {/*                 marginTop: 0,*/}
                                                {/*                 fontSize: '19px'*/}
                                                {/*             }}*/}
                                                {/*        >*/}
                                                {/*            <i className="far fa-check-circle"></i>*/}
                                                {/*        </div>*/}
                                                {/*        <div className="invalid-feedback feedback-icon"*/}
                                                {/*             style={{*/}
                                                {/*                 position: 'absolute',*/}
                                                {/*                 width: 'auto',*/}
                                                {/*                 top: '6px',*/}
                                                {/*                 bottom: 0,*/}
                                                {/*                 right: '10px',*/}
                                                {/*                 marginTop: 0,*/}
                                                {/*                 fontSize: '19px'*/}
                                                {/*             }}*/}
                                                {/*        >*/}
                                                {/*            <i className="far fa-times-circle"></i>*/}
                                                {/*        </div>*/}


                                                {/*    </div>*/}
                                                {/*</div>*/}
                                                {/*<hr/>*/}

                                                <div className="form-group">
                                                    <label htmlFor="validationCustom01">Brand Color</label>
                                                    <div>
                                                        {!this.state.openColor &&
                                                        <input
                                                            name="brief_bio"
                                                            id="brand_pallete_color"
                                                            type="text"
                                                            className={'form-control'}
                                                            defaultValue={this.state.color}
                                                            onFocus={this.handleColorFocus}
                                                            placeholder={'Choose the best color that matches your brand or type your own hex code.'}
                                                        />
                                                        }
                                                        {this.state.openColor &&
                                                        <SwatchesPicker onChange={this.handleColorChange}/>
                                                        }
                                                    </div>
                                                </div>
                                                <hr/>

                                                <div className="form-group">
                                                    <label>Profile Logo</label>
                                                    <div className="input-cropper"
                                                         style={{padding: '5px', width: '100%'}}>
                                                        {/*<label For="cover-file-input" className="custom-input-file-design">Choose*/}
                                                        {/*    a square .jpg or .png</label>*/}

                                                        <input type="file" name="profile_pic"
                                                               defaultValue={this.state.src2}
                                                               onChange={this.onSelectFile_profile}
                                                               className="custom-file-width"
                                                        />
                                                        {this.state.profile_logo_error ? <span
                                                            className="mt-1 text-left txt-danger">{this.state.profile_logo_error_msg}</span> : ""}
                                                    </div>

                                                    {this.state.profile_pic && (
                                                        <ReactCrop
                                                            src={this.state.profile_pic}
                                                            crop={crop2}
                                                            cropShape="round"
                                                            onImageLoaded={this.onImageLoaded_profile}
                                                            onComplete={this.onCropComplete_profile}
                                                            onChange={this.onCropChange_profile}
                                                        />
                                                    )}

                                                    {this.state.croppedImageUrl2 && (
                                                        <img alt="Crop" style={{width: "100px !important"}}
                                                             src={this.state.croppedImageUrl2}
                                                             className="crop-portion"/>
                                                    )}

                                                </div>
                                                <hr/>

                                                <div className="form-group">
                                                    <label>Banner Top heading</label>
                                                    <div>
                                                        <div className="input-group">
                                                            <input className="form-control input-field-style"
                                                                   type="text"
                                                                   name="banner-top-heading"
                                                                   defaultValue={ this.state.banner_top_heading != 'null' ? this.state.banner_top_heading : '' }
                                                                   onChange={this.handleChange('banner_top_heading')}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="form-group">
                                                    <label>Banner Middle heading</label>
                                                    <div>
                                                        <div className="input-group">
                                                            <input className="form-control input-field-style"
                                                                   type="text"
                                                                   name="banner-middle-heading"
                                                                   defaultValue={ this.state.banner_middle_heading != 'null' ? this.state.banner_middle_heading : '' }
                                                                   onChange={this.handleChange('banner_middle_heading')}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                                {/*<div className="form-group">*/}
                                                {/*    <label>Banner bottom heading</label>*/}
                                                {/*    <div>*/}
                                                {/*        <div className="input-group">*/}
                                                {/*            <input className="form-control input-field-style" type="text"*/}
                                                {/*                   name="banner-bottom-heading"*/}
                                                {/*                   defaultValue={this.state.banner_bottom_heading}*/}
                                                {/*                   onChange={this.handleChange('banner_bottom_heading')}*/}
                                                {/*            />*/}
                                                {/*        </div>*/}
                                                {/*    </div>*/}
                                                {/*</div>*/}


                                                <button className="btn btn-secondary" type="button"
                                                        disabled={this.state.disableButton}
                                                        onClick={this.OnSubmit}>{this.state.disableButton ? 'Please Wait ...' : 'Save Changes'}
                                                </button>

                                            </div>
                                            <div className="col-lg-3"></div>
                                        </div>

                                    </div>

                                    <div className="card-footer text-right">

                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }

}

export default Setting;
