import React, {Fragment, useState, useEffect, Component,useRef} from 'react';
import {useSelector, useDispatch, connect} from 'react-redux';
import Breadcrumb from "../../common/breadcrumb";
import * as credentials from "../../../constant/framesAPI";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
    filterAge,
    filterBrand, filterColor,
    filterGender, filterGroup,
    filterMaterial, filterRim,
    filterShape,
    getList
} from "../../../actions/ecommerce.actions";
import {getUser, showProduct, hideProduct, favoriteProduct, RemoveFavoriteProduct} from "../functions";
import {getColors, getVisibleproductsFrame, productCall} from "../../../services";
import {useHistory} from "react-router-dom";
import {
    Pagination,
    PaginationItem,
    PaginationLink,
    Button,
    Popover,
    PopoverHeader,
    PopoverBody,
    UncontrolledPopover
} from "reactstrap";
import errorImg from "../../../assets/images/search-not-found.png";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import _ from 'lodash';

var gg = '';
const FramesFeatures = (props) => {

    const data = useSelector(content => content.data.productItems);
    const colors = getColors(data);
    const filters = useSelector(content => content.filters);
    const products = getVisibleproductsFrame(data, filters);
    const symbol = useSelector(content => content.data.symbol);
    const searchProducts = useSelector(content => content.data.products);
    let testData = productCall(products, props.token);
    const dispatch = useDispatch();
    const [arr, setArr] = useState([]);

    const [singleProduct, setSingleProduct] = useState([]);
    const [searchKeyword, setSearchKeyword] = useState('');
    const [layoutColumns, setLayoutColumns] = useState(3);
    const [sidebaron, setSidebaron] = useState(true);
    const [stock, setStock] = useState('');
    const [quantity, setQuantity] = useState(1);
    const [open, setOpen] = useState(false);
    const [filterSidebar, setFilterSidebar] = useState(true);
    const [apiToken, setapiToken] = useState('');
    const [publicUrl, setpublicUrl] = useState();
    const [productData, setproductData] = useState([]);
    const [pagesSize, setpagesSize] = useState(10);
    const [CurrentPage, setCurrentPage] = useState(0);
    const [pagesCount, setpagesCount] = useState(0);
    const [featuresData, setfeaturesData] = useState([]);
    const [popoverOpen, setPopoverOpen] = useState(false);
    const table = useRef(null);

    const onCloseModal = () => {
        setOpen(false)
    };

    useEffect(() => {
        // let adpostData1 = [];
        // let adpostData2 = [];
        //
        // adpostData1 = testData;
        // adpostData2 = testData;
        //
        // adpostData1 = _.filter(
        //     adpostData1,
        //     (adpostData1) => adpostData1.favorite.favorite === true
        // );
        //
        // adpostData2 = _.filter(
        //     adpostData2,
        //     (adpostData2) => adpostData2.favorite.favorite === false
        // );



        // gg = adpostData1.concat(adpostData2);
        console.log("test - ", gg );

        var user_id = localStorage.getItem('user_id');
        if (user_id) {
            getUser(user_id).then(res => {
                let url = res.data.public_url ? res.data.public_url : '';
                // console.log(res.data.public_url);
                setpublicUrl(url);
            });
        }
        setpagesCount(Math.ceil(products.length / pagesSize));
        // setCurrentPage(0);

    });

    const handleClick = (index) => {
        props.showLoader();
        setCurrentPage(index);
        window.scrollTo(0, 130)
        setTimeout(() => {
            props.hideLoader();
        }, 1500);
    };

    const filterSortFunc = (event) => {
        dispatch({type: 'SORT_BY', sort_by: event})
    }

    const gridLayout = () => {
        document.querySelector(".product-wrapper-grid").classList.remove("list-view");
        var elems = document.querySelector(".gridRow").childNodes;
        [].forEach.call(elems, function (el) {
            el.className = '';
            el.classList.add('col-xl-3');
            el.classList.add('col-sm-6');
            el.classList.add('xl-4')
        });
    };

    const handleSearchKeyword = (keyword) => {
        setSearchKeyword(keyword)
        dispatch({type: 'SEARCH_BY', search: keyword})
    }


    // const toggle = () => {
    //     setpopoverOpen(!popoverOpen);
    //     // this.setState({
    //     //     popoverOpen: !this.state.popoverOpen,
    //     // })
    // }

    const toggle = () => setPopoverOpen(!popoverOpen);

    const onHover = () => {
        //  setpopoverOpen(true);
    }

    const onHoverLeave = () => {
        //setpopoverOpen(false);
    }

    let history = useHistory();

    const [activeTab, setActiveTab] = useState('1');
    const [color, setcolor] = useState([]);

    const custom_img_size = {
        width: '200px',
        height: '100px'
    }



    function imageFormatter(cell, row) {
        var style = {
            backgroundImage: "url(" + 'https://api.framesdata.com/api/images?auth=' + props.token + cell.data.img + ")",
            backgroundPosition: 'center',
            backgroundSize: 'contain',
            backgroundColor: '#fff',
            border: '0px',
            width: '100px',
            height: '50px',
        }
        return (
            <div>
                <Button type="button"
                        id={`popover-${cell.index}`}
                        style={style}>
                </Button>
                <UncontrolledPopover trigger="focus" placement="right" target={`popover-${cell.index}`}>
                    <PopoverBody>
                        <img
                            src={'https://api.framesdata.com/api/images?auth=' + props.token + cell.data.img}
                            alt=""
                            className="custom-img-frameFeatures" style={custom_img_size}
                        />
                    </PopoverBody>
                </UncontrolledPopover>
            </div>);
    }

    function showHideFormatter(cell, row) {
        return (<div>
            {cell.enable ?
                <td><i className="far fa-eye cu-fa-eye-color f-20"
                       onClick={(e) => {
                           props.showProduct('hide', cell.sku)
                       }}/></td>
                :
                <td><i className="far fa-eye-slash cu-fa-eye-color-hide f-20"
                       onClick={(e) => {
                           props.showProduct('show', cell.sku)
                       }}/></td>
            }
        </div>);
    }

    function favoriteFormatter(cell, row) {

        return (
            <div>
            {!cell.favorite ?
                <td><i stle={{color: '#bcbcbc'}} className="far fa-thumbtack cu-fa-thumbtack-color f-20"
                       onClick={(e) => {
                           props.showProduct('add_favorite', cell.sku)
                       }}
                /></td>
                :
                <td><i style={{color: '#49a8fe'}}  className="fas fa-thumbtack cu-fa-thumbtack-color-trans f-20"
                       onClick={(e) => {
                           props.showProduct('remove_favorite', cell.sku)
                       }}
                /></td>}
        </div>);
    }

    return (
        <Fragment>
            <Breadcrumb publicURL={publicUrl} title="Frames" parent="Dashboard" sub_title="Frames"/>
            <div className="container-fluid">

                <div className="row">

                    <div className="col-lg-6">
                    </div>

                    <div className="col-lg-12">
                        <div className="search-section">
                            <form className="form-inline search-form row">
                                <div className="form-group col-lg-6">
                                    <input
                                        className="form-control-plaintext searchIcon"
                                        type="text"
                                        placeholder="search"
                                        defaultValue={searchKeyword}
                                        onChange={(e) => handleSearchKeyword(e.target.value)}
                                    />
                                </div>
                            </form>
                        </div>

                        <div className="custom-dataTable ">
                            <BootstrapTable className="table-bordered bg-white mt-4 mb-4"
                                            data={testData}
                                            striped hover
                                            pagination>
                                <TableHeaderColumn dataField='image' dataFormat={imageFormatter}
                                                   export={false}>Image</TableHeaderColumn>
                                <TableHeaderColumn isKey dataField='brand' dataSort={true}> Brand</TableHeaderColumn>
                                <TableHeaderColumn dataField='frame_id'>Frame ID</TableHeaderColumn>
                                <TableHeaderColumn dataField='type'>Type</TableHeaderColumn>
                                <TableHeaderColumn dataField='date_added'>Date Added</TableHeaderColumn>
                                <TableHeaderColumn dataField='show_hide'
                                                   dataFormat={showHideFormatter}>Show/Hide</TableHeaderColumn>
                                <TableHeaderColumn dataField='favorite'
                                                   dataFormat={favoriteFormatter} dataSort={ true }>Favorite</TableHeaderColumn>

                            </BootstrapTable>
                        </div>



                        {true == true || products.length <= 0 ?
                            <div></div>
                            // <div className="search-not-found text-center">
                            //     <div>
                            //         <img className="img-fluid second-search" src={errorImg}/>
                            //         <p>Sorry, We didn't find any results matching this search</p>
                            //     </div>
                            // </div>
                            :
                            <div></div>
                            // <div className="table-responsive bg-white mt-4 mb-4">
                            //     <table className="table frameFeatures-table">
                            //         <thead>
                            //         <tr>
                            //             {/*<th scope="col">Image</th>*/}
                            //             <th scope="col">Brand</th>
                            //             <th scope="col">Frame ID</th>
                            //             <th scope="col">Type</th>
                            //             <th scope="col">Date Added</th>
                            //             <th scope="col">Action</th>
                            //             {/*<th scope="col">Show Hide</th>*/}
                            //             {/*<th scope="col">Pin Fav</th>*/}
                            //         </tr>
                            //         </thead>
                            //         <tbody>
                            //         {
                            //             products ? products.slice(CurrentPage * pagesSize, (CurrentPage + 1) * pagesSize).map((item, i) => {
                            //                 const style = {
                            //                     backgroundImage: "url(" + 'https://api.framesdata.com/api/images?auth=' + props.token + item.img + ")",
                            //                     backgroundPosition: 'center',
                            //                     backgroundSize: 'contain',
                            //                     backgroundColor: '#fff',
                            //                     border: '0px',
                            //                     width: '100px',
                            //                     height: '50px',
                            //                 }
                            //                 return (
                            //                     <tr>
                            //                         {/*<td scope="row" className="img-td">*/}
                            //                         {/*<img*/}
                            //                         {/*    src={'https://api.framesdata.com/api/images?auth=' + props.token + item.img}*/}
                            //                         {/*    alt=""*/}
                            //                         {/*    className="custom-img-frameFeatures"*/}
                            //                         {/*    id={`popover-${i}`}*/}
                            //                         {/*/>*/}
                            //
                            //                         {/*<Button type="button"*/}
                            //                         {/*        id={`popover-${i}`}*/}
                            //                         {/*        style={style}>*/}
                            //                         {/*</Button>*/}
                            //
                            //                         {/*<UncontrolledPopover trigger="focus" placement="right" target={`popover-${i}`}>*/}
                            //                         {/*    <PopoverBody>*/}
                            //                         {/*        <img*/}
                            //                         {/*            src={'https://api.framesdata.com/api/images?auth=' + props.token + item.img}*/}
                            //                         {/*            alt=""*/}
                            //                         {/*            className="custom-img-frameFeatures" style={custom_img_size}*/}
                            //                         {/*        />*/}
                            //                         {/*    </PopoverBody>*/}
                            //                         {/*</UncontrolledPopover>*/}
                            //                         {/*</td>*/}
                            //
                            //                         {/*<td>{item.name}</td>*/}
                            //                         {/*<td>{item.sku}</td>*/}
                            //                         {/*<td>{item.group}</td>*/}
                            //                         {/*<td>{item.modified}</td>*/}
                            //                         {/*{item.enable ?*/}
                            //                         {/*    <td><i className="far fa-eye cu-fa-eye-color f-20"*/}
                            //                         {/*           onClick={(e) => {*/}
                            //                         {/*               props.showProduct('hide', item.sku)*/}
                            //                         {/*           }}/></td>*/}
                            //                         {/*    :*/}
                            //                         {/*    <td><i className="far fa-eye-slash cu-fa-eye-color-hide f-20"*/}
                            //                         {/*           onClick={(e) => {*/}
                            //                         {/*               props.showProduct('show', item.sku)*/}
                            //                         {/*           }}/></td>*/}
                            //                         {/*}*/}
                            //                         {/*{!item.favorite ?*/}
                            //                         {/*    <td><i stle={{color:'#bcbcbc'}} className="far fa-thumbtack cu-fa-thumbtack-color f-20"*/}
                            //                         {/*           onClick={(e) => {*/}
                            //                         {/*                 props.showProduct('add_favorite', item.sku)*/}
                            //                         {/*           }}/></td>*/}
                            //                         {/*    :*/}
                            //                         {/*    <td><i style={{color:'#49a8fe'}} className="fas fa-thumbtack cu-fa-thumbtack-color-trans f-20"*/}
                            //                         {/*           onClick={(e) => {*/}
                            //                         {/*               props.showProduct('remove_favorite', item.sku)*/}
                            //                         {/*           }}/></td>*/}
                            //
                            //
                            //                         {/*}*/}
                            //                     </tr>
                            //                 )
                            //             }) : ''}
                            //         </tbody>
                            //     </table>
                            // </div>
                        }

                        {/*<div className="row">*/}
                        {/*    <div className="col-md-12 mt-3 mb-3 text-center">*/}
                        {/*        <Pagination aria-label="Page navigation"*/}
                        {/*                    className="pagination  justify-content-center  pagination-primary">*/}

                        {/*            <PaginationItem disabled={CurrentPage <= 0}>*/}
                        {/*                <PaginationLink onClick={(e) => {*/}
                        {/*                    e.preventDefault();*/}
                        {/*                    handleClick(CurrentPage - 1)*/}
                        {/*                }} href="#">*/}
                        {/*                    Prev*/}
                        {/*                </PaginationLink>*/}
                        {/*            </PaginationItem>*/}

                        {/*            {[...Array(pagesCount)].map((page, i) => {*/}
                        {/*                    if (CurrentPage + 3 >= i && CurrentPage - 3 <= i) {*/}
                        {/*                        return <PaginationItem active={i === CurrentPage} key={i}>*/}
                        {/*                            <PaginationLink onClick={e => {*/}
                        {/*                                e.preventDefault();*/}
                        {/*                                handleClick(i)*/}
                        {/*                            }} href="#">*/}
                        {/*                                {i + 1}*/}
                        {/*                            </PaginationLink>*/}
                        {/*                        </PaginationItem>*/}
                        {/*                    }*/}
                        {/*                }*/}
                        {/*            )}*/}

                        {/*            <PaginationItem disabled={CurrentPage >= pagesCount - 1}>*/}
                        {/*                <PaginationLink last onClick={(e) => {*/}
                        {/*                    e.preventDefault();*/}
                        {/*                    handleClick(CurrentPage + 1)*/}
                        {/*                }} href="#">*/}
                        {/*                    Next*/}
                        {/*                </PaginationLink>*/}
                        {/*            </PaginationItem>*/}

                        {/*        </Pagination>*/}

                        {/*    </div>*/}
                        {/*</div>*/}

                    </div>
                </div>
            </div>
        </Fragment>
    );

};


class FramesFeaturesComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loader: true,
            token: '',
            product: [],
            data: [],
            featureArray: [],
        }
    }

    componentWillMount() {

        var user_id = localStorage.getItem('user_id');
        if (!user_id) {
            this.props.history.push('/login')
        } else {
            getUser(user_id).then(res => {
                console.log("hello = " + res.data.productdata);
                var productData = res.data.productdata;

                this.props.getList(productData[0].products)
                this.props.filterBrand(productData[0].filterbrands);
                this.props.filterGender(productData[0].filterGender);
                this.props.filterAge(productData[0].filterAge);
                this.props.filterMaterial(productData[0].filterMaterial);
                this.props.filterShape(productData[0].filterShape);
                if (productData[0].filterGroup.includes('Sunglasses')) {
                    const index = productData[0].filterGroup.indexOf('Sunglasses');
                    if (index > -1) {
                        var group = productData[0].filterGroup.splice(index, 1);
                    }
                    console.log(group)
                }
                this.props.filterGroup(productData[0].filterGroup);
                this.props.filterRim(productData[0].filterRim);

                setTimeout(() => {
                    this.setState({
                        product: productData[0],
                    })
                }, 2000)

            })
            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                .then(response => response.json())
                .then((data) => {
                    let token = data.Auth.AuthorizationTicket;
                    this.setState({
                        token: token,
                        loader: false,
                    })
                })
        }

    }

    updateProduct = (action, sku) => {
        var user_id = localStorage.getItem('user_id');
        if (action == "show") {
            showProduct(user_id, sku)
                .then(res => {
                    console.log(res.products);
                    this.props.getList(res.products)
                    this.setState({
                        product: res.data,
                    })
                })
        } else if (action == "hide") {

            hideProduct(user_id, sku)
                .then(res => {
                    console.log(res);
                    this.props.getList(res.products)
                    this.setState({
                        product: res
                    })
                })
        } else if (action == "add_favorite") {

            favoriteProduct(user_id, sku)
                .then(res => {
                    console.log(res);
                    this.props.getList(res.products)
                    this.setState({
                        product: res
                    })
                })
        } else if (action == "remove_favorite") {

            RemoveFavoriteProduct(user_id, sku)
                .then(res => {
                    console.log(res);
                    this.props.getList(res.products)
                    this.setState({
                        product: res
                    })
                })
        }
    }


    showLoader = () => {
        this.setState({
            loader: true
        })
    }

    hideLoader = () => {
        this.setState({
            loader: false
        })
    }

    render() {
        return (
            <div>
                <div loader="thisONe"
                     className={`custom-loader-css loader-wrapper ${this.state.loader ? '' : 'loderhide'}`}>
                    <div className="loader bg-white">
                        <div className="whirly-loader"></div>
                    </div>
                </div>

                <FramesFeatures showLoader={this.showLoader}
                                hideLoader={this.hideLoader}
                                dummyData={this.state.featureArray}
                                product={this.state.product}
                                token={this.state.token}
                                showProduct={this.updateProduct}
                />

            </div>

        );
    }


}

const mapStateToProps = (state) => ({
    products: state.data.productItems,
    data: state.data.productItems
});

export default connect(
    mapStateToProps,
    {getList, filterBrand, filterGender, filterMaterial, filterAge, filterShape, filterRim, filterGroup}
)(FramesFeaturesComponent)
