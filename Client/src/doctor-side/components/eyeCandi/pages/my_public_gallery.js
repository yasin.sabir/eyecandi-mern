import React, {Fragment, useState, useEffect,Component} from 'react';
import {useSelector, useDispatch,connect} from 'react-redux';
import Breadcrumb from '../../common/breadcrumb';
import {Grid, List, ChevronDown, User} from 'react-feather';
import banner from '../../../assets/images/ecommerce/banner.jpg';
import errorImg from '../../../assets/images/search-not-found.png';
import Modal from 'react-responsive-modal';
import {getColors, getVisibleproducts} from '../../../services';
import Carousal from '../../ecommerce-app/filters/carousal';
import avatar from '../../../assets/images/image-placeholder/1.jpg';
import AllFilters from '../../ecommerce-app/filters/allfilters';
import {
    filterAge,
    filterBrand,
    filterColor,
    filterGender,
    filterMaterial,
    filterPrice, filterRim, filterShape,
    getList
} from '../../../actions/ecommerce.actions';
import {TabContent, TabPane, Nav, NavItem, NavLink, Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import logo from '../../../assets/images/eyeCandi/logo-ss.png';
import one from "../../../assets/images/image-placeholder/1.jpg";
import * as credentials from "../../../constant/framesAPI";
import jwt_decode from 'jwt-decode'
import {getUser} from "../functions";
import Loader from "../../common/loader";
import placeholder from "../../../assets/images/user/Profile_avatar_placeholder_large.png";
import {getURL} from "../functions";
import {useHistory} from "react-router";
import LazyLoad from "react-lazy-load";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import SingleProductComponent from "./singleProductComponent";


const PublicGallery = (props) => {

    // const route =props;
    // console.log(props);
    const data = useSelector(content => content.data.productItems);
    const colors = getColors(data);
    const filters = useSelector(content => content.filters);
    const products = getVisibleproducts(data, filters);
    let practiceName = '';
    const symbol = useSelector(content => content.data.symbol);
    const searchProducts = useSelector(content => content.data.products);
    const dispatch = useDispatch();

    const [singleProduct, setSingleProduct] = useState([]);
    const [searchKeyword, setSearchKeyword] = useState('');
    const [layoutColumns, setLayoutColumns] = useState(3);
    const [sidebaron, setSidebaron] = useState(true);
    const [stock, setStock] = useState('');
    const [quantity, setQuantity] = useState(1);
    const [open, setOpen] = useState(false);
    const [filterSidebar, setFilterSidebar] = useState(true);
    const [apiToken , setapiToken]  = useState('');
    const [publicUrl , setpublicUrl] = useState();

    const [productData, setproductData] = useState([]);
    const [pagesSize, setpagesSize] = useState(22);
    const [CurrentPage, setCurrentPage] = useState(0);
    const [pagesCount, setpagesCount] = useState(0);



    const onCloseModal = () => {
        setOpen(false)
    };

    useEffect((match) => {
        setpagesCount(Math.ceil(products.length/pagesSize));
    });

    const handleClick = (index) => {
        props.showLoader();
        setCurrentPage(index);
        window.scrollTo(0, 300)
        setTimeout(()=> {
            props.hideLoader();
        },1500)

    };

    const filterSortFunc = (event) => {
        dispatch({type: 'SORT_BY', sort_by: event})
    }

    const gridLayout = () => {
        document.querySelector(".product-wrapper-grid").classList.remove("list-view");
        var elems = document.querySelector(".gridRow").childNodes;
        [].forEach.call(elems, function (el) {
            el.className = '';
            el.classList.add('col-xl-3');
            el.classList.add('col-sm-6');
            el.classList.add('xl-4')
        });
    }

    //Grid Layout View
    const listLayout = () => {
        document.querySelector(".product-wrapper-grid").classList.add("list-view");
        var elems = document.querySelector(".gridRow").childNodes;
        [].forEach.call(elems, function (el) {
            el.className = '';
            el.classList.add('col-xl-12');
        });
    }

    // Layout Column View
    const LayoutView = (layoutColumns) => {
        if (!document.querySelector(".product-wrapper-grid").classList.contains("list-view")) {
            var elems = document.querySelector(".gridRow").childNodes;
            [].forEach.call(elems, function (el) {
                el.className = '';
                el.classList.add('col-xl-' + layoutColumns);
            });
        }
    }

    let history = useHistory();
    const onClickDetailPage = (product) => {
        const id = product.sku;
        history.push(`${process.env.PUBLIC_URL}/`+props.publicURL+`/product-detail/${id}`)
    }

    const onClickFilter = () => {
        if (sidebaron) {
            setSidebaron(false)
            document.querySelector(".product-wrapper").classList.add('sidebaron');
        } else {
            setSidebaron(true)
            document.querySelector(".product-wrapper").classList.remove('sidebaron');
        }
    }

    const minusQty = () => {
        if (quantity > 1) {
            setStock('InStock')
            setQuantity(quantity - 1)
        }
    }

    const onOpenModal = (productId) => {
        setOpen(true);
        products.map((product, i) => {
            if (product.id === productId) {
                setSingleProduct(product)
            }
        })
    };

    const plusQty = () => {
        if (quantity >= 1) {
            setQuantity(quantity + 1)
        } else {
            setStock('Out of Stock !')
        }
    }

    const changeQty = (e) => {
        setQuantity(parseInt(e.target.value))
    }

    const addcart = (product, qty) => {
        dispatch({type: 'ADD_TO_CART', payload: {product, qty}})
        //props.history.push(`${process.env.PUBLIC_URL}/ecommerce/cart/${product.id}`);
    }

    const addWishList = (product) => {
        dispatch({type: 'ADD_TO_WISHLIST', payload: product});
       // props.history.push(`${process.env.PUBLIC_URL}/ecommerce/wishlist/${product.id}`);
    }

    const handleSearchKeyword = (keyword) => {
        handleClick(0)
        setSearchKeyword(keyword)
        dispatch({type: 'SEARCH_BY', search: keyword})
    }

    const colorHandle = (event, color) => {
        var elems = document.querySelectorAll(".color-selector ul li");
        [].forEach.call(elems, function (el) {
            el.classList.remove("active");
        });
        event.target.classList.add('active');
        filterColor(color);
    }

    const [activeTab, setActiveTab] = useState('1');

    const handleActiveTab = val => () =>{
        if (val == 1) {
            var list = document.getElementsByClassName('checkbox_animated');
            for (let item of list) {
                if(item.checked)
                {
                    item.click();
                }
            }
            console.log(props);
            props.fun.getList(props.product.products)
            props.fun.filterBrand(props.product.filterbrands);
            props.fun.filterGender(props.product.filterGender);
            props.fun.filterAge(props.product.filterAge);
            props.fun.filterMaterial(props.product.filterMaterial);
            props.fun.filterShape(props.product.filterShape);
            props.fun.filterRim(props.product.filterRim);
            // props.getlist(props.product);
            // this.props.history.push('/public/'+this.state.public_gallery_url)
            setActiveTab(1);
        } else if (val == 2) {
            var list = document.getElementsByClassName('checkbox_animated');
            for (let item of list) {
                if(item.checked)
                {
                    item.click();
                }
            }
            if(document.getElementById("Unisex").checked)
                document.getElementById("Unisex").click();
            if(document.getElementById("Female").checked)
                document.getElementById("Female").click();
            if(!document.getElementById("Male").checked)
                document.getElementById("Male").click();
            setActiveTab(2);
        } else if( val == 3){
            var list = document.getElementsByClassName('checkbox_animated');
                for (let item of list) {
                    if(item.checked)
                    {
                        item.click();
                    }
                }
            if(document.getElementById("Unisex").checked)
                document.getElementById("Unisex").click();
            if(document.getElementById("Male").checked)
                document.getElementById("Male").click();
            if(!document.getElementById("Female").checked)
                document.getElementById("Female").click();
            setActiveTab(3);
        }else if( val == 4){
            var list = document.getElementsByClassName('checkbox_animated');
            for (let item of list) {
                if(item.checked)
                {
                    item.click();
                }
            }
            if(document.getElementById("Youth/Teen") !== null && !document.getElementById("Youth/Teen").checked)
                document.getElementById('Youth/Teen').click();
                if(document.getElementById("Child") !== null && document.getElementById("Child").checked)
                document.getElementById('Child').click();
            if(document.getElementById("Adult") !== null && document.getElementById("Adult").checked)
                document.getElementById('Adult').click();
            setActiveTab(4);
        }else{
            setActiveTab(1);
        }
    };

    return (
        <Fragment>


           <div className="custom-public-gallery-margin">
                <div className="container-fluid pl-0 pr-0">
                    <div className="user-profile social-app-profile">
                        <div className="row">
                            {/* <!-- user profile first-style start--> */}
                            <div className="col-sm-12">
                                <div className="card hovercard text-center">
                                    <div className="cardheader socialheader custom-bg-color" style={{height: '250px'}}>
                                    </div>
                                    <div className="user-image">
                                        <div className="avatar"><img alt="user" src={props.profilepic}/></div>
                                    </div>
                                    <div className="info p-0">
                                        <Nav tabs className="tabs-scoial borderb-tab-primary market-tabs">
                                            <NavItem className="nav nav-tabs" id="myTab all" role="tablist">
                                                <NavLink id="all-eyewear" style={{borderBottom: "none !important"}} className={ activeTab == '1' ? 'custom-nav-link active' : ''}
                                                         onClick={handleActiveTab('1')}>
                                                    All Eyewear
                                                </NavLink>
                                            </NavItem>
                                            <NavItem className="nav nav-tabs " id="myTab men" role="tablist">
                                                <NavLink  className={activeTab == '2' ? 'custom-nav-link active' : ''}
                                                         onClick={handleActiveTab('2')}>
                                                    Mens
                                                </NavLink>
                                            </NavItem>
                                            <li className="nav-item">
                                                <div className="user-designation"></div>
                                                <div className="title"><a href="#all-eyewear">{props.name}</a>
                                                </div>
                                                {/*<div className="desc mt-2">general manager</div>*/}
                                            </li>
                                            <NavItem className="nav nav-tabs" id="myTab women" role="tablist">
                                                <NavLink className={activeTab == '3' ? 'custom-nav-link active' : ''}
                                                         onClick={handleActiveTab('3')}>
                                                    Womens
                                                </NavLink>
                                            </NavItem>
                                            <NavItem className="nav nav-tabs" id="myTab youth" role="tablist">
                                                <NavLink className={activeTab == '4' ? 'custom-nav-link active' : ''}
                                                         onClick={handleActiveTab('4')}>
                                                    Youth
                                                </NavLink>
                                            </NavItem>
                                        </Nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container-fluid product-wrapper sidebaron pl-0 pr-0">
                    <div className="product-grid">
                        <div className="feature-products">
                            <div className="row">
                                <div className="col-md-6 products-total mb-3">
                                    <div className="square-product-setting d-inline-block">
                                        <a className="icon-grid grid-layout-view ml-0" onClick={gridLayout}
                                           data-original-title="" title="">
                                            <Grid/>
                                        </a>
                                    </div>
                                    <div className="square-product-setting d-inline-block">
                                        <a className="icon-grid m-0 list-layout-view" onClick={listLayout}
                                           data-original-title="" title="">
                                            <List/>
                                        </a>
                                    </div>
                                    <span className="d-none-productlist filter-toggle"
                                          onClick={() => setFilterSidebar(!filterSidebar)}>
                                      <h6 className="mb-0">Filters
                                          <span className="ml-2">
                                          <ChevronDown className="toggle-data"/>
                                        </span>
                                      </h6>
                                    </span>
                                    <div className="grid-options d-inline-block">
                                        <ul>
                                            <li>
                                                <a className="product-2-layout-view" onClick={() => LayoutView(6)}
                                                   data-original-title="" title="">
                                                    <span className="line-grid line-grid-1 bg-primary"></span>
                                                    <span className="line-grid line-grid-2 bg-primary"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a className="product-3-layout-view" onClick={() => LayoutView(4)}
                                                   data-original-title="" title="">
                                                    <span className="line-grid line-grid-3 bg-primary"></span>
                                                    <span className="line-grid line-grid-4 bg-primary"></span>
                                                    <span className="line-grid line-grid-5 bg-primary"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a className="product-4-layout-view" onClick={() => LayoutView(3)}
                                                   data-original-title="" title="">
                                                    <span className="line-grid line-grid-6 bg-primary"></span>
                                                    <span className="line-grid line-grid-7 bg-primary"></span>
                                                    <span className="line-grid line-grid-8 bg-primary"></span>
                                                    <span className="line-grid line-grid-9 bg-primary"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a className="product-6-layout-view" onClick={() => LayoutView(2)}
                                                   data-original-title="" title="">
                                                    <span className="line-grid line-grid-10 bg-primary"></span>
                                                    <span className="line-grid line-grid-11 bg-primary"></span>
                                                    <span className="line-grid line-grid-12 bg-primary"></span>
                                                    <span className="line-grid line-grid-13 bg-primary"></span>
                                                    <span className="line-grid line-grid-14 bg-primary"></span>
                                                    <span className="line-grid line-grid-15 bg-primary"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>


                            <div className="row">
                                <div className="col-sm-3">
                                    <div className={`product-sidebar ${filterSidebar ? '' : 'open'}`}>
                                        <div className="filter-section">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h6 className="mb-0 f-w-600">Filters
                                                        <span className="pull-right">
                                                            <i className="fa fa-chevron-down toggle-data" onClick={onClickFilter}></i>
                                                        </span>
                                                    </h6>
                                                </div>
                                                <div className="left-filter">
                                                    <div className="card-body filter-cards-view animate-chk">
                                                        <AllFilters data={props.product} handleClick={handleClick}/>
                                                        {/*<Carousal/>*/}
                                                        {/*<div className="product-filter text-center">*/}
                                                        {/*        <img className="img-fluid banner-product" src={banner} alt=""*/}
                                                        {/*             data-original-title="" title=""/>*/}
                                                        {/*</div>*/}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-6 col-sm-12">
                                    <form>
                                        <div className="form-group m-0">
                                            <input
                                                className="form-control"
                                                type="text"
                                                placeholder="search"
                                                defaultValue={searchKeyword}
                                                onChange={(e) => handleSearchKeyword(e.target.value)}
                                            />
                                        </div>
                                    </form>
                                </div>

                                <div className="col-md-3 text-right">
                                    {/*<span className="f-w-600 m-r-10">Showing Products 1 - 24 Of 200 Results</span>*/}
                                    <div className="select2-drpdwn-product select-options">
                                        <select className="form-control" style={{height:'50px'}} onChange={(e) => filterSortFunc(e.target.value)}>
                                            <option value="">Sorting items</option>
                                            <option value="Newest">Newest Items</option>
                                            <option value="AscOrder">Sort By Name: A To Z</option>
                                            <option value="DescOrder">Sort By Name: Z To A</option>
                                        </select>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div className="product-wrapper-grid">
                            {products.length <= 0 ?
                                <div className="search-not-found text-center">
                                    <div>
                                        <img className="img-fluid second-search" src={errorImg}/>
                                        <p>Sorry, We didn't find any results matching this search</p>
                                    </div>
                                </div>
                                :
                                <div className="row gridRow">
                                    {products ? products.slice(CurrentPage * pagesSize, (CurrentPage + 1) * pagesSize).map((item, i) => {
                                        // console.log(products)
                                        return <div
                                            className={`${layoutColumns === 3 ? 'col-xl-4 col-sm-4 xl-4 col-grid-box' : 'col-xl-' + layoutColumns}`}
                                            key={i}>
                                            {/*<LazyLoad>*/}
                                            <SingleProductComponent item={item} onClickDetailPage={onClickDetailPage} token={props.token}/>
                                        </div>}
                                    ) : ''}
                                    {/*<Modal open={open} onClose={onCloseModal}>*/}
                                    {/*    <div className="modal-body">*/}
                                    {/*        <div className="product-modal row">*/}
                                    {/*            <div className="product-img col-md-6">*/}
                                    {/*                <img className="img-fluid" src={singleProduct.img} alt=""/></div>*/}
                                    {/*            <div className="product-details col-md-6 text-left">*/}
                                    {/*                <h3>{singleProduct.name}</h3>*/}
                                    {/*                <h5><strong>Gender:</strong> {singleProduct.category}</h5>*/}
                                    {/*                /!*<div className="product-price">*!/*/}
                                    {/*                /!*    <del>{symbol}{singleProduct.discountPrice}*!/*/}
                                    {/*                /!*    </del>*!/*/}
                                    {/*                /!*    {symbol}{singleProduct.price}*!/*/}
                                    {/*                /!*</div>*!/*/}
                                    {/*                <div className="product-view">*/}
                                    {/*                    <h6 className="f-w-600">Product Details</h6>*/}
                                    {/*                    /!*<p className="mb-0">{singleProduct.discription}</p>*!/*/}
                                    {/*                </div>*/}
                                    {/*                <div className="product-size">*/}
                                    {/*                    <ul>*/}
                                    {/*                        <li>*/}
                                    {/*                            <button className="btn btn-outline-light" type="button">M*/}
                                    {/*                            </button>*/}
                                    {/*                        </li>*/}
                                    {/*                        <li>*/}
                                    {/*                            <button className="btn btn-outline-light" type="button">L*/}
                                    {/*                            </button>*/}
                                    {/*                        </li>*/}
                                    {/*                        <li>*/}
                                    {/*                            <button className="btn btn-outline-light" type="button">Xl*/}
                                    {/*                            </button>*/}
                                    {/*                        </li>*/}
                                    {/*                    </ul>*/}
                                    {/*                </div>*/}
                                    {/*                <div className="product-qnty">*/}
                                    {/*                    <h6 className="f-w-600">Quantity</h6>*/}
                                    {/*                    <fieldset className="qty-box">*/}
                                    {/*                        <div className="input-group">*/}
                                    {/*                              <span className="input-group-prepend">*/}
                                    {/*                                <button type="button" className="btn quantity-left-minus" onClick={minusQty}*/}
                                    {/*                                        data-type="minus" data-field="">*/}
                                    {/*                                  <i className="fa fa-minus"></i>*/}
                                    {/*                                </button>*/}
                                    {/*                              </span>*/}
                                    {/*                            <input type="text" name="quantity" value={quantity}*/}
                                    {/*                                   onChange={changeQty}*/}
                                    {/*                                   className="form-control input-number"/>*/}
                                    {/*                            <span className="input-group-append">*/}
                                    {/*                                <button type="button" className="btn quantity-right-plus" onClick={plusQty}*/}
                                    {/*                                        data-type="plus" data-field="">*/}
                                    {/*                                  <i className="fa fa-plus"></i>*/}
                                    {/*                                </button>*/}
                                    {/*                            </span>*/}
                                    {/*                        </div>*/}
                                    {/*                    </fieldset>*/}
                                    {/*                    <div className="addcart-btn">*/}
                                    {/*                        <a className="btn btn-success txt-white"*/}
                                    {/*                           onClick={() => onClickDetailPage(singleProduct)}>*/}
                                    {/*                            View Details*/}
                                    {/*                        </a>*/}
                                    {/*                    </div>*/}
                                    {/*                </div>*/}
                                    {/*            </div>*/}
                                    {/*        </div>*/}
                                    {/*    </div>*/}
                                    {/*</Modal>*/}
                                </div>
                            }
                            <div className="row">
                                <div className="col-md-12 mt-3 mb-3 text-center">
                                    <Pagination aria-label="Page navigation"
                                                className="pagination  justify-content-center  pagination-primary">

                                        <PaginationItem disabled={CurrentPage <= 0}>
                                            <PaginationLink onClick={(e) => {e.preventDefault();handleClick( CurrentPage - 1)}} href="#">
                                                Prev
                                            </PaginationLink>
                                        </PaginationItem>

                                        {[...Array(pagesCount)].map((page, i) => {
                                                if(CurrentPage +3 >= i && CurrentPage - 3 <= i) {
                                                    return <PaginationItem active={i === CurrentPage} key={i}>
                                                        <PaginationLink onClick={e => {
                                                            e.preventDefault();
                                                            handleClick(i)
                                                        }} href="#">
                                                            {i + 1}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                }
                                            }
                                        )}

                                        <PaginationItem disabled={CurrentPage >= pagesCount - 1}>
                                            <PaginationLink last onClick={(e) => {e.preventDefault();handleClick(CurrentPage + 1)}} href="#">
                                                Next
                                            </PaginationLink>
                                        </PaginationItem>

                                    </Pagination>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>

        </Fragment>
    );
};


// export default FramesGallery;
class FramesGalleryComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            practiceName : '',
            profilePic   : avatar,
            public_gallery_url : '',
            loader : true,
            product: []
        }
    }
    componentWillMount() {
        getURL(this.props.match.params.publicUrl).then(res => {
            if (res.data.error) {
                this.props.history.push('/404')
            } else {
                var user_id = res.data.id;
                var productData = res.data.productdata;
                this.props.getList(productData[0].products)
                this.props.filterBrand(productData[0].filterbrands);
                this.props.filterGender(productData[0].filterGender);
                this.props.filterAge(productData[0].filterAge);
                this.props.filterMaterial(productData[0].filterMaterial);
                this.props.filterShape(productData[0].filterShape);
                this.props.filterRim(productData[0].filterRim);
                setTimeout(() => {
                    this.setState({
                        practiceName : res.data.practice_name,
                        profilePic    : res.data.data[0].profile_pic,
                        public_gallery_url : res.data.public_url,

                        product: productData[0],
                    })
                },2000)

                fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                    .then(response => response.json())
                    .then((data) => {
                        let token = data.Auth.AuthorizationTicket;
                        console.log(token);
                        this.setState({
                            token: token,
                            loader: false
                        })
                    })

            }

        });
    }

    componentDidMount() {
        const filter = localStorage.getItem('filter');
        if(filter) {
            if (filter == 'men') {
                setTimeout(() => {
                    document.getElementById('myTab men').children[0].click();
                }, 5000)
            } else if (filter == 'women') {
                setTimeout(() => {
                    document.getElementById('myTab women').children[0].click();
                }, 5000)
            } else if (filter == 'youth') {
                setTimeout(() => {
                    document.getElementById('myTab youth').children[0].click();
                }, 5000)
            }
        }
    }

    showLoader = () => {
        this.setState({
            loader:true
        })
    }
    hideLoader = () => {
        this.setState({
            loader:false
        })
    }

    render() {
        return (
            <div>
                <div className={`public-loader-page loader-wrapper ${this.state.loader ? '' : 'loderhide'}`} >
                    <div className="loader bg-white">
                        <div className="whirly-loader"> </div>
                    </div>
                </div>
                <PublicGallery showLoader={this.showLoader} hideLoader={this.hideLoader} token={this.state.token} name={this.state.practiceName} product={this.state.product} profilepic={this.state.profilePic} publicURL={this.state.public_gallery_url} fun={this.props}/>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    products: state.data.productItems,
    practiceName : '',
    profilePic   : ''
})

export default connect(
    mapStateToProps,
    {getList,filterBrand,filterGender,filterMaterial,filterAge,filterShape,filterRim}
)(FramesGalleryComponent)
