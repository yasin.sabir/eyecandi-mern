import React, {Fragment, useState, useEffect, Component} from 'react';
import {useSelector, useDispatch, connect} from 'react-redux';
import Breadcrumb from '../../common/breadcrumb';
import {Grid, List, ChevronDown} from 'react-feather';
import banner from '../../../assets/images/ecommerce/banner.jpg';
import errorImg from '../../../assets/images/search-not-found.png';
import Modal from 'react-responsive-modal';
import {getColors, getVisibleproducts} from '../../../services';
import Carousal from '../../ecommerce-app/filters/carousal';
import LazyLoad from 'react-lazy-load';
import AllFilters from '../../ecommerce-app/filters/allfilters';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import SingleProductComponent from "./singleProductComponent";
import {
    filterBrand,
    filterColor,
    filterPrice,
    getList,
    filterRim,
    filterShape,
    filterMaterial,
    filterGender,
    filterAge,
    filterGroup
} from '../../../actions/ecommerce.actions';
import {TabContent, TabPane, Nav, NavItem, NavLink} from 'reactstrap';
import logo from '../../../assets/images/eyeCandi/logo-ss.png';
import one from "../../../assets/images/image-placeholder/1.jpg";
import * as credentials from "../../../constant/framesAPI";
import jwt_decode from 'jwt-decode'
import {getUser} from "../functions";
import Loader from "../../common/loader";
import {Pagination, PaginationItem, PaginationLink} from "reactstrap";
import {useHistory} from "react-router-dom";

const FramesGallery = (props) => {

    const data = useSelector(content => content.data.productItems);
    // console.log('data',data);
    const colors = getColors(data);
    const filters = useSelector(content => content.filters);
    // console.log(filters);
    const products = getVisibleproducts(data, filters);
    // console.log('data',products)
    const symbol = useSelector(content => content.data.symbol);
    const searchProducts = useSelector(content => content.data.products);
    const dispatch = useDispatch();

    const [singleProduct, setSingleProduct] = useState([]);
    const [searchKeyword, setSearchKeyword] = useState('');
    const [layoutColumns, setLayoutColumns] = useState(3);
    const [sidebaron, setSidebaron] = useState(true);
    const [stock, setStock] = useState('');
    const [quantity, setQuantity] = useState(1);
    const [open, setOpen] = useState(false);
    const [filterSidebar, setFilterSidebar] = useState(true);
    const [apiToken, setapiToken] = useState('');
    const [publicUrl, setpublicUrl] = useState();

    const [productData, setproductData] = useState([]);
    const [pagesSize, setpagesSize] = useState(22);
    const [CurrentPage, setCurrentPage] = useState(0);
    const [pagesCount, setpagesCount] = useState(0);
    const onCloseModal = () => {
        setOpen(false)
    };

    useEffect(() => {

        // products.map((product, i) => {
        //    // console.log(product);
        //     if (product.id === productId) {
        //         setSingleProduct(product)
        //     }
        // })


        var user_id = localStorage.getItem('user_id');
        if (user_id) {
            getUser(user_id).then(res => {
                let url = res.data.public_url ? res.data.public_url : '';
                // console.log(res.data.public_url);
                setpublicUrl(url);
            });
        }
        setpagesCount(Math.ceil(products.length / pagesSize));
        // setCurrentPage(0);


    });

    const handleClick = (index) => {
        props.showLoader();
        setCurrentPage(index);
        window.scrollTo(0, 130)
        setTimeout(() => {
            props.hideLoader();
        }, 1500)

    };

    const filterSortFunc = (event) => {
        dispatch({type: 'SORT_BY', sort_by: event})
    }

    const gridLayout = () => {
        document.querySelector(".product-wrapper-grid").classList.remove("list-view");
        var elems = document.querySelector(".gridRow").childNodes;
        [].forEach.call(elems, function (el) {
            el.className = '';
            el.classList.add('col-xl-3');
            el.classList.add('col-sm-6');
            el.classList.add('xl-4')
        });
    }
    //Grid Layout View
    const listLayout = () => {
        document.querySelector(".product-wrapper-grid").classList.add("list-view");
        var elems = document.querySelector(".gridRow").childNodes;
        [].forEach.call(elems, function (el) {
            el.className = '';
            el.classList.add('col-xl-12');
        });
    }

    // Layout Column View
    const LayoutView = (layoutColumns) => {
        if (!document.querySelector(".product-wrapper-grid").classList.contains("list-view")) {
            var elems = document.querySelector(".gridRow").childNodes;
            [].forEach.call(elems, function (el) {
                el.className = '';
                el.classList.add('col-xl-' + layoutColumns);
            });
        }
    }

    let history = useHistory();
    const onClickDetailPage = (product) => {
        const id = product.sku;
        const brand = product.name;
        history.push(`${process.env.PUBLIC_URL}/pages/FramesGallery/product-detail/${brand}/${id}`)
    }

    const onClickFilter = () => {
        if (sidebaron) {
            setSidebaron(false)
            document.querySelector(".product-wrapper").classList.add('sidebaron');
        } else {
            setSidebaron(true)
            document.querySelector(".product-wrapper").classList.remove('sidebaron');
        }
    }

    const minusQty = () => {
        if (quantity > 1) {
            setStock('InStock')
            setQuantity(quantity - 1)
        }
    }

    const onOpenModal = (productId) => {
        setOpen(true);
        products.map((product, i) => {
            if (product.id === productId) {
                setSingleProduct(product)
            }
        })
    };

    const plusQty = () => {
        if (quantity >= 1) {
            setQuantity(quantity + 1)
        } else {
            setStock('Out of Stock !')
        }
    }

    const changeQty = (e) => {
        setQuantity(parseInt(e.target.value))
    }

    const addcart = (product, qty) => {
        dispatch({type: 'ADD_TO_CART', payload: {product, qty}})
        //props.history.push(`${process.env.PUBLIC_URL}/ecommerce/cart/${product.id}`);
    }

    const addWishList = (product) => {
        dispatch({type: 'ADD_TO_WISHLIST', payload: product});
        //props.history.push(`${process.env.PUBLIC_URL}/ecommerce/wishlist/${product.id}`);
    }

    const handleSearchKeyword = (keyword) => {
        setSearchKeyword(keyword)
        dispatch({type: 'SEARCH_BY', search: keyword})
    }

    const colorHandle = (event, color) => {
        var elems = document.querySelectorAll(".color-selector ul li");
        [].forEach.call(elems, function (el) {
            el.classList.remove("active");
        });
        event.target.classList.add('active');
        filterColor(color);
    }

    const [activeTab, setActiveTab] = useState('1');
    const [color, setcolor] = useState([]);
    return (
        <Fragment>
            <Breadcrumb publicURL={publicUrl} title="Frames" parent="Frames"/>

            <div className="container-fluid product-wrapper">{/*sidebaron*/}

            <div className="product-grid">
                    <div className="feature-products">
                        <div className="row">
                            <div className="col-md-6 products-total mb-3">
                                <div className="square-product-setting d-inline-block">
                                    <a className="icon-grid grid-layout-view " onClick={gridLayout}
                                       data-original-title="" title="">
                                        <Grid/>
                                    </a>
                                </div>
                                <div className="square-product-setting d-inline-block">
                                    <a className="icon-grid m-0 list-layout-view" onClick={listLayout}
                                       data-original-title="" title="">
                                        <List/>
                                    </a>
                                </div>
                                <span className="d-none-productlist filter-toggle"
                                      onClick={() => setFilterSidebar(!filterSidebar)}>
                                  <h6 className="mb-0">Filters
                                      <span className="ml-2">
                                      <ChevronDown className="toggle-data"/>
                                    </span>
                                  </h6>
                                </span>
                                <div className="grid-options d-inline-block">
                                    <ul>
                                        <li>
                                            <a className="product-2-layout-view" onClick={() => LayoutView(6)}
                                               data-original-title="" title="">
                                                <span className="line-grid line-grid-1 bg-primary"></span>
                                                <span className="line-grid line-grid-2 bg-primary"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a className="product-3-layout-view" onClick={() => LayoutView(4)}
                                               data-original-title="" title="">
                                                <span className="line-grid line-grid-3 bg-primary"></span>
                                                <span className="line-grid line-grid-4 bg-primary"></span>
                                                <span className="line-grid line-grid-5 bg-primary"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a className="product-4-layout-view" onClick={() => LayoutView(3)}
                                               data-original-title="" title="">
                                                <span className="line-grid line-grid-6 bg-primary"></span>
                                                <span className="line-grid line-grid-7 bg-primary"></span>
                                                <span className="line-grid line-grid-8 bg-primary"></span>
                                                <span className="line-grid line-grid-9 bg-primary"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a className="product-6-layout-view" onClick={() => LayoutView(2)}
                                               data-original-title="" title="">
                                                <span className="line-grid line-grid-10 bg-primary"></span>
                                                <span className="line-grid line-grid-11 bg-primary"></span>
                                                <span className="line-grid line-grid-12 bg-primary"></span>
                                                <span className="line-grid line-grid-13 bg-primary"></span>
                                                <span className="line-grid line-grid-14 bg-primary"></span>
                                                <span className="line-grid line-grid-15 bg-primary"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>


                        <div className="row">
                            <div className="col-sm-3">
                                <div className={`product-sidebar ${filterSidebar ? '' : 'open'}`}>
                                    <div className="filter-section">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6 className="mb-0 f-w-600">Filters
                                                    <span className="pull-right">
                                                        <i className="fa fa-chevron-down toggle-data"
                                                           onClick={onClickFilter}></i>
                                                    </span>
                                                </h6>
                                            </div>
                                            <div className="left-filter">
                                                <div className="card-body filter-cards-view animate-chk">
                                                    <AllFilters data={props.product} handleClick={handleClick}/>
                                                    {/*<Carousal/>*/}
                                                    {/*<div className="product-filter text-center">*/}
                                                    {/*        <img className="img-fluid banner-product" src={banner} alt=""*/}
                                                    {/*             data-original-title="" title=""/>*/}
                                                    {/*</div>*/}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-sm-12">
                                <form>
                                    <div className="form-group m-0">
                                        <input
                                            className="form-control"
                                            type="text"
                                            placeholder="search"
                                            defaultValue={searchKeyword}
                                            onChange={(e) => handleSearchKeyword(e.target.value)}
                                        />
                                    </div>
                                </form>
                            </div>

                            <div className="col-md-3 text-right">
                                {/*<span className="f-w-600 m-r-10">Showing Products 1 - 24 Of 200 Results</span>*/}
                                <div className="select2-drpdwn-product select-options">
                                    <select className="form-control" style={{height: '50px'}}
                                            onChange={(e) => filterSortFunc(e.target.value)}>
                                        <option value="">Sorting items</option>
                                        <option value="Newest">Newest Items</option>
                                        <option value="AscOrder">Sort By Name: A To Z</option>
                                        <option value="DescOrder">Sort By Name: Z To A</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div className="product-wrapper-grid">
                        {products.length <= 0 ?
                            <div className="search-not-found text-center">
                                <div>
                                    <img className="img-fluid second-search" src={errorImg}/>
                                    <p>Sorry, We didn't find any results matching this search</p>
                                </div>
                            </div>
                            :
                            <div className="row gridRow">
                                {products ? products.slice(CurrentPage * pagesSize, (CurrentPage + 1) * pagesSize).map((item, i) => {
                                        {/*{products ? products.map((item, i) => {*/
                                        }
                                        return <div
                                            className={`${layoutColumns === 3 ? 'col-sm-3 col-md-3 col-xl-3 col-grid-box' : 'col-xl-' + layoutColumns}`}
                                            key={i}>
                                            {/*<LazyLoad>*/}
                                            <SingleProductComponent item={item} onClickDetailPage={onClickDetailPage}
                                                                    token={props.token}/>

                                        </div>
                                    }
                                ) : ''}

                            </div>

                        }

                        <div className="row">
                            <div className="col-md-12 mt-3 mb-3 text-center">
                                <Pagination aria-label="Page navigation"
                                            className="pagination  justify-content-center  pagination-primary">

                                    <PaginationItem disabled={CurrentPage <= 0}>
                                        <PaginationLink onClick={(e) => {
                                            e.preventDefault();
                                            handleClick(CurrentPage - 1)
                                        }} href="#">
                                            Prev
                                        </PaginationLink>
                                    </PaginationItem>

                                    {[...Array(pagesCount)].map((page, i) => {
                                            if (CurrentPage + 3 >= i && CurrentPage - 3 <= i) {
                                                return <PaginationItem active={i === CurrentPage} key={i}>
                                                    <PaginationLink onClick={e => {
                                                        e.preventDefault();
                                                        handleClick(i)
                                                    }} href="#">
                                                        {i + 1}
                                                    </PaginationLink>
                                                </PaginationItem>
                                            }
                                        }
                                    )}

                                    <PaginationItem disabled={CurrentPage >= pagesCount - 1}>
                                        <PaginationLink last onClick={(e) => {
                                            e.preventDefault();
                                            handleClick(CurrentPage + 1)
                                        }} href="#">
                                            Next
                                        </PaginationLink>
                                    </PaginationItem>

                                </Pagination>

                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </Fragment>
    );
};

// export default FramesGallery;
class FramesGalleryComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: true,
            token: '',
            product: []
        }
    }

    componentWillMount() {

        var user_id = localStorage.getItem('user_id');
        if (!user_id) {
            this.props.history.push('/login')
        } else {
            getUser(user_id).then(res => {
                // console.log("hello = "+res.data.productdata);
                var productData = res.data.productdata;

                this.props.getList(productData[0].products)
                this.props.filterBrand(productData[0].filterbrands);
                this.props.filterGender(productData[0].filterGender);
                this.props.filterAge(productData[0].filterAge);
                this.props.filterMaterial(productData[0].filterMaterial);
                this.props.filterShape(productData[0].filterShape);
                if(productData[0].filterGroup.includes('Sunglasses'))
                {
                    const index = productData[0].filterGroup.indexOf('Sunglasses');
                    if (index > -1) {
                       var group = productData[0].filterGroup.splice(index, 1);
                    }
                    console.log(group)
                }
                this.props.filterGroup(productData[0].filterGroup);
                this.props.filterRim(productData[0].filterRim);

                setTimeout(() => {
                    this.setState({
                        product: productData[0],
                    })
                }, 2000)

            })
            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                .then(response => response.json())
                .then((data) => {
                    let token = data.Auth.AuthorizationTicket;
                    this.setState({
                        token: token,
                        loader: false,
                    })
                })
        }

    }

    showLoader = () => {
        this.setState({
            loader: true
        })
    }
    hideLoader = () => {
        this.setState({
            loader: false
        })
    }

    render() {
        // console.log('loader',this.state.loader)
        return (
            <div>
                <div loader="thisONe"
                     className={`custom-loader-css loader-wrapper ${this.state.loader ? '' : 'loderhide'}`}>
                    <div className="loader bg-white">
                        <div className="whirly-loader"></div>
                    </div>
                </div>
                <FramesGallery showLoader={this.showLoader} hideLoader={this.hideLoader} product={this.state.product}
                               token={this.state.token}/>
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
    products: state.data.productItems,

})

export default connect(
    mapStateToProps,
    {getList, filterBrand, filterGender, filterMaterial, filterAge, filterShape, filterRim, filterGroup}
)(FramesGalleryComponent)
