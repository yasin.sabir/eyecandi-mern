import React, {Component ,Fragment} from "react";
import logo from '../../assets/images/eyeCandi/logo-ss.png'
import SimpleReactValidator from 'simple-react-validator';
import {forgetPwd} from "./functions";


export default class resetPwd extends Component{

    constructor(props) {
        super(props);
        this.state = {
            resetPwdEmail:'',
            resMsg_message:false,
            resMsg_error:false,
            resMsg: '',
        }

        this.validator = new SimpleReactValidator();
    }

    handleOnChangeResetEmail = (e) =>{
        this.setState({
            resetPwdEmail: e.target.value
        })
    }


    OnSubmit = (e) =>{
        e.preventDefault();

        if(this.validator.allValid()){
            forgetPwd(this.state.resetPwdEmail).then(res => {
                    console.log(res.data.message);
                if(!res.data.error){
                    this.setState({
                        resMsg: res.data.message,
                        resMsg_message : true,
                        resMsg_error : false,
                    })
                }else{
                    console.log(res.data.error);
                    this.setState({
                        resMsg: res.data.error,
                        resMsg_error : true,
                        resMsg_message : false
                    })
                }
                })
        }else{
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    render() {

        return(
            <Fragment>
                <div className="page-wrapper">
                    <div className="container-fluid">
                        {/* <!-- Unlock page start--> */}
                        <div className="authentication-main">
                            <div className="row">
                                <div className="col-md-12 p-0">
                                    <div className="auth-innerright">
                                        <div className="authentication-box">
                                            <div className="text-center"><img src={logo} alt="" /></div>
                                            <div className="card mt-4 p-4 mb-0">
                                                <form className="theme-form">
                                                    <h5 className="text-capitalize text-center txt-danger f-w-700">RESET PASSWORD</h5>
                                                    <div className="form-group">
                                                        <label className="col-form-label">Enter your Email</label>
                                                        <input className="form-control"
                                                               type="email"
                                                               name="resetPwdEmail"
                                                               onChange={this.handleOnChangeResetEmail}
                                                               value={this.state.resetPwdEmail}
                                                               placeholder="John@gmail.com" />
                                                        {this.validator.message('resetPwdEmail' ,this.state.resetPwdEmail , 'required|email')}
                                                        { this.state.resMsg_message ? <span className="txt-success">{this.state.resMsg}</span> : null }
                                                        { this.state.resMsg_error ? <span className="txt-danger">{this.state.resMsg}</span> : null }
                                                    </div>

                                                    <div className="form-group form-row mb-2">
                                                        <div className="col-md-12 text-center">
                                                            <button className="btn btn-primary" type="submit" onClick={this.OnSubmit}>Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- Unlock page end--> */}
                    </div>
                </div>
            </Fragment>
        );



    }

}
