import React, { Component ,Fragment } from 'react';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import options from './typeheadData';
import { Typeahead } from 'react-bootstrap-typeahead';
import {FormGroup ,ButtonToolbar,Button} from 'react-bootstrap';

class TypeaheadOne extends React.Component {

    handleInputChange(input, e) {
        console.log("value", input)
    }

    handleChange(selectedOptions) {
       console.log(selectedOptions);
    }

    render() {

        const {values} = this.props;


        return (
          <Fragment>
            <Typeahead
              id="public-typeahead"
              defaultSelected={options.slice(0, 0)}
              name="all_brands"
              labelKey="name"
              multiple
              //defaultValue={values.all_brands}
              //onInputChange={this.handleInputChange}
              onChange={this.handleChange}
              options={options}
              placeholder="Choose a state..."
              ref={(ref) => this._typeahead = ref}
            />
            {/*<ButtonToolbar style={{marginTop: '10px',margin:'4px'}}>*/}
            {/*  <Button*/}
            {/*    className="m-2"*/}
            {/*    onClick={() => this._typeahead.clear()}>*/}
            {/*    Clear*/}
            {/*  </Button>*/}
            {/*  <Button*/}
            {/*    className="m-2"*/}
            {/*    onClick={() => this._typeahead.focus()}>*/}
            {/*    Focus*/}
            {/*  </Button>*/}
            {/*  <Button*/}
            {/*    className=" m-2" */}
            {/*    onClick={() => {*/}
            {/*      const instance = this._typeahead;*/}
            {/*      instance.focus();*/}
            {/*      setTimeout(() => instance.blur(), 1000);*/}
            {/*    }}>*/}
            {/*    Focus, then blur after 1 second*/}
            {/*  </Button>*/}
            {/*</ButtonToolbar>*/}
          </Fragment>
        );
      }
    }

export default TypeaheadOne;