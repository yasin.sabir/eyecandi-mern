import React, { Fragment , useState , useEffect } from 'react';

const Loader = (props) => {
    const [show, setShow] = useState(true);
    useEffect(() => {
       setShow(props.show)

    },[show]);
    return (
        <Fragment>
            <div className={`loader-wrapper ${show ? '' : 'loderhide'}`} >
                <div className="loader bg-white">
                    <div className="whirly-loader"> </div>
                </div>
            </div>
        </Fragment>
    );
};

export default Loader;
