import React, { Fragment, useState , useEffect } from 'react';
import { Home } from 'react-feather';
import { Link } from 'react-router-dom'
import Bookmark from './bookmark';
import {getUser} from "../eyeCandi/functions";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import EditSetupWizard from "../eyeCandi/edit-Setup-Wizard";


const Breadcrumb = props => {
    const [breadcrumb, setBreadcrumb] = useState(props);

    const [modal8, setModal8] = useState();

    const toggle8 = () => {
        setModal8(!modal8)
    };
    return (
        <Fragment>
            <div className="container-fluid">
                <div className="page-header">
                    <div className="row">
                        <div className="col">
                            <div className="page-header-left">
                                <h3 className="txt-danger">{breadcrumb.title}</h3>
                                <ol className="breadcrumb pull-right">
                                    <li className="breadcrumb-item">
                                        <Link to="/">
                                            <Home />
                                        </Link>
                                    </li>
                                    {breadcrumb.parent ?
                                        <li className="breadcrumb-item">{breadcrumb.parent}</li> :''
                                    }
                                    {breadcrumb.sub_parent ?
                                        <li className="breadcrumb-item">{breadcrumb.sub_parent}</li> :''
                                    }
                                    {breadcrumb.sub_title ?
                                        <li className="breadcrumb-item active">{breadcrumb.sub_title}</li> : ''
                                    }
                                </ol>
                            </div>
                        </div>

                         {
                            props.title === "Frames" || props.title === "Dashboard" ?

                                <div className="col text-right" style={{zIndex:4}}>
                                    {/*<a href={process.env.PUBLIC_URL+"/profile/wizard"} className="btn btn-outline-info mr-3">Setting</a>*/}
                                    <a href={process.env.PUBLIC_URL+"/"+props.publicURL} target='_blank' className="btn btn-secondary">My Public Gallery</a>

                                    {/*<button className="btn btn-outline-success mr-3" onClick={toggle8}>Setting</button>*/}

                                    <Modal isOpen={modal8} toggle={toggle8} size="lg" className="custom-large-model">
                                        <ModalHeader toggle={toggle8}>Edit Setup Wizard</ModalHeader>
                                        <ModalBody>
                                            <EditSetupWizard toggle={toggle8} />
                                        </ModalBody>
                                    </Modal>

                                </div>

                                : null
                        }

                        {/* <!-- Bookmark Start--> */}
                       {/*<Bookmark />*/}
                        {/* <!-- Bookmark Ends--> */}
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Breadcrumb
