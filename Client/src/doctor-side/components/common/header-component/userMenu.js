import React, {Component, Fragment} from 'react';
import man from '../../../assets/images/user/Profile_avatar_placeholder_large.png';
import { User, Mail, Lock, Settings, LogOut } from 'react-feather';
import { useHistory } from "react-router-dom";
import jwt_decode from "jwt-decode";
import * as credentials from "../../../constant/framesAPI";
import {getUser} from "../../eyeCandi/functions";


const UserMenu = (props) =>{
    let history = useHistory();
    const logOut =(e) => {
        e.preventDefault();
        localStorage.removeItem('usertoken');
        localStorage.removeItem('publicURL');
        history.push('/login');
    }

    return (
        <Fragment>
            <li className="onhover-dropdown">
                <div className="media align-items-center">
                    <img className="align-self-center pull-right img-50 rounded-circle blur-up lazyloaded" src={man} alt="header-user" />
                    <div className="dotted-animation">
                        {/*<span className="animate-circle"></span>*/}
                        {/*<span className="main-circle"></span>*/}
                    </div>
                </div>

                <ul className="profile-dropdown onhover-show-div p-20 profile-dropdown-hover">
                    {/*<li><a href="#_"><User />Edit Profile</a></li>*/}
                    {/*<li><a href="#javascript"><Mail />Inbox</a></li>*/}
                    {/*<li></li>*/}
                    <li><a href="#javascript"><Lock />Lock Screen</a></li>
                    <li><a href={`${process.env.PUBLIC_URL}/profile/wizard`}><Settings />Settings</a></li>
                    <li><a href="#_" onClick={logOut}><LogOut /> Log out</a></li>
                </ul>
            </li>
        </Fragment>
    );

};

class UserMenuComponent extends Component{

    constructor(props) {
        super(props)

        this.state = {
            profile_pic : ""
        }
    }

    componentDidMount() {

        const token = localStorage.usertoken;

        if(token){
            const decoded = jwt_decode(token);
            this.setState({
                first_name : decoded.first_name,
                last_name : decoded.last_name,
                email : decoded.email,
            });

            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                .then(response => response.json())
                .then((data) => {
                        this.setState({api_token: data.Auth.AuthorizationTicket});
                    }
                );

            getUser(decoded._id).then(res => {
                this.setState({
                    profile_pic:res.data.data[0].profile_pic,
                });
            });
        }
    }

    render() {
        return (
            <div>
                <UserMenu profilePic={this.state.profile_pic}/>
            </div>
        )
    }

}

export default (UserMenuComponent)
