import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import {
    getBrandsFront,
    getColorsFront,
    getMinMaxPrice,getShapesFront, getRimFront, getAgeFront, getMaterialFront
} from '../../../services';

import {
    filterBrand,
    filterColor,
    filterGender,
    filterPrice,
    filterMaterial,
    filterRim,
    filterShape, filterAge
} from '../../../actions/ecommerce.actions';
import {useSelector} from 'react-redux';
import {getAge} from "../../../services";
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import X from "../../../assets/images/New-Design/filtersSVG/x.svg";

const AllFiltersFront = (props, {filterPrice, filterBrand, filterColor}) => {
    console.log(props)
    const data = useSelector(content => content.data.productItems);
    const brands = getBrandsFront(data);
    const colors = getColorsFront(data);
    // console.log(genders);
    const shapes = getShapesFront(data);
    const Rims = getRimFront(data);
    const Ages = getAgeFront(data);
    const Materials = getMaterialFront(data);
    // const prices = getMinMaxPrice(data);
    const filteredBrand = useSelector(content => content.filters.brand);
    const filteredGender = useSelector(content => content.filters.gender);
    const filteredAge = useSelector(content => content.filters.age);
    // console.log('Gender:',filteredGender);
    const filteredMaterial = useSelector(content => content.filters.material);
    const filteredRim = useSelector(content => content.filters.rim);
    const filteredShape = useSelector(content => content.filters.shape);
    // console.log(brands,filteredBrand,filteredBrand.includes(brands[0]));
    const value = useSelector(content => content.filters.value);

    const clickBrandHendle = (event, brands) => {
        var index = brands.indexOf(event.target.value);
        props.handleClick(0);
        //brands.push(event.target.value); // push in array checked value
        var li = document.getElementsByClassName('brands');
        console.log(li);
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterBrand(brand);
        }else{
            props.filterBrand(props.data.filterbrands)
        }
    }

    const colorHandle = (event, color) => {
        var elems = document.querySelectorAll(".color-selector ul li");
        [].forEach.call(elems, function (el) {
            el.classList.remove("active");
        });
        event.target.classList.add('active');
        props.filterColor(color);
        props.handleClick(0);
    }

    const clickgenderHandle = (event, genders) => {

        var index = genders.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('genders');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterGender(brand);
        }else{
            props.filterGender(props.data.filterGender);
        }
    }

    const clickageHandle = (event, ages) => {

        var index = ages.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('ages');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterAge(brand);
        }else{
            props.filterAge(props.data.filterAge);
        }
    }

    const clickrimHandle = (event, rims) => {

        var index = rims.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('rims');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterRim(brand);
        }else{
            props.filterRim(props.data.filterRim);
        }
    }

    const clickmaterialHandle = (event, matertials) => {

        var index = matertials.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('materials');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterMaterial(brand);
        }else{
            props.filterMaterial(props.data.filterMaterial);
        }
    }

    const clickshapeHandle = (event, shapes) => {
        props.handleClick(0);
        var li = document.getElementsByClassName('shapes');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterShape(brand);
        }else{
            props.filterShape(props.data.filterShape);
        }
    }

    return (
        <Fragment>
            <div className="container-fluid filters">
                <div className="row px-4 py-4">
                    <div className="col-md-9">
                        <ul className="list-inline">
                            <li className="list-inline-item">

                                <ReactMultiSelectCheckboxes
                                    placeholderButtonLabel="Brands"
                                    value={{}}
                                    onChange={{}}
                                    options={brands}/>

                            </li>
                            <li className="list-inline-item">

                                <ReactMultiSelectCheckboxes
                                    placeholderButtonLabel="Colors"
                                    value={{}}
                                    onChange={{}}
                                    options={colors}/>

                            </li>
                            <li className="list-inline-item">

                                <ReactMultiSelectCheckboxes
                                    placeholderButtonLabel="Shape"
                                    value={{}}
                                    onChange={{}}
                                    options={shapes}/>

                            </li>
                            <li className="list-inline-item">

                                <ReactMultiSelectCheckboxes
                                    placeholderButtonLabel="Material"
                                    value={{}}
                                    onChange={{}}
                                    options={Materials}/>


                            </li>
                            <li className="list-inline-item mt-sm-0 mt-md-0 mt-lg-3 mt-xl-0">
                                <ReactMultiSelectCheckboxes
                                    placeholderButtonLabel="Age"
                                    value={{}}
                                    onChange={{}}
                                    options={Ages}/>
                            </li>
                            <li className="list-inline-item">
                                <button className="cus-btn cus-btn-transparent px-0"><img
                                    className="custom-cross-icon" src={X} alt="" srcSet=""/>Reset Filters
                                </button>
                            </li>
                            <li className="list-inline-item">

                            </li>
                        </ul>
                    </div>

                    <div className="col-md-3 text-sm-center">
                        <ul className="list-inline">
                            <li className="list-inline-item ">
                                <button className="cus-btn cus-btn-transparent px-0"><i className="fa fa-search"></i>
                                </button>
                            </li>
                            <li className="list-inline-item ">

                                <ReactMultiSelectCheckboxes
                                    placeholderButtonLabel="Staff Pick"
                                    value={{}}
                                    onChange={{}}
                                    isMulti={false}
                                    className="custom_select_staffPick"
                                    options={{}}/>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>


        </Fragment>
    );
    // }
}

const mapStateToProps = (props) => ({})

export default  connect(
    mapStateToProps,
    {filterBrand, filterColor, filterPrice, filterGender, filterRim, filterAge, filterShape, filterMaterial}
)(AllFiltersFront)
