import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import {
    getBrands,
    getColors,
    getMinMaxPrice,
    getGenders,
    getShapes,
    getRim,
    getMaterial
} from '../../../services';

import {
    filterBrand,
    filterColor,
    filterGender,
    filterPrice,
    filterMaterial,
    filterRim,
    filterShape, filterAge,filterGroup
} from '../../../actions/ecommerce.actions';
import {useSelector} from 'react-redux';
import {getAge} from "../../../services";

const AllFilters = (props, {filterPrice, filterBrand, filterColor}) => {
    // console.log(props)
    const data = useSelector(content => content.data.productItems);
    const brands = getBrands(data);
    const colors = getColors(data);
    const genders = getGenders(data);
    // console.log(genders);
    const shapes = getShapes(data);
    const Rims = getRim(data);
    const Ages = getAge(data);
    const Materials = getMaterial(data);
    // const prices = getMinMaxPrice(data);
    const filteredBrand = useSelector(content => content.filters.brand);
    const filteredGender = useSelector(content => content.filters.gender);
    const filteredAge = useSelector(content => content.filters.age);
    // console.log('Gender:',filteredGender);
    const filteredMaterial = useSelector(content => content.filters.material);
    const filteredRim = useSelector(content => content.filters.rim);
    const filteredShape = useSelector(content => content.filters.shape);
    // console.log(brands,filteredBrand,filteredBrand.includes(brands[0]));
    const value = useSelector(content => content.filters.value);

    const clickBrandHendle = (event, brands) => {
        var index = brands.indexOf(event.target.value);
        props.handleClick(0);
        //brands.push(event.target.value); // push in array checked value
        var li = document.getElementsByClassName('brands');
        console.log(li);
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterBrand(brand);
        }else{
            props.filterBrand(props.data.filterbrands)
        }
    }

    const colorHandle = (event, color) => {
        var elems = document.querySelectorAll(".color-selector ul li");
        [].forEach.call(elems, function (el) {
            el.classList.remove("active");
        });
        event.target.classList.add('active');
        props.filterColor(color);
        props.handleClick(0);
    }

    const clickgenderHandle = (event, genders) => {

        var index = genders.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('genders');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterGender(brand);
        }else{
            props.filterGender(props.data.filterGender);
        }
    }

    const clickageHandle = (event, ages) => {

        var index = ages.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('ages');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterAge(brand);
        }else{
            props.filterAge(props.data.filterAge);
        }
    }

    const clickrimHandle = (event, rims) => {

        var index = rims.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('rims');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterRim(brand);
        }else{
            props.filterRim(props.data.filterRim);
        }
    }

    const clickmaterialHandle = (event, matertials) => {

        var index = matertials.indexOf(event.target.value);
        props.handleClick(0);
        var li = document.getElementsByClassName('materials');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterMaterial(brand);
        }else{
            props.filterMaterial(props.data.filterMaterial);
        }
    }

    const clickshapeHandle = (event, shapes) => {
        props.handleClick(0);
        var li = document.getElementsByClassName('shapes');
        var brand = [];
        for (let item of li) {
            if(item.checked === true){
                brand.push(item.value);
            }
        }
        if(brand.length > 0) {
            props.filterShape(brand);
        }else{
            props.filterShape(props.data.filterShape);
        }
    }

    return (
        <Fragment>
            {/*get brands */}
            <div className="product-filter">
                <h6 className="f-w-600">Brand</h6>
                <div className="checkbox-animated mt-0">
                    {brands.map((brand, index) => {
                        return (
                            <label className="d-block" key={index}>
                                <input className="checkbox_animated brands" onClick={(e) => clickBrandHendle(e, filteredBrand)}
                                       value={brand} defaultChecked={false} id={brand} type="checkbox"
                                       data-original-title="" title=""/>
                                {brand}
                            </label>
                        )
                    })}
                </div>
            </div>

            {/* color */}
            <div className="product-filter slider-product">
                <h6 className="f-w-600">Colors</h6>
                <div className="color-selector">
                    <ul>
                        {colors.map((color, i) => {
                            return (
                                <li className={color} key={i} title={color} onClick={(e) => colorHandle(e, color)}></li>
                            )
                        })}

                    </ul>
                </div>
            </div>


            {/* gender */}
            <div className="product-filter">
                <h6 className="f-w-600">Gender</h6>
                <div className="checkbox-animated mt-0">
                    {genders.map((gender, index) => {
                            return <label className="d-block" key={index}>
                                <input className="checkbox_animated genders"
                                       value={gender} onClick={(e) => clickgenderHandle(e, filteredGender)}
                                       id={gender} type="checkbox"
                                       data-original-title=""
                                       defaultChecked={false} title=""/>
                                {gender}
                            </label>
                    })}
                </div>
            </div>

            <div className="product-filter slider-product">
                <h6 className="f-w-600">Age</h6>
                <div className="checkbox-animated mt-0">
                    {Ages.map((age, index) => {
                        return (
                            <label className="d-block" key={index}>
                                <input className="checkbox_animated ages"
                                       value={age} onClick={(e) => clickageHandle(e, filteredAge)}
                                       id={age} type="checkbox" data-original-title="" defaultChecked={false} title=""/>
                                {age}
                            </label>
                        )
                    })}
                </div>
            </div>

            {/* shape */}
            <div className="product-filter">
                <h6 className="f-w-600">Frame Shapes</h6>
                <div className="checkbox-animated mt-0">
                    {shapes.map((shape, index) => {
                        return (
                            <label className="d-block" key={index}>
                                <input className="checkbox_animated shapes"
                                       value={shape} onClick={(e) => clickshapeHandle(e, filteredShape)} defaultChecked={false} id={shape} type="checkbox"
                                       data-original-title="" title=""/>
                                {shape}
                            </label>
                        )
                    })}
                </div>
            </div>

            {/* rim */}
            <div className="product-filter slider-product">
                <h6 className="f-w-600">Rims</h6>
                <div className="checkbox-animated mt-0">
                    {Rims.map((Rim, index) => {
                        return (
                            <label className="d-block" key={index}>
                                <input className="checkbox_animated rims"
                                       value={Rim} onClick={(e) => clickrimHandle(e, filteredRim)}
                                       defaultChecked={false} id={Rim} type="checkbox" data-original-title=""
                                       title=""/>
                                {Rim}
                            </label>
                        )
                    })}
                </div>
            </div>

            {/* material */}
            <div className="product-filter">
                <h6 className="f-w-600">Materials</h6>
                <div className="checkbox-animated mt-0">
                    {Materials.map((Material, index) => {
                        return (
                            <label className="d-block" key={index}>
                                <input className="checkbox_animated materials"
                                       value={Material} onClick={(e) => clickmaterialHandle(e, filteredMaterial)}
                                       defaultChecked={false} id={Material} type="checkbox" data-original-title="" title=""/>
                                {Material}
                            </label>
                        )
                    })}
                </div>
            </div>


        </Fragment>
    );
    // }
}

const mapStateToProps = (props) => ({})

export default  connect(
    mapStateToProps,
    {filterBrand, filterColor, filterPrice, filterGender, filterRim, filterAge, filterShape, filterMaterial,filterGroup}
)(AllFilters)
