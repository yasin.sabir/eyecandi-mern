import React, {Component, Fragment, useState} from 'react';
import { Link } from 'react-router-dom';
import Breadcrumb from '../../common/breadcrumb';
import { connect } from 'react-redux';
import Tablet from './tabset';
import {getSingleItem, addToCart, getList} from '../../../actions/ecommerce.actions';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {Nav, NavItem, NavLink} from "reactstrap";
import {getURL, getUser} from "../../eyeCandi/functions";
import * as credentials from "../../../constant/framesAPI";
import avatar from '../../../assets/images/image-placeholder/1.jpg';

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: 1,
            nav1: null,
            nav2: null,
            profilePic : avatar,
            name  : '',
            public_gallery_url : '',
            ActiveTab : 1,
            loader: true,
            mail_subject:"Eyeglasses%20Try-on%20Request",
            token: '',
            slideIndex: 0,
            updateCount: 0
        }
    }

    componentWillMount() {
        var user_id = "";
        localStorage.removeItem('filter');
        //this.props.getSingleItem(this.props.match.params.id);
        // console.log(this.props.match.params.publicUrl);
        getURL(this.props.match.params.publicUrl).then(res => {
            // console.log(res)
            if (res.data.error) {
                this.props.history.push('/404')
            } else {
                fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                    .then(response => response.json())
                    .then((data) => {
                        let token = data.Auth.AuthorizationTicket;
                        this.setState({
                            token: token,
                        })
                    })
                user_id = res.data.id;
                var productData = res.data.productdata[0];
                // console.log(productData);
                this.props.getList(productData.products)
                this.props.getSingleItem(this.props.match.params.id);
                setTimeout(() => {
                    this.setState({
                        practiceName : res.data.practice_name,
                        profilePic    : res.data.data[0].profile_pic,
                        public_gallery_url : res.data.public_url,
                        main_contact_email : res.data.data[0].main_contact_email,
                        loader: false

                    })
                },1500)


            }

        });

    }

    componentDidMount() {

        this.setState({
            nav1: this.slider1,
            nav2: this.slider2,
        });
    }

    handleActiveTab = val => () =>{
        if (val === 1) {
            localStorage.removeItem('filter');
            this.props.history.push('/'+this.state.public_gallery_url);
            this.ActiveTab(1)
        } else if (val == 2) {
            localStorage.setItem('filter','men');
            this.props.history.push('/'+this.state.public_gallery_url);
            this.ActiveTab(2)
        } else if( val === 3){
            localStorage.setItem('filter','women');
            this.props.history.push('/'+this.state.public_gallery_url);
            this.ActiveTab(3)
        }else if( val === 4){
            localStorage.setItem('filter','youth');
            this.props.history.push('/'+this.state.public_gallery_url);
            this.ActiveTab(4)
        }else{
            this.ActiveTab(1)
        }
    };

    ActiveTab(val){
        this.setState({
            ActiveTab : val
        })
    }

    render() {
        const { quantity } = this.state
        const { products,singleItem, symbol } = this.props;
        var settings = {
            dots: true,
            infinite: true,
            draggable: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: () =>
                this.setState(state => ({ updateCount: state.updateCount + 1 })),
            beforeChange: (current, next) => this.setState({ slideIndex: next })
        };

        const frame_name = "Hello.%0D%0AI would like to try on the "+singleItem.name+" "+singleItem.sku+".";

        return (
            <Fragment>
                {/*<Breadcrumb title="Product Detail" parent="Ecommerce" />*/}
                <div className={`public-loader-page loader-wrapper ${this.state.loader ? '' : 'loderhide'}`}>
                    <div className="loader bg-white">
                        <div className="whirly-loader"></div>
                    </div>
                </div>

                <div className="container-fluid mt-3">
                    <div className="user-profile social-app-profile">
                        <div className="row">
                            {/* <!-- user profile first-style start--> */}
                            <div className="col-sm-12">
                                <div className="card hovercard text-center">
                                    <div className="cardheader socialheader custom-bg-color" style={{height: '250px'}}>
                                    </div>
                                    <div className="user-image">

                                        <div className="avatar"><img onClick={this.handleActiveTab(1)} src={this.state.profilePic}/></div>
                                        {/*<div className="icon-wrapper"><i className="icofont icofont-pencil-alt-5"></i></div>*/}

                                        {/*<ul className="share-icons">*/}
                                        {/*    <li><a className="social-icon bg-primary" href="#"><i className="fa fa-smile-o"></i></a></li>*/}
                                        {/*    <li><a className="social-icon bg-secondary" href="#javascripts"><i className="fa fa-wechat"></i></a></li>*/}
                                        {/*    <li><a className="social-icon bg-warning" href="#javascripts"><i className="fa fa-share-alt"></i></a></li>*/}
                                        {/*</ul>*/}
                                    </div>
                                    <div className="info p-0">
                                        <Nav tabs className="tabs-scoial borderb-tab-primary market-tabs">
                                            <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                                <NavLink className={this.state.ActiveTab == 1 ? 'custom-nav-link active' : ''}
                                                         onClick={this.handleActiveTab(1)}>
                                                    All Eyewear
                                                </NavLink>
                                            </NavItem>
                                            <NavItem className="nav nav-tabs " id="myTab" role="tablist">
                                                <NavLink className={this.state.ActiveTab == 2 ? 'custom-nav-link active' : ''}
                                                         onClick={this.handleActiveTab(2)}>
                                                    Mens
                                                </NavLink>
                                            </NavItem>
                                            <li className="nav-item">
                                                <div className="user-designation"></div>
                                                <div className="title"><a onClick={this.handleActiveTab(1)}>{this.state.practiceName}</a>
                                                </div>
                                                {/*<div className="desc mt-2">general manager</div>*/}
                                            </li>
                                            <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                                <NavLink className={this.state.ActiveTab == 3 ? 'custom-nav-link active' : ''}
                                                         onClick={this.handleActiveTab(3)}>
                                                    Womens
                                                </NavLink>
                                            </NavItem>
                                            <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                                <NavLink className={this.state.ActiveTab == 4 ? 'custom-nav-link active' : ''}
                                                         onClick={this.handleActiveTab(4)}>
                                                    Youth
                                                </NavLink>
                                            </NavItem>
                                        </Nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container-fluid">

                    <div className="card">
                        <div className="row product-page-main">

                            <div className="col-md-6 col-xl-6">
                                <Slider {...settings} asNavFor={this.state.nav2} ref={slider => (this.slider = slider)}
                                        className="product-slider">
                                    {singleItem.variants ? singleItem.variants.map((item, i) => {
                                            return (
                                                <div className="item" key={i}>
                                                    <img src={'https://api.framesdata.com/api/images?auth='+this.state.token+item.images} alt="" className="img-fluid" />
                                                </div>
                                            )
                                        }) :
                                        <img src={'https://api.framesdata.com/api/images?auth='+this.state.token+singleItem.img} alt="" className="img-fluid" />}
                                </Slider>

                                {/*<Slider asNavFor={this.state.nav1}*/}
                                {/*    ref={slider => (this.slider2 = slider)}*/}
                                {/*    slidesToShow={4}*/}
                                {/*    swipeToSlide={true}*/}
                                {/*    focusOnSelect={true}*/}
                                {/*    infinite={true}*/}
                                {/*    className="small-slick">*/}
                                {/*    {singleItem.variants ? singleItem.variants.map((item, i) => {*/}
                                {/*        return (*/}
                                {/*            <div className="item" key={i}>*/}
                                {/*                <img src={item.images} alt="" className="img-fluid" />*/}
                                {/*            </div>*/}
                                {/*        )*/}
                                {/*    }) : ''}*/}
                                {/*</Slider>*/}

                            </div>
                            <div className="col-md-6 col-xl-6">
                                <div className="product-page-details">
                                    <h3>{singleItem.name}</h3>
                                    <h5><strong>{singleItem.sku}</strong></h5>
                                </div>
                                <hr />
                                <div>
                                    <table className="product-page-width">
                                        <tbody>
                                        <tr>
                                            <td>Brand:</td>
                                            <td>{singleItem.tags}</td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td>{singleItem.gender}</td>
                                        </tr>
                                        <tr>
                                            <td>Country:</td>
                                            <td>{singleItem.country}</td>
                                        </tr>
                                        <tr>
                                            <td>Shape:</td>
                                            <td>{singleItem.shape}</td>
                                        </tr>
                                        <tr>
                                            <td>Temple:</td>
                                            <td>{singleItem.temple}</td>
                                        </tr>
                                        <tr>
                                            <td>Rim:</td>
                                            <td>{singleItem.rim}</td>
                                        </tr>
                                        <tr>
                                            <td>Material:</td>
                                            <td>{singleItem.material}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <hr />

                                {/* color */}
                                <div className="product-filter slider-product border-0">
                                    <div className="color-selector">
                                        <ul>

                                            { products.map((item, i) => {
                                                {/* color */}
                                                {
                                                    if(item.id == singleItem.id) {
                                                        const color = Array.from(new Set(item.colors))
                                                       return(
                                                            <ul>
                                                                {item.colors.map((item, i) => {
                                                                    return (
                                                                        <li onClick={e => this.slider.slickGoTo(e.target.value)} value={i} className={item} key={i} title={item}></li>
                                                                    )
                                                                })}
                                                            </ul>
                                                          )
                                                    }else{
                                                        // console.log("id not match");
                                                    }
                                                }
                                            }) }

                                        </ul>
                                    </div>
                                </div>

                                <div className="m-t-15">
                                    <a href={"mailto:"+this.state.main_contact_email+"?subject="+this.state.mail_subject+"&body="+frame_name} className="btn btn-secondary m-r-10">Request a Try-on</a>

                                    {/*<button className="btn btn-primary-gradien m-r-10" type="button" data-original-title="btn btn-info-gradien" title="" onClick={() => this.addcart(singleItem, quantity)}>*/}
                                    {/*    Order Now*/}
                                    {/*</button>*/}

                                    {/*<button className="btn btn-success-gradien m-r-10" type="button" data-original-title="btn btn-info-gradien" title="" onClick={() => this.buyProduct(singleItem, quantity)}>*/}
                                    {/*    Buy Now*/}
                                    {/*</button>*/}
                                    {/*<Link to="/ecommerce/product" ><button className="btn btn-secondary-gradien" type="button" data-original-title="btn btn-info-gradien" title="">continue shopping</button></Link>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<Tablet />*/}
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    products: state.data.productItems,
    singleItem: state.data.singleItem,
    symbol: state.data.symbol,
    practiceName : '',
    profilePic   : '',
    main_contact_email : ''
})

export default connect(
    mapStateToProps,
    { getSingleItem, addToCart , getList }
)(ProductDetail)
