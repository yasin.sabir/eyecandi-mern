import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import Breadcrumb from '../../common/breadcrumb';
import { connect } from 'react-redux';
import Tablet from './tabset';
import { getSingleItem, addToCart,getList } from '../../../actions/ecommerce.actions';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Ratings from 'react-ratings-declarative';
import { interval } from 'rxjs';
import {getURL, getUser} from "../../eyeCandi/functions";
import * as credentials from "../../../constant/framesAPI";

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: 1,
            nav1: null,
            nav2: null,
            main_contact_email : '',
            images :[],
            loader: true,
            mail_subject:"Eyeglasses%20Try-on%20Request",
            token: '',
            slideIndex: 0,
            updateCount: 0,
            singleItem: [],

        }
    }

    componentWillMount() {


        var user_id = localStorage.getItem('user_id');
        if (user_id) {
            getUser(user_id).then(res => {
                fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                    .then(response => response.json())
                    .then((data) => {
                        let token = data.Auth.AuthorizationTicket;
                        console.log(token);
                        this.setState({
                            token: token
                        })
                    })
                console.log(res.data.productdata[0])
                var productData = res.data.productdata[0];
                this.props.getList(productData.products)
                this.props.getSingleItem(this.props.match.params.id);
                //let url = res.data.data[0].main_contact_email ? res.data.data[0].main_contact_email : '';
                this.setState({
                    loader: false,main_contact_email : res.data.data[0].main_contact_email ? res.data.data[0].main_contact_email : ''
                })
            });
        }

    }

    componentDidMount() {
        this.setState({
            nav1: this.slider1,
            nav2: this.slider2
        });
    }

    render() {
        const { quantity } = this.state
        const { products,singleItem, symbol } = this.props;

        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            draggable: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: () =>
                this.setState(state => ({ updateCount: state.updateCount + 1 })),
            beforeChange: (current, next) => this.setState({ slideIndex: next })
        };

        const frame_name = "Hello.%0D%0AI would like to try on the "+singleItem.name+" "+singleItem.sku+".";

        console.log(singleItem.name);

        return (
            <Fragment>
                <div loader="thisONe" className={`custom-loader-css loader-wrapper ${this.state.loader ? '' : 'loderhide'}`}>
                    <div className="loader bg-white">
                        <div className="whirly-loader"></div>
                    </div>
                </div>
                <Breadcrumb title="Product Detail" parent='Frames' sub_parent={this.props.match.params.brand} sub_title={this.props.match.params.id}/>
                <div className="container-fluid">
                    <div className="card">
                        <div className="row product-page-main">

                            <div className="col-md-6 col-xl-6">

                                <Slider  {...settings} asNavFor={this.state.nav2} ref={slider => (this.slider = slider)}
                                    className="product-slider">

                                    {
                                        singleItem.variants ? singleItem.variants.map((item, i) => {
                                            return (
                                            <div className="item" key={i}>
                                                <img src={'https://api.framesdata.com/api/images?auth='+this.state.token+item.images} alt="" className="img-fluid" />
                                            </div>
                                        )
                                    }) :
                                        <img src={'https://api.framesdata.com/api/images?auth='+this.state.token+singleItem.img} alt="" className="img-fluid" />}
                                </Slider>

                                {/*<Slider asNavFor={this.state.nav1}*/}
                                {/*    ref={slider => (this.slider2 = slider)}*/}
                                {/*    slidesToShow={3}*/}
                                {/*    swipeToSlide={true}*/}
                                {/*    focusOnSelect={true}*/}
                                {/*    infinite={true}*/}
                                {/*    className="small-slick">*/}
                                {/*    {singleItem.variants ? singleItem.variants.map((item, i) => {*/}
                                {/*        return (*/}
                                {/*            <div className="item" key={i}>*/}
                                {/*                <img src={item.images} alt="" className="img-fluid" />*/}
                                {/*            </div>*/}
                                {/*        )*/}
                                {/*    }) : ''}*/}
                                {/*</Slider>*/}

                            </div>
                            <div className="col-md-6 col-xl-6">
                                <div className="product-page-details">
                                    <h3>{singleItem.name}</h3>
                                    <h5><strong>{singleItem.sku}</strong></h5>
                                </div>
                                <hr />
                                <div>
                                    <table className="product-page-width">
                                        <tbody>
                                            <tr>
                                                <td>Brand:</td>
                                                <td>{singleItem.tags}</td>
                                            </tr>
                                            <tr>
                                                <td>Gender:</td>
                                                <td>{singleItem.gender}</td>
                                            </tr>
                                            <tr>
                                                <td>Country:</td>
                                                <td>{singleItem.country}</td>
                                            </tr>
                                            <tr>
                                                <td>Shape:</td>
                                                <td>{singleItem.shape}</td>
                                            </tr>
                                            <tr>
                                                <td>Temple:</td>
                                                <td>{singleItem.temple}</td>
                                            </tr>
                                            <tr>
                                                <td>Rim:</td>
                                                <td>{singleItem.rim}</td>
                                            </tr>
                                            <tr>
                                                <td>Material:</td>
                                                <td>{singleItem.material}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <hr />

                                {/* color */}
                                <div className="product-filter slider-product border-0">
                                    <div className="color-selector">
                                        <ul>

                                            { products.map((item, i) => {
                                                {/* color */}
                                                {
                                                    if(item.id == singleItem.id) {
                                                        const color = Array.from(new Set(item.colors))
                                                        return(
                                                            <ul>
                                                                {item.colors.map((item, i) => {
                                                                    return (
                                                                        <li onClick={e => this.slider.slickGoTo(e.target.value)} className={item} key={i} title={item}></li>
                                                                    )
                                                                })}
                                                            </ul>
                                                        )
                                                    }else{
                                                        console.log("id not match");
                                                    }
                                                }
                                            }) }

                                        </ul>
                                    </div>
                                </div>

                                <div className="m-t-15">

                                    <a href={"mailto:"+this.state.main_contact_email+"?subject="+this.state.mail_subject+"&body="+frame_name} className="btn btn-secondary m-r-10">Request a Try-on</a>

                                    {/*<button className="btn btn-primary-gradien m-r-10" type="button" data-original-title="btn btn-info-gradien" title="" onClick={() => this.addcart(singleItem, quantity)}>*/}
                                    {/*    Request An Exam*/}
                                    {/*</button>*/}
                                    {/*<button className="btn btn-success-gradien m-r-10" type="button" data-original-title="btn btn-info-gradien" title="" onClick={() => this.buyProduct(singleItem, quantity)}>*/}
                                    {/*    Buy Now*/}
                                    {/*</button>*/}
                                    {/*<Link to="/ecommerce/product" ><button className="btn btn-secondary-gradien" type="button" data-original-title="btn btn-info-gradien" title="">continue shopping</button></Link>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<Tablet />*/}
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    products: state.data.productItems,
    singleItem: state.data.singleItem,
    symbol: state.data.symbol,
    main_contact_email : ''
})

export default connect(
    mapStateToProps,
    { getSingleItem, addToCart,getList }
)(ProductDetail)
