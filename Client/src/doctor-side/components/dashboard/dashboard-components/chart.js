import React, {Component, Fragment} from "react";
import CountUp from "react-countup";
import {
    Navigation,
    Box,
    MessageSquare,
    Users,
    ShoppingBag,
    Layers,
    ShoppingCart,
    DollarSign,
    ArrowDown,
    ArrowUp,
    CloudDrizzle
} from 'react-feather';
import { Chart } from "react-google-charts";

class Chart_Widget extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: props.title,
            data: props.data,
        }
    }

    componentDidMount() {
    }

    render() {
    //     data={[
    //         ['Task', 'Hours per Day'],
    //         ['Work', 11],
    //         ['Eat', 2],
    //         ['Commute', 2],
    //         ['Watch TV', 2],
    //         ['Sleep', 7],
    // ]}
    // options={{
    //     title: 'My Daily Activities',
    //     colors: ["#4466f2", "#1ea6ec", "#22af47", "#1b7aff", "#f85370"],
    //     // Just add this option
    //     pieHole: 0.4,
    //  }}

        console.log(this.props.data);

        // this.props.data.map((index, key)=>{
        //
        // });


        return (
            <Fragment>
                <div className="card">
                    <div className="card-header">
                        <h5>{this.state.title}</h5>
                    </div>
                    <div className="card-body">
                        <Chart
                            width={'100%'}
                            height={'400px'}
                            chartType="PieChart"
                            loader={<div>Loading Chart</div>}
                            data={ this.props.data }
                            options={this.props.option}
                            rootProps={{ 'data-testid': '1' }}
                        />
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Chart_Widget
