import React, {Component, Fragment} from "react";
import CountUp from "react-countup";
import {
    Navigation,
    Box,
    MessageSquare,
    Users,
    ShoppingBag,
    Layers,
    ShoppingCart,
    DollarSign,
    ArrowDown,
    ArrowUp,
    CloudDrizzle
} from 'react-feather';
import { Chart } from "react-google-charts";
import glasses1 from "../../../assets/images/eyeCandi/g-1.png";
import glasses2 from "../../../assets/images/eyeCandi/g-2.png";
import glasses3 from "../../../assets/images/eyeCandi/g-3.png";
import glasses4 from "../../../assets/images/eyeCandi/g-4.png";
import {Button, Popover, PopoverBody, PopoverHeader, UncontrolledPopover} from "reactstrap";

class Table_Widget extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: props.title,
            data: props.data,
            token: props.token,
            popoverOpen: false,
        }
        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
    }

    toggle() {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
        })
    }

    onHover = () => {
        this.setState({
            popoverOpen: true,
        })
    }

    onHoverLeave = () => {
        this.setState({
            popoverOpen: false,
        })
    }



    render() {
        const state = this.props.token;

        const custom_img_size ={
            width: '200px',
            height : '100px',
            zIndex : '-100',
            position: 'relative',
        }


        return (
            <Fragment>
                <div className="card">
                    <div className="card-header">
                        <h5>{this.state.title}</h5>
                    </div>
                    <div className="table-responsive">
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Image</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Frame ID</th>
                                <th scope="col">Type</th>
                                <th scope="col">views</th>
                                <th scope="col">Date Added</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.product &&
                            this.props.product.map(function(item, key){

                                const style = {
                                    backgroundImage:"url(" + 'https://api.framesdata.com/api/images?auth=' + state + item.img + ")",
                                    backgroundPosition : 'center',
                                    backgroundSize : 'contain',
                                    backgroundColor: '#fff',
                                    border: '0px',
                                    width: '100px',
                                    height: '50px',
                                }


                                return (<tr>
                                    <td>{key + 1}</td>
                                    <td scope="row" className="img-td">

                                        {/*<img*/}
                                        {/*    src={'https://api.framesdata.com/api/images?auth=' + state + item.img}*/}
                                        {/*    alt=""*/}
                                        {/*    className="custom-img-frameFeatures"*/}
                                        {/*/>*/}

                                        <Button type="button"
                                                id={`popover-${key}`}
                                                style={style}>
                                        </Button>

                                        <UncontrolledPopover trigger="focus" placement="right" target={`popover-${key}`}>
                                            <PopoverBody>
                                                <img
                                                    src={'https://api.framesdata.com/api/images?auth=' + state + item.img}
                                                    alt=""
                                                    className="custom-img-frameFeatures" style={custom_img_size}
                                                />
                                            </PopoverBody>
                                        </UncontrolledPopover>





                                    </td>
                                    <td>{item.name}</td>
                                    <td>{item.sku}</td>
                                    <td>{item.group}</td>
                                    <td>{item.views}</td>
                                    <td>{item.modified}</td>
                                </tr>)
                            })
                            }

                            </tbody>
                        </table>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Table_Widget
