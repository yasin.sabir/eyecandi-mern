import React, {Component, Fragment} from 'react';
import Breadcrumb from '../common/breadcrumb';
import jwt_decode from 'jwt-decode';
import {getUser, verify} from '../eyeCandi/functions';
import TopWidget from "./dashboard-components/top-widget";
import Chart_Widget from "./dashboard-components/chart";
import Table_Widget from "./dashboard-components/table";
import * as credentials from "../../constant/framesAPI";
import {Button, Popover, PopoverHeader, PopoverBody} from "reactstrap";

class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            practice_name: '',
            state: '',
            oa_member: '',
            session_count: '',
            tryOnRequestCount: '',
            profileViewCount: '',
            newFrames: '',
            Brands: [],
            BrandsData: [],
            productsData: [],
            currentMonthProduct: [],
            currentDate: '',
            publicUrl: '',
            popoverOpen : false,

        }
    }

    componentDidMount() {

        const newDate = new Date();
        const formattedDate = (newDate.getMonth() + 1) + "/" + newDate.getDate() + "/" + newDate.getFullYear();

        this.setState({
            currentDate: formattedDate
        })

        const token = localStorage.usertoken;
        verify().then(res => {
            if (res.data.name == 'TokenExpiredError') {
                this.props.history.push('/login');
            }
        })

        var user_id = localStorage.getItem('user_id');
        if (user_id) {
            getUser(user_id).then(res => {
                let url = res.data.public_url ? res.data.public_url : '';

                this.setState({
                    publicUrl: url
                });

            });
        }


        if (token) {
            const decoded = jwt_decode(token);
            this.setState({
                first_name: decoded.first_name,
                last_name: decoded.last_name,
                email: decoded.email,
                session_count: decoded.sessionCount
            })
            getUser(decoded._id).then(res => {
                this.setState({
                    tryOnRequestCount: res.data.tryOnRequestCount,
                    profileViewCount: res.data.profileViewCount,
                });
                if (!res.data.is_completed) {
                    this.props.history.push('/SetupWizard');
                }
            });
            // console.log(user_data);
            // if(!user_data.data.iscompleted){
            //     this.props.history.push('/SetupWizard');
            // }
        } else {
            this.props.history.push('/login');
        }


    }

    componentWillMount() {
        var user_id = localStorage.getItem('user_id');

        if (!user_id) {
            this.props.history.push('/login')
        } else {
            getUser(user_id).then(res => {
                var brandData = res.data.data[0].all_brands.all_brands;
                var productData = res.data.productdata;
                this.setState({
                    productsData: productData[0].products,
                    BrandsData: brandData
                })
            })
            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=' + credentials.partnerID_ + '&username=' + credentials.username_ + ' ')
                .then(response => response.json())
                .then((data) => {
                    let token = data.Auth.AuthorizationTicket;
                    this.setState({
                        token: token,
                        loader: false,
                    })
                })
        }
    }

    search(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return i;
            } else {

            }
        }
        return -1;
    }

    render() {

        const brandData = [];
        const colorData = [];
        const productData = [];
        var totalbrandviews = 0;
        var totalColorViews = 0;
        var countbrand = 0;
        var countcolor = 0;
        var countProduct = 0;

        this.state.BrandsData.map((brand) => {
            var newval = [];
            newval.name = brand.name;
            newval.value = brand.views;
            totalbrandviews = totalbrandviews + brand.views;
            brandData.push(newval);
        });

        this.state.productsData.map((product) => {
            productData.push(product);
            product.variants.map((value) => {
                var val = this.search(value.color, colorData)
                // console.log(val);
                totalColorViews = totalColorViews + value.views;
                if (val > -1) {
                    colorData[val].value = colorData[val].value + value.views;

                } else {
                    var newval = [];
                    newval.name = value.color;
                    newval.value = value.views;
                    colorData.push(newval);
                }
            })
        });

        brandData.sort(function(a,b){
            return b.value - a.value;
        })

        colorData.sort(function(a,b){
            return b.value - a.value;
        })

        productData.sort(function(a,b){
            return b.views - a.views;
        })

        const dd = [
            ['Task', 'Hours per Day']
        ];



        brandData.map((value, key) => {
            if (countbrand < 6) {
                var newval = (value.value / totalbrandviews) * 100;

                if(!isNaN(newval)){
                    dd.push([value.name, newval]);
                    countbrand = countbrand + 1;
                }else{
                    dd.push([value.name, 1]);
                    countbrand = countbrand + 1;
                }

                // dd.push([value.name, newval])
                // countbrand = countbrand + 1;
            }
        });

        const dd2 = [
            ['Task', 'Hours per Day']
        ];
        colorData.map((value, key) => {
            if (countcolor < 6) {
                var newval = (value.value / totalColorViews) * 100;

                if(!isNaN(newval)){
                    dd2.push([value.name, newval]);
                    countcolor = countcolor + 1;
                }else{
                    dd2.push([value.name, 1]);
                    countcolor = countcolor + 1;
                }

                // dd2.push([value.name, newval])
                // countcolor = countcolor + 1;
            }
        })
        const dd3 = [];
        productData.map((value, key) => {
            if (key < 10 && value.views > 0) {
                dd3.push(value)
                countProduct = countProduct + 1;
            }
        })
        const opt = {
            title: '',
            colors: ["#6a0dad", "#a7d8de", "#FF007F", "#1f801c", "#803259", "#23806A", "#1F801C"]
        };


        const opt2 = {
            title: '',
            colors: ["#47523b", "#964B00", "#d4af37", "#c2b280", "#faf0be", "#B3C24E", "#B0C237", "#C2AB19"]
        };


        return (
            <Fragment>

                <Breadcrumb publicURL={this.state.publicUrl} title="Dashboard"/>
                <div className="container-fluid">
                    <div className="row">

                        <div className="col-sm-6 col-xl-3 col-lg-6">
                            <TopWidget title="Total Sessions" icon="Navigation" count={this.state.session_count}/>
                        </div>
                        <div className="col-sm-6 col-xl-3 col-lg-6">
                            <TopWidget title="Try-On Requests" icon="Box" count={this.state.tryOnRequestCount}/>
                        </div>
                        <div className="col-sm-6 col-xl-3 col-lg-6">
                            <TopWidget title="New Frames" icon="MessageSquare" count="33"/>
                        </div>
                        <div className="col-sm-6 col-xl-3 col-lg-6">
                            <TopWidget title="Profile Visits" icon="Users" count={this.state.profileViewCount}/>
                        </div>


                        <div className="col-xl-6">
                            <Chart_Widget title="Top Colors" data={dd2} option={opt2}/>
                        </div>

                        <div className="col-xl-6">
                            <Chart_Widget title="Top Brands" data={dd} option={opt}/>
                        </div>

                        <div className="col-sm-12">
                            <Table_Widget product={dd3} token={this.state.token}/>
                        </div>


                    </div>
                </div>
            </Fragment>
        );
    }

}

export default Dashboard;
