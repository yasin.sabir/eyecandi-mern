import React, {Component, Fragment} from "react";
import CountUp from "react-countup";
import {
    Navigation,
    Box,
    MessageSquare,
    Users,
    ShoppingBag,
    Layers,
    ShoppingCart,
    DollarSign,
    ArrowDown,
    ArrowUp,
    CloudDrizzle
} from 'react-feather';

class TopWidget extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: props.title,
            count: props.count,
            icon: props.icon,
        }
    }

    componentDidMount() {
    }

    render() {
        return (
            <Fragment>
                <div className="card o-hidden">
                    <div className="bg-primary b-r-4 card-body">
                        <div className="media static-top-widget">
                            <div className="align-self-center text-center">

                                {this.props.icon == "Navigation" && <Navigation/>}

                                {this.props.icon == "Box" && <Box/>}

                                {this.props.icon == "MessageSquare" && <MessageSquare/>}

                                {this.props.icon == "Users" && <Users/>}

                            </div>
                            <div className="media-body">
                                <span className="m-0">{this.state.title}</span>
                                <h4 className="mb-0 counter">
                                    <CountUp className="counter" end={this.props.count}/>
                                </h4>

                                {this.props.icon == "Navigation" && <Navigation className="icon-bg"/>}

                                {this.props.icon == "Box" && <Box className="icon-bg"/>}

                                {this.props.icon == "MessageSquare" && <MessageSquare className="icon-bg"/>}

                                {this.props.icon == "Users" && <Users className="icon-bg"/>}

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default TopWidget