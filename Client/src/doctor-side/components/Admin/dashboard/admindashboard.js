import React, {Component, Fragment} from 'react';
import Breadcrumb from '../../common/breadcrumb';
import jwt_decode from 'jwt-decode';
import {getCsv, getUser, verify} from '../../eyeCandi/functions';
import TopWidget from "./dashboard-components/top-widget";
import Chart_Widget from "./dashboard-components/chart";
import Table_Widget from "./dashboard-components/table";
import * as credentials from "../../../constant/framesAPI";
import Header from '../../common/admin-header-components/header-component/header';
import Sidebar from '../../common/admin-header-components/sidebar-component/sidebar';
import RightSidebar from '../../common/admin-header-components/right-sidebar';
import Footer from '../../common/admin-header-components/footer';
import {Button, Popover, PopoverHeader, PopoverBody, Pagination, PaginationItem, PaginationLink} from "reactstrap";
import Loader from "../../common/loader";
import {ToastContainer} from "react-toastify";
import Modal from "react-bootstrap/Modal";
import {get_list_all} from "../../eyeCandi/promoFunctions";


class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            practice_name: '',
            state: '',
            oa_member: '',
            session_count: '',
            tryOnRequestCount: '',
            profileViewCount: '',
            newFrames: '',
            Brands: [],
            BrandsData: [],
            productsData: [],
            currentMonthProduct: [],
            currentDate: '',
            publicUrl: '',
            popoverOpen: false,
            user_count: 10,
            promocode: 0,
            show: true,
            password: '',
            authentication_error: false,
            authentication_error_msg: 'Wrong Password',
            userCount: '',
            promocodeCount: '',

        }
    }

    componentDidMount() {

        getCsv().then(res => {
            this.setState({
                userCount : res.data.length,
            })
        });

        get_list_all().then(res => {
            this.setState({
                promocodeCount : res.data.length,
            })
        });

    }

    componentWillMount() {

    }

    handleOnChangePassword = (e) => {
        this.setState({
            password: e.target.value,
        });
    };

    onSubmit = () => {
        if (this.state.password === 'joshchild123!') {
            this.setState({
                show: false
            })
        } else {
            this.setState({
                password: '',
                authentication_error: true,
            })
        }
    }

    onExit = () => {
        if (this.state.password !== 'joshchild123!') {
            this.setState({
                show: true
            })
        }
    }


    render() {
        const d1 = [
            ['Task', 'Hours per Day'],
            ['Gold', 26.7],
            ['Purple', 13.3],
            ['Crystal', 20],
            ['Rose', 26.7],
        ];

        const opt = {
            title: '',
            colors: ["#d4af37", "#6a0dad", "#a7d8de", "#FF007F"]
        };


        const d2 = [
            ['Task', 'Hours per Day'],
            ['Rayban', 26.7],
            ['Acadia', 73.3],
        ];

        const opt2 = {
            title: '',
            colors: ["#800020","#00FFFF"]
        };

        return (
            <Fragment>
                <div className="page-wrapper">
                    <div className="page-body-wrapper">
                        <Header/>
                        <Sidebar/>npm start
                        <RightSidebar/>
                        <div className="page-body">
                            <Modal
                                show={this.state.show}
                                onHide={() => this.setState({
                                    show: false
                                })}
                                onExit={this.onExit}
                                dialogClassName="modal-90w"
                                aria-labelledby="example-custom-modal-styling-title"
                                style={{top: '20%'}}
                            >
                                <Modal.Header>
                                    <p className="h4 txt-info text-center col-md-12">
                                        Enter Password For Super Admin
                                    </p>
                                </Modal.Header>
                                <Modal.Body>
                                    <div className={'container'}>
                                        <div className={'row'}>
                                            <label
                                                className="col-form-label pt-0 col-md-6 offset-3 text-center">Password</label>
                                            <input className="col-form-label pt-0 col-md-6 offset-3 text-center"
                                                   placeholder={'Enter Password'} type="password" name="password"
                                                   required=""
                                                   value={this.state.password} onChange={this.handleOnChangePassword}/>
                                            {
                                                this.state.authentication_error ?
                                                    <span
                                                        className="txt-danger col-md-6 offset-3 text-center">{this.state.authentication_error_msg}</span> : ""
                                            }
                                            <div className="form-group mt-3 mb-0 col-md-6 offset-md-3">
                                                <button
                                                    className="btn btn-primary btn-block custom-btn-color text-center"
                                                    type="submit"
                                                    onClick={this.onSubmit}>Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </Modal.Body>
                            </Modal>
                            <div className="container-fluid">
                                <div className="row">

                                    <div className="col-sm-6 col-xl-6 col-lg-6">
                                        <TopWidget title="Total Users" icon="Navigation" count={this.state.userCount ? this.state.userCount : 0}/>
                                    </div>
                                    <div className="col-sm-6 col-xl-6 col-lg-6">
                                        <TopWidget title="Total Promo Code" icon="Box" count={this.state.promocodeCount ? this.state.promocodeCount : 0}/>
                                    </div>

                                    <div className="col-xl-6">
                                        <Chart_Widget title="Top Colors" data={d1} option={opt}/>
                                    </div>

                                    <div className="col-xl-6">
                                        <Chart_Widget title="Top Brands" data={d2} option={opt2}/>
                                    </div>


                                </div>
                            </div>

                        </div>
                        <Footer/>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default Dashboard;
