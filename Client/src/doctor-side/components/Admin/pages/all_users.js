import React, {Component, Fragment, useEffect, useState} from "react";
import Header from "../../common/admin-header-components/header-component/header";
import Sidebar from "../../common/admin-header-components/sidebar-component/sidebar";
import RightSidebar from "../../common/admin-header-components/right-sidebar";
import Footer from "../../common/admin-header-components/footer";
import Breadcrumb from '../../common/breadcrumb';

import {
    getURL,
    getUser,
    getCsv,
    delete_userBy_id,
    update_UserData,
    showProduct,
    hideProduct,
    favoriteProduct, RemoveFavoriteProduct
} from "../../eyeCandi/functions";
import {
    Button,
    Pagination,
    PaginationItem,
    PaginationLink,
    PopoverBody,
    UncontrolledPopover,
    Modal, ModalHeader, ModalBody, ModalFooter
} from "reactstrap";
import {get_list_all} from "../../eyeCandi/promoFunctions";

const All_Users = (props) => {

    const [userData, setuserData] = useState([]);
    const [pagesSize, setpagesSize] = useState(10);
    const [CurrentPage, setCurrentPage] = useState(0);
    const [pagesCount, setpagesCount] = useState(0);
    const [userCount, setuserCount] = useState(0);
    const [modal, setModal] = useState();
    const [modalData, setmodalData] = useState();
    const [userID, setuserID] = useState();

    const toggle = (data, id) => {
        setModal(!modal);
        setmodalData(data);
        setuserID(id);
        console.log(id);
    };

    const closeModal = () => {
        setModal(!modal);
    };

    useEffect(() => {
        getCsv().then(res => {
            setuserData(
                res.data.map((key, val) => {
                    console.log(key.date);
                    return {
                        id: key._id,
                        email: key.email,
                        phone: key.phone_number,
                        practice_address: key.practice_address,
                        practice_address2: key.practice_address2,
                        city: key.city,
                        state: key.state,
                        zip: key.zip,
                        public_url: key.public_url,
                        promo_code: key.promo_code,
                        affiliate_link: key.affiliate_link,
                        date: (new Date(key.date)).toLocaleDateString()
                    }
                }),

                setpagesCount(Math.ceil(res.data.length / pagesSize))
            );
        });

    });

    const handleClick = (index) => {
        //props.showLoader();
        setCurrentPage(index);
        // window.scrollTo(0, 130)
        // setTimeout(() => {
        //     props.hideLoader();
        // }, 1500);
    };

    const userDelete = (val) => {
        //console.log(val);
        delete_userBy_id(val).then(res => {
            if (res) {
                props.history.push('/admin/users');
            } else {
                alert("Something went wrong user is not delete.");
            }
        });
        setModal(!modal);
    };

    return (
        <Fragment>
            <div className="page-wrapper">
                <div className="page-body-wrapper">
                    <Header/>
                    <Sidebar/>
                    <RightSidebar/>
                    <div className="page-body">

                        <Breadcrumb title="All Users" parent="All Registered Users"/>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="card">
                                        <div className="card-header">
                                            <h5>Users</h5>
                                        </div>
                                        <div className="table-responsive">
                                            <table className="table">
                                                <thead>
                                                <tr>
                                                    {/*<th scope="col">#</th>*/}
                                                    <th scope="col">Email</th>
                                                    <th scope="col">City</th>
                                                    <th scope="col">State</th>
                                                    <th scope="col">Public URL</th>
                                                    <th scope="col">Promo Code</th>
                                                    <th scope="col">Affiliate</th>
                                                    <th scope="col">Created At</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                {
                                                    userData.map((item, id) => {
                                                        return (
                                                            <tr>
                                                                {/*<th scope="row">{id}</th>*/}
                                                                <td>{item.email}</td>
                                                                <td>{item.city ? item.city : "Not Found!"}</td>
                                                                <td>{item.state ? item.state : "Not Found!"}</td>
                                                                <td>
                                                                     {item.public_url ? <a target="_blank" href={`${window.location.origin}/${item.public_url}`}>{window.location.origin+'/'+item.public_url}</a> : "Not Found!"}
                                                                </td>
                                                                <td>{item.promo_code ? item.promo_code : "Not Found!"}</td>
                                                                <td>{item.affiliate_link ? item.affiliate_link : "Not Found!"}</td>
                                                                <td>{item.date ? item.date : "Not Found!"}</td>
                                                                <td style={{textAlign: 'center'}}>
                                                                       <span className="text-dark"
                                                                             onClick={() => {
                                                                                 toggle(item.email, item.id)
                                                                             }}
                                                                             style={{cursor: 'pointer'}}><i
                                                                           className="fas fa-trash"></i></span>
                                                                </td>
                                                            </tr>
                                                        )
                                                    }) }

                                                {/*{*/}
                                                {/*    userData ? userData.slice(CurrentPage * pagesSize, (CurrentPage + 1) * pagesSize).map((item, id) => {*/}
                                                {/*        return (*/}
                                                {/*            <tr>*/}
                                                {/*                /!*<th scope="row">{id}</th>*!/*/}
                                                {/*                <td>{item.email}</td>*/}
                                                {/*                <td>{item.city ? item.city : "Not Found!"}</td>*/}
                                                {/*                <td>{item.state ? item.state : "Not Found!"}</td>*/}
                                                {/*                <td>*/}
                                                {/*                     {item.public_url ? <a target="_blank" href={`${window.location.origin}/${item.public_url}`}>{window.location.origin+'/'+item.public_url}</a> : "Not Found!"}*/}
                                                {/*                </td>*/}
                                                {/*                <td>{item.promo_code ? item.promo_code : "Not Found!"}</td>*/}
                                                {/*                <td>{item.affiliate_link ? item.affiliate_link : "Not Found!"}</td>*/}
                                                {/*                <td>{item.date ? item.date : "Not Found!"}</td>*/}
                                                {/*                <td style={{textAlign: 'center'}}>*/}
                                                {/*                       <span className="text-dark"*/}
                                                {/*                             onClick={() => {*/}
                                                {/*                                 toggle(item.email, item.id)*/}
                                                {/*                             }}*/}
                                                {/*                             style={{cursor: 'pointer'}}><i*/}
                                                {/*                           className="fas fa-trash"></i></span>*/}
                                                {/*                </td>*/}
                                                {/*            </tr>*/}
                                                {/*        )*/}
                                                {/*    }) : ''}*/}

                                                </tbody>
                                            </table>

                                            <Modal isOpen={modal} toggle={toggle}>
                                                <ModalHeader toggle={toggle}>Delete User</ModalHeader>
                                                <ModalBody>
                                                    Are you sure you want to delete this user? {modalData}
                                                </ModalBody>
                                                <ModalFooter>
                                                    <Button onClick={closeModal} color="secondary">Cancel</Button>
                                                    <Button userID={{userID}} className="btn btn-danger"
                                                            onClick={() => {
                                                                userDelete(userID)
                                                            }}
                                                    >Delete</Button>
                                                </ModalFooter>
                                            </Modal>


                                        </div>

                                    </div>
                                </div>
                            </div>


                            {/*<div className="row">*/}
                            {/*    <div className="col-md-12 mt-3 mb-3 text-center">*/}
                            {/*        <Pagination aria-label="Page navigation"*/}
                            {/*                    className="pagination  justify-content-center  pagination-primary">*/}

                            {/*            <PaginationItem disabled={CurrentPage <= 0}>*/}
                            {/*                <PaginationLink onClick={(e) => {*/}
                            {/*                    e.preventDefault();*/}
                            {/*                    handleClick(CurrentPage - 1)*/}
                            {/*                }} href="#">*/}
                            {/*                    Prev*/}
                            {/*                </PaginationLink>*/}
                            {/*            </PaginationItem>*/}

                            {/*            {[...Array(pagesCount)].map((page, i) => {*/}
                            {/*                    if (CurrentPage + 3 >= i && CurrentPage - 3 <= i) {*/}
                            {/*                        return <PaginationItem active={i === CurrentPage} key={i}>*/}
                            {/*                            <PaginationLink onClick={e => {*/}
                            {/*                                e.preventDefault();*/}
                            {/*                                handleClick(i)*/}
                            {/*                            }} href="#">*/}
                            {/*                                {i + 1}*/}
                            {/*                            </PaginationLink>*/}
                            {/*                        </PaginationItem>*/}
                            {/*                    }*/}
                            {/*                }*/}
                            {/*            )}*/}

                            {/*            <PaginationItem disabled={CurrentPage >= pagesCount - 1}>*/}
                            {/*                <PaginationLink last onClick={(e) => {*/}
                            {/*                    e.preventDefault();*/}
                            {/*                    handleClick(CurrentPage + 1)*/}
                            {/*                }} href="#">*/}
                            {/*                    Next*/}
                            {/*                </PaginationLink>*/}
                            {/*            </PaginationItem>*/}

                            {/*        </Pagination>*/}

                            {/*    </div>*/}
                            {/*</div>*/}

                        </div>

                    </div>
                    <Footer/>
                </div>
            </div>
        </Fragment>
    );

};
export default All_Users
