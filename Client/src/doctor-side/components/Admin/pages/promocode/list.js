import React, {Component, Fragment, useEffect, useState} from "react";
import Header from "../../../common/admin-header-components/header-component/header";
import Sidebar from "../../../common/admin-header-components/sidebar-component/sidebar";
import RightSidebar from "../../../common/admin-header-components/right-sidebar";
import Footer from "../../../common/admin-header-components/footer";
import Breadcrumb from '../../../common/breadcrumb';
import moment from "moment";
import {
    getURL,
    getUser,
    getUserEmail,
    getCsv,
    update_UserData,
    showProduct,
    hideProduct,
    favoriteProduct, RemoveFavoriteProduct
} from "../../../eyeCandi/functions";

import {
    get_list_all, delete_promoBy_id, get_promoBy_id
} from "../../../eyeCandi/promoFunctions";

import {Button, Pagination, PaginationItem, PaginationLink, PopoverBody, UncontrolledPopover} from "reactstrap";
import * as credentials from "../../../../constant/framesAPI";
import {
    filterAge,
    filterBrand,
    filterGender, filterGroup,
    filterMaterial, filterRim,
    filterShape,
    getList
} from "../../../../actions/ecommerce.actions";
import Promo_Code_Edit from "./promocode_edit";

const PromoCode_list = (props) => {

    const [userData, setuserData] = useState([]);
    const [promoData, setpromoData] = useState([]);
    const [userEmail, setuserEmail] = useState();
    const [pagesSize, setpagesSize] = useState(10);
    const [CurrentPage, setCurrentPage] = useState(0);
    const [pagesCount, setpagesCount] = useState(0);

    useEffect(() => {

        var dataList = [];
        // var userEmail = [];
        var userEmail = "";

        get_list_all().then(res => {
            res.data.map((value, key) => {
                dataList.push({
                    _id: value._id,
                    user_id: value.user_id,
                    promo_code: value.promo_code,
                    discount_frequency: value.discount_frequency,
                    created_at: value.created_at,
                });
            });
            setpromoData(dataList);
            setpagesCount(Math.ceil(dataList.length / pagesSize))
        });

        console.log(userEmail);

        // getCsv().then(res => {
        //     setuserData(
        //         res.data.map((key , val) => {
        //             return {
        //                 id : val ,
        //                 email : key.email,
        //                 phone : key.phone_number,
        //                 practice_address: key.practice_address,
        //                 practice_address2: key.practice_address2,
        //                 city : key.city,
        //                 state : key.state,
        //                 zip : key.zip,
        //                 public_url : key.public_url,
        //                 promo_code : key.promo_code,
        //                 affiliate_link : key.affiliate_link
        //             }
        //         }),
        //
        //         setpagesCount(Math.ceil(res.data.length / pagesSize))
        //
        //     );
        // });

    },[]);

    const handleClick = (index) => {
        //props.showLoader();
        setCurrentPage(index);
        // window.scrollTo(0, 130)
        // setTimeout(() => {
        //     props.hideLoader();
        // }, 1500);
    };

    const promoEdit = (val) => {
        var id = val;
        get_promoBy_id(val).then(res => {
            console.log(res);
        });
        props.history.push( `${process.env.PUBLIC_URL}/admin/promocode/detail/${val}`  );
    };

    const promoDelete = (val) => {
        delete_promoBy_id(val).then(res => {
            if (res) {
                alert("Deleted!");
                props.history.push('/admin/promocode/list');
            } else {
                alert("Something went wrong data is not delete.");
            }
        });
    };

    return (
        <Fragment>
            <div className="page-wrapper">
                <div className="page-body-wrapper">
                    <Header/>
                    <Sidebar/>
                    <RightSidebar/>
                    <div className="page-body">

                        <Breadcrumb title="All Users" parent="All Registered Users"/>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="card">
                                        <div className="card-header">
                                            <h5>Users</h5>
                                        </div>
                                        <div className="table-responsive">
                                            <table className="table">
                                                <thead>
                                                <tr>
                                                    {/*<th scope="col">#</th>*/}
                                                    <th scope="col">User</th>
                                                    <th scope="col">Assigned Promo-Code</th>
                                                    <th scope="col">Discount Frequency</th>
                                                    <th scope="col">Created At</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                {
                                                    promoData ? promoData.slice(CurrentPage * pagesSize, (CurrentPage + 1) * pagesSize).map((item, id) => {
                                                        return (
                                                            <tr>
                                                                {/*<th scope="row">{id}</th>*/}
                                                                <td>{item.user_id ? item.user_id : "Not Found!"}</td>
                                                                <td>{item.promo_code ? item.promo_code : "Not Found!"}</td>
                                                                <td>{item.discount_frequency ? item.discount_frequency : "Not Found!"}</td>
                                                                <td>{item.created_at ? moment(item.created_at).format('YYYY-MM-DD h:mm:ss a') : "Not Found!"}</td>
                                                                <td><span onClick={() => {
                                                                    promoEdit(item._id)
                                                                }} data-id={item._id} className="text-dark mr-3"
                                                                          style={{cursor: 'pointer'}}><i
                                                                    className="fas fa-edit"></i></span>
                                                                    <span onClick={() => {
                                                                        promoDelete(item._id)
                                                                    }} className="text-dark"
                                                                          style={{cursor: 'pointer'}}><i
                                                                        className="fas fa-trash"></i></span></td>


                                                            </tr>
                                                        )
                                                    }) : ''}

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12 mt-3 mb-3 text-center">
                                    <Pagination aria-label="Page navigation"
                                                className="pagination  justify-content-center  pagination-primary">

                                        <PaginationItem disabled={CurrentPage <= 0}>
                                            <PaginationLink onClick={(e) => {
                                                e.preventDefault();
                                                handleClick(CurrentPage - 1)
                                            }} href="#">
                                                Prev
                                            </PaginationLink>
                                        </PaginationItem>

                                        {[...Array(pagesCount)].map((page, i) => {
                                                if (CurrentPage + 3 >= i && CurrentPage - 3 <= i) {
                                                    return <PaginationItem active={i === CurrentPage} key={i}>
                                                        <PaginationLink onClick={e => {
                                                            e.preventDefault();
                                                            handleClick(i)
                                                        }} href="#">
                                                            {i + 1}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                }
                                            }
                                        )}

                                        <PaginationItem disabled={CurrentPage >= pagesCount - 1}>
                                            <PaginationLink last onClick={(e) => {
                                                e.preventDefault();
                                                handleClick(CurrentPage + 1)
                                            }} href="#">
                                                Next
                                            </PaginationLink>
                                        </PaginationItem>

                                    </Pagination>

                                </div>
                            </div>

                        </div>

                    </div>
                    <Footer/>
                </div>
            </div>
        </Fragment>
    );

};
export default PromoCode_list
//
// class PromoCode_listComponent extends Component {
//
//     constructor(props) {
//         super();
//         this.state = {
//             val : "Hello",
//             promoData : []
//         }
//     }
//
//     componentDidMount() {
//         let dataList = [];
//
//         get_list_all().then( res => {
//             res.data.map( (value , key) => {
//                 dataList.push({
//                     _id: value._id,
//                     user_id: value.user_id,
//                     promo_code: value.promo_code,
//                     discount_frequency: value.discount_frequency,
//                     created_at: value.created_at,
//                 })
//             })
//         });
//
//         this.setState({})
//     }
//
//     render() {
//         return (
//             <PromoCode_list data={this.state.val} />
//         );
//     }
// }
//
// export default PromoCode_listComponent;


