import React, {Component, Fragment} from "react";
import Header from "../../../common/admin-header-components/header-component/header";
import Sidebar from "../../../common/admin-header-components/sidebar-component/sidebar";
import RightSidebar from "../../../common/admin-header-components/right-sidebar";
import Footer from "../../../common/admin-header-components/footer";
import Breadcrumb from '../../../common/breadcrumb';
import SimpleReactValidator from "simple-react-validator";
import {register, update} from "../../../eyeCandi/functions";
import {
    getURL,
    getUser,
    getCsv,
    update_UserData,
    showProduct,
    hideProduct,
    favoriteProduct, RemoveFavoriteProduct
} from "../../../eyeCandi/functions";

import {
    get_promoBy_id , update_promoBy_id
} from "../../../eyeCandi/promoFunctions";


import  { store } from "../../../eyeCandi/promoFunctions";

class Promo_Code_Edit extends Component {

    constructor(props) {
        super(props);

        this.state = {
            all_users: [],
            selected_user:"",
            promo_code : "",
            discount_frequency : "",
            disableButton: false,
            promoID : '',
        }
        this.validator = new SimpleReactValidator();
    }



    componentDidMount() {

        var data = this.props.match.params;

        this.setState({
            promoID : data.id
        })

        getCsv().then(res => {
            this.setState({
                all_users : res.data.map((key , val)=>{
                    return {
                        id : key._id,
                        email : key.email
                    }
                })
            });
        });

        if(data){
            get_promoBy_id(data.id).then(res => {
                this.setState({
                    selected_user : res.user_id,
                    promo_code : res.promo_code,
                    discount_frequency : res.discount_frequency,
                })
            });
        }

    }

    componentWillMount() {

    }

    handleChange = input => (e) => {
        this.setState({
            [input] : e.target.value
        })
    };

    onSubmit = e => {
        e.preventDefault();

        this.setState({
            disableButton : true
        });

        if (this.validator.allValid()) {

            setTimeout(()=>{

                const data = {
                    'discount_frequency' : this.state.discount_frequency,
                    'promo_code' : this.state.promo_code,
                    'user_id': this.state.selected_user,
                };

                //console.log(data);
                var promoID = localStorage.getItem('promoID');
                update_promoBy_id( this.state.promoID , data).then( res => {
                    if(!res.error){
                        console.log(res);
                        // this.setState({
                        //     disableButton: false
                        // });
                        window.location.reload(true);
                    }else{
                        console.log(res.error);
                        alert(res.error);
                    }

                });

                this.setState({
                    disableButton : false
                });

            },2000);


        } else {
            setTimeout( () => {
                this.validator.showMessages();
                this.setState({
                    disableButton : false
                });
            },1000);
            this.forceUpdate();
        }
    };

    render() {

        // this.state.all_users.map((item , i )=> {
        //   console.log(item);
        // });

        return (
            <Fragment>
                <div className="page-wrapper">
                    <div className="page-body-wrapper">
                        <Header/>
                        <Sidebar/>
                        <RightSidebar/>
                        <div className="page-body">
                            <Breadcrumb title="Promo Code"/>
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="card">
                                            <div className="card-header">
                                                <h5>Promo Code {this.props.promoID}</h5>
                                            </div>
                                            <div className="card-body">
                                                <form className="needs-validation">
                                                    <div className="form-group">
                                                        <label className="col-sm-3 col-form-label">Select User:</label>
                                                        <div className="col-sm-5">
                                                            <select className="form-control digits" name="selected_user" onChange={this.handleChange('selected_user')}>
                                                                <option value="">Select User</option>
                                                                {
                                                                    this.state.all_users.map((item , i ) => {

                                                                        if(this.state.selected_user == item.id){
                                                                            return (
                                                                                <option value={item.id} selected>{item.email}</option>
                                                                            )
                                                                        }else{
                                                                            return (
                                                                                <option value={item.id}>{item.email}</option>
                                                                            )
                                                                        }

                                                                    })
                                                                }

                                                            </select>
                                                            {this.validator.message('selected_user', this.state.selected_user, 'required',)}
                                                        </div>
                                                    </div>

                                                    <div className="form-group">
                                                        <label className="col-sm-3 col-form-label">Enter Promo Code:</label>
                                                        <div className="col-sm-5">
                                                            <input className="form-control"
                                                                   type="text"
                                                                   name="promo_code"
                                                                   value={this.state.promo_code}
                                                                   onChange={this.handleChange('promo_code')}/>
                                                            {this.validator.message('promo_code', this.state.promo_code, 'required',)}
                                                        </div>
                                                    </div>

                                                    <div className="form-group">
                                                        <label className="col-sm-3 col-form-label">Discount Frequency:</label>
                                                        <div className="col-sm-5">
                                                            <input className="form-control"
                                                                   type="text"
                                                                   name="discount_frequency"
                                                                   value={this.state.discount_frequency}
                                                                   onChange={this.handleChange('discount_frequency')}/>
                                                            {this.validator.message('discount_frequency', this.state.discount_frequency , 'required',)}
                                                        </div>
                                                    </div>

                                                    <div className="form-group">
                                                        <div className="col-sm-5">
                                                            <button
                                                                className="btn btn-primary btn-block custom-btn-color"
                                                                type="submit" onClick={this.onSubmit}>{this.state.disableButton ? 'Please Wait ...' : 'Generate Promo Code'}
                                                            </button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <Footer/>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default Promo_Code_Edit;
