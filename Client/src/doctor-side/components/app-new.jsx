import React, {useLayoutEffect, useState} from 'react';
import Header from './eyeCandi/front-pages/common/header-component/header';
import RightSidebar from './eyeCandi/front-pages/common/right-sidebar';
import Footer from './eyeCandi/front-pages/common/footer';
import ThemeCustomizer from './eyeCandi/front-pages/common/theme-customizer'
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Loader from './eyeCandi/front-pages/common/loader';
import Sidebar from './eyeCandi/front-pages/common/sidebar-component/sidebar';


const AppNewLayout = ({children}) => {

    // const [size, setSize] = useState([0, 0]);
    //
    // if (size[0] > 100 && size[0] < 991) {
    //     console.log("-mobile and tablet");
    //     document.querySelector(".page-wrapper").className = 'page-wrapper default';
    //     document.querySelector(".page-body-wrapper").className = 'page-body-wrapper default';
    //
    // } else {
    //     console.log("-desktop");
    //     document.querySelector(".page-wrapper").className = 'page-wrapper horizontal_sidebar';
    //     document.querySelector(".page-body-wrapper").className = 'page-body-wrapper horizontal_sidebar';
    // }

    return (
        <div>
            {/*<Loader/>*/}
            <div className="page-wrapper" id="page-wrapper">
                <div className="page-body-wrapper" id="page-body-wrapper">
                    <Header/>
                    <Sidebar/>
                    <RightSidebar/>
                    <div className="page-body pl-0 pr-0 m-t-50 p-b-10">
                        {children}
                    </div>
                    <Footer/>
                    {/*<ThemeCustomizer/>*/}
                </div>
            </div>
            <ToastContainer/>
        </div>
    );



}

export default AppNewLayout;
