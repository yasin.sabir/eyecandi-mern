import {
    FILTER_BRAND,
    FILTER_COLOR,
    FILTER_PRICE,
    SORT_BY,
    SEARCH_BY, FILTER_GENDER, FILTER_MATERIAL, FILTER_RIM, FILTER_SHAPE, FILTER_AGE, FILTER_GROUP
} from '../constant/actionTypes';

const filtersReducerDefaultState = {
    brand: [],
    color: '',
    gender: [],
    material: [],
    rim: [],
    shape: [],
    group: [],
    age: [],
    sortBy: "",
    searchBy: ""
};

const filtersReducer = (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case FILTER_BRAND:
            return {
                ...state,
                brand: action.brand
            };
        case FILTER_COLOR:
            return {
                ...state,
                color: action.color
            };
        case FILTER_GENDER:
            return {
                ...state,
                gender: action.gender
            };
        case FILTER_MATERIAL:
            return {
                ...state,
                material: action.material
            };
        case FILTER_RIM:
            return {
                ...state,
                rim: action.rim
            };
        case FILTER_AGE:
            return {
                ...state,
                age: action.age
            };
        case FILTER_SHAPE:
            return {
                ...state,
                shape: action.shape
            };
        case FILTER_GROUP:
            return {
                ...state,
                group: action.group
            };
        case SORT_BY:
            return {
                ...state,
                sortBy: action.sort_by
            };
        case SEARCH_BY:
            return {
                ...state,
                searchBy: action.search
            };
        default:
            return state;
    }
}

export default filtersReducer;
