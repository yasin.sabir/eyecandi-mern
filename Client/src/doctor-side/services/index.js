//total add cart items
import React from "react";

export const getCartTotal = cartItems => {
    var total = 0;
    var items = 0;
    for (var i = 0; i < cartItems.length; i++) {
        items = cartItems[i].qty * cartItems[i].price
        total = total + items;
    }
    return total;
}

// Get Unique Brands from Json Data
export const getBrands = (products) => {

    var uniqueBrands = [];
    products.map((product, index) => {
        if (product.tags) {
            product.tags.map((tag) => {
                if (uniqueBrands.indexOf(tag) === -1) {
                    uniqueBrands.push(tag);
                }
            })
        }
    })
    return uniqueBrands;
}


// Get Unique Brands front from Json Data
export const getBrandsFront = (products) => {
    //console.log(products);
    var uniqueBrands = [];
    products.map((product, index) => {
        if (product.tags) {
            product.tags.map((tag) => {
                if (uniqueBrands.filter((e) => e.value === tag).length == 0) {
                    uniqueBrands.push({value: tag, label: tag});
                }
            })
        }
    })
    return uniqueBrands;
}


//Get unique genders
export const getGenders = (products) => {

    var uniqueGenders = [];
    products.map((product, index) => {
        if (product.gender) {
            var gender = product.gender;
            if (uniqueGenders.indexOf(gender) === -1) {
                uniqueGenders.push(gender);
            }
        }
    })
    return uniqueGenders;
}


//Get unique frameShape
export const getShapes = (products) => {

    var uniqueShape = [];
    products.map((product, index) => {
        if (product.shape) {
            var shape = product.shape;
            if (uniqueShape.indexOf(shape) === -1) {
                uniqueShape.push(shape);
            }
        }
    })
    return uniqueShape;
}


//Get unique frameShape front
export const getShapesFront = (products) => {

    var uniqueShape = [];
    products.map((product, index) => {
        if (product.shape) {
            var shape = product.shape;
            if (uniqueShape.filter((e) => e.value === shape).length == 0) {
                uniqueShape.push({value: shape, label: shape});
            }
        }
    })
    return uniqueShape;
}

//Get unique Rim
export const getRim = (products) => {

    var uniqueRim = [];
    products.map((product, index) => {
        if (product.rim) {
            var rim = product.rim;
            if (uniqueRim.indexOf(rim) === -1) {
                uniqueRim.push(rim);
            }
        }
    })
    return uniqueRim;
}


//Get unique Rim front
export const getRimFront = (products) => {

    var uniqueRim = [];
    products.map((product, index) => {
        if (product.rim) {
            var rim = product.rim;
            if (uniqueRim.filter((e) => e.value === rim).length == 0) {
                uniqueRim.push({value: rim, label: rim});
            }
        }
    })
    return uniqueRim;
}

export const getAge = (products) => {

    var uniqueAge = [];
    products.map((product, index) => {
        if (product.age) {
            var age = product.age;
            if (uniqueAge.indexOf(age)) {
                uniqueAge.push(age);
            }
        }
    })
    return uniqueAge;
}

export const getAgeFront = (products) => {

    var uniqueAge = [];
    products.map((product, index) => {
        if (product.age) {
            var age = product.age;
            if (uniqueAge.filter((e) => e.value === age).length == 0) {
                uniqueAge.push({value: age, label: age});
            }
        }
    })
    return uniqueAge;
}

//Get unique Material
export const getMaterial = (products) => {

    var uniqueMaterial = [];
    products.map((product, index) => {
        if (product.material) {
            var material = product.material;
            if (uniqueMaterial.indexOf(material) === -1) {
                uniqueMaterial.push(material);
            }
        }
    })
    return uniqueMaterial;
}


export const getMaterialFront = (products) => {

    var uniqueMaterial = [];
    products.map((product, index) => {
        if (product.material) {
            var material = product.material;
            if (uniqueMaterial.filter((e) => e.value === material).length == 0) {
                uniqueMaterial.push({value: material, label: material});
            }
        }
    })
    return uniqueMaterial;
}

// Get Unique Colors from Json Data
export const getColors = (products) => {
    var uniqueColors = [];
    products.map((product, index) => {
        if (product.colors) {
            product.colors.map((color) => {
                if (uniqueColors.indexOf(color) === -1) {
                    uniqueColors.push(color);
                }
            })
        }
    })
    return uniqueColors;
}

export const getColorsFront = (products) => {
    var uniqueColors = [];
    products.map((product, index) => {
        if (product.colors) {
            product.colors.map((color) => {
                if (uniqueColors.filter((e) => e.value === color).length == 0) {
                    uniqueColors.push({value: color, label: color});
                }
            })
        }
    })
    return uniqueColors;
}

// Get Minimum and Maximum Prices from Json Data
export const getMinMaxPrice = (products) => {

    let min = 100, max = 1000;

    products.map((product, index) => {
        let v = product.price;
        min = (v < min) ? v : min;
        max = (v > max) ? v : max;
    })

    return {'min': min, 'max': max};
}


export const getVisibleproducts = (data, {brand, color, gender, material, shape, rim, age, group, sortBy, searchBy, enable}) => {
    return data.filter(product => {
        if(color !== "" && color !== undefined) {
            console.log(color);
        }
        let brandMatch;
        if (product.tags)
            brandMatch = product.tags.some(tag => brand.includes(tag))
        else
            brandMatch = true;

        let genderMatch;
        if (product.gender)
            genderMatch = gender.includes(product.gender)
        else
            genderMatch = true;

        let ageMatch;
        if (product.age)
            ageMatch = age.includes(product.age)
        else
            ageMatch = true;

        let materialMatch;
        if (product.material)
            materialMatch = material.includes(product.material)
        else
            materialMatch = true;

        let shapeMatch;
        if (product.shape)
            shapeMatch = shape.includes(product.shape)
        else
            shapeMatch = true;

        let rimMatch;
        if (product.rim)
            rimMatch = rim.includes(product.rim)
        else
            rimMatch = true;

        let colorMatch;
        if(color !== "" && color !== undefined) {
            colorMatch = color.every(tag => product.colors.includes(tag))
        } else {
            colorMatch = true;
        }
        let groupMatch;
        if (product.group)
            groupMatch = group.includes(product.group)
        else
            groupMatch = true;

        let favoriteMatch;
        if (sortBy === 'Staff Picks') {
            if (product.favorite) {
                favoriteMatch = true;
            } else {
                favoriteMatch = false;
            }
        } else {
            favoriteMatch = true;
        }

        let enableMatch;
        if (product.enable) {
            enableMatch = true;
        } else {
            enableMatch = false;
        }
        // const startPriceMatch = typeof value.min !== 'number' || value.min <= product.price;
        // const endPriceMatch = typeof value.max !== 'number' || product.price <= value.max;

        const searchByName = (product.name.toLowerCase().indexOf(searchBy.toLowerCase()) > -1 || product.sku.toLowerCase().indexOf(searchBy.toLowerCase()) > -1);

        return enableMatch && brandMatch && genderMatch && ageMatch && materialMatch && shapeMatch && rimMatch && colorMatch && groupMatch && searchByName && favoriteMatch;
    }).sort((product1, product2) => {
        if (sortBy === 'Newest') {
            // var date1 = new Date(product1.modified);
            // var date2 = new Date(product2.modified);
            // console.log(date2 + '<' + date1);//
            return product2.modified < product1.modified ? -1 : 1;
        } else if (sortBy === 'AscOrder') {
            return product1.name.localeCompare(product2.name);
        } else if (sortBy === 'DescOrder') {
            return product2.name.localeCompare(product1.name);
        } else if (sortBy === 'Price (Low - Hi)') {
            return product2.price > product1.price ? -1 : 1;
        } else if (sortBy === 'Price (Hi - Low)') {
            return product2.price < product1.price ? -1 : 1;
        } else {
            return product1.favorite ? -1 : 1;
        }
    });

}

export const getVisibleproductsFrame = (data, {brand, color, gender, material, shape, rim, age, group, sortBy, searchBy, enable}) => {
    return data.filter(product => {
        // console.log('product',product.gender);
        let brandMatch;
        if (product.tags)
            brandMatch = product.tags.some(tag => brand.includes(tag))
        else
            brandMatch = true;

        let genderMatch;
        if (product.gender)
            genderMatch = gender.includes(product.gender)
        else
            genderMatch = true;

        let ageMatch;
        if (product.age)
            ageMatch = age.includes(product.age)
        else
            ageMatch = true;

        let materialMatch;
        if (product.material)
            materialMatch = material.includes(product.material)
        else
            materialMatch = true;

        let shapeMatch;
        if (product.shape)
            shapeMatch = shape.includes(product.shape)
        else
            shapeMatch = true;

        let rimMatch;
        if (product.rim)
            rimMatch = rim.includes(product.rim)
        else
            rimMatch = true;

        let colorMatch;
        if (product.color)
            colorMatch = color.includes(product.color)
        else
            colorMatch = true;

        let groupMatch;
        if (product.group)
            groupMatch = group.includes(product.group)
        else
            groupMatch = true;

        let favoriteMatch;
        if (sortBy === 'Staff Picks') {
            if (product.favorite) {
                favoriteMatch = true;
            } else {
                favoriteMatch = false;
            }
        } else {
            favoriteMatch = true;
        }


        // const startPriceMatch = typeof value.min !== 'number' || value.min <= product.price;
        // const endPriceMatch = typeof value.max !== 'number' || product.price <= value.max;

        const searchByName = (product.name.toLowerCase().indexOf(searchBy.toLowerCase()) > -1 || product.sku.toLowerCase().indexOf(searchBy.toLowerCase()) > -1);

        return brandMatch && genderMatch && ageMatch && materialMatch && shapeMatch && rimMatch && colorMatch && groupMatch && searchByName && favoriteMatch;
    }).sort((product1, product2) => {
        if (sortBy === 'Newest') {
            // var date1 = new Date(product1.modified);
            // var date2 = new Date(product2.modified);
            // console.log(date2 + '<' + date1);//
            return product2.modified < product1.modified ? -1 : 1;
        } else if (sortBy === 'AscOrder') {
            return product1.name.localeCompare(product2.name);
        } else if (sortBy === 'DescOrder') {
            return product2.name.localeCompare(product1.name);
        } else if (sortBy === 'Price (Low - Hi)') {
            return product2.price > product1.price ? -1 : 1;
        } else if (sortBy === 'Price (Hi - Low)') {
            return product2.price < product1.price ? -1 : 1;
        } else {
            if((product1.favorite == true && product2.favorite == false) || (product1.enable == false && product2.enable == true) ){
                return -1;
            }
        }
    });

}

export const getVisibleProductsFront = (data, {brand, color, gender, material, shape, rim, age, sortBy, searchBy}) => {
    return data.filter(product => {
        // console.log('genders',gender)
        // console.log('product',product.gender);
        let brandMatch;
        if (product.tags)
            brandMatch = product.tags.some(tag => brand.includes(tag))
        else
            brandMatch = true;

        let genderMatch;
        if (product.gender)
            genderMatch = gender.includes(product.gender)
        else
            genderMatch = true;

        let ageMatch;
        if (product.age)
            ageMatch = age.includes(product.age)
        else
            ageMatch = true;

        let materialMatch;
        if (product.material)
            materialMatch = material.includes(product.material)
        else
            materialMatch = true;

        let shapeMatch;
        if (product.shape)
            shapeMatch = shape.includes(product.shape)
        else
            shapeMatch = true;

        let rimMatch;
        if (product.rim)
            rimMatch = rim.includes(product.rim)
        else
            rimMatch = true;

        let colorMatch;
        if (product.colors) {
            colorMatch = color.includes(product.colors)
        } else {
            colorMatch = true;
        }

        // const startPriceMatch = typeof value.min !== 'number' || value.min <= product.price;
        // const endPriceMatch = typeof value.max !== 'number' || product.price <= value.max;

        const searchByName = (product.name.toLowerCase().indexOf(searchBy.toLowerCase()) > -1 || product.sku.toLowerCase().indexOf(searchBy.toLowerCase()) > -1);

        return brandMatch && genderMatch && ageMatch && materialMatch && shapeMatch && rimMatch && colorMatch && searchByName;

    }).sort((product1, product2) => {
        if (sortBy === 'Newest') {
            // var date1 = new Date(product1.modified);
            // var date2 = new Date(product2.modified);
            // console.log(date2 + '<' + date1);//
            return product2.modified < product1.modified ? -1 : 1;
        } else if (sortBy === 'AscOrder') {
            return product1.name.localeCompare(product2.name);
        } else if (sortBy === 'DescOrder') {
            return product2.name.localeCompare(product1.name);
        } else {
            return product2.id > product1.id ? -1 : 1;
        }
    });

}

export const productCall = (products,token) => {
    var data = [];
    products.map((item, i) => {
        data.push({
            brand: item.name,
            frame_id: item.sku,
            type: item.group,
            date_added: item.modified,
            image: {data: item,index: i},
            show_hide : { enable: item.enable , sku : item.sku },
            favorite : { favorite : item.favorite , sku : item.sku },
        })
    })
    return data;
}

