const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var uniqueKey = require('unique-key');
const crypto = require('crypto');


const schema = new Schema({
    // username: { type: String, unique: true, required: true },
    // hash: { type: String, required: true },

    //first_name                 : { type: String , required: true },
    //last_name                  : { type: String , required: true },
    //oa_member                  : { type: String , required: false },
    //oa_member_code             : { type: String , required: false },
    email                        : { type: String , unique: true , required: true },
    password                     : { type: String , required: true },
    practice_name                : { type: String , required: false },
    practice_phone               : { type: Number , required: false },
    promo_code                   : { type: String , required: false },
    date                         : { type: Date   , default : Date.now()  },
    public_url                   : { type: String , unique: true},
    data                         : [],
    productdata                  : [],
    is_completed                 : { type: Boolean , default: false },
    resetPasswordToken           : { type: String, required: false},
    resetPasswordExpires         : { type: Date, required: false},
    sessionCount                 : { type: Number , required: false},
    tryOnRequestCount            : { type : Number , required : false},
    profileViewCount             : { type : Number , required : false},
    affiliateLink                : { type : String , required : false},
    stripeCustomerID             : { type : String , required : false},
    stripeCustomerSubscriptionID : { type : String , required : false},
});



schema.set('toJSON', { virtuals: true });

schema.methods.generatePasswordReset = function() {
    this.resetPasswordToken = crypto.randomBytes(20).toString('hex');
    this.resetPasswordExpires = Date.now() + 3600000; //expires in an hour
};

module.exports = mongoose.model('Users', schema);
