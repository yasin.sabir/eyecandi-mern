const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var uniqueKey = require('unique-key');
const crypto = require('crypto');

const schema = new Schema({
    user_id                 : { type: String , unique: true , required: true },
    promo_code              : { type: String , required: false },
    discount_frequency      : { type: Number , required: false },
    date                    : { type: Date   , default : Date.now()  },
},{ timestamps : true });


schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Promocode', schema);
