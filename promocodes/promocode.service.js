// const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const Promo = db.Promocode;

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Promo.find().select('-hash');
}

async function getById(id) {
    return await Promo.findById(id).select('-hash');
}

async function create(promoParam) {
    const promo = new Promo(promoParam);
    await promo.save();
}

// async function update(id, userParam) {
//     const promo = await Promo.findById(id);
//
//     // validate
//     if (!promo) throw 'Promo Code not found';
//     if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
//         throw 'Username "' + userParam.username + '" is already taken';
//     }
//
//     // hash password if it was entered
//     if (userParam.password) {
//         userParam.hash = bcrypt.hashSync(userParam.password, 10);
//     }
//
//     // copy userParam properties to user
//     Object.assign(user, userParam);
//
//
//     await user.save();
// }

async function _delete(id) {
    await Promo.findByIdAndRemove(id);
}

