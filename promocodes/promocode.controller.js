const express = require('express');
const router = express.Router();
const promoService = require('./promocode.service');


// routes
router.post('/store', store);
router.get('/all', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/promocodes/delete/:id', _delete);

module.exports = router;

function store(req, res, next) {
    promoService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    promoService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    promoService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    promoService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    promoService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    promoService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}
function _checkUnique(req,res,next) {
    promoService.checkUnique(req.params.url).
    then(() => res.json)
}
