const express = require('express');
const promocodes = express.Router();
const cons = require("cors");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const uniqid = require('uniqid');
const sgMail = require('@sendgrid/mail');
const crypto = require('crypto');
const fetch = require('node-fetch');
const cron = require("node-cron");

sgMail.setApiKey('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');


const app = express();

const Promo = require('../models/promocode.model');
promocodes.use(cons());

promocodes.post('/store', (req, res) => {
    const today = new Date();
    const promoData = {
        user_id: req.body.user_id,
        promo_code: req.body.promo_code,
        discount_frequency: req.body.discount_frequency,
        date: today
    };

    Promo.findOne({
        user_id: req.body.user_id
    })
        .then(promo => {
            if (!promo) {
                Promo.create(promoData)
                    .then(user => {
                        res.send({Message: "Promo code successfully created!"})
                    })
                    .catch(err => {
                        res.send('error' + err)
                    })

            } else {
                res.json({error: "Promo Code already exists"})
            }
        })
        .catch(err => {
            res.send('error ' + err)
        })
});

promocodes.get('/list', (req, res) => {

    var mySort = {_id: -1};

    Promo.find().sort(mySort).then(promocodes => {
        var promoCodesData = [];
        if (promocodes) {
            promocodes.map(promo => {
                var data = {
                    '_id': promo._id,
                    'user_id': promo.user_id,
                    'promo_code': promo.promo_code,
                    'discount_frequency': promo.discount_frequency,
                    'created_at': promo.createdAt,
                };
                promoCodesData.push(data);
            });
            res.send({data: promoCodesData});
        } else {
            res.json({error: "Promo codes not exist!"});
        }
    })
});

promocodes.get('/get/:id', (req, res) => {
    Promo.findOne({
        _id: req.params.id
    }).then(promocode => {
            if (promocode) {
                return res.json(promocode)
            }else{
                res.json({error:"Promo code does not exist !"})
            }
        }).catch(err => {
            console.log("Error - "+err);
    })
});

promocodes.put('/update/:id', (req, res) => {
    Promo.findById(req.params.id).then(promocode => {
        if (promocode) {
            promocode.user_id = req.body.user_id;
            promocode.promo_code = req.body.promo_code;
            promocode.discount_frequency = req.body.discount_frequency;
            promocode.save();
            res.json({Message: "Successfully Updated!"});
            // res.json(promocode);
        }else{
            res.send({error: 'error in update'});
        }
    })
});

promocodes.delete('/delete/:id', (req, res) => {
    Promo.deleteOne(req.body.id)
        .then(data => {
            if (!data) {
                return res.status(404).send({
                    message: "data not found with id " + req.body.id
                });
            }
            res.send({message: "data deleted successfully!"});
        }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "data not found with id " + req.body.id
            });
        }
        return res.status(500).send({
            message: "Could not delete data with id " + req.body.id
        });
    });

});

module.exports = promocodes;

// const user = require('users/users.controller');
//const user = app.use('/users', require('../users/users.controller'));
