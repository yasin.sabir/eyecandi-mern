const express = require('express');
const users = express.Router();
const cons = require("cors");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const uniqid = require('uniqid');
const sgMail = require('@sendgrid/mail');
const crypto = require('crypto');
const fetch = require('node-fetch');
const cron = require("node-cron");
const nodemailer = require('nodemailer');

var transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
        user: "323141ec0c49e0",
        pass: "f7a4ce6c866399"
    }
});

sgMail.setApiKey('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');
// const stripe = require('stripe')('sk_test_51I6NL0Iu6dlBZMnLwyK5uiOG0DJKRdMFoLzjBPPeSW131KG8EyUZE7YlYlOd2TMTKGAzeR4JJxUy1OT4njedBQgb00SS0JZ39x');
const stripe = require('stripe')('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');

const app = express();

const User = require('../models/user.model');
users.use(cons());

process.env.SECRET_KEY = 'secret';


users.post('/create-customer', async (req, res) => {
    // Create a new customer object
    const customer = await stripe.customers.create({
        payment_method: req.body.payment_method,
        email: req.body.email,
        card: req.body.tokenID,
        description: 'EyeCandi Monthly Subscription Plan',
        invoice_settings: {
            default_payment_method : req.body.payment_method
        },
    });
    // save the customer.id as stripeCustomerId
    // in your database.
    res.send({ customer });
});


users.post('/create-subscription', async (req, res) => {
    // Create a new customer object
    const customer = await stripe.customers.create({
        payment_method: req.body.payment_method,
        email: req.body.email,
        card: req.body.tokenID,
        description: 'EyeCandi Monthly Subscription Plan',
        invoice_settings: {
            default_payment_method : req.body.payment_method
        },
    });

    // Create a new customer Subscription object
    const subscription = await stripe.subscriptions.create({
        customer: customer.id,
        // below is live product
        items: [{price: 'price_1Iw2uFIu6dlBZMnLMGwEzINV'}],
        // below is test product
        // items: [{price: 'price_1Is7VmIu6dlBZMnLLmw9bKOy'}],
        expand:['latest_invoice.payment_intent']
    });

    // save the subscription.id as stripeCustomerSubscriptionId
    // in your database.
    const status = subscription['latest_invoice']['payment_intent']['status']
    const client_secret = subscription['latest_invoice']['payment_intent']['client_secret']
    res.send({ customerID : customer.id , subscriptionId: subscription.id , status : status , client_secret : client_secret });
});

users.post('/create-subscription-one-dollar', async (req, res) => {
    // Create a new customer object
    const customer = await stripe.customers.create({
        payment_method: req.body.payment_method,
        email: req.body.email,
        card: req.body.tokenID,
        description: 'EyeCandi Subscription Plan $1/month',
        invoice_settings: {
            default_payment_method : req.body.payment_method
        },
    });

    // Create a new customer Subscription object
    const subscription = await stripe.subscriptions.create({
        customer: customer.id,
        // below is live product
        items: [{price: 'price_1Iw2qoIu6dlBZMnLqp4zRa6R'}],
        // below is test product
        // items: [{price: 'price_1Iv4lpIu6dlBZMnLzxAaMZQP'}],
        expand:['latest_invoice.payment_intent']
    });

    // save the subscription.id as stripeCustomerSubscriptionId
    // in your database.
    const status = subscription['latest_invoice']['payment_intent']['status']
    const client_secret = subscription['latest_invoice']['payment_intent']['client_secret']
    res.send({ customerID : customer.id , subscriptionId: subscription.id , status : status , client_secret : client_secret });
});


users.get('/getcsv',(req, res) => {

    User.find({ is_completed: true }).then(users => {
        var userMap = [];
        if (users) {
            users.map(user =>{
                var userdata = {
                    '_id' : user._id,
                    'email' : user.email,
                    'phone_number' : user.data[0].practice_phone_number,
                    'practice_address' : user.data[0].practice_address,
                    'practice_address2': user.data[0].practice_address2,
                    'city' : user.data[0].city,
                    'state' : user.data[0].state,
                    'zip' : user.data[0].zip,
                    'public_url' :  user.public_url,
                    'promo_code' : user.promo_code,
                    'affiliate_link' : user.affiliateLink,
                    'date' : user.date
                };
                userMap.push(userdata);
            })
            res.json(userMap);
        } else {
            res.json({error: "User does not exist !~~"})
        }
    })
});

users.post('/register', (req, res) => {
    const today = new Date();
    const uniq = uniqid();
    const userData = {
        //first_name: req.body.first_name,
        //last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password,
        //practice_name: req.body.practice_name,
        //lat: req.body.lat,
        //lng: req.body.lng,
        //oa_member: req.body.oa_member,
        //oa_member_code: req.body.oa_member_code,
        promo_code: req.body.promo_code,
        created: today,
        public_url: uniq,
        is_completed: false,
        sessionCount: 1,
        tryOnRequestCount: 0,
        profileViewCount: 0,
        affiliateLink: req.body.affiliateLink,
        stripeCustomerID: '',
        stripeCustomerSubscriptionID: '',

    };


    User.findOne({
        email: req.body.email
    })
        .then(user => {
            if (!user) {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    userData.password = hash
                    User.create(userData)
                        .then(user => {
                            const payload = {
                                _id: user._id,
                                //first_name: user.first_name,
                                //last_name: user.last_name,
                                email: user.email,
                            }
                            let token = jwt.sign(payload, process.env.SECRET_KEY, {
                                expiresIn: 1440
                            })
                            res.send({token: token, id: user._id})
                        })
                        .catch(err => {
                            res.send('error' + err)
                        })
                });

            } else {
                res.json({error: "User already exists"})
            }
        })
        .catch(err => {
            res.send('error ' + err)
        })
});

users.get('/change', (req, res) => {
    User.findOne({
        public_url: "utaheyecenters "
    }).then(user => {
        if (user) {
            bcrypt.hash('admin@123', 10, (err, hash) => {
                user.password = hash
                user.save();
                res.send({success: 'User updated'});
            })
        } else {
            res.json({error: "User does not exist !~~"})
        }

    })
})

users.post('/login', (req, res) => {
    User.findOne({
        email: req.body.email
    })
        .then(user => {
            if (user) {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    user.sessionCount = user.sessionCount + 1;
                    const payload = {
                        _id: user._id,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        email: user.email,
                        sessionCount: user.sessionCount,
                        tryOnRequestCount: user.tryOnRequestCount
                    }
                    //user.sessionCount = user.sessionCount++;
                    user.save();
                    let token = jwt.sign(payload, process.env.SECRET_KEY, {
                        expiresIn: 1440
                    })
                    res.send({token: token, id: user._id})
                } else {
                    res.send({error: "User does not exist !~"})
                }
            } else {
                res.json({error: "User does not exist !~~"})
            }
        })
        .catch(err => {
            res.send('error ' + err)
        })
});

users.put('/:id', (req, res) => {
    var res = res;
    User.findById(req.params.id).then(user => {
        if (user) {
            user.data = req.body.data;
            // user.public_url = req.body.data.public_gallery_url;
            user.is_completed = true;
            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=3ae95b14-3c0d-461b-aaea-aec194dc6a23&username=KA0277847')
                .then(res => res.json()) // expecting a json response
                .then(data => {
                    // res.json(user.data[0].all_brands);
                    let token = data.Auth.AuthorizationTicket;
                    let products = [];
                    let filterbrands = [];
                    let filterShape = [];
                    let filterRim = [];
                    let filterGender = [];
                    let filterMaterial = [];
                    let filterAge = [];
                    let filterGroup = [];
                    let id = 0;
                    let brands = user.data[0].all_brands.all_brands;
                    let brands_len = brands.length;
                    let res_len = 0;
                    let MaleCount = 0;
                    let FemaleCount = 0;
                    brands.map((item, index) => {
                        fetch('https://api.framesdata.com/api/brands/' + item.BrandFrameMasterID + '/styleconfigurations/?auth=' + token)
                            .then(response => response.json())
                            .then((data) => {

                                data = data.StyleConfigurations;
                                filterbrands.push(data[0].BrandName);

                                data.map((brandata, index) => {

                                    if (brandata.Configurations[0].ConfigurationImageName !== '') {
                                        var product = {
                                            id: id + 1,
                                            img: '&size=Large&fpc=' + brandata.Configurations[0].ConfigurationFPC,
                                            name: brandata.CollectionName,
                                            note: brandata.FramesPD,
                                            gender: brandata.Gender,
                                            age: brandata.Age,
                                            modified: brandata.StyleLastModifiedOn,
                                            country: brandata.Country,
                                            shape: brandata.FrameShape,
                                            temple: brandata.Temple,
                                            rim: brandata.Rim,
                                            material: brandata.Material,
                                            group: brandata.ProductGroup,
                                            price: brandata.Configurations[0].CompletePrice,
                                            sku: brandata.StyleName,
                                            discription: brandata.FramesPDDescription,
                                            category: brandata.Gender,
                                            rx: brandata.RX,
                                            enable: true,
                                            favorite: false,
                                            views: 0,
                                            colors: [],
                                            tags: [
                                                brandata.BrandName
                                            ],
                                            variants: []
                                        };

                                        if (filterMaterial.indexOf(brandata.Material) === -1) {
                                            filterMaterial.push(brandata.Material);
                                        }
                                        if (filterShape.indexOf(brandata.FrameShape) === -1) {
                                            filterShape.push(brandata.FrameShape);
                                        }
                                        if (filterRim.indexOf(brandata.Rim) === -1) {
                                            filterRim.push(brandata.Rim);
                                        }
                                        if (filterGender.indexOf(brandata.Gender) === -1) {
                                            filterGender.push(brandata.Gender);
                                        }
                                        if (filterAge.indexOf(brandata.Age) === -1) {
                                            filterAge.push(brandata.Age);
                                        }
                                        if (filterGroup.indexOf(brandata.ProductGroup) === -1) {
                                            filterGroup.push(brandata.ProductGroup);
                                        }

                                        brandata.Configurations.map((brandconf, index) => {
                                            if (brandconf.FrameColorGroup) {
                                                if (brandconf.ConfigurationImageName !== '') {
                                                    if (!product.colors.some(color => color === brandconf.FrameColorGroup.toLowerCase())) {
                                                        product.colors.push(brandconf.FrameColorGroup.toLowerCase());
                                                        var variant = {
                                                            color: brandconf.FrameColorGroup.toLowerCase(),
                                                            images: '&size=Large&fpc=' + brandconf.ConfigurationFPC,
                                                            views: 0
                                                        }
                                                        product.variants.push(variant);
                                                    }
                                                }
                                            }
                                            // console.log(brandconf.ConfigurationImageName);
                                        })
                                        if (brandata.Gender == 'Male') {
                                            MaleCount++;
                                        } else if (brandata.Gender == 'Female') {
                                            FemaleCount++;
                                        } else {
                                            MaleCount++;
                                            FemaleCount++;
                                        }
                                        id++;
                                        // setTimeout(function() {
                                        products.push(product)
                                    }

                                })

                                res_len++;

                                if (res_len >= brands_len) {
                                    // alert(products.length);
                                    var productdata = {
                                        products: products,
                                        filterbrands: filterbrands,
                                        filterShape: filterShape,
                                        filterRim: filterRim,
                                        filterGender: filterGender,
                                        filterMaterial: filterMaterial,
                                        filterAge: filterAge,
                                        filterGroup: filterGroup,
                                        MaleCount: MaleCount,
                                        FemaleCount: FemaleCount
                                    };
                                    // console.log(productdata);
                                    //                     // console.log('as');
                                    //                     // setTimeout(() => {
                                    //
                                    user.productdata = productdata;
                                    user.save();
                                    res.send({success: 'User updated'});

                                    //                     // },2000);
                                    //
                                }
                            });
                    })
                    //     // return products;
                });
            //     .then(products => {
            //     setTimeout(() => {
            //         this.props.filterBrand(filterbrands)
            //     }, 8000)
            // });
            // console.log(req.body);
        } else {
            res.send({error: 'User not available'});
        }
    })
});

users.put('/stripeCredentials/:id', (req, res) => {
    var res = res;
    console.log(req);
    User.findById(req.params.id).then(user => {
        if (user) {
            user.stripeCustomerID             = req.body.stripeCustomerID;
            user.stripeCustomerSubscriptionID = req.body.stripeCustomerSubscriptionID;
            user.save();
            res.send({success: 'User updated'});
        } else {
            res.send({error: 'User not available'});
        }
    })
});


users.get('/verify/:token', (req, res) => {
    jwt.verify(req.params.token, process.env.SECRET_KEY, function (err, decoded) {
        if (err) {
            err = {
                name: 'TokenExpiredError',
                message: 'jwt expired',
                expiredAt: 1408621000
            }
            res.send(err);
        } else {
            res.send(decoded)
        }

    });
});

users.put('/update/:id', (req, res) => {

    User.findById(req.params.id).then(user => {

        if (user) {
            // res.json(req);
            user.data = req.body.data;
            user.is_completed = true;
            fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=3ae95b14-3c0d-461b-aaea-aec194dc6a23&username=KA0277847')
                .then(res => res.json()) // expecting a json response
                .then(data => {
                    // res.json(user.data[0].all_brands);
                    let token = data.Auth.AuthorizationTicket;
                    let products = [];
                    let filterbrands = [];
                    let filterShape = [];
                    let filterRim = [];
                    let filterGroup = [];
                    let filterGender = [];
                    let filterMaterial = [];
                    let filterAge = [];
                    let MaleCount = 0;
                    let FemaleCount = 0;
                    let id = 0;
                    var brands = user.data[0].all_brands.all_brands;
                    let brands_len = brands.length;
                    let res_len = 0;
                    // console.log(brands.brands);
                    brands.map((item, index) => {
                        fetch('https://api.framesdata.com/api/brands/' + item.BrandFrameMasterID + '/styleconfigurations/?auth=' + token)
                            .then(response => response.json())
                            .then((data) => {
                                // console.log(brands_len);
                                data = data.StyleConfigurations;
                                filterbrands.push(data[0].BrandName);

                                data.map((brandata, index) => {

                                    if (brandata.Configurations[0].ConfigurationImageName !== '') {
                                        var product = {
                                            id: id + 1,
                                            img: '&size=Large&fpc=' + brandata.Configurations[0].ConfigurationFPC,
                                            name: brandata.CollectionName,
                                            note: brandata.FramesPD,
                                            gender: brandata.Gender,
                                            age: brandata.Age,
                                            modified: brandata.StyleLastModifiedOn,
                                            country: brandata.Country,
                                            shape: brandata.FrameShape,
                                            temple: brandata.Temple,
                                            rim: brandata.Rim,
                                            material: brandata.Material,
                                            group: brandata.ProductGroup,
                                            price: brandata.Configurations[0].CompletePrice,
                                            sku: brandata.StyleName,
                                            discription: brandata.FramesPDDescription,
                                            category: brandata.Gender,
                                            rx: brandata.RX,
                                            views: 0,
                                            enable: true,
                                            favorite: false,
                                            colors: [],
                                            tags: [
                                                brandata.BrandName
                                            ],
                                            variants: []
                                        };

                                        if (filterMaterial.indexOf(brandata.Material) === -1) {
                                            filterMaterial.push(brandata.Material);
                                        }
                                        if (filterShape.indexOf(brandata.FrameShape) === -1) {
                                            filterShape.push(brandata.FrameShape);
                                        }
                                        if (filterRim.indexOf(brandata.Rim) === -1) {
                                            filterRim.push(brandata.Rim);
                                        }
                                        if (filterGender.indexOf(brandata.Gender) === -1) {
                                            filterGender.push(brandata.Gender);
                                        }
                                        if (filterAge.indexOf(brandata.Age) === -1) {
                                            filterAge.push(brandata.Age);
                                        }
                                        if (filterGroup.indexOf(brandata.ProductGroup) === -1) {
                                            filterGroup.push(brandata.ProductGroup);
                                        }

                                        brandata.Configurations.map((brandconf, index) => {
                                            if (brandconf.FrameColorGroup) {
                                                if (brandconf.ConfigurationImageName !== '') {
                                                    if (!product.colors.some(color => color === brandconf.FrameColorGroup.toLowerCase())) {
                                                        product.colors.push(brandconf.FrameColorGroup.toLowerCase());
                                                        var variant = {
                                                            color: brandconf.FrameColorGroup.toLowerCase(),
                                                            images: '&size=Large&fpc=' + brandconf.ConfigurationFPC,
                                                            views: 0
                                                        }
                                                        product.variants.push(variant);
                                                    }
                                                }
                                            }
                                            // console.log(brandconf.ConfigurationImageName);
                                        })
                                        if (brandata.Gender == 'Male') {
                                            MaleCount++;
                                        } else if (brandata.Gender == 'Female') {
                                            FemaleCount++;
                                        } else {
                                            MaleCount++;
                                            FemaleCount++;
                                        }
                                        id++;
                                        // setTimeout(function() {
                                        products.push(product)
                                    }

                                })

                                res_len++;
                                if (res_len >= brands_len) {
                                    // alert(products.length);
                                    var productdata = {
                                        products: products,
                                        filterbrands: filterbrands,
                                        filterShape: filterShape,
                                        filterRim: filterRim,
                                        filterGender: filterGender,
                                        filterMaterial: filterMaterial,
                                        filterAge: filterAge,
                                        filterGroup: filterGroup,
                                        MaleCount: MaleCount,
                                        FemaleCount: FemaleCount
                                    };
                                    user.productdata = productdata;
                                    user.save();
                                    res.json(user.productdata);
                                    // res.send({success: 'User updated'});

                                    //                     // },2000);
                                    //
                                }
                            });
                    })
                    //     // return products;
                });

        } else {
            res.send({error: 'error in update'});
        }
    });
});

users.delete('/delete/:id', (req, res) => {
    User.findByIdAndRemove(req.params.id)
        .then(data => {
            if (!data) {
                return res.status(404).send({
                    message: "data not found with id " + req.body.id
                });
            }
            res.send({message: "data deleted successfully!"});
        }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "data not found with id " + req.body.id
            });
        }
        return res.status(500).send({
            message: "Could not delete data with id " + req.body.id
        });
    });
});

users.put('/product/:id', (req, res) => {

    User.findById(req.params.id).then(user => {

        if (user) {
            user.productdata = req.body.data;
            // console.log(req.body);
            user.save();
            res.send({success: 'product updated'});
        } else {
            res.json({error: "Product save Failed"})
        }
    })
})

users.get('/:id', (req, res) => {
    User.findOne({
        _id: req.params.id
    })
    .then(user => {
        if (user) {
            res.json(user)
        } else {
            res.send("User does not exist !@")
        }
    })
    .catch(err => {
        res.send("error " + err)
    })
});

users.get('/email/:id', (req, res) => {
    User.findOne({
        _id: req.params.id
    }).then(user => {
        var email = "";
        if (user) {
            email = user.email,
            res.json(email);
        } else {
            res.send("User does not exist !@")
        }
    }).catch(err => {
        res.send("error " + err)
    })
});


users.put('/product/show/:id/:sku', (req, res) => {
    var sku = req.params.sku;
    User.findById(req.params.id)
        .then(user => {
            if (user) {
                // var products = user.productdata;
                var data = JSON.parse(JSON.stringify(user.productdata));
                for (let i in data[0].products) {
                    if (data[0].products[i].sku == sku) {
                        data[0].products[i]['enable'] = true;
                    }
                    // data.push(user.productdata[0].products[i]);
                }
                user.productdata = data;
                user.is_completed = true;
                user.save();
                res.json(user.productdata[0]);
            } else {
                res.send("User does not exist !@")
            }
        })
        .catch(err => {
            res.send("error " + err)
        })
});

users.put('/brand/view/:id/:brand', (req, res) => {
    var brand = req.params.brand;
    User.findById(req.params.id)
        .then(user => {
            if (user) {
                // var products = user.productdata;
                var data = JSON.parse(JSON.stringify(user.data));
                for (let i in data[0].all_brands.all_brands) {
                    if (data[0].all_brands.all_brands[i].name == brand) {
                        data[0].all_brands.all_brands[i].views = data[0].all_brands.all_brands[i].views + 1;
                    }
                }
                //     // data.push(user.productdata[0].products[i]);
                // }

                user.data = data;
                user.is_completed = true;
                user.save();

                res.json(user.data[0].all_brands.all_brands);
            } else {
                res.send("User does not exist !@")
            }
        })
        .catch(err => {
            res.send("error " + err)
        })
});

users.put('/product/view/:id/:sku', (req, res) => {
    var sku = req.params.sku;
    User.findById(req.params.id)
        .then(user => {
            if (user) {
                // var products = user.productdata;
                var data = JSON.parse(JSON.stringify(user.productdata));
                for (let i in data[0].products) {
                    if (data[0].products[i].sku == sku) {
                        data[0].products[i]['views'] = data[0].products[i]['views'] + 1;
                        data[0].products[i]['variants'].map((variant) => {
                            variant.views = variant.views + 1;
                        })
                    }
                    // data.push(user.productdata[0].products[i]);
                }
                user.productdata = data;
                user.is_completed = true;
                user.save();
                res.json(user.productdata[0]);
            } else {
                res.send("User does not exist !@")
            }
        })
        .catch(err => {
            res.send("error " + err)
        })
});

users.put('/product/hide/:id/:sku', (req, res) => {
    var sku = req.params.sku;
    User.findById(req.params.id)
        .then(user => {
            if (user) {
                // var products = user.productdata;
                var data = JSON.parse(JSON.stringify(user.productdata));
                // console.log(data);
                for (let i in data[0].products) {
                    if (data[0].products[i].sku == sku) {
                        data[0].products[i]['enable'] = false;
                    }
                    // data.push(user.productdata[0].products[i]);
                }
                user.productdata = data;
                user.is_completed = true;
                user.save();
                res.json(user.productdata[0]);
            } else {
                res.send("User does not exist !@")
            }
        })
        .catch(err => {
            res.send("error " + err)
        })
});

users.put('/product/favorite/:id/:sku', (req, res) => {
    var sku = req.params.sku;
    User.findById(req.params.id)
        .then(user => {
            if (user) {
                // var products = user.productdata;
                var data = JSON.parse(JSON.stringify(user.productdata));
                for (let i in data[0].products) {
                    if (data[0].products[i].sku == sku) {
                        data[0].products[i]['favorite'] = true;
                    }
                    // data.push(user.productdata[0].products[i]);
                }
                user.productdata = data;
                user.is_completed = true;
                user.save();
                res.json(user.productdata[0]);
            } else {
                res.send("User does not exist !@")
            }
        })
        .catch(err => {
            res.send("error " + err)
        })
});

users.put('/product/remove_favorite/:id/:sku', (req, res) => {
    var sku = req.params.sku;
    User.findById(req.params.id)
        .then(user => {
            if (user) {
                // var products = user.productdata;
                var data = JSON.parse(JSON.stringify(user.productdata));
                for (let i in data[0].products) {
                    if (data[0].products[i].sku == sku) {
                        data[0].products[i]['favorite'] = false;
                    }
                    // data.push(user.productdata[0].products[i]);
                }
                user.productdata = data;
                user.is_completed = true;
                user.save();
                res.json(user.productdata[0]);
            } else {
                res.send("User does not exist !@")
            }
        })
        .catch(err => {
            res.send("error " + err)
        })
});

users.get('/public/:url', (req, res) => {
    // res.json(req.params.url);
    User.findOne({
        public_url: req.params.url
    })
        .then(user => {
            if (user) {
                user.profileViewCount = user.profileViewCount + 1;
                user.save();
                res.json(user);

            } else {
                res.json({
                    error: 'page not found'
                })
            }
        })
        .catch(err => {
            res.send("error " + err)
        })
});

users.post('/emailtryOnRequest', (req, res) => {

    const mailOptions = {
        to: req.body.email_to,
        from: 'info@eyecandi.app',
        subject: req.body.email_subject,
        text: `${req.body.email_body}`,
        html: `${req.body.email_body_frame_data}`
    };
    // console.log(mailOptions);
    sgMail.send(mailOptions, (error, result) => {
        if (error) return res.json({message: error.message});
        res.json({message: 'Email has been send!'});
    });

    User.findOne({
        email: req.body.adminEmail
    }).then(user => {

        if (user) {
            user.tryOnRequestCount = user.tryOnRequestCount + 1;
            user.save();
            res.send("tryOnRequest Counted");
        } else {
            res.send("Something went wrong in tryOnRequest!");
        }

    }).catch(err => {
        res.send("error " + err)
    })

    // sgMail.send(mailOptions, (error, result) => {
    //     if (error) return res.status(500).json({messageEmail: error.message});
    // });
})

users.post('/bookanexam', (req, res) => {

    const mailOptions = {
        to: req.body.email_to,
        from: 'info@eyecandi.app',
        subject: req.body.email_subject,
        text: `${req.body.email_body}`,
        html: `${req.body.email_body_frame_data}`
    };
    // console.log(mailOptions);
    sgMail.send(mailOptions, (error, result) => {
        if (error) return res.json({message: error.message});

        res.json({message: 'Email has been send!'});
    });
    // sgMail.send(mailOptions, (error, result) => {
    //     if (error) return res.status(500).json({messageEmail: error.message});
    // });
})



// ===PASSWORD RECOVER AND RESET

// @route POST api/auth/recover
// @desc Recover Password - Generates token and Sends password reset email
// @access Public
users.post('/auth/recover', (req, res) => {
    User.findOne({email: req.body.email})
        .then(user => {
            if (!user) return res.json({error: 'The email address ' + req.body.email + ' is not associated with any account. Double-check your email address and try again.'});

            //Generate and set password reset token
            user.resetPasswordToken = crypto.randomBytes(20).toString('hex');
            user.resetPasswordExpires = Date.now() + 3600000; //expires in an hour

            // return res.json(user);
            // die();
            // Save the updated user object
            user.save()
                .then(user => {
                    // send email
                    let link = "http://" + req.headers.host + "/forgotPwd/" + user.resetPasswordToken;
                    // res.json(link);
                    const mailOptions = {
                        to: user.email,
                        from: 'info@od.eyecandi.app',
                        subject: "Password change request",
                        text: `Hi ${user.practice_name} \n
                    Please click on the following link ${link} to reset your password. \n\n
                    If you did not request this, please ignore this email and your password will remain unchanged.\n`,
                    };
                    // res.json(mailOptions);

                    sgMail.send(mailOptions, (error, result) => {
                        if (error) return res.json({message: error.message});

                        res.json({message: 'A reset email has been sent to  ' + user.email + '.'});
                    });
                })
                .catch(err => res.json({message: err.message}));
        })
        .catch(err => res.json({message: err.message}));
});

// @route POST api/auth/reset
// @desc Reset Password - Validate password reset token and shows the password reset view
// @access Public
users.post('/auth/reset', (req, res) => {
    // res.json(req.body);
    User.findOne({resetPasswordToken: req.body.token})
        .then((user) => {
            if (!user) {
                if (!user) return res.json({message: 'Password reset token is invalid or has expired.'});
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    user.password = hash;
                    user.resetPasswordToken = undefined;
                    user.resetPasswordExpires = undefined;
                    user.save((err) => {
                        if (err) return res.status(500).json({message: err.message});

                        // send email
                        const mailOptions = {
                            to: user.email,
                            from: 'info@od.eyecandi.app',
                            subject: "Your password has been changed",
                            text: `Hi ${user.practice_name} \n
                    This is a confirmation that the password for your account ${user.email} has just been changed.\n`
                        };

                        sgMail.send(mailOptions, (error, result) => {
                            if (error) return res.status(500).json({message: error.message});

                            res.json({success: true, message: 'Your password has been updated.'});
                        });
                    });
                });
            }
            ;
        })
        .catch(err => res.json({message: err.message}));
});

cron.schedule("00 00 00 * * *", function () {
    // console.log('cron job')
    User.find({}, function (err, users) {
        if (!err) {
            users.forEach(user => {
                fetch('https://api.framesdata.com/api/signreadmeagreement?partnerid=3ae95b14-3c0d-461b-aaea-aec194dc6a23&username=KA0277847')
                    .then(res => res.json()) // expecting a json response
                    .then(data => {
                        // res.json(user.data[0].all_brands);
                        let token = data.Auth.AuthorizationTicket;
                        let products = [];
                        let filterbrands = [];
                        let filterShape = [];
                        let filterRim = [];
                        let filterGender = [];
                        let filterMaterial = [];
                        let filterAge = [];
                        let filterGroup = [];
                        let MaleCount = 0;
                        let FemaleCount = 0;
                        let id = 0;
                        let brands = user.data[0].all_brands.all_brands;
                        let brands_len = brands.length;
                        let res_len = 0;
                        brands.map((item, index) => {
                            fetch('https://api.framesdata.com/api/brands/' + item.BrandFrameMasterID + '/styleconfigurations/?auth=' + token)
                                .then(response => response.json())
                                .then((data) => {
                                    data = data.StyleConfigurations;
                                    filterbrands.push(data[0].BrandName);

                                    data.map((brandata, index) => {

                                        if (brandata.Configurations[0].ConfigurationImageName !== '') {
                                            var product = {
                                                id: id + 1,
                                                img: '&size=XL&fpc=' + brandata.Configurations[0].ConfigurationFPC,
                                                name: brandata.CollectionName,
                                                note: brandata.FramesPD,
                                                gender: brandata.Gender,
                                                age: brandata.Age,
                                                modified: brandata.StyleLastModifiedOn,
                                                country: brandata.Country,
                                                shape: brandata.FrameShape,
                                                temple: brandata.Temple,
                                                rim: brandata.Rim,
                                                material: brandata.Material,
                                                group: brandata.ProductGroup,
                                                price: brandata.Configurations[0].CompletePrice,
                                                sku: brandata.StyleName,
                                                discription: brandata.FramesPDDescription,
                                                category: brandata.Gender,
                                                rx: brandata.RX,
                                                colors: [],
                                                views: 0,
                                                enable: true,
                                                favorite: false,
                                                tags: [
                                                    brandata.BrandName
                                                ],
                                                variants: []
                                            };

                                            if (filterMaterial.indexOf(brandata.Material) === -1) {
                                                filterMaterial.push(brandata.Material);
                                            }
                                            if (filterShape.indexOf(brandata.FrameShape) === -1) {
                                                filterShape.push(brandata.FrameShape);
                                            }
                                            if (filterRim.indexOf(brandata.Rim) === -1) {
                                                filterRim.push(brandata.Rim);
                                            }
                                            if (filterGender.indexOf(brandata.Gender) === -1) {
                                                filterGender.push(brandata.Gender);
                                            }
                                            if (filterAge.indexOf(brandata.Age) === -1) {
                                                filterAge.push(brandata.Age);
                                            }
                                            if (filterGroup.indexOf(brandata.ProductGroup) === -1) {
                                                filterGroup.push(brandata.ProductGroup);
                                            }
                                            brandata.Configurations.map((brandconf, index) => {
                                                if (brandconf.FrameColorGroup) {
                                                    if (brandconf.ConfigurationImageName !== '') {
                                                        if (!product.colors.some(color => color === brandconf.FrameColorGroup.toLowerCase())) {
                                                            product.colors.push(brandconf.FrameColorGroup.toLowerCase());
                                                            var variant = {
                                                                color: brandconf.FrameColorGroup.toLowerCase(),
                                                                images: '&size=XL&fpc=' + brandconf.ConfigurationFPC,
                                                                views: 0
                                                            }
                                                            product.variants.push(variant);
                                                        }
                                                    }
                                                }
                                                // console.log(brandconf.ConfigurationImageName);
                                            })
                                            if (brandata.Gender == 'Male') {
                                                MaleCount++;
                                            } else if (brandata.Gender == 'Female') {
                                                FemaleCount++;
                                            } else {
                                                MaleCount++;
                                                FemaleCount++;
                                            }
                                            id++;
                                            // setTimeout(function() {
                                            products.push(product)
                                        }

                                    })

                                    res_len++;

                                    if (res_len >= brands_len) {
                                        // alert(products.length);
                                        var productdata = {
                                            products: products,
                                            filterbrands: filterbrands,
                                            filterShape: filterShape,
                                            filterRim: filterRim,
                                            filterGender: filterGender,
                                            filterMaterial: filterMaterial,
                                            filterGroup: filterGroup,
                                            filterAge: filterAge,
                                            MaleCount: MaleCount,
                                            FemaleCount: FemaleCount
                                        };
                                        // console.log(productdata);
                                        //                     // console.log('as');
                                        //                     // setTimeout(() => {
                                        //
                                        user.productdata = productdata;
                                        user.save();

                                        //                     // },2000);
                                        //
                                    }
                                });
                        })
                        //     // return products;
                    });
            })
        }
    })
});



module.exports = users;

// const user = require('users/users.controller');
//const user = app.use('/users', require('../users/users.controller'));
